<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    
    xmlns:dc="http://purl.org/dc/elements/1.1/" 
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" 
    xmlns:owl="http://www.w3.org/2002/07/owl#" 
    xmlns:RiC="http://www.ica.org/standards/RiC/ontology#" 
    xmlns:isni="http://isni.org/ontology#"
    xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" 
    
    
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:eac="urn:isbn:1-931666-33-4"
   xmlns="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
   xmlns:piaaf="http://www.piaaf.net"
    xmlns:piaaf-onto="http://piaaf.demo.logilab.fr/ontology#"
    xmlns:iso-thes="http://purl.org/iso25964/skos-thes#"
   
    xmlns:skos="http://www.w3.org/2004/02/skos/core#"
    xmlns:foaf="http://xmlns.com/foaf/0.1/"
   
    xmlns:dct="http://purl.org/dc/terms/"
    xmlns:xl="http://www.w3.org/2008/05/skos-xl#"
  
    xmlns:ginco="http://data.culture.fr/thesaurus/ginco/ns/"
    
    exclude-result-prefixes="xs xd eac iso-thes foaf dct xl dc ginco piaaf xlink"
    version="2.0"
    
    >
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> July 3rd, 2017</xd:p>
            <xd:p><xd:b>Author:</xd:b> Florence</xd:p>
            <xd:p>Sémantisation : étape 1, génération du fichier RDF des fonctions</xd:p>
            <xd:p>Finalement, on ne tient compte que du premier niveau de l'indexation fonctions</xd:p>
        </xd:desc>
    </xd:doc>
    <!--  -->
    
    
  <xsl:variable name="chemin-vocabs">
        <xsl:value-of
            select="concat('fichiers-def-2/vocabulaires/', '?select=*.rdf;recurse=yes;on-error=warning')"
        />
    </xsl:variable>
    <xsl:variable name="vocabs" select="collection($chemin-vocabs)"/>
    
    
    <xsl:variable name="chemin-EAC-BnF">
        <xsl:value-of
            select="concat('fichiers-def-2/notices-EAC/BnF/', '?select=*.xml;recurse=yes;on-error=warning')"
        />
    </xsl:variable>
    <xsl:variable name="collection-EAC-BnF" select="collection($chemin-EAC-BnF)"/>
    <xsl:variable name="chemin-EAC-SIAF">
        <xsl:value-of
            select="concat('fichiers-def-2/notices-EAC/SIAF/', '?select=*.xml;recurse=yes;on-error=warning')"
        />
    </xsl:variable>
    <xsl:variable name="collection-EAC-SIAF" select="collection($chemin-EAC-SIAF)"/>
    <xsl:variable name="chemin-EAC-AN">
        <xsl:value-of
            select="concat('fichiers-def-2/notices-EAC/AN/', '?select=*.xml;recurse=yes;on-error=warning')"
        />
    </xsl:variable>
    <xsl:variable name="collection-EAC-AN" select="collection($chemin-EAC-AN)"/>
    <xsl:template match="/piaaf:vide">
        <xsl:result-document href="rdf/functions.rdf" method="xml" encoding="utf-8" indent="yes">
            <rdf:RDF xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:owl="http://www.w3.org/2002/07/owl#" xmlns:RiC="http://www.ica.org/standards/RiC/ontology#" xmlns:isni="http://isni.org/ontology#"
                xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:piaaf-onto="http://piaaf.demo.logilab.fr/ontology#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" >
     
              <!--  <xsl:for-each-group select="$collection-EAC-BnF/eac:eac-cpf/eac:cpfDescription/eac:description/eac:function[@localType='piaafFunction'] | $collection-EAC-SIAF/eac:eac-cpf/eac:cpfDescription/eac:description/eac:function[@localType='piaafFunction'] | $collection-EAC-AN/eac:eac-cpf/eac:cpfDescription/eac:description/eac:function[@localType='piaafFunction']" group-by="
            if (count(eac:term)=1)
            then (eac:term/@piaaf:conceptId)
            else(
            if (count(eac:term)=2)
            then (concat(eac:term[@piaaf:rank='1']/@piaaf:conceptId, ' , ', eac:term[@piaaf:rank='2']/@piaaf:conceptId))
            else(concat(eac:term[@piaaf:rank='1']/@piaaf:conceptId, ' , ', eac:term[@piaaf:rank='2']/@piaaf:conceptId, ' , ', eac:term[@piaaf:rank='3']/@piaaf:conceptId))
            
            
            )
            ">-->
                <xsl:for-each-group select="$collection-EAC-BnF/eac:eac-cpf/eac:cpfDescription/eac:description/eac:function[@localType='piaafFunction'] | $collection-EAC-SIAF/eac:eac-cpf/eac:cpfDescription/eac:description/eac:function[@localType='piaafFunction'] | $collection-EAC-AN/eac:eac-cpf/eac:cpfDescription/eac:description/eac:function[@localType='piaafFunction']" group-by="
                  eac:term[@piaaf:rank='1']/@piaaf:conceptId
                  
                    
                    
                    ">
            <xsl:sort select="current-grouping-key()"></xsl:sort>
     
            <rdf:Description>
                <xsl:attribute name="rdf:about">
                    <xsl:text>http://piaaf.demo.logilab.fr/resource/</xsl:text>
                   
                    <xsl:text>function_</xsl:text>
                    <xsl:value-of select="substring-after(current-group()[1]/@xml:id, 'fu_')"/>
                </xsl:attribute>
                <rdf:type rdf:resource="http://www.ica.org/standards/RiC/ontology#FunctionAbstract"/>
               
                    <rdfs:label xml:lang="fr">
                    
                        <xsl:for-each select="tokenize(current-grouping-key(), ',\s*')">
                            <xsl:variable name="concept" select="normalize-space(.)"/>
                            <xsl:value-of select="$vocabs/rdf:RDF/skos:Concept[@rdf:about=$concept]/skos:prefLabel"/>
                            <!--<xsl:if test="position()!=last()"><xsl:text> -\- </xsl:text></xsl:if>-->
                        </xsl:for-each>
                    </rdfs:label>
                    <piaaf-onto:functionComponentList rdf:parseType="Collection">
                        <xsl:for-each select="tokenize(current-grouping-key(), ',\s*')">
                            <xsl:choose>
                                <xsl:when test="position()=1">
                                    <skos:Concept>
                                        <xsl:attribute name="rdf:about">
                                            <xsl:value-of select="normalize-space(.)"/>
                                        </xsl:attribute>
                                    </skos:Concept></xsl:when>
                                
                                <xsl:otherwise>
                                    <skos:Concept>
                                        <xsl:attribute name="rdf:about">
                                            <xsl:value-of select="normalize-space(.)"/>
                                        </xsl:attribute>
                                    </skos:Concept>
                                </xsl:otherwise>
                            </xsl:choose>
                         
                        </xsl:for-each>
                    </piaaf-onto:functionComponentList>
              
                    
          
          <!--  removed : this object property does not exist any more in RiC-O, and the relation (ric:isFunctionTypeOf) can be generated automatically since the inverse one will be present in the RDF graphs on corporate bodies<xsl:for-each select="current-group()">
                     <piaaf-onto:typeOfFunctionFulfilledBy>
                         <xsl:attribute name="rdf:resource">
                             <xsl:text>http://wwww.piaaf.net</xsl:text>
                             <xsl:choose>
                                 <xsl:when test="starts-with(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FRBNF')">
                                     
                                     
                                     <xsl:text>/agents/FRBNF_corporate-body_</xsl:text>
                                     <xsl:value-of select="substring-after(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'DGEARC_')"/>
                                 </xsl:when>
                                 <xsl:when test="starts-with(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FRAN')">
                                     <xsl:text>/agents/FRAN_corporate-body_</xsl:text>
                                     <xsl:value-of select="substring-after(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FRAN_NP_')"/>
                                 </xsl:when>
                                 
                                 <xsl:when test="starts-with(normalize-space(ancestor::eac:eac-cpf/eac:control/eac:recordId), 'FR784')">
                                     <xsl:text>/group-types/FRSIAF_group-type_</xsl:text>
                                     <xsl:value-of select="ancestor::eac:eac-cpf/eac:control/eac:recordId"/>
                                 </xsl:when>
                                 <xsl:otherwise>
                                     <xsl:text>/agents/FRSIAF_corporate-body_</xsl:text>
                                     <xsl:value-of select="ancestor::eac:eac-cpf/eac:control/eac:recordId"/>
                                 </xsl:otherwise>
                                 
                             </xsl:choose>
                         </xsl:attribute>
                     </piaaf-onto:typeOfFunctionFulfilledBy>
                 </xsl:for-each>-->
       
            </rdf:Description>
            
        </xsl:for-each-group>
            </rdf:RDF>
        </xsl:result-document>
    </xsl:template>
</xsl:stylesheet>