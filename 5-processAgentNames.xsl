<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:owl="http://www.w3.org/2002/07/owl#"
    xmlns:RiC="http://www.ica.org/standards/RiC/ontology#" xmlns:isni="http://isni.org/ontology#"
    xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:eac="urn:isbn:1-931666-33-4" xmlns="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:piaaf="http://www.piaaf.net" xmlns:piaaf-onto="http://piaaf.demo.logilab.fr/ontology#"
    xmlns:iso-thes="http://purl.org/iso25964/skos-thes#"
    xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:foaf="http://xmlns.com/foaf/0.1/"
    xmlns:dct="http://purl.org/dc/terms/" xmlns:xl="http://www.w3.org/2008/05/skos-xl#"
    xmlns:ginco="http://data.culture.fr/thesaurus/ginco/ns/"
    exclude-result-prefixes="xs xd eac iso-thes rdf foaf dct xl dc ginco piaaf piaaf-onto xlink isni skos" version="2.0">
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> July 3, 2017</xd:p>
            <xd:p><xd:b>Author:</xd:b> Florence</xd:p>
            <xd:p>Sémantisation : étape 5, génération d'un fichier RDF pour chaque partenaire  pour
                les noms des agents</xd:p>
        </xd:desc>
    </xd:doc>
   
    <xsl:param name="coll">AN</xsl:param>
   
    <xsl:variable name="chemin-EAC-BnF">
        <xsl:value-of
            select="concat('fichiers-def-2/notices-EAC/BnF/', '?select=*.xml;recurse=yes;on-error=warning')"
        />
    </xsl:variable>
    <xsl:variable name="collection-EAC-BnF" select="collection($chemin-EAC-BnF)"/>
    <xsl:variable name="chemin-EAC-SIAF">
        <xsl:value-of
            select="concat('fichiers-def-2/notices-EAC/SIAF/', '?select=*.xml;recurse=yes;on-error=warning')"
        />
    </xsl:variable>
    <xsl:variable name="collection-EAC-SIAF" select="collection($chemin-EAC-SIAF)"/>
    <xsl:variable name="chemin-EAC-AN">
        <xsl:value-of
            select="concat('fichiers-def-2/notices-EAC/AN/', '?select=*.xml;recurse=yes;on-error=warning')"
        />
    </xsl:variable>
    <xsl:variable name="collection-EAC-AN" select="collection($chemin-EAC-AN)"/>
    <xsl:variable name="SIAF-mandates" select="document('rdf/mandates/FRSIAF_mandates.rdf')/rdf:RDF"/>
    <xsl:variable name="BnF-mandates" select="document('rdf/mandates/FRBNF_mandates.rdf')/rdf:RDF"/>
    <xsl:variable name="AN-mandates" select="document('rdf/mandates/FRAN_mandates.rdf')/rdf:RDF"/>
    <xsl:template match="/piaaf:vide">
        <xsl:if test="$coll = 'SIAF'">
            <xsl:result-document href="rdf/agent-names/FRSIAF_agent-names.rdf" method="xml" encoding="utf-8" indent="yes">
                <rdf:RDF 
                    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                    xmlns:owl="http://www.w3.org/2002/07/owl#"
                    xmlns:RiC="http://www.ica.org/standards/RiC/ontology#"
                    
                    xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
                    
                    >
                    <!-- <part localType="nom">La Conté</part>
            <part localType="prenom">Marie-Christiane de</part>
            <part localType="dates_biographiques">1950- </part>-->
                   
                    
                    <xsl:for-each-group
                        select="$collection-EAC-SIAF/eac:eac-cpf[not(starts-with(normalize-space(eac:control/eac:recordId), 'FR784'))]/eac:cpfDescription/eac:identity/eac:nameEntry"
                        group-by="
                       
                       if (count(eac:part)=1)
                       then (normalize-space(eac:part))
                       else (
                       concat(eac:part[@localType='nom'], ', ', eac:part[@localType='prenom'], ' (', eac:part[@localType='dates_biographiques'], ')')
                       )
                       
                       
                       
                       
                       
                        ">
                        <xsl:sort select="current-grouping-key()"></xsl:sort>
                   <!--     <xsl:variable name="num" select="format-number(number(position()), '#000')"/>-->
                        <rdf:Description>
                            <xsl:attribute name="rdf:about">
                                <xsl:text>http://piaaf.demo.logilab.fr/resource/FRSIAF_agent-name_</xsl:text>
                               
                                <xsl:value-of select="substring-after(current-group()[1]/@xml:id, 'an_')"/>
                               <!-- <xsl:value-of select="concat('http://www.piaaf.net/', 'agent-names/FRSIAF_agent-name_', $num)"/>-->
                                
                            </xsl:attribute>
                            <rdf:type
                                rdf:resource="http://www.ica.org/standards/RiC/ontology#AgentName"/>
                            
                            
                            <rdfs:label xml:lang="fr">
                                <xsl:value-of select="current-grouping-key()"/>
                            </rdfs:label>
                          <xsl:for-each select="current-group()">
                      <!--      </xsl:if>-->
                            <!--</xsl:for-each>-->
                            <xsl:variable name="theEntry" select="
                                
                                if (ancestor::eac:identity/eac:nameEntry[count(eac:part)=1 and normalize-space(eac:part)=current-grouping-key()])
                                
                                then (ancestor::eac:identity/eac:nameEntry[count(eac:part)=1 and normalize-space(eac:part)=current-grouping-key()])
                                
                                else(
                                
                                if (ancestor::eac:identity/eac:nameEntry[concat(eac:part[@localType='nom'], ', ', eac:part[@localType='prenom'], ' (', eac:part[@localType='dates_biographiques'], ')')=current-grouping-key()])
                                
                                then (ancestor::eac:identity/eac:nameEntry[concat(eac:part[@localType='nom'], ', ', eac:part[@localType='prenom'], ' (', eac:part[@localType='dates_biographiques'], ')')=current-grouping-key()])
                                else()
                                )
                                
                                "                 
                            />
                          
                                <!--<xsl:variable name="auth" select="replace(normalize-space(eac:authorizedForm[1]), '_', ' ')"/>-->
                              <!--<xsl:variable name="theEntry" select="ancestor::eac:identity/eac:nameEntry[normalize-space(eac:part)=current-grouping-key()]">-->
                                  
                              <!--</xsl:variable>-->
                                <xsl:variable name="auth">
                                    <xsl:choose>
                                        <xsl:when test="contains($theEntry/eac:authorizedForm[1], '44-060')">
                                            <xsl:text>AFNOR NF Z 44-060</xsl:text>
                                        </xsl:when>
                                        <xsl:when test="contains($theEntry/eac:authorizedForm[1], '44-081')">
                                            <xsl:text>AFNOR NF Z 44-081</xsl:text>
                                        </xsl:when>
                                        <xsl:when test="contains($theEntry/eac:authorizedForm[1], 'ISAAR')">
                                            <xsl:text>ISAAR(CPF)</xsl:text>
                                        </xsl:when>
                                        <xsl:when test="contains($theEntry/eac:authorizedForm[1], '8601')">
                                            <xsl:text>ISO8601</xsl:text>
                                        </xsl:when>
                                    </xsl:choose>
                                </xsl:variable>
                                <xsl:if test="$auth!='' and $SIAF-mandates/rdf:Description[rdfs:label=$auth]">
                                    <RiC:conformsToRule>
                                        <xsl:attribute name="rdf:resource">
                                            <xsl:value-of select="$SIAF-mandates/rdf:Description[rdfs:label=$auth]/@rdf:about"/>
                                        </xsl:attribute>
                                    </RiC:conformsToRule>
                                </xsl:if>
                          
                            </xsl:for-each>
                         
                           <!-- <xsl:for-each-group select="current-group()" group-by="ancestor::eac:eac-cpf/eac:control/eac:recordId">
                                <RiC:nameOf>
                                    <xsl:attribute name="rdf:resource">
                                        <xsl:text>http://www.piaaf.net/agents/FRSIAF_</xsl:text>
                                        
                                        <xsl:choose>
                                            <xsl:when test="ancestor::eac:identity/eac:entityType='person'">
                                                <xsl:text>person_</xsl:text>
                                            </xsl:when>
                                            <xsl:when test="ancestor::eac:identity/eac:entityType='corporateBody'">
                                                <xsl:text>corporate-body_</xsl:text>
                                            </xsl:when>
                                        </xsl:choose>
                                        <xsl:value-of select="current-grouping-key()"/>  
                                    </xsl:attribute>
                                </RiC:nameOf>
                            </xsl:for-each-group>-->
                            
                        </rdf:Description>
                    </xsl:for-each-group>
                    
              
                    
                </rdf:RDF>
            </xsl:result-document>
        </xsl:if>
        <xsl:if test="$coll = 'BnF'">
            <xsl:result-document href="rdf/agent-names/FRBNF_agent-names.rdf" method="xml" encoding="utf-8" indent="yes">
                <rdf:RDF
                    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                    xmlns:owl="http://www.w3.org/2002/07/owl#"
                    xmlns:RiC="http://www.ica.org/standards/RiC/ontology#"
                    
                    xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
                   
                    >
                    
                    <xsl:for-each-group
                        select="$collection-EAC-BnF/eac:eac-cpf/eac:cpfDescription/eac:identity/eac:nameEntry"
                        group-by="
                        
                        if (count(eac:part)=1)
                        then (normalize-space(eac:part))
                        else (
                        concat(eac:part[@localType='nom'], ', ', eac:part[@localType='prenom'], ' (', eac:part[@localType='dates_biographiques'], ')')
                        )
                        
                        
                        
                        
                        
                        ">
                        <xsl:sort select="current-grouping-key()"></xsl:sort>
                        <xsl:variable name="num" select="format-number(number(position()), '#000')"/>
                        <rdf:Description>
                            <xsl:attribute name="rdf:about">
                               
                               <!-- <xsl:value-of select="concat('http://www.piaaf.net/', 'agent-names/FRBNF_agent-name_', $num)"/>
                                
                                -->
                                
                                <xsl:text>http://piaaf.demo.logilab.fr/resource/FRBNF_agent-name_</xsl:text>
                           
                                <xsl:value-of select="substring-after(current-group()[1]/@xml:id, 'an_')"/>
                               
                                
                                
                            </xsl:attribute>
                            <rdf:type
                                rdf:resource="http://www.ica.org/standards/RiC/ontology#AgentName"/>
                            
                            
                            <rdfs:label xml:lang="fr">
                                <xsl:value-of select="current-grouping-key()"/>
                            </rdfs:label>
                            
                           
                            <!-- <xsl:for-each select="current-group()">-->
                           
                            <xsl:for-each-group select="current-group()" group-by="
                                if (eac:authorizedForm)
                                then (normalize-space(eac:authorizedForm))
                                else(
                                if (eac:alternativeForm)
                                then (normalize-space(eac:alternativeForm))
                                
                                else()
                                
                                )">
                                <xsl:sort select="current-grouping-key()"></xsl:sort>
                               
                                
                              <!--  <xsl:variable name="auth" select="
                                    if ($theEntry/eac:authorizedForm)
                                    then (normalize-space($theEntry/eac:authorizedForm))
                                    else(
                                    if ($theEntry/eac:alternativeForm)
                                    then (normalize-space($theEntry/eac:alternativeForm))
                                    
                                    else()
                                    
                                    )
                                    "/>-->
                                <xsl:if test="$BnF-mandates/rdf:Description[rdfs:label=current-grouping-key()]">
                                    <RiC:conformsToRule>
                                        <xsl:attribute name="rdf:resource">
                                            <xsl:value-of select="$BnF-mandates/rdf:Description[rdfs:label=current-grouping-key()]/@rdf:about"/>
                                        </xsl:attribute>
                                    </RiC:conformsToRule>
                                    <!--      </xsl:if>-->
                                    <!--</xsl:for-each>-->
                                </xsl:if>
                               
                            </xsl:for-each-group>
                          <!--  <xsl:for-each-group select="current-group()" group-by="ancestor::eac:eac-cpf/eac:control/eac:recordId">
                            <RiC:nameOf>
                                <xsl:attribute name="rdf:resource">
                                    <xsl:text>http://www.piaaf.net/agents/FRBNF_</xsl:text>
                                    
                                    <xsl:choose>
                                        <xsl:when test="ancestor::eac:identity/eac:entityType='person'">
                                            <xsl:text>person_</xsl:text>
                                        </xsl:when>
                                        <xsl:when test="ancestor::eac:identity/eac:entityType='corporateBody'">
                                            <xsl:text>corporate-body_</xsl:text>
                                        </xsl:when>
                                    </xsl:choose>
                                    <xsl:value-of select="substring-after(current-grouping-key(), 'DGEARC_')"/>  
                                </xsl:attribute>
                            </RiC:nameOf>
                            </xsl:for-each-group>-->
                        </rdf:Description>
                    </xsl:for-each-group>
                    
                   
                    
                </rdf:RDF>
            </xsl:result-document>
        </xsl:if>
        <xsl:if test="$coll = 'AN'">
            <xsl:result-document href="rdf/agent-names/FRAN_agent-names.rdf" method="xml" encoding="utf-8" indent="yes">
                <rdf:RDF 
                    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                    xmlns:owl="http://www.w3.org/2002/07/owl#"
                    xmlns:RiC="http://www.ica.org/standards/RiC/ontology#"
                    
                    xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
                    >
                 
                    <xsl:for-each-group
                        select="$collection-EAC-AN/eac:eac-cpf/eac:cpfDescription/eac:identity/eac:nameEntry"
                        group-by="
                        
                      concat(normalize-space(eac:part), ' -- ', parent::eac:identity/eac:entityType, ' | ', @localType)
                        
                        
                        
                        
                        ">
                        <xsl:sort select="current-grouping-key()"></xsl:sort>
                        <xsl:variable name="num" select="format-number(number(position()), '#000')"/>
                        <rdf:Description>
                            <xsl:attribute name="rdf:about">
                               
                             <!--   <xsl:value-of select="concat('http://www.piaaf.net/', 'agent-names/FRAN_agent-name_', $num)"/>
                              --> 
                                
                                <xsl:text>http://piaaf.demo.logilab.fr/resource/FRAN_agent-name_</xsl:text>
                                
                                <xsl:value-of select="substring-after(current-group()[1]/@xml:id, 'an_')"/>
                            </xsl:attribute>
                            <rdf:type
                                rdf:resource="http://www.ica.org/standards/RiC/ontology#AgentName"/>
                            
                            
                            <rdfs:label xml:lang="fr">
                                <xsl:value-of select="substring-before(current-grouping-key(), ' --')"/>
                            </rdfs:label>
                            <xsl:choose>
                                <xsl:when test="contains(current-grouping-key(), 'person | preferredFormForProject')">
                                    <RiC:conformsToRule>
                                        <xsl:attribute name="rdf:resource">
                                            <xsl:value-of select="$AN-mandates/rdf:Description[rdfs:label='AFNOR_Z44-061']/@rdf:about"/>
                                        </xsl:attribute>
                                    </RiC:conformsToRule>
                                </xsl:when>
                                <xsl:when test="contains(current-grouping-key(), 'corporateBody | preferredFormForProject')">
                                    <RiC:conformsToRule>
                                        <xsl:attribute name="rdf:resource">
                                            <xsl:value-of select="$AN-mandates/rdf:Description[rdfs:label='AFNOR_Z44-060']/@rdf:about"/>
                                        </xsl:attribute>
                                    </RiC:conformsToRule>
                                </xsl:when>
                            </xsl:choose>
                            
                         <!--   <xsl:for-each-group select="current-group()" group-by="ancestor::eac:eac-cpf/eac:control/eac:recordId">
                                <RiC:nameOf>
                                    <xsl:attribute name="rdf:resource">
                                        <xsl:text>http://www.piaaf.net/agents/FRAN_</xsl:text>
                                        
                                        <xsl:choose>
                                            <xsl:when test="ancestor::eac:eac-cpf/eac:cpfDescription/eac:identity/eac:entityType='person'">
                                                <xsl:text>person_</xsl:text>
                                            </xsl:when>
                                            <xsl:when test="ancestor::eac:eac-cpf/eac:cpfDescription/eac:identity/eac:entityType='corporateBody'">
                                                <xsl:text>corporate-body_</xsl:text>
                                            </xsl:when>
                                        </xsl:choose>
                                        <xsl:value-of select="substring-after(current-grouping-key(), 'FRAN_NP_')"/>  
                                    </xsl:attribute>
                                </RiC:nameOf>
                              
                            </xsl:for-each-group>-->
                           
                         
                        </rdf:Description>
                    </xsl:for-each-group>
                    
                  
                    
                </rdf:RDF>
            </xsl:result-document>
        </xsl:if>
    </xsl:template>
    
</xsl:stylesheet>