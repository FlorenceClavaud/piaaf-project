<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:owl="http://www.w3.org/2002/07/owl#" xmlns:RiC="http://www.ica.org/standards/RiC/ontology#"
    xmlns:isni="http://isni.org/ontology#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
    xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:eac="urn:isbn:1-931666-33-4"
    xmlns="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:piaaf="http://www.piaaf.net"
    xmlns:piaaf-onto="http://piaaf.demo.logilab.fr/ontology#"
    xmlns:iso-thes="http://purl.org/iso25964/skos-thes#"
    xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:foaf="http://xmlns.com/foaf/0.1/"
    xmlns:dct="http://purl.org/dc/terms/" xmlns:xl="http://www.w3.org/2008/05/skos-xl#"
    xmlns:ginco="http://data.culture.fr/thesaurus/ginco/ns/"
    exclude-result-prefixes="xs xd eac iso-thes rdf dct xl piaaf xlink piaaf-onto skos isni foaf ginco dc" version="2.0">
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> July 4, 2017, checked and updated Dec. 8, 2017</xd:p>
            <xd:p><xd:b>Author:</xd:b> Florence Clavaud (Archives nationales)</xd:p>
            <xd:p>Sémantisation : étape 9, génération de fichiers RDF pour les types de groupes à partir des notices génériques du SIAF</xd:p>
        </xd:desc>
    </xd:doc>
    
    <!-- 
    <rdf:type rdf:resource="http://wwww.ica.org/standards/RiC/ontology#GroupType"></rdf:type>
        <rdfs:label xml:lang="fr">France. Région. Direction régionale des affaires culturelles</rdfs:label>
      
        <RiC:history xml:lang="fr"></RiC:history>
        <RiC:description xml:lang="fr"></RiC:description>
        <RiC:legalStatus rdf:resource="http://www.piaaf.net/legal-statuses/legal-status_009"></RiC:legalStatus>
        <RiC:name>France. Région. Direction régionale des affaires culturelles (DRAC)</RiC:name>
        
        <RiC:beginningDate>1977</RiC:beginningDate>
        <RiC:authorizedBy rdf:resource="http://wwww.piaaf.net/mandates/SIAF_mandate_001"></RiC:authorizedBy>
       
        <piaaf-onto:fulfilledFunctionOfType rdf:resource="http://www.piaaf.net/functions/function_407"/>
        <RiC:hasPart rdf:resource="http://www.piaaf.net/relations/SIAF_relation_of_group-type_FR78422804100033_000000369_01"/>
        <RiC:hasPart rdf:resource="http://www.piaaf.net/relations/SIAF_relation_of_group-type_FR78422804100033_000000369_02"/>
        <RiC:hasBusinessRelation rdf:resource="http://www.piaaf.net/relations/SIAF_relation_of_group-type_FR78422804100033_000000369_03"/>
       <RiC:jurisdiction>{place/descriptiveNote}</RiC:jurisdiction>
        <RiC:noteOnStructureOrGenealogy></RiC:noteOnStructureOrGenealogy>
        <owl:sameAs><!-\- si lien d'identité vers notice BnF-\-></owl:sameAs>
        <RiC:describedBy rdf:resource="http://www.piaaf.net/descriptions/SIAF_description_of_group-type_FR78422804100033_000000369"/>
    
    -\->-->
    <xsl:variable name="chemin-EAC-BnF">
        <xsl:value-of
            select="concat('fichiers-def-2/notices-EAC/BnF/', '?select=*.xml;recurse=yes;on-error=warning')"
        />
    </xsl:variable>
    <xsl:variable name="collection-EAC-BnF" select="collection($chemin-EAC-BnF)"/>
   
    <xsl:variable name="chemin-EAC-AN">
        <xsl:value-of
            select="concat('fichiers-def-2/notices-EAC/AN/', '?select=*.xml;recurse=yes;on-error=warning')"
        />
    </xsl:variable>
    <xsl:variable name="collection-EAC-AN" select="collection($chemin-EAC-AN)"/>
    
    
    <xsl:variable name="chemin-EAC-SIAF">
        <xsl:value-of
            select="concat('fichiers-def-2/notices-EAC/SIAF/', '?select=*.xml;recurse=yes;on-error=warning')"
        />
    </xsl:variable>
    <xsl:variable name="collection-EAC-SIAF" select="collection($chemin-EAC-SIAF)"/>
    <xsl:variable name="siaf-mandates" select="document('rdf/mandates/FRSIAF_mandates.rdf')/rdf:RDF"/>
    <xsl:variable name="siaf-events" select="document('rdf/events/FRSIAF_events.rdf')/rdf:RDF"/>
    <xsl:variable name="functions" select="document('rdf/functions.rdf')/rdf:RDF"/>
    <xsl:variable name="legalsts" select="document('rdf/legal-statuses.rdf')/rdf:RDF"/>
    <xsl:variable name="siaf-names" select="document('rdf/agent-names/FRSIAF_agent-names.rdf')/rdf:RDF"/>
    <xsl:variable name="siaf-gtypes-relations" select="document('rdf/relations/FRSIAF_relations-between-group-types.rdf')/rdf:RDF"/>
    
    <xsl:template match="/piaaf:vide">
        <xsl:for-each select="$collection-EAC-SIAF/eac:eac-cpf[starts-with(normalize-space(eac:control/eac:recordId), 'FR784')]">
            <xsl:variable name="recId" select="normalize-space(eac:control/eac:recordId)"/>
            
            <xsl:result-document href="rdf/group-types/FRSIAF_group-type_{$recId}.rdf" method="xml" encoding="utf-8" indent="yes">
                <rdf:RDF 
                    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                    xmlns:owl="http://www.w3.org/2002/07/owl#"
                    xmlns:RiC="http://www.ica.org/standards/RiC/ontology#"
                    
                    xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
                    
                    
                    >
                    <rdf:Description>
                        <xsl:attribute name="rdf:about">
                            <xsl:value-of select="concat('http://piaaf.demo.logilab.fr/resource/FRSIAF_group-type_', $recId)"/>
                        </xsl:attribute>
                        <rdf:type rdf:resource="http://www.ica.org/standards/RiC/ontology#GroupType"/>
                        <rdfs:label xml:lang="fr">
                            <xsl:value-of select="normalize-space(eac:cpfDescription/eac:identity/eac:nameEntry[@localType='preferredFormForProject']/eac:part)"/>
                        </rdfs:label>
                        <!-- relation vers chacun des noms -->
                        <xsl:for-each select="eac:cpfDescription/eac:identity/eac:nameEntry">
                            <RiC:hasDenominationRelation>
                                <xsl:attribute name="rdf:resource">
                                    <xsl:value-of select="concat('http://piaaf.demo.logilab.fr/resource/FRSIAF_DenominationRelation_', $recId, '_',generate-id() )"/>
                                    
                                </xsl:attribute>
                            </RiC:hasDenominationRelation>
                        </xsl:for-each>
                        
                        
                        
                  
                        <xsl:if test="eac:cpfDescription/eac:description/eac:function[not(@localType)]/*[not(self::eac:term) and normalize-space(.)!='']">
                        <RiC:description xml:lang="fr">
                            <xsl:for-each select="eac:cpfDescription/eac:description/eac:function[not(@localType)]/*[not(self::eac:term) and normalize-space(.)!='']">
                                <xsl:apply-templates></xsl:apply-templates>
                                <xsl:if test="position()!=last()">
                                    <xsl:text> </xsl:text>
                                </xsl:if>
                            </xsl:for-each>
                          
                        </RiC:description>
                        </xsl:if>
                        <!--<RiC:beginningDate rdf:datatype="http://www.w3.org/2001/XMLSchema#dateTime">
                            <xsl:value-of select="eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate/@standardDate"/>
                        </RiC:beginningDate>-->
                        <xsl:if test="eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate">
                            <RiC:beginningDate>
                                
                                <xsl:choose>
                                    
                                    <xsl:when test="string-length(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate/@standardDate)=4">
                                        <xsl:attribute name="rdf:datatype"> <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text></xsl:attribute>
                                        
                                    </xsl:when>
                                    <xsl:when test="string-length(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate/@standardDate)=7">
                                        <xsl:attribute name="rdf:datatype">
                                            <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                        </xsl:attribute>
                                    </xsl:when>
                                    <xsl:when test="string-length(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate/@standardDate)=10">
                                        <xsl:attribute name="rdf:datatype">
                                            <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                        </xsl:attribute>
                                    </xsl:when>
                                    <xsl:otherwise/>
                                </xsl:choose>
                                
                                <xsl:value-of select="
                                    if (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate/@standardDate)!='')
                                    then (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate/@standardDate))
                                    else(
                                    if (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate)!='')
                                    then (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate))
                                    else()
                                    )
                                    "/>
                                
                            </RiC:beginningDate>
                        </xsl:if>
                        <xsl:if test="eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate">
                            <RiC:endDate>
                                <xsl:choose>
                                    
                                    <xsl:when test="string-length(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate/@standardDate)=4">
                                        <xsl:attribute name="rdf:datatype"> <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text></xsl:attribute>
                                        
                                    </xsl:when>
                                    <xsl:when test="string-length(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate/@standardDate)=7">
                                        <xsl:attribute name="rdf:datatype">
                                            <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                        </xsl:attribute>
                                    </xsl:when>
                                    <xsl:when test="string-length(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate/@standardDate)=10">
                                        <xsl:attribute name="rdf:datatype">
                                            <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                        </xsl:attribute>
                                    </xsl:when>
                                    <xsl:otherwise/>
                                </xsl:choose>
                                
                                <!--  <RiC:endDate rdf:datatype="http://www.w3.org/2001/XMLSchema#dateTime">-->
                                <xsl:value-of select="
                                    if (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate/@standardDate)!='')
                                    then (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate/@standardDate))
                                    else(
                                    if (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate)!='')
                                    then (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate))
                                    else()
                                    )
                                    "/>
                            </RiC:endDate>
                        </xsl:if>
                    
                        <xsl:if test="eac:cpfDescription/eac:description/eac:biogHist/*[not(self::eac:chronList) and normalize-space(.)!='']">
                            
                                    <RiC:history xml:lang="fr">
                                        <xsl:for-each
                                            select="eac:cpfDescription/eac:description/eac:biogHist/*[not(self::eac:chronList) and normalize-space(.)!='']">
                                           <xsl:apply-templates/>
                                            <xsl:if test="position() != last()">
                                                <xsl:text> </xsl:text>
                                            </xsl:if>
                                        </xsl:for-each>
                                    </RiC:history>
                                    
                                
                             
                        </xsl:if>
                        <!-- événements -->
                        <xsl:for-each select="eac:cpfDescription/eac:description/eac:biogHist/eac:chronList/eac:chronItem[normalize-space(.)!='']">
                            <xsl:variable name="event" select="concat(normalize-space(eac:date), ' : ', normalize-space(eac:event))"/>
                            <xsl:if test="$siaf-events/rdf:Description[rdfs:label=$event]">
                                <RiC:affectedBy>
                                    <xsl:attribute name="rdf:resource">
                                        
                                        <xsl:value-of select="$siaf-events/rdf:Description[rdfs:label=$event]/@rdf:about"/>
                                    </xsl:attribute>
                                </RiC:affectedBy>
                            </xsl:if>
                            
                        </xsl:for-each>
                        <xsl:choose>
                            <xsl:when test="count(eac:cpfDescription/eac:description/eac:place)=1">
                                <RiC:jurisdiction  xml:lang="fr">
                                    <xsl:value-of select="
                                        if (eac:cpfDescription/eac:description/eac:place/eac:descriptiveNote[normalize-space(.)!=''])
                                        then (                                       eac:cpfDescription/eac:description/eac:place/eac:descriptiveNote)
                                        else(
                                        if (eac:cpfDescription/eac:description/eac:place/eac:placeEntry)
                                        
                                        then (eac:cpfDescription/eac:description/eac:place/eac:placeEntry)
                                        else()
                                        )
                                        
                                        "/>
                                </RiC:jurisdiction>
                            </xsl:when>
                            <xsl:otherwise>
                                
                            </xsl:otherwise>
                        </xsl:choose>
                      
                        <xsl:if test="eac:cpfDescription/eac:description/eac:structureOrGenealogy/*[normalize-space(.)!='']">
                           
                            <RiC:noteOnStructureOrGenealogy xml:lang="fr">
                                <xsl:for-each select="eac:cpfDescription/eac:description/eac:structureOrGenealogy/*[normalize-space(.)!='']">
                                    <xsl:apply-templates/>
                                    <xsl:if test="position() != last()">
                                        <xsl:text> </xsl:text>
                                    </xsl:if>
                                    
                                </xsl:for-each>
                             
                            </RiC:noteOnStructureOrGenealogy>
                        </xsl:if>
                        <!-- les mandats-->
                        <xsl:for-each select="eac:cpfDescription/eac:description/descendant::eac:mandate">
                            <xsl:variable name="descNote" select="normalize-space(eac:descriptiveNote)"/>
                            <xsl:variable name="citation" select="
                                if (eac:citation[@xlink:href!=''])
                                then (normalize-space(eac:citation/@xlink:href))
                                else ('non')
                                "/>
                            <!--<xsl:variable name="link" select="eac:citation/@xlink:href"/>-->
                            <xsl:if test="$siaf-mandates/rdf:Description[RiC:hasTitle=$descNote and rdf:type[@rdf:resource='http://www.ica.org/standards/RiC/ontology#Mandate']]">
                                
                                <xsl:choose>
                                    <xsl:when test="$citation='non'">
                                        <xsl:for-each select="$siaf-mandates/rdf:Description[RiC:hasTitle=$descNote and rdf:type[@rdf:resource='http://www.ica.org/standards/RiC/ontology#Mandate']]">
                                            <RiC:conformsToRule>
                                            <xsl:attribute name="rdf:resource">
                                                <xsl:value-of select="./@rdf:about"/>
                                                
                                            </xsl:attribute>
                                            
                                            </RiC:conformsToRule>
                                        </xsl:for-each>
                                            
                                    </xsl:when>
                                    <xsl:when test="$citation!='non'">
                                        <xsl:comment>citation pas vide</xsl:comment>
                                        <xsl:for-each select="$siaf-mandates/rdf:Description[RiC:hasTitle=$descNote and rdf:type[@rdf:resource='http://www.ica.org/standards/RiC/ontology#Mandate']]">
                                            
                                            
                                                <xsl:choose>
                                                    <xsl:when test="RiC:evidencedBy">
                                                        <xsl:variable name="theEvidence" select="RiC:evidencedBy/@rdf:resource"/>
                                                        <xsl:comment>il y a un RiC:evidencedBy</xsl:comment>
                                                        <xsl:if test="ancestor::rdf:RDF/rdf:Description[@rdf:about=$theEvidence and RiC:identifier=$citation]">
                                                            
                                                            <RiC:conformsToRule>
                                                                <xsl:attribute name="rdf:resource">
                                                                    <xsl:value-of select="@rdf:about"/>
                                                                    
                                                                </xsl:attribute>
                                                                
                                                            </RiC:conformsToRule>
                                                        </xsl:if>
                                                        
                                                        
                                                       
                                                    </xsl:when>
                                                </xsl:choose>
                                            
                                       
                                        </xsl:for-each>    
                                    </xsl:when>
                                </xsl:choose>
                          
                                
                            </xsl:if>
                            
                            
                        </xsl:for-each>
                        <!-- les fonctions abstraites-->
                        <xsl:for-each select="eac:cpfDescription/eac:description/descendant::eac:function[@localType='piaafFunction']">
                            <xsl:variable name="functionName">
                               <!-- <xsl:for-each select="eac:term">
                                    <xsl:value-of select="."/>
                                    <xsl:if test="position()!=last()"><xsl:text> -\- </xsl:text></xsl:if>
                                </xsl:for-each>-->
                                <xsl:value-of select="eac:term[1]"/>
                            </xsl:variable>
                            <xsl:if test="$functions/rdf:Description[rdfs:label=$functionName]">
                                <piaaf-onto:hasFunctionOfType>
                                    <xsl:attribute name="rdf:resource">
                                        <xsl:value-of select="$functions/rdf:Description[rdfs:label=$functionName]/@rdf:about"/>
                                    </xsl:attribute>
                                </piaaf-onto:hasFunctionOfType>
                            </xsl:if>
                            
                        </xsl:for-each>
                        
                       
                        <!-- les relations d'identité -->
                        <xsl:for-each select="eac:cpfDescription/eac:relations/eac:cpfRelation[@cpfRelationType='identity']">
                            
                            <xsl:variable name="lnk" select="normalize-space(@xlink:href)"/>
                            <owl:sameAs>
                               <xsl:attribute name="rdf:resource">
                                   <xsl:if test="contains($lnk, 'bnf.fr')">
                                       <xsl:choose>
                                           <xsl:when test="contains($lnk, 'catalogue.bnf.fr/ark:/12148/')">
                                               <xsl:text>http://data.bnf.fr/ark:/12148/</xsl:text>
                                               <xsl:value-of select="substring-before(substring-after($lnk, 'http://catalogue.bnf.fr/ark:/12148/'), '/PUBLIC')"/>
                                               <xsl:text>#foaf:Organization</xsl:text>
                                           </xsl:when>
                                       </xsl:choose>
                                   </xsl:if>
                                   
                               </xsl:attribute>
                            </owl:sameAs>
                        </xsl:for-each>
                        <!-- les relations cpfType autres que identité -->
                        <xsl:for-each select="eac:cpfDescription/eac:relations/eac:cpfRelation[contains(@xlink:arcrole,'isControlledBy')]">
                            <xsl:call-template name="outputObjectPropertyForRelation">
                                <xsl:with-param name="link" select="
                                    if (normalize-space(@xlink:href)='' or not(@xlink:href))
                                    then ('non')
                                    else(normalize-space(@xlink:href))
                                    "></xsl:with-param>
                                <xsl:with-param name="relEntry" select="normalize-space(eac:relationEntry)"></xsl:with-param>
                                <xsl:with-param name="recId" select="$recId"/>
                                <xsl:with-param name="srcRelType" select="'isControlledBy'"></xsl:with-param>
                                <xsl:with-param name="arcToRelName" select="'underAuthorityRelation'"></xsl:with-param>
                                <xsl:with-param name="toAncRelArcName" select="'authorityOn'"></xsl:with-param>
                                <xsl:with-param name="relName" select="'AuthorityRelation'"></xsl:with-param>
                                <xsl:with-param name="relSecondArcName" select="'authorityHeldBy'"></xsl:with-param>
                                <xsl:with-param name="shortcutRelName" select="'controlledBy'"></xsl:with-param>
                                <xsl:with-param name="relFromDate" select="eac:dateRange/eac:fromDate/@standardDate"/>
                                <xsl:with-param name="relToDate" select="eac:dateRange/eac:toDate/@standardDate"/>
                                <xsl:with-param name="relNote" select="normalize-space(eac:descriptiveNote/eac:p)"/>
                                
                            </xsl:call-template>
                            
                     
                        </xsl:for-each>
                        <xsl:for-each select="eac:cpfDescription/eac:relations/eac:cpfRelation[contains(@xlink:arcrole,'isFunctionallyLinkedTo')]">
                            <xsl:call-template name="outputObjectPropertyForRelation">
                                <xsl:with-param name="link" select="
                                    if (normalize-space(@xlink:href)='' or not(@xlink:href))
                                    then ('non')
                                    else(normalize-space(@xlink:href))
                                    "></xsl:with-param>
                                <xsl:with-param name="recId" select="$recId"/>
                                <xsl:with-param name="relEntry" select="normalize-space(eac:relationEntry)"></xsl:with-param>
                                <xsl:with-param name="srcRelType" select="'isFunctionallyLinkedTo'"></xsl:with-param>
                                <xsl:with-param name="arcToRelName" select="'hasBusinessRelation'"></xsl:with-param>
                                <xsl:with-param name="toAncRelArcName" select="'businessRelationWith'"></xsl:with-param>
                                <xsl:with-param name="relName" select="'BusinessRelation'"></xsl:with-param>
                                <xsl:with-param name="relSecondArcName" select="'businessRelationWith'"></xsl:with-param>
                                <xsl:with-param name="shortcutRelName" select="'functionallyLinkedTo'"></xsl:with-param>
                                <xsl:with-param name="relFromDate" select="eac:dateRange/eac:fromDate/@standardDate"/>
                                <xsl:with-param name="relToDate" select="eac:dateRange/eac:toDate/@standardDate"/>
                                <xsl:with-param name="relNote" select="normalize-space(eac:descriptiveNote/eac:p)"/>
                                
                            </xsl:call-template>
                        
                        </xsl:for-each>
                        <xsl:for-each select="eac:cpfDescription/eac:relations/eac:cpfRelation[contains(@xlink:arcrole,'hasPart')]">
                            <xsl:call-template name="outputObjectPropertyForRelation">
                                <xsl:with-param name="link" select="
                                    if (normalize-space(@xlink:href)='' or not(@xlink:href))
                                    then ('non')
                                    else(normalize-space(@xlink:href))
                                    "></xsl:with-param>
                                <xsl:with-param name="recId" select="$recId"/>
                                <xsl:with-param name="relEntry" select="normalize-space(eac:relationEntry)"></xsl:with-param>
                                <xsl:with-param name="srcRelType" select="'hasPart'"></xsl:with-param>
                                <xsl:with-param name="arcToRelName" select="'hasPart'"></xsl:with-param>
                                <xsl:with-param name="toAncRelArcName" select="'partOf'"></xsl:with-param>
                                <xsl:with-param name="relName" select="'WholePartRelation'"></xsl:with-param>
                                <xsl:with-param name="relSecondArcName" select="'wholeHasPart'"></xsl:with-param>
                                <xsl:with-param name="shortcutRelName" select="'integrates'"></xsl:with-param>
                                <xsl:with-param name="relFromDate" select="eac:dateRange/eac:fromDate/@standardDate"/>
                                <xsl:with-param name="relToDate" select="eac:dateRange/eac:toDate/@standardDate"/>
                                <xsl:with-param name="relNote" select="normalize-space(eac:descriptiveNote/eac:p)"/>
                                
                            </xsl:call-template>
                        
                        </xsl:for-each>
                        <xsl:for-each select="eac:cpfDescription/eac:relations/eac:cpfRelation[contains(@xlink:arcrole,'isPartOf')]">
                            <xsl:call-template name="outputObjectPropertyForRelation">
                                <xsl:with-param name="link" select="
                                    if (normalize-space(@xlink:href)='' or not(@xlink:href))
                                    then ('non')
                                    else(normalize-space(@xlink:href))
                                    "></xsl:with-param>
                                <xsl:with-param name="recId" select="$recId"/>
                                <xsl:with-param name="relEntry" select="normalize-space(eac:relationEntry)"></xsl:with-param>
                                <xsl:with-param name="srcRelType" select="'isPartOf'"></xsl:with-param>
                                <xsl:with-param name="arcToRelName" select="'hasWholePartRelation'"></xsl:with-param>
                                <xsl:with-param name="toAncRelArcName" select="'wholeHasPart'"></xsl:with-param>
                                <xsl:with-param name="relName" select="'WholePartRelation'"></xsl:with-param>
                                <xsl:with-param name="relSecondArcName" select="'partOf'"></xsl:with-param>
                                <xsl:with-param name="shortcutRelName" select="'integratedIn'"></xsl:with-param>
                                <xsl:with-param name="relFromDate" select="eac:dateRange/eac:fromDate/@standardDate"/>
                                <xsl:with-param name="relToDate" select="eac:dateRange/eac:toDate/@standardDate"/>
                                <xsl:with-param name="relNote" select="normalize-space(eac:descriptiveNote/eac:p)"/>
                                
                            </xsl:call-template>
                        
                        </xsl:for-each>
                        <xsl:for-each select="eac:cpfDescription/eac:relations/eac:cpfRelation[@cpfRelationType='temporal-earlier']">
                            <xsl:call-template name="outputObjectPropertyForRelation">
                                <xsl:with-param name="link" select="
                                    if (normalize-space(@xlink:href)='' or not(@xlink:href))
                                    then ('non')
                                    else(normalize-space(@xlink:href))
                                    "></xsl:with-param>
                                <xsl:with-param name="recId" select="$recId"/>
                                <xsl:with-param name="relEntry" select="normalize-space(eac:relationEntry)"></xsl:with-param>
                                <xsl:with-param name="srcRelType" select="'temporal-earlier'"></xsl:with-param>
                                <xsl:with-param name="arcToRelName" select="'thingFollows'"></xsl:with-param>
                                <xsl:with-param name="toAncRelArcName" select="'followingThingInTime'"></xsl:with-param>
                                <xsl:with-param name="relName" select="'TemporalRelation'"></xsl:with-param>
                                <xsl:with-param name="relSecondArcName" select="'precedingThingInTime'"></xsl:with-param>
                                <xsl:with-param name="shortcutRelName" select="'successorOf'"></xsl:with-param>
                                <xsl:with-param name="relFromDate" select="eac:dateRange/eac:fromDate/@standardDate"/>
                                <xsl:with-param name="relToDate" select="eac:dateRange/eac:toDate/@standardDate"/>
                                <xsl:with-param name="relNote" select="normalize-space(eac:descriptiveNote/eac:p)"/>
                                
                            </xsl:call-template>
                        
                        </xsl:for-each>
                        <xsl:for-each select="eac:cpfDescription/eac:relations/eac:cpfRelation[@cpfRelationType='temporal-later']">
                            <xsl:call-template name="outputObjectPropertyForRelation">
                                <xsl:with-param name="link" select="
                                    if (normalize-space(@xlink:href)='' or not(@xlink:href))
                                    then ('non')
                                    else(normalize-space(@xlink:href))
                                    "></xsl:with-param>
                                <xsl:with-param name="recId" select="$recId"/>
                                <xsl:with-param name="relEntry" select="normalize-space(eac:relationEntry)"></xsl:with-param>
                                <xsl:with-param name="srcRelType" select="'temporal-later'"></xsl:with-param>
                                <xsl:with-param name="arcToRelName" select="'thingPrecedes'"></xsl:with-param>
                                <xsl:with-param name="toAncRelArcName" select="'precedingThingInTime'"></xsl:with-param>
                                <xsl:with-param name="relName" select="'TemporalRelation'"></xsl:with-param>
                                <xsl:with-param name="relSecondArcName" select="'followingThingInTime'"></xsl:with-param>
                                <xsl:with-param name="shortcutRelName" select="'predecessorOf'"></xsl:with-param>
                                <xsl:with-param name="relFromDate" select="eac:dateRange/eac:fromDate/@standardDate"/>
                                <xsl:with-param name="relToDate" select="eac:dateRange/eac:toDate/@standardDate"/>
                                <xsl:with-param name="relNote" select="normalize-space(eac:descriptiveNote/eac:p)"/>
                                
                            </xsl:call-template>
                          
                        </xsl:for-each>
                     
                        <!-- métadonnées -->
                        
                        <RiC:describedBy>
                            <xsl:attribute name="rdf:resource">
                                <xsl:value-of select="concat('http://piaaf.demo.logilab.fr/resource/FRSIAF_description_', $recId)"/></xsl:attribute>
                        </RiC:describedBy>
                    </rdf:Description>
                    <!-- sortie des relations de catégorisation -->
                    <!-- dénominations ; on a choisi ici de ne pas traiter les dénominations de ces group types comme des entités à part entière -->
                    <xsl:for-each select="eac:cpfDescription/eac:identity/eac:nameEntry">
                        <xsl:variable name="theName" select="normalize-space(eac:part)"/>
                        <rdf:Description>
                            <xsl:attribute name="rdf:about">
                                <xsl:value-of select="concat('http://piaaf.demo.logilab.fr/resource/FRSIAF_DenominationRelation_', $recId, '_', generate-id())"/>
                                
                            </xsl:attribute>
                            <rdf:type rdf:resource="http://www.ica.org/standards/RiC/ontology#DenominationRelation"/>
                            <xsl:if test="eac:useDates/eac:dateRange/eac:fromDate[normalize-space(.)!='']">
                               
                                <RiC:beginningDate>
                                    <xsl:choose>
                                        
                                        <xsl:when test="string-length(eac:useDates/eac:dateRange/eac:fromDate/@standardDate)=4">
                                            <xsl:attribute name="rdf:datatype"> <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text></xsl:attribute>
                                            
                                        </xsl:when>
                                        <xsl:when test="string-length(eac:useDates/eac:dateRange/eac:fromDate/@standardDate)=7">
                                            <xsl:attribute name="rdf:datatype">
                                                <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                            </xsl:attribute>
                                        </xsl:when>
                                        <xsl:when test="string-length(eac:useDates/eac:dateRange/eac:fromDate/@standardDate)=10">
                                            <xsl:attribute name="rdf:datatype">
                                                <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                            </xsl:attribute>
                                        </xsl:when>
                                        <xsl:otherwise/>
                                    </xsl:choose>
                                    
                                    <xsl:value-of select="
                                        if (normalize-space(eac:useDates/eac:dateRange/eac:fromDate/@standardDate)!='')
                                        then (normalize-space(eac:useDates/eac:dateRange/eac:fromDate/@standardDate))
                                        else(
                                        if (normalize-space(eac:useDates/eac:dateRange/eac:fromDate)!='')
                                        then (normalize-space(eac:useDates/eac:dateRange/eac:fromDate))
                                        else()
                                        )
                                        "/>
                                   <!-- <xsl:value-of select="eac:useDates/eac:dateRange/eac:fromDate"/>-->
                                </RiC:beginningDate>
                            </xsl:if>
                            <xsl:if test="eac:useDates/eac:dateRange/eac:toDate[normalize-space(.)!='']">
                                <RiC:endDate>
                                    <xsl:choose>
                                        
                                        <xsl:when test="string-length(eac:useDates/eac:dateRange/eac:toDate/@standardDate)=4">
                                            <xsl:attribute name="rdf:datatype"> <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text></xsl:attribute>
                                            
                                        </xsl:when>
                                        <xsl:when test="string-length(eac:useDates/eac:dateRange/eac:toDate/@standardDate)=7">
                                            <xsl:attribute name="rdf:datatype">
                                                <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                            </xsl:attribute>
                                        </xsl:when>
                                        <xsl:when test="string-length(eac:useDates/eac:dateRange/eac:toDate/@standardDate)=10">
                                            <xsl:attribute name="rdf:datatype">
                                                <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                            </xsl:attribute>
                                        </xsl:when>
                                        <xsl:otherwise/>
                                    </xsl:choose>
                                    
                                    <!--  <RiC:endDate rdf:datatype="http://www.w3.org/2001/XMLSchema#dateTime">-->
                                    <xsl:value-of select="
                                        if (normalize-space(eac:useDates/eac:dateRange/eac:toDate/@standardDate)!='')
                                        then (normalize-space(eac:useDates/eac:dateRange/eac:toDate/@standardDate))
                                        else(
                                        if (normalize-space(eac:useDates/eac:dateRange/eac:toDate)!='')
                                        then (normalize-space(eac:useDates/eac:dateRange/eac:toDate))
                                        else()
                                        )
                                        "/>
                                   <!-- <xsl:value-of select="eac:useDates/eac:dateRange/eac:toDate"/>-->
                                </RiC:endDate>
                                
                            </xsl:if>
                            <RiC:certainty xml:lang="fr">certain</RiC:certainty>
                            <RiC:denominationOf>
                                <xsl:attribute name="rdf:resource">
                                    <xsl:value-of select="concat('http://piaaf.demo.logilab.fr/resource/FRSIAF_group-type_', $recId)"/>
                                </xsl:attribute>
                            </RiC:denominationOf>
                            <RiC:denominatedBy xml:lang="fr">
                               
                                 
                                    
                                   <xsl:value-of select="normalize-space(eac:part)"/>
                                    
                                
                            </RiC:denominatedBy>
                        </rdf:Description>
                     
                    </xsl:for-each>
                 
                        
            
                 
                  
                  
                    
                    <!-- la notice sur le group type (entité Description) -->
                    <rdf:Description>
                        
                        <xsl:attribute name="rdf:about"><xsl:value-of select="concat('http://piaaf.demo.logilab.fr/resource/FRSIAF_description_', $recId)"/></xsl:attribute>
                       
                        <rdf:type rdf:resource="http://www.ica.org/standards/RiC/ontology#Description"/>
                        <RiC:describes>
                            <xsl:attribute name="rdf:resource">
                                <xsl:value-of select="concat('http://piaaf.demo.logilab.fr/resource/FRSIAF_group-type_', $recId)"/>
                            </xsl:attribute>
                        </RiC:describes>
                        <RiC:creationDate>
                            <xsl:choose>
                                
                                <xsl:when test="string-length(eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType='created']/eac:eventDateTime/@standardDateTime)=4">
                                    <xsl:attribute name="rdf:datatype"> <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text></xsl:attribute>
                                    
                                </xsl:when>
                                <xsl:when test="string-length(eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType='created']/eac:eventDateTime/@standardDateTime)=7">
                                    <xsl:attribute name="rdf:datatype">
                                        <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                    </xsl:attribute>
                                </xsl:when>
                                <xsl:when test="string-length(eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType='created']/eac:eventDateTime/@standardDateTime)=10">
                                    <xsl:attribute name="rdf:datatype">
                                        <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                    </xsl:attribute>
                                </xsl:when>
                                <xsl:otherwise/>
                            </xsl:choose>
                            <xsl:value-of select="eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType='created']/eac:eventDateTime/@standardDateTime"/>
                            
                        </RiC:creationDate>
                        <RiC:lastUpdateDate>
                            <xsl:choose>
                                
                                <xsl:when test="string-length(eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType='revised'][last()]/eac:eventDateTime/@standardDateTime)=4">
                                    <xsl:attribute name="rdf:datatype"> <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text></xsl:attribute>
                                    
                                </xsl:when>
                                <xsl:when test="string-length(eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType='revised'][last()]/eac:eventDateTime/@standardDateTime)=7">
                                    <xsl:attribute name="rdf:datatype">
                                        <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                    </xsl:attribute>
                                </xsl:when>
                                <xsl:when test="string-length(eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType='revised'][last()]/eac:eventDateTime/@standardDateTime)=10">
                                    <xsl:attribute name="rdf:datatype">
                                        <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                    </xsl:attribute>
                                </xsl:when>
                                <xsl:otherwise/>
                            </xsl:choose>
                            <xsl:value-of select="eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType='revised'][last()]/eac:eventDateTime/@standardDateTime"/>
                        </RiC:lastUpdateDate>
                        <RiC:identifier><xsl:value-of select="eac:control/eac:recordId"/></RiC:identifier>
                        <RiC:authoredBy rdf:resource="http://piaaf.demo.logilab.fr/resource/FRAN_corporate-body_005568"/>
                        <xsl:for-each select="eac:control/eac:sources/eac:source">
                            <RiC:evidencedBy xml:lang="fr">
                                <xsl:value-of select="normalize-space(eac:sourceEntry)"/>
                                <xsl:if test="@xlink:href[normalize-space(.)!='']">
                                    <xsl:value-of select="concat(' (', normalize-space(@xlink:href), ')')"/>
                                </xsl:if>
                            </RiC:evidencedBy>
                        </xsl:for-each>
                        <xsl:for-each select="eac:control/descendant::eac:conventionDeclaration">
                            <xsl:variable name="abbr" select="normalize-space(eac:abbreviation)"/>
                            <!--<xsl:variable name="link" select="eac:citation/@xlink:href"/>-->
                            <xsl:if test="$siaf-mandates/rdf:Description[rdfs:label=$abbr and rdf:type[@rdf:resource='http://www.ica.org/standards/RiC/ontology#Mandate']]">
                                <RiC:conformsToRule>
                                    <xsl:attribute name="rdf:resource">
                                        <xsl:value-of select="$siaf-mandates/rdf:Description[rdfs:label=$abbr and rdf:type[@rdf:resource='http://www.ica.org/standards/RiC/ontology#Mandate']]/@rdf:about"/>
                                        
                                    </xsl:attribute>
                                    
                                </RiC:conformsToRule>
                                
                            </xsl:if>
                           
                            
                        </xsl:for-each>
                        <rdfs:seeAlso>
                            <xsl:attribute name="rdf:resource">
                                <!--  <!-\\-  https://aaf.ica-atom.org/actor/browse?subquery=FR78422804100033_000000122-\\->-->
                                <xsl:value-of select="concat('https://aaf.ica-atom.org/actor/browse?subquery=', $recId)"/>
                            </xsl:attribute>
                        </rdfs:seeAlso>
                    </rdf:Description>
                  
                </rdf:RDF>
            </xsl:result-document>
        </xsl:for-each>
        
        
    </xsl:template>
    <xsl:template name="outputObjectPropertyForRelation">
        <xsl:param name="recId"></xsl:param>
        <xsl:param name="link"></xsl:param>
        <xsl:param name="relType"/>
        <xsl:param name="arcToRelName"></xsl:param>
        <xsl:param name="relName"></xsl:param>
        <xsl:param name="relSecondArcName"></xsl:param>
        <xsl:param name="srcRelType"></xsl:param>
        <xsl:param name="shortcutRelName"></xsl:param>
        <xsl:param name="relFromDate"/>
        <xsl:param name="relToDate"/>
        <xsl:param name="relNote"/>
        <xsl:param name="relEntry"/>
        <xsl:param name="toAncRelArcName"/>
    
        <xsl:comment>$link=
            <xsl:value-of select="$link"/></xsl:comment>
        <xsl:element name="RiC:{$arcToRelName}">
            
            <xsl:attribute name="rdf:resource">
            <!-- plusieurs cas -->
            <!-- cas 1 : pas de cible, juste le nom de l'entité -->
            <xsl:choose>
                <xsl:when test="$link='non'">
                    <!-- pas d'URL cible. On cherche une relation qui a pour cible un literal -->
                    <xsl:if test="$siaf-gtypes-relations/rdf:Description[
                        ends-with(rdf:type/@rdf:resource, $relName)
                        and 
                        child::*[local-name()=$toAncRelArcName and ends-with(@rdf:resource, concat('_', $recId))]
                        and
                        child::*[local-name()=$relSecondArcName and .=$relEntry]
                       ]">
                        <xsl:variable name="theTargetRel" select="$siaf-gtypes-relations/rdf:Description[
                            ends-with(rdf:type/@rdf:resource, $relName)
                            and 
                            child::*[local-name()=$toAncRelArcName and ends-with(@rdf:resource, concat('_', $recId))]
                            and
                            child::*[local-name()=$relSecondArcName and .=$relEntry]
                            ]"/>
                        <xsl:choose>
                            <xsl:when test="boolean($relFromDate) and boolean($relToDate)=false()">
                                <xsl:if test="$theTargetRel/RiC:beginningDate=$relFromDate and not($theTargetRel/RiC:endDate)">
                                    <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                </xsl:if>
                            </xsl:when>
                            <xsl:when test="boolean($relFromDate)=false() and boolean($relToDate)">
                                <xsl:if test="not($theTargetRel/RiC:beginningDate) and $theTargetRel/RiC:endDate=$relToDate">
                                    <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                </xsl:if>
                            </xsl:when>
                            <xsl:when test="boolean($relFromDate) and boolean($relToDate)">
                                <xsl:if test="$theTargetRel/RiC:beginningDate=$relFromDate and $theTargetRel/RiC:endDate=$relToDate">
                                    <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                </xsl:if>
                            </xsl:when>
                            <xsl:when test="boolean($relFromDate)=false() and boolean($relToDate)=false()">
                                <xsl:if test="not($theTargetRel/RiC:beginningDate) and not($theTargetRel/RiC:endDate)">
                                    <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                </xsl:if>
                            </xsl:when>
                            <xsl:otherwise>AREVOIR</xsl:otherwise>
                        </xsl:choose>
                    </xsl:if>
                </xsl:when>
                <xsl:when test="$link!='non'">
                    
                    <!-- il y a une URL pour la cible. on prend le cas FRAN et le cas SIAF uniquement -->
                    <xsl:choose>
                        
                        <xsl:when test="starts-with($link, 'FRAN_NP_') and $siaf-gtypes-relations/rdf:Description[
                            ends-with(rdf:type/@rdf:resource, $relName)
                            and
                            child::*[local-name()=$toAncRelArcName and ends-with(@rdf:resource, concat('_', $recId))]
                            and
                            child::*[local-name()=$relSecondArcName and (ends-with(@rdf:resource, concat('_', substring-after($link, 'FRAN_NP_'))) or contains(@rdf:nodeID, substring-after($link, 'FRAN_NP_')))]
                            
                            
                            ]">
                            <xsl:variable name="theTargetRel" select="$siaf-gtypes-relations/rdf:Description[
                                ends-with(rdf:type/@rdf:resource, $relName)
                                and
                                child::*[local-name()=$toAncRelArcName and ends-with(@rdf:resource, concat('_', $recId))]
                                and
                                child::*[local-name()=$relSecondArcName and (ends-with(@rdf:resource, concat('_', substring-after($link, 'FRAN_NP_'))) or contains(@rdf:nodeID, substring-after($link, 'FRAN_NP_')))]
                                
                                
                                ]"></xsl:variable>
                            <xsl:choose>
                                <xsl:when test="boolean($relFromDate) and boolean($relToDate)=false()">
                                    <xsl:if test="$theTargetRel/RiC:beginningDate=$relFromDate and not($theTargetRel/RiC:endDate)">
                                        <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                    </xsl:if>
                                </xsl:when>
                                <xsl:when test="boolean($relFromDate)=false() and boolean($relToDate)">
                                    <xsl:if test="not($theTargetRel/RiC:beginningDate) and $theTargetRel/RiC:endDate=$relToDate">
                                        <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                    </xsl:if>
                                </xsl:when>
                                <xsl:when test="boolean($relFromDate) and boolean($relToDate)">
                                    <xsl:if test="$theTargetRel/RiC:beginningDate=$relFromDate and $theTargetRel/RiC:endDate=$relToDate">
                                        <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                    </xsl:if>
                                </xsl:when>
                                <xsl:when test="boolean($relFromDate)=false() and boolean($relToDate)=false()">
                                    <xsl:if test="not($theTargetRel/RiC:beginningDate) and not($theTargetRel/RiC:endDate)">
                                        <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                    </xsl:if>
                                </xsl:when>
                                <xsl:otherwise>AREVOIR</xsl:otherwise>
                            </xsl:choose>
                            
                            
                        </xsl:when>
                        <xsl:when test="starts-with($link, 'FR784') and $siaf-gtypes-relations/rdf:Description[
                            ends-with(rdf:type/@rdf:resource, $relName)
                            and
                            child::*[local-name()=$toAncRelArcName and ends-with(@rdf:resource, $recId)]
                            and
                            child::*[local-name()=$relSecondArcName and ends-with(@rdf:resource, $link)]
                            
                            
                            ]">
                         
                            <xsl:variable name="theTargetRel" select="$siaf-gtypes-relations/rdf:Description[
                                ends-with(rdf:type/@rdf:resource, $relName)
                                and
                                child::*[local-name()=$toAncRelArcName and ends-with(@rdf:resource, $recId)]
                                and
                                child::*[local-name()=$relSecondArcName and ends-with(@rdf:resource, $link)]
                                
                                
                                ]"></xsl:variable>
                           
                            <xsl:choose>
                                <xsl:when test="boolean($relFromDate) and boolean($relToDate)=false()">
                                    <xsl:if test="$theTargetRel/RiC:beginningDate=$relFromDate and not($theTargetRel/RiC:endDate)">
                                        <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                    </xsl:if>
                                </xsl:when>
                                <xsl:when test="boolean($relFromDate)=false() and boolean($relToDate)">
                                    <xsl:if test="not($theTargetRel/RiC:beginningDate) and $theTargetRel/RiC:endDate=$relToDate">
                                        <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                    </xsl:if>
                                </xsl:when>
                                <xsl:when test="boolean($relFromDate) and boolean($relToDate)">
                                    <xsl:if test="$theTargetRel/RiC:beginningDate=$relFromDate and $theTargetRel/RiC:endDate=$relToDate">
                                        <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                    </xsl:if>
                                </xsl:when>
                                <xsl:when test="boolean($relFromDate)=false() and boolean($relToDate)=false()">
                                    <xsl:if test="not($theTargetRel/RiC:beginningDate) and not($theTargetRel/RiC:endDate)">
                                        <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                    </xsl:if>
                                </xsl:when>
                                <xsl:otherwise>AREVOIR</xsl:otherwise>
                            </xsl:choose>
                            
                            
                        </xsl:when>
                        <xsl:when test="starts-with($link, 'FR784') and $siaf-gtypes-relations/rdf:Description[
                            ends-with(rdf:type/@rdf:resource, $relName)
                            and
                            child::*[local-name()=$toAncRelArcName and ends-with(@rdf:resource, $recId)]
                            and
                            child::*[local-name()=$relSecondArcName and contains(@rdf:nodeID, $link)]
                            
                            
                            ]">
                            
                            <xsl:variable name="theTargetRel" select="$siaf-gtypes-relations/rdf:Description[
                                ends-with(rdf:type/@rdf:resource, $relName)
                                and
                                child::*[local-name()=$toAncRelArcName and ends-with(@rdf:resource, $recId)]
                                and
                                child::*[local-name()=$relSecondArcName and contains(@rdf:nodeID, $link)]
                                
                                
                                ]"></xsl:variable>
                            
                            <xsl:choose>
                                <xsl:when test="boolean($relFromDate) and boolean($relToDate)=false()">
                                    <xsl:if test="$theTargetRel/RiC:beginningDate=$relFromDate and not($theTargetRel/RiC:endDate)">
                                        <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                    </xsl:if>
                                </xsl:when>
                                <xsl:when test="boolean($relFromDate)=false() and boolean($relToDate)">
                                    <xsl:if test="not($theTargetRel/RiC:beginningDate) and $theTargetRel/RiC:endDate=$relToDate">
                                        <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                    </xsl:if>
                                </xsl:when>
                                <xsl:when test="boolean($relFromDate) and boolean($relToDate)">
                                    <xsl:if test="$theTargetRel/RiC:beginningDate=$relFromDate and $theTargetRel/RiC:endDate=$relToDate">
                                        <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                    </xsl:if>
                                </xsl:when>
                                <xsl:when test="boolean($relFromDate)=false() and boolean($relToDate)=false()">
                                    <xsl:if test="not($theTargetRel/RiC:beginningDate) and not($theTargetRel/RiC:endDate)">
                                        <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                    </xsl:if>
                                </xsl:when>
                                <xsl:otherwise>AREVOIR</xsl:otherwise>
                            </xsl:choose>
                            
                            
                        </xsl:when>
                        <xsl:when test="starts-with($link, 'FRAD') and $siaf-gtypes-relations/rdf:Description[
                            ends-with(rdf:type/@rdf:resource, $relName)
                            and
                            child::*[local-name()=$toAncRelArcName and ends-with(@rdf:resource, concat('_', $recId))]
                            and
                            child::*[local-name()=$relSecondArcName and (ends-with(@rdf:resource, $link) or contains(@rdf:nodeID, $link))]
                            
                            
                            ]">
                            <xsl:variable name="theTargetRel" select="$siaf-gtypes-relations/rdf:Description[
                                ends-with(rdf:type/@rdf:resource, $relName)
                                and
                                child::*[local-name()=$toAncRelArcName and ends-with(@rdf:resource, concat('_', $recId))]
                                and
                                child::*[local-name()=$relSecondArcName and (ends-with(@rdf:resource, $link) or contains(@rdf:nodeID, $link))]
                                
                                
                                ]"></xsl:variable>
                            <xsl:choose>
                                <xsl:when test="boolean($relFromDate) and boolean($relToDate)=false()">
                                    <xsl:if test="$theTargetRel/RiC:beginningDate=$relFromDate and not($theTargetRel/RiC:endDate)">
                                        <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                    </xsl:if>
                                </xsl:when>
                                <xsl:when test="boolean($relFromDate)=false() and boolean($relToDate)">
                                    <xsl:if test="not($theTargetRel/RiC:beginningDate) and $theTargetRel/RiC:endDate=$relToDate">
                                        <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                    </xsl:if>
                                </xsl:when>
                                <xsl:when test="boolean($relFromDate) and boolean($relToDate)">
                                    <xsl:if test="$theTargetRel/RiC:beginningDate=$relFromDate and $theTargetRel/RiC:endDate=$relToDate">
                                        <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                    </xsl:if>
                                </xsl:when>
                                <xsl:when test="boolean($relFromDate)=false() and boolean($relToDate)=false()">
                                    <xsl:if test="not($theTargetRel/RiC:beginningDate) and not($theTargetRel/RiC:endDate)">
                                        <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                    </xsl:if>
                                </xsl:when>
                                <xsl:otherwise>AREVOIR</xsl:otherwise>
                            </xsl:choose>
                            
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text>problème :</xsl:text>
                            <xsl:text> $relName=</xsl:text>
                            <xsl:value-of select="$relName"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
            </xsl:choose>
            </xsl:attribute>
            
            
        </xsl:element>
        
        
        
    </xsl:template>
    
</xsl:stylesheet>