<?xml version="1.0" encoding="utf-8"?>
<eac-cpf xmlns="urn:isbn:1-931666-33-4"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:piaaf="http://www.piaaf.net"
         xsi:schemaLocation="urn:isbn:1-931666-33-4 ../schema/cpf-modified.xsd">
   <control>
      <recordId>FRAN_NP_050095</recordId>
      <maintenanceStatus>revised</maintenanceStatus>
      <maintenanceAgency>
         <agencyName>Archives nationales de France</agencyName>
      </maintenanceAgency>
      <languageDeclaration>
         <language languageCode="fre">Français</language>
         <script scriptCode="Latn"/>
      </languageDeclaration>
      <conventionDeclaration>
         <citation> Notice établie conformément à la norme ICA-ISAAR (CPF) 2004, aux normes ISO 8601 pour le système de normalisation des dates utilisé, ainsi que AFNOR NF Z44-061 juin 1986 Catalogage : forme et structure des vedettes noms de personne, des vedettes titres, des rubriques de classement et des titres forgés et NF Z 44-060, décembre 1996 Catalogue d'auteurs et d'anonymes – Forme et structure des vedettes de collectivités-auteurs. </citation>
      </conventionDeclaration>
      <localControl localType="niveau_de_detail">
         <term>Moyenne</term>
      </localControl>
      <maintenanceHistory>
         <maintenanceEvent>
            <eventType>created</eventType>
            <eventDateTime>2013-08-13</eventDateTime>
            <agentType>human</agentType>
            <agent>Raphael BAUMARD</agent>
         </maintenanceEvent>
         <maintenanceEvent>
            <eventType>revised</eventType>
            <eventDateTime>2015-12-03</eventDateTime>
            <agentType>human</agentType>
            <agent>Florence CLAVAUD</agent>
            <eventDescription>F. Clavaud, 3 décembre 2015 : revu le nom et le lien IR, ajouté ISNI, n° notice BnF, indexation activités</eventDescription>
         </maintenanceEvent>
         <maintenanceEvent>
            <eventType>revised</eventType>
            <eventDateTime>2015-12-09</eventDateTime>
            <agentType>human</agentType>
            <agent>Florence CLAVAUD</agent>
         </maintenanceEvent>
         <maintenanceEvent>
            <eventType>revised</eventType>
            <eventDateTime>2017-03-23</eventDateTime>
            <agentType>human</agentType>
            <agent>Mission.referentiels MISSION.REFERENTIELS</agent>
         </maintenanceEvent>
         <maintenanceEvent>
            <eventType>updated</eventType>
            <eventDateTime>2017-03-23</eventDateTime>
            <agentType>human</agentType>
            <agent>Mission.referentiels MISSION.REFERENTIELS</agent>
            <eventDescription>F. Clavaud, 23 mars 217 : revu l'historique, ajouté lien d'association et liens d'identité</eventDescription>
         </maintenanceEvent>
         <maintenanceEvent>
            <eventType>revised</eventType>
            <eventDateTime standardDateTime="2017-05">mai 2017</eventDateTime>
            <agentType>machine</agentType>
            <agent>Programme XSLT écrit par Florence Clavaud (Archives nationales)</agent>
            <eventDescription>Modifications et enrichissements en vue d'obtenir des données utilisables pour le projet PIAAF : 
                    -- déclaration de l'espace de noms PIAAF et association avec schéma EAC-CPF modifié ; 
                    -- réécriture des relations entre entités (sous-catégorisations via @arcrole et renvois à ontologie, ajout des relations entre types génériques et instances) ;
                    -- intégration des descripteurs choisis dans les vocabulaires Ginco du projet pour indexer les fonctions des entités;
                    -- traitement des statuts juridiques et des textes de référence ; 
                    -- suppression des relations de type subjectOf dans les notices AN (relations parfois inexactes, et de toute façon toutes sortantes et beaucoup trop nombreuses)
                    -- autres petites modifications visant à homgénéiser les notices</eventDescription>
         </maintenanceEvent>
      </maintenanceHistory>
      <sources>
         <source>
            <sourceEntry>Notice d'autorité BnF n° FRBNF12245110</sourceEntry>
         </source>
      </sources>
   </control>
   <cpfDescription>
      <identity>
         <entityId>ISNI 0000 0003 5707 0766</entityId>
         <entityType>person</entityType>
         <nameEntry localType="preferredFormForProject"
                    scriptCode="Latn"
                    xml:lang="fre">
            <part>Saulcy, Félicien de (1807-1880)</part>
         </nameEntry>
         <nameEntry>
            <part>Saulcy, Félicien Caignart de (1807-1880)</part>
         </nameEntry>
         <nameEntry>
            <part>Saulcy, Louis Félicien Joseph Caignart de (1807-1880)</part>
         </nameEntry>
      </identity>
      <description>
         <existDates>
            <dateRange>
               <fromDate standardDate="1807-03-19">19 mars 1807</fromDate>
               <toDate standardDate="1880-11-04">4 novembre 1880</toDate>
            </dateRange>
         </existDates>
         <occupations>
            <occupation>
               <term vocabularySource="d699msk5oth-xqvujelh8c39">chercheur</term>
               <descriptiveNote>
                  <p>numismate et archéologue</p>
               </descriptiveNote>
            </occupation>
            <occupation>
               <term vocabularySource="d699msj9l7t-1gb5a09kkzrmp">sénateur</term>
            </occupation>
         </occupations>
         <biogHist>
            <p>Louis-Félicien-Joseph Caignart de Saulcy (1807-1880) est admis en 1826 à l’École polytechnique ; il en sort deux ans plus tard pour entrer à l’École d’application de Metz. C’est là qu’il commence à se passionner pour les médailles et, devenu lieutenant d’artillerie, il réussit à mener de front ses obligations militaires et ses recherches d’archéologue et de numismate. Professeur de mécanique à l’École de Metz, il publie des études sur les monnaies de la ville et de ses évêques et un 
     <span style="italic">Essai de classification des suites monétaires byzantines</span>, résultat de ses travaux de classement d’une précieuse collection de médailles byzantines. En 1841, il devient conservateur du musée d’artillerie à Paris, et, l’année suivante, l’Académie des inscriptions lui ouvre ses portes. Il se spécialise alors dans l’épigraphie orientale et entreprend en 1845 un grand voyage archéologique avec La Saussaye à travers l’Italie, la Grèce, la Turquie et l’Égypte. Cinq ans plus tard, accompagné de son fils et d’Édouard Delessert, il se rend en Palestine. On doit à Saulcy de nombreux travaux de numismatique, des récits de voyages, et l’animation, comme président, de la commission instituée pour publier la carte de l’ancienne Gaule, commission qui donna une impulsion très vive aux recherches d’archéologie gauloise et latine en France. 
    </p>
            <p>Veuf de mademoiselle de Brye en 1850, Saulcy épouse en secondes noces le 20 décembre 1852, Mademoiselle de Billing, fille du ministre de France à Copenhague, bientôt dame d’honneur de l’impératrice Eugénie. Fréquentant ainsi la cour et apprécié par Napoléon III, il est nommé en novembre 1859 sénateur. S’intéressant peu à la politique, il consacre tous ses loisirs à ses travaux d’érudition. À deux reprises, en 1863 et en 1869, il se rend en Terre Sainte, et lorsqu’il s’éteint en 1880, il est en train de rédiger une description de sa collection de monnaies françaises de Philippe II à François Ier.</p>
         </biogHist>
      </description>
      <relations>
         <cpfRelation xlink:href="FRAN_NP_051164"
                      xlink:type="simple"
                      cpfRelationType="associative"
                      xlink:arcrole="http://www.piaaf.net/ontology/piaaf#isMemberOf">
            <relationEntry>France. Commission des monuments historiques (1837-....)</relationEntry>
            <dateRange>
               <fromDate standardDate="1861-01-01">1861</fromDate>
               <toDate standardDate="1869-12-31">1869</toDate>
            </dateRange>
         </cpfRelation>
         <cpfRelation cpfRelationType="identity"
                      xlink:href="http://catalogue.bnf.fr/ark:/12148/cb12245110d"
                      xlink:type="simple">
            <relationEntry>Saulcy, Félicien de (1807-1880)</relationEntry>
            <dateRange>
               <fromDate standardDate="1807-03-19">19 mars 1807</fromDate>
               <toDate standardDate="1880-11-04">4 novembre 1880</toDate>
            </dateRange>
            <descriptiveNote>
               <p>notice d'autorité BnF</p>
            </descriptiveNote>
         </cpfRelation>
         <!--Attention : la cible du lien n'est pas une notice du corpus-->
         <cpfRelation cpfRelationType="identity"
                      xlink:href="https://fr.wikipedia.org/wiki/F%C3%A9licien_de_Saulcy"
                      xlink:type="simple">
            <relationEntry>Félicien de Saulcy</relationEntry>
            <dateRange>
               <fromDate standardDate="1807-03-19">19 mars 1807</fromDate>
               <toDate standardDate="1880-11-04">4 novembre 1880</toDate>
            </dateRange>
            <descriptiveNote>
               <p>Notice Wikipédia</p>
            </descriptiveNote>
         </cpfRelation>
         <!--Attention : la cible du lien n'est pas une notice du corpus-->
         <!--La cible du lien est une notice Wikipédia-->
         <cpfRelation xlink:href="FRAN_NP_051177"
                      xlink:type="simple"
                      cpfRelationType="associative"
                      xlink:arcrole="http://www.piaaf.net/ontology/piaaf#knows"><!--Attention : la cible du lien n'est pas une notice du corpus--><!--La cible du lien est une notice des AN, qui ne fait pas partie du corpus mais est publiée en salle des inventaires virtuelle-->
            <relationEntry>Napoléon III (empereur des Français ; 1808-1873)</relationEntry>
            <dateRange>
               <fromDate standardDate="1852-01-01">1852</fromDate>
               <toDate standardDate="1870-12-31">1870</toDate>
            </dateRange>
         </cpfRelation>
         <resourceRelation resourceRelationType="creatorOf"
                           xlink:href="FRAN_IR_003467"
                           xlink:type="simple">
            <relationEntry localType="archival">Fonds Saulcy (1773-1933)</relationEntry>
         </resourceRelation>
      </relations>
   </cpfDescription>
</eac-cpf>
