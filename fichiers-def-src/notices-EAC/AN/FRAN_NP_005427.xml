<?xml version="1.0" encoding="utf-8"?>
<eac-cpf xmlns="urn:isbn:1-931666-33-4"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:piaaf="http://www.piaaf.net"
         xsi:schemaLocation="urn:isbn:1-931666-33-4 ../schema/cpf-modified.xsd">
   <control>
      <recordId>FRAN_NP_005427</recordId>
      <otherRecordId localType="Ancien_identifiant">5190</otherRecordId>
      <maintenanceStatus>revised</maintenanceStatus>
      <maintenanceAgency>
         <agencyName>Archives nationales de France</agencyName>
      </maintenanceAgency>
      <languageDeclaration>
         <language languageCode="fre">Français</language>
         <script scriptCode="Latn"/>
      </languageDeclaration>
      <conventionDeclaration>
         <citation>Notice établie conformément à la norme ICA-ISAAR (CPF) 2004, aux normes ISO 8601 pour le système de normalisation des dates utilisé, ainsi que AFNOR NF Z 44-061, juin 1986. Catalogage : forme et structure des vedettes noms de personne, des vedettes titres, des rubriques de classement et des titres forgés.</citation>
      </conventionDeclaration>
      <localControl localType="niveau_de_detail">
         <term>Complète</term>
      </localControl>
      <maintenanceHistory>
         <maintenanceEvent>
            <eventType>derived</eventType>
            <eventDateTime>2015-12-04</eventDateTime>
            <agentType>machine</agentType>
            <agent>Import_SIA</agent>
         </maintenanceEvent>
         <maintenanceEvent>
            <eventType>revised</eventType>
            <eventDateTime>2016-08-18</eventDateTime>
            <agentType>human</agentType>
            <agent>Marine ZELVERTE</agent>
         </maintenanceEvent>
         <maintenanceEvent>
            <eventType>updated</eventType>
            <eventDateTime>2017-04-12</eventDateTime>
            <agentType>human</agentType>
            <agent>Mission.referentiels MISSION.REFERENTIELS</agent>
            <eventDescription>F. Clavaud, 12 avril 2017 : ajouté liens d'identité. NOTA : les liens IR sont à revoir et probablement à redistribuer vers d'autres notices</eventDescription>
         </maintenanceEvent>
         <maintenanceEvent>
            <eventType>revised</eventType>
            <eventDateTime standardDateTime="2017-05">mai 2017</eventDateTime>
            <agentType>machine</agentType>
            <agent>Programme XSLT écrit par Florence Clavaud (Archives nationales)</agent>
            <eventDescription>Modifications et enrichissements en vue d'obtenir des données utilisables pour le projet PIAAF : 
                    -- déclaration de l'espace de noms PIAAF et association avec schéma EAC-CPF modifié ; 
                    -- réécriture des relations entre entités (sous-catégorisations via @arcrole et renvois à ontologie, ajout des relations entre types génériques et instances) ;
                    -- intégration des descripteurs choisis dans les vocabulaires Ginco du projet pour indexer les fonctions des entités;
                    -- traitement des statuts juridiques et des textes de référence ; 
                    -- suppression des relations de type subjectOf dans les notices AN (relations parfois inexactes, et de toute façon toutes sortantes et beaucoup trop nombreuses)
                    -- autres petites modifications visant à homgénéiser les notices</eventDescription>
         </maintenanceEvent>
      </maintenanceHistory>
      <sources>
         <source>
            <sourceEntry>Journal Officiel de la République française</sourceEntry>
         </source>
         <source>
            <sourceEntry>Site officiel de l'OPPIC (http://www.oppic.fr/pages/presentation.php)</sourceEntry>
         </source>
         <source>
            <sourceEntry>Notice Wikipédia (https://fr.wikipedia.org/wiki/Op%C3%A9rateur_du_patrimoine_et_des_projets_immobiliers_de_la_culture)</sourceEntry>
         </source>
         <source>
            <sourceEntry>Notice BnF n° FRBNF16530757</sourceEntry>
         </source>
      </sources>
   </control>
   <cpfDescription>
      <identity>
         <entityType>corporateBody</entityType>
         <nameEntry localType="preferredFormForProject"
                    scriptCode="Latn"
                    xml:lang="fre">
            <part>Opérateur du patrimoine et des projets immobiliers de la culture (France)</part>
         </nameEntry>
         <nameEntry>
            <part>OPPIC</part>
         </nameEntry>
      </identity>
      <description>
         <existDates>
            <dateRange>
               <fromDate standardDate="2010-07-21">21 juillet 2010</fromDate>
            </dateRange>
         </existDates>
         <legalStatuses>
            <legalStatus>
               <term vocabularySource="d5blonb3ky--u69ytier8ih6">établissement public à caractère administratif</term>
            </legalStatus>
         </legalStatuses>
         <mandates>
            <mandate>
               <citation>Décret n° 2010-818 du 14 juillet 2010 relatif à l'Opérateur du patrimoine et des projets immobiliers de la culture (https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000022497544&amp;fastPos=4&amp;fastReqId=1037663746&amp;categorieLien=id&amp;oldAction=rechTexte)</citation>
            </mandate>
         </mandates>
         <function localType="piaafFunction">
            <term vocabularySource="http://data.culture.fr/thesaurus/resource/ark:/67717/51232822-adac-4a33-aa14-29e2c701a5ee"
                  piaaf:rank="1"
                  piaaf:conceptId="http://data.culture.fr/thesaurus/resource/ark:/67717/8597d97e-4ea7-42b8-b635-ff1a9a3cc6d4">construction</term>
         </function>
         <biogHist>
            <p>
               <span style="underline">Historique :</span>
            </p>
            <p>En application d'une décision du Conseil de modernisation des politiques publiques de juin 2008, le Service national des travaux (SNT) et l'Établissement public de maîtrise d'ouvrage des travaux culturels (EMOC), chargés de missions voisines, fusionnent dans un établissement public à caractère administratif, l'Opérateur du patrimoine et des projets immobiliers de la Culture (OPPIC), créé par décret n° 2010-818 du 14 juillet 2010 (publié au Journal Officiel le 20 juillet). Il est placé sous la tutelle du ministre de la Culture. </p>
            <p/>
            <p>
               <span style="underline">Missions :</span>
            </p>
            <p>Les misssions de l'OPPIC sont fixées par l'article 2 du décret du 14 juillet 2010 :</p>
            <p>"I. L'établissement a pour mission, pour le compte de l'État ou des établissements publics nationaux :</p>
            <list>
               <item>1° De réaliser toute étude et analyse préalable relatives :</item>
            </list>
            <p>a) Aux investissements immobiliers du ministère chargé de la culture ou des établissements publics placés sous sa tutelle ;</p>
            <p>b) À l'entretien et à la mise en valeur du patrimoine immobilier mis à disposition de ce ministère ou de ces établissements publics, qu'il appartienne à l'État ou que l'État détienne sur lui un droit réel ;</p>
            <list>
               <item>2° D'assurer la réalisation d'opérations de construction, de restauration, de réhabilitation, d'aménagement ou de maintenance de ces immeubles ;</item>
               <item>3° De mener à bien toute mission d'assistance et de conseil dans le domaine de la gestion et de la mise en valeur de ces immeubles ;</item>
               <item>4° De participer à l'organisation de cérémonies nationales et au transfert au Panthéon de cendres illustres ;</item>
               <item>5° À titre accessoire, d'accomplir pour d'autres ministères ou pour les établissements publics placés sous leur tutelle les missions prévues aux 1°, 2° et 3°.</item>
            </list>
            <p>II. À titre accessoire et onéreux, l'établissement peut en outre :</p>
            <list>
               <item>1° Accomplir les missions mentionnées aux 1°, 2° et 3° du I pour le compte des collectivités territoriales, de leurs établissements publics ou d'autres personnes publiques ;</item>
               <item>2° Exercer à l'étranger des missions dans les domaines relevant de son champ de compétence. </item>
            </list>
         </biogHist>
      </description>
      <relations>
         <cpfRelation xlink:href="FRAN_NP_000005"
                      xlink:type="simple"
                      cpfRelationType="hierarchical-parent"
                      xlink:arcrole="http://www.piaaf.net/ontology/piaaf#isControlledBy">
            <relationEntry>France. Ministère de la Culture et de la Communication (1959-....)</relationEntry>
            <dateRange>
               <fromDate standardDate="2010-07-21">21 juillet 2010</fromDate>
            </dateRange>
         </cpfRelation>
         <cpfRelation xlink:href="FRAN_NP_005661"
                      xlink:type="simple"
                      cpfRelationType="associative"><!--Attention : la cible du lien n'est pas une notice du corpus--><!--La cible du lien est une notice des AN, qui ne fait pas partie du corpus mais est publiée en salle des inventaires virtuelle-->
            <relationEntry>Secrétariat général (ministère de la Culture et de la Communication)</relationEntry>
            <dateRange>
               <fromDate standardDate="2010-07-21">21 juillet 2010</fromDate>
            </dateRange>
         </cpfRelation>
         <cpfRelation xlink:href="FRAN_NP_051355"
                      xlink:type="simple"
                      cpfRelationType="associative"
                      xlink:arcrole="http://www.piaaf.net/ontology/piaaf#isFunctionallyLinkedTo">
            <relationEntry>Bibliothèque nationale de France (1994-....)</relationEntry>
            <dateRange>
               <fromDate standardDate="2010-07-21">21 juillet 2010</fromDate>
            </dateRange>
         </cpfRelation>
         <cpfRelation cpfRelationType="identity"
                      xlink:href="http://catalogue.bnf.fr/ark:/12148/cb16530757c"
                      xlink:type="simple">
            <relationEntry>Opérateur du patrimoine et des projets immobiliers de la culture (France)</relationEntry>
            <dateRange>
               <fromDate standardDate="2010-07-21">21 juillet 2010</fromDate>
            </dateRange>
            <descriptiveNote>
               <p>notice d'autorité BnF</p>
            </descriptiveNote>
         </cpfRelation>
         <!--Attention : la cible du lien n'est pas une notice du corpus-->
         <cpfRelation cpfRelationType="identity"
                      xlink:href="https://fr.wikipedia.org/wiki/Op%C3%A9rateur_du_patrimoine_et_des_projets_immobiliers_de_la_culture"
                      xlink:type="simple">
            <relationEntry>Opérateur du patrimoine et des projets immobiliers de la culture</relationEntry>
            <dateRange>
               <fromDate standardDate="2010-07-21">21 juillet 2010</fromDate>
            </dateRange>
            <descriptiveNote>
               <p>Notice Wikipédia</p>
            </descriptiveNote>
         </cpfRelation>
         <!--Attention : la cible du lien n'est pas une notice du corpus-->
         <!--La cible du lien est une notice Wikipédia-->
         <cpfRelation cpfRelationType="temporal-earlier"
                      xlink:href="FRAN_NP_051119"
                      xlink:type="simple">
            <relationEntry>Établissement public de maîtrise d'ouvrage des travaux culturels (France)</relationEntry>
            <dateRange>
               <fromDate standardDate="2010-07-21">21 juillet 2010</fromDate>
            </dateRange>
         </cpfRelation>
         <cpfRelation cpfRelationType="temporal-earlier"
                      xlink:href="FRAN_NP_005055"
                      xlink:type="simple">
            <relationEntry>France. Ministère de la Culture et de la Communication. Service national des travaux (1990-2010)</relationEntry>
            <dateRange>
               <fromDate standardDate="2010-07-21">21 juillet 2010</fromDate>
            </dateRange>
         </cpfRelation>
         <resourceRelation resourceRelationType="creatorOf"
                           xlink:href="FRAN_IR_008834"
                           xlink:type="simple">
            <relationEntry localType="archival">Culture ; Etablissement public de maîtrise d'ouvrage des travaux culturels (1982-1991)</relationEntry>
         </resourceRelation>
         <resourceRelation resourceRelationType="creatorOf"
                           xlink:href="FRAN_IR_054637"
                           xlink:type="simple">
            <relationEntry localType="archival">OPERATEUR DU PATRIMOINE ET DES PROJETS IMMOBILIERS DE LA CULTURE Mission interministérielle de coordination des grandes opérations d'architecture et d'urbanisme puis Établissement public de maîtrise d'ouvrage des travaux culturels</relationEntry>
         </resourceRelation>
         <resourceRelation resourceRelationType="creatorOf"
                           xlink:href="FRAN_IR_053707"
                           xlink:type="simple">
            <relationEntry localType="archival">OPERATEUR DU PATRIMOINE ET DES PROJETS IMMOBILIERS DE LA CULTURE Mission interministérielle de coordination des grandes opérations d'architecture et d'urbanisme puis Établissement public de maîtrise d'ouvrage des travaux culturels</relationEntry>
         </resourceRelation>
         <resourceRelation resourceRelationType="creatorOf"
                           xlink:href="FRAN_IR_055671"
                           xlink:type="simple">
            <relationEntry localType="archival">OPÉRATEUR DU PATRIMOINE ET DES PROJETS IMMOBILIERS DE LA CULTURE Mission interministérielle de coordination des grandes opérations d'architecture et d'urbanisme  (MiGT)</relationEntry>
         </resourceRelation>
         <resourceRelation resourceRelationType="creatorOf"
                           xlink:href="FRAN_IR_055504"
                           xlink:type="simple">
            <relationEntry localType="archival">OPERATEUR DU PATRIMOINE et DES PROJETS IMMOBILIERS de la culture</relationEntry>
         </resourceRelation>
      </relations>
   </cpfDescription>
</eac-cpf>
