<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:eac="urn:isbn:1-931666-33-4"
    xmlns:piaaf="http://www.piaaf.net"
    exclude-result-prefixes="xs xd eac xlink"
    version="2.0">
    <xsl:strip-space elements="*"/>
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> February 14, 2016</xd:p>
            <xd:p><xd:b>Author:</xd:b> Florence</xd:p>
            <xd:p></xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:output encoding="utf-8" method="text" omit-xml-declaration="yes" indent="no"/>
    <!-- des variables utiles, fin de ligne et tab -->
    <xsl:variable name="lf" select="'&#10;'"/>
    <xsl:variable name="tab" select="'&#9;'"/>
    <xsl:strip-space elements="*"/>
   <!-- <xsl:variable name="chemin-AN-MH">
        <xsl:value-of
            select="concat('AN/MH/', '?select=*.xml;recurse=yes;on-error=warning')"
        />
    </xsl:variable>
    <xsl:variable name="collection-AN-MH" select="collection($chemin-AN-MH)"/>
    <xsl:variable name="chemin-AN-Bib">
        <xsl:value-of
            select="concat('AN/Bib/', '?select=*.xml;recurse=yes;on-error=warning')"
        />
    </xsl:variable>
    <xsl:variable name="collection-AN-Bib" select="collection($chemin-AN-Bib)"/>-->
    <xsl:variable name="chemin-BnF">
        <xsl:value-of
            select="concat('BnF/', '?select=*.xml;recurse=yes;on-error=warning')"
        />
    </xsl:variable>
    <xsl:variable name="collection-BnF" select="collection($chemin-BnF)"/>
    <xsl:variable name="chemin-SIAF">
        <xsl:value-of
            select="concat('SIAF/', '?select=*.xml;recurse=yes;on-error=warning')"
        />
    </xsl:variable>
    <xsl:variable name="collection-SIAF" select="collection($chemin-SIAF)"/>
    <xsl:variable name="chemin-AN">
        <xsl:value-of
            select="concat('AN/', '?select=*.xml;recurse=yes;on-error=warning')"
        />
    </xsl:variable>
    <xsl:variable name="collection-AN" select="collection($chemin-AN)"/>
    <xsl:template match="/">
        <xsl:value-of select="concat('provenance', $tab)"/>
    <xsl:value-of select="concat('identifiantNotice', $tab)"/>
   
    <xsl:value-of select="concat('typeEntite', $tab)"/>
    <xsl:value-of select="concat('formeAutoriseeNomEntite', $tab)"/>
 
    <xsl:value-of select="concat('dateDeDebut', $tab)"/>
        <xsl:value-of select="concat('dateDeFin', $tab)"/>
   
        <xsl:value-of select="concat('statutJuridique', $tab)"/>
  
        <xsl:value-of select="concat('ISNI', $tab)"/>
    
   
   <xsl:for-each select="$collection-BnF | $collection-SIAF | $collection-AN">
       <xsl:value-of select="$lf"/>
       <!-- provenance -->
       <xsl:choose>
           <xsl:when test="starts-with(/eac:eac-cpf/eac:control/eac:recordId, 'FRAN')"><xsl:text>ANF</xsl:text>
           </xsl:when>
           <xsl:when test="starts-with(/eac:eac-cpf/eac:control/eac:recordId, 'FRBNF')">
               <xsl:text>BnF</xsl:text>
           </xsl:when>
           <xsl:otherwise><xsl:text>SIAF</xsl:text></xsl:otherwise>
       </xsl:choose>
       <xsl:value-of select="$tab"/>
       <xsl:value-of select="/eac:eac-cpf/eac:control/eac:recordId"/>
       <xsl:value-of select="$tab"/>
       <xsl:variable name="type" select="/eac:eac-cpf/eac:cpfDescription/eac:identity/eac:entityType"/>
       <xsl:choose>
           <xsl:when test="$type='person'">personne</xsl:when>
           <xsl:when test="$type='family'">famille</xsl:when>
           <xsl:when test="$type='corporateBody'">collectivité</xsl:when>
       </xsl:choose>
       <xsl:value-of select="$tab"/>
      
       <xsl:for-each select="/eac:eac-cpf/eac:cpfDescription/eac:identity/eac:nameEntry[@localType and (starts-with(@localType,'Aut') or starts-with(@localType, 'aut') or starts-with(@localType, 'pref'))]/eac:part">
           <xsl:value-of select="normalize-space(.)"/>
           <xsl:if test="position()!=last()">
               <xsl:text> </xsl:text>
           </xsl:if>
             
           
       </xsl:for-each>
     
       <xsl:value-of select="$tab"/>
       <xsl:value-of select="substring(/eac:eac-cpf/eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate/@standardDate, 1, 4)"/>
       <xsl:value-of select="$tab"/>
       <xsl:if test="/eac:eac-cpf/eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate">
           <xsl:value-of select="substring(/eac:eac-cpf/eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate/@standardDate, 1, 4)"/>
           
       </xsl:if>
    
       <xsl:value-of select="$tab"/>
       <xsl:for-each select="/eac:eac-cpf/eac:cpfDescription/eac:description/descendant::eac:legalStatus">
           <xsl:value-of select="normalize-space(eac:term)"/>
           <xsl:if test="position()!=last()"> | </xsl:if>
       </xsl:for-each>
      
       <xsl:value-of select="$tab"/>
    
       <xsl:choose>
           <xsl:when test="/eac:eac-cpf/eac:control/eac:sources/eac:source/eac:descriptiveNote/eac:p[contains(., 'ISNI :')]">
               <xsl:value-of select="normalize-space(substring-after(/eac:eac-cpf/eac:control/eac:sources/eac:source/eac:descriptiveNote/eac:p[contains(., 'ISNI :')], 'ISNI :'))"/>
           </xsl:when>
           <xsl:when test="/eac:eac-cpf/eac:cpfDescription/eac:identity/eac:entityId[contains(., 'ISNI')]">
               <xsl:for-each select="/eac:eac-cpf/eac:cpfDescription/eac:identity/eac:entityId[contains(., 'ISNI')]"><xsl:value-of select="normalize-space(.)"/><xsl:if test="position()!=last()"><xsl:text> ; </xsl:text></xsl:if></xsl:for-each>
               <!--<xsl:value-of select="normalize-space(/eac:eac-cpf/eac:cpfDescription/eac:identity/eac:entityId)"/>-->
           </xsl:when>
           <xsl:when test="/eac:eac-cpf/eac:cpfDescription/eac:identity/eac:entityId[@localType='ISNI']">
               <xsl:value-of select="normalize-space(/eac:eac-cpf/eac:cpfDescription/eac:identity/eac:entityId[@localType='ISNI'])"/>
           </xsl:when>
       </xsl:choose>
    
   </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>