<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:owl="http://www.w3.org/2002/07/owl#" xmlns:RiC="http://www.ica.org/standards/RiC/ontology#"
    xmlns:isni="http://isni.org/ontology#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
    xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:eac="urn:isbn:1-931666-33-4"
    xmlns="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:piaaf="http://www.piaaf.net"
    xmlns:piaaf-onto="http://piaaf.demo.logilab.fr/ontology#"
    xmlns:iso-thes="http://purl.org/iso25964/skos-thes#"
    xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:foaf="http://xmlns.com/foaf/0.1/"
    xmlns:dct="http://purl.org/dc/terms/" xmlns:xl="http://www.w3.org/2008/05/skos-xl#"
    xmlns:ginco="http://data.culture.fr/thesaurus/ginco/ns/"
    exclude-result-prefixes="xs xd eac iso-thes rdf dct xl piaaf xlink piaaf-onto skos foaf ginco dc" version="2.0">
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> July 4, 2017, checked and updated Dec. 12, 2017</xd:p>
            <xd:p><xd:b>Author:</xd:b> Florence Clavaud (Archives nationales)</xd:p>
            <xd:p>Sémantisation : étape 10, génération de fichiers RDF pour les agents </xd:p>
            <xd:p>Le script reprend les instructions de celui consacré aux catégories de
                collectivités, qui sont complétées et auxquelles d'autres sont ajoutées</xd:p>
        </xd:desc>
    </xd:doc>

    <xsl:param name="coll">FRAN</xsl:param>
    <xsl:variable name="chemin-EAC-BnF">
        <xsl:value-of
            select="concat('fichiers-def-2/notices-EAC/BnF/', '?select=*.xml;recurse=yes;on-error=warning')"
        />
    </xsl:variable>
    <xsl:variable name="collection-EAC-BnF" select="collection($chemin-EAC-BnF)"/>

    <xsl:variable name="chemin-EAC-AN">
        <xsl:value-of
            select="concat('fichiers-def-2/notices-EAC/AN/', '?select=*.xml;recurse=yes;on-error=warning')"
        />
    </xsl:variable>
    <xsl:variable name="collection-EAC-AN" select="collection($chemin-EAC-AN)"/>


    <xsl:variable name="chemin-EAC-SIAF">
        <xsl:value-of
            select="concat('fichiers-def-2/notices-EAC/SIAF/', '?select=*.xml;recurse=yes;on-error=warning')"
        />
    </xsl:variable>
    <xsl:variable name="collection-EAC-SIAF" select="collection($chemin-EAC-SIAF)"/>
    <xsl:variable name="siaf-mandates" select="document('rdf/mandates/FRSIAF_mandates.rdf')/rdf:RDF"/>
    <xsl:variable name="bnf-mandates" select="document('rdf/mandates/FRBNF_mandates.rdf')/rdf:RDF"/>
    <xsl:variable name="an-mandates" select="document('rdf/mandates/FRAN_mandates.rdf')/rdf:RDF"/>
    <xsl:variable name="siaf-events" select="document('rdf/events/FRSIAF_events.rdf')/rdf:RDF"/>

    <xsl:variable name="functions" select="document('rdf/functions.rdf')/rdf:RDF"/>
    <xsl:variable name="legalsts" select="document('rdf/legal-statuses.rdf')/rdf:RDF"/>
    <xsl:variable name="siaf-names" select="document('rdf/agent-names/FRSIAF_agent-names.rdf')/rdf:RDF"/>
    <xsl:variable name="bnf-names" select="document('rdf/agent-names/FRBNF_agent-names.rdf')/rdf:RDF"/>
    <xsl:variable name="an-names" select="document('rdf/agent-names/FRAN_agent-names.rdf')/rdf:RDF"/>
    <xsl:variable name="siaf-places" select="document('rdf/places/FRSIAF_places.rdf')/rdf:RDF"/>
    <xsl:variable name="bnf-places" select="document('rdf/places/FRBNF_places.rdf')/rdf:RDF"/>
    <xsl:variable name="an-places" select="document('rdf/places/FRAN_places.rdf')/rdf:RDF"/>
 
   <!-- <xsl:variable name="siaf-gtypes-relations"
        select="document('FRSIAF_relations-between-gtypes.rdf')/rdf:RDF"/>-->
    <xsl:variable name="siaf-relations" select="document('rdf/relations/FRSIAF_relations-between-agents.rdf')/rdf:RDF"/>
    <xsl:variable name="bnf-relations" select="document('rdf/relations/FRBNF_relations-between-agents.rdf')/rdf:RDF"/>
    <xsl:variable name="an-relations" select="document('rdf/relations/FRAN_relations-between-agents.rdf')/rdf:RDF"/>
    
    <xsl:variable name="chemin-rdf-gtypes">
        <xsl:value-of select="concat('rdf/group-types/', '?select=*.rdf;recurse=yes;on-error=warning')"/>
    </xsl:variable>
    <xsl:variable name="collection-RDF-gtypes" select="collection($chemin-rdf-gtypes)"/>
    <xsl:variable name="siaf-prov-rels" select="document('rdf/relations/FRSIAF_provenance-relations.rdf')/rdf:RDF"/>
    <xsl:variable name="bnf-prov-rels" select="document('rdf/relations/FRBNF_provenance-relations.rdf')/rdf:RDF"/>
    <xsl:variable name="an-prov-rels" select="document('rdf/relations/FRAN_provenance-relations.rdf')/rdf:RDF"/>
    <xsl:strip-space elements="*"/>
    <xsl:template match="/piaaf:vide">
        <xsl:if test="$coll = 'FRSIAF'">

            <xsl:for-each
                select="$collection-EAC-SIAF/eac:eac-cpf[not(starts-with(normalize-space(eac:control/eac:recordId), 'FR784'))]">
                <xsl:variable name="recId" select="normalize-space(eac:control/eac:recordId)"/>
                <xsl:variable name="entType" select="eac:cpfDescription/eac:identity/eac:entityType"/>
                <xsl:variable name="entTypeName">
                    <xsl:choose>
                        <xsl:when test="$entType = 'corporateBody'">corporate-body</xsl:when>
                        <xsl:when test="$entType = 'person'">person</xsl:when>
                    </xsl:choose>
                </xsl:variable>
                <xsl:variable name="RiCClass">
                    <xsl:choose>
                        <xsl:when test="$entType = 'corporateBody'">CorporateBody</xsl:when>
                        <xsl:when test="$entType = 'person'">Person</xsl:when>
                    </xsl:choose>
                </xsl:variable>
                <xsl:variable name="URI"
                    select="concat('http://piaaf.demo.logilab.fr/resource/', $coll, '_', $entTypeName, '_', $recId)"/>
               
                <xsl:result-document method="xml" encoding="utf-8" indent="yes" href="rdf/agents/{$coll}_{$entTypeName}_{$recId}.rdf">
                    <rdf:RDF 
                        xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                        xmlns:owl="http://www.w3.org/2002/07/owl#"
                        xmlns:RiC="http://www.ica.org/standards/RiC/ontology#"
                        xmlns:isni="http://isni.org/ontology#"
                        xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
                        
                        xmlns:piaaf-onto="http://piaaf.demo.logilab.fr/ontology#"
                        >
                        <rdf:Description>
                            <xsl:attribute name="rdf:about">
                                <xsl:value-of select="$URI"/>
                            </xsl:attribute>
                            <rdf:type
                                rdf:resource="http://www.ica.org/standards/RiC/ontology#{$RiCClass}"/>
                            <rdfs:label xml:lang="fr">
                                <xsl:value-of select="
                                    if (count(eac:cpfDescription/eac:identity/eac:nameEntry[@localType = 'preferredFormForProject']/eac:part)=1)
                                    then (normalize-space(eac:cpfDescription/eac:identity/eac:nameEntry[@localType = 'preferredFormForProject']/eac:part))
                                    else (
                                    concat(eac:cpfDescription/eac:identity/eac:nameEntry[@localType = 'preferredFormForProject']/eac:part[@localType='nom'], ', ', eac:cpfDescription/eac:identity/eac:nameEntry[@localType = 'preferredFormForProject']/eac:part[@localType='prenom'], ' (', eac:cpfDescription/eac:identity/eac:nameEntry[@localType = 'preferredFormForProject']/eac:part[@localType='dates_biographiques'], ')')
                                    )
                                    
                                    
                                    "/>
                              
                            </rdfs:label>
                            <!-- relation vers chacun des noms -->
                            <xsl:for-each select="eac:cpfDescription/eac:identity/eac:nameEntry">
                                <RiC:hasDenominationRelation>
                                    <xsl:attribute name="rdf:resource">
                                        <xsl:value-of
                                            select="concat('http://piaaf.demo.logilab.fr/resource/FRSIAF_DenominationRelation_', $recId, '_', generate-id())"/>

                                    </xsl:attribute>
                                </RiC:hasDenominationRelation>
                            </xsl:for-each>
                            <!-- relation vers chacun des statuts juridiques -->
                            <xsl:for-each select="eac:cpfDescription/eac:description/descendant::eac:legalStatus">
                                <RiC:hasTypeRelation>
                                    <xsl:attribute name="rdf:resource">
                                        <xsl:value-of
                                            select="concat('http://piaaf.demo.logilab.fr/resource/FRSIAF_TypeRelation_', $recId, '_', generate-id())"/>
                                        
                                    </xsl:attribute>
                                </RiC:hasTypeRelation>
                            </xsl:for-each>
                            <xsl:choose>
                                <xsl:when test="$entType='person'">
                                    <xsl:if test="eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate">
                                    <RiC:birthDate>
                                        <xsl:call-template name="outputDate">
                                            <xsl:with-param name="stdDate" select="eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate/@standardDate"></xsl:with-param>
                                            <xsl:with-param name="date" select="eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate"></xsl:with-param>
                                        </xsl:call-template>
                                        
                                      <!--  <xsl:choose>
                                            
                                            <xsl:when test="string-length(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate/@standardDate)=4">
                                                <xsl:attribute name="rdf:datatype"> <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text></xsl:attribute>
                                                
                                            </xsl:when>
                                            <xsl:when test="string-length(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate/@standardDate)=7">
                                                <xsl:attribute name="rdf:datatype">
                                                    <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                                </xsl:attribute>
                                            </xsl:when>
                                            <xsl:when test="string-length(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate/@standardDate)=10">
                                                <xsl:attribute name="rdf:datatype">
                                                    <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                                </xsl:attribute>
                                            </xsl:when>
                                            <xsl:otherwise/>
                                        </xsl:choose>
                                        
                                        <xsl:value-of select="
                                            if (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate/@standardDate)!='')
                                            then (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate/@standardDate))
                                            else(
                                            if (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate)!='')
                                            then (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate))
                                            else()
                                            )
                                            "/>
                                        -->
                                        
                                        
                                      <!--  <xsl:value-of
                                            select="eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate/@standardDate"
                                        />-->
                                    </RiC:birthDate>
                                    </xsl:if>
                                    <xsl:if
                                        test="eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate">
                                        <RiC:deathDate>
                                            <xsl:call-template name="outputDate">
                                                <xsl:with-param name="stdDate" select="eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate/@standardDate"></xsl:with-param>
                                                <xsl:with-param name="date" select="eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate"></xsl:with-param>
                                            </xsl:call-template>
                                           <!-- <xsl:choose>
                                                
                                                <xsl:when test="string-length(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate/@standardDate)=4">
                                                    <xsl:attribute name="rdf:datatype"> <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text></xsl:attribute>
                                                    
                                                </xsl:when>
                                                <xsl:when test="string-length(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate/@standardDate)=7">
                                                    <xsl:attribute name="rdf:datatype">
                                                        <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                                    </xsl:attribute>
                                                </xsl:when>
                                                <xsl:when test="string-length(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate/@standardDate)=10">
                                                    <xsl:attribute name="rdf:datatype">
                                                        <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                                    </xsl:attribute>
                                                </xsl:when>
                                                <xsl:otherwise/>
                                            </xsl:choose>
                                            
                                            <!-\-  <RiC:endDate rdf:datatype="http://www.w3.org/2001/XMLSchema#dateTime">-\->
                                            <xsl:value-of select="
                                                if (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate/@standardDate)!='')
                                                then (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate/@standardDate))
                                                else(
                                                if (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate)!='')
                                                then (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate))
                                                else()
                                                )
                                                "/>-->
                                            
                                            
                                      <!--      <xsl:value-of
                                                select="eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate/@standardDate"
                                            />-->
                                        </RiC:deathDate>
                                    </xsl:if>
                                </xsl:when>
                                <xsl:when test="$entType='corporateBody'">
                                    <xsl:if test="eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate">
                                    <RiC:beginningDate>
                                        <xsl:call-template name="outputDate">
                                            <xsl:with-param name="stdDate" select="eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate/@standardDate"></xsl:with-param>
                                            <xsl:with-param name="date" select="eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate"></xsl:with-param>
                                        </xsl:call-template>
                                   <!--     <xsl:choose>
                                            
                                            <xsl:when test="string-length(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate/@standardDate)=4">
                                                <xsl:attribute name="rdf:datatype"> <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text></xsl:attribute>
                                                
                                            </xsl:when>
                                            <xsl:when test="string-length(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate/@standardDate)=7">
                                                <xsl:attribute name="rdf:datatype">
                                                    <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                                </xsl:attribute>
                                            </xsl:when>
                                            <xsl:when test="string-length(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate/@standardDate)=10">
                                                <xsl:attribute name="rdf:datatype">
                                                    <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                                </xsl:attribute>
                                            </xsl:when>
                                            <xsl:otherwise/>
                                        </xsl:choose>
                                        
                                        <xsl:value-of select="
                                            if (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate/@standardDate)!='')
                                            then (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate/@standardDate))
                                            else(
                                            if (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate)!='')
                                            then (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate))
                                            else()
                                            )
                                            "/>
                                        -->
                                        
                                    </RiC:beginningDate>
                                    </xsl:if>
                                    <xsl:if
                                        test="eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate">
                                        <RiC:endDate>
                                            <xsl:call-template name="outputDate">
                                                <xsl:with-param name="stdDate" select="eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate/@standardDate"></xsl:with-param>
                                                <xsl:with-param name="date" select="eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate"></xsl:with-param>
                                            </xsl:call-template>
                                         <!--   <xsl:choose>
                                                
                                                <xsl:when test="string-length(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate/@standardDate)=4">
                                                    <xsl:attribute name="rdf:datatype"> <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text></xsl:attribute>
                                                    
                                                </xsl:when>
                                                <xsl:when test="string-length(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate/@standardDate)=7">
                                                    <xsl:attribute name="rdf:datatype">
                                                        <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                                    </xsl:attribute>
                                                </xsl:when>
                                                <xsl:when test="string-length(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate/@standardDate)=10">
                                                    <xsl:attribute name="rdf:datatype">
                                                        <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                                    </xsl:attribute>
                                                </xsl:when>
                                                <xsl:otherwise/>
                                            </xsl:choose>
                                            
                                            <!-\-  <RiC:endDate rdf:datatype="http://www.w3.org/2001/XMLSchema#dateTime">-\->
                                            <xsl:value-of select="
                                                if (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate/@standardDate)!='')
                                                then (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate/@standardDate))
                                                else(
                                                if (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate)!='')
                                                then (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate))
                                                else()
                                                )
                                                "/>
                                            -->
                                            
                                        </RiC:endDate>
                                    </xsl:if>
                                </xsl:when>
                            </xsl:choose>
                          
                       
                            <!-- on cherche si l'entité appartient à une catégorie (group-type)-->
                            <xsl:variable name="hasGroupType">
                                <xsl:choose>
                                    <xsl:when test="eac:cpfDescription/eac:relations/eac:cpfRelation[ends-with(@xlink:arcrole, 'isInstanceOfGenericType')]">
                                        <xsl:value-of select="eac:cpfDescription/eac:relations/eac:cpfRelation[ends-with(@xlink:arcrole, 'isInstanceOfGenericType')]/@xlink:href"/>
                                       
                                    </xsl:when>
                                    <xsl:otherwise>non</xsl:otherwise>
                                </xsl:choose>
                            </xsl:variable>
                           <xsl:choose>
                               <xsl:when test="$hasGroupType='non'">
                                   
                                  <!-- <xsl:comment>pas de GroupType</xsl:comment>-->
                                   <xsl:if
                                       test="eac:cpfDescription/eac:description/eac:function[not(@localType)]/*[not(self::eac:term) and normalize-space(.)!='']">
                                      
                                       <RiC:description xml:lang="fr">
                                           <xsl:for-each select="eac:cpfDescription/eac:description/eac:function[not(@localType)]/*[not(self::eac:term) and normalize-space(.)!='']">
                                               <xsl:apply-templates></xsl:apply-templates>
                                               <xsl:if test="position()!=last()">
                                                   <xsl:text> </xsl:text>
                                               </xsl:if>
                                           </xsl:for-each>
                                           
                                       </RiC:description>
                                     
                                   </xsl:if>
                                   <xsl:if test="eac:cpfDescription/eac:description/eac:biogHist/*[not(self::eac:chronList) and normalize-space(.)!='']">
                                       <RiC:history xml:lang="fr">
                                           <xsl:for-each
                                               select="eac:cpfDescription/eac:description/eac:biogHist/*[not(self::eac:chronList) and normalize-space(.)!='']">
                                               <xsl:apply-templates/>
                                               <xsl:if test="position() != last()">
                                                   <xsl:text> </xsl:text>
                                               </xsl:if>
                                           </xsl:for-each>
                                       </RiC:history>
                                       
                                   </xsl:if>
                                   <xsl:if
                                       test="eac:cpfDescription/eac:description/eac:structureOrGenealogy[normalize-space(.) != '']">
                                       <RiC:noteOnStructureOrGenealogy xml:lang="fr">
                                       <xsl:for-each select="eac:cpfDescription/eac:description/eac:structureOrGenealogy/*[normalize-space(.)!='']">
                                           <xsl:apply-templates/>
                                           <xsl:if test="position() != last()">
                                               <xsl:text> </xsl:text>
                                           </xsl:if>
                                       </xsl:for-each>
                                     
                                       </RiC:noteOnStructureOrGenealogy>
                                       
                                   </xsl:if>
                                   <xsl:for-each
                                       select="eac:cpfDescription/eac:description/eac:biogHist/eac:chronList/eac:chronItem[normalize-space(.) != '']">
                                       <xsl:variable name="event"
                                           select="concat(normalize-space(eac:date), ' : ', normalize-space(eac:event))"/>
                                       <xsl:if test="$siaf-events/rdf:Description[rdfs:label = $event]">
                                           <RiC:affectedBy>
                                               <xsl:attribute name="rdf:resource">
                                                   
                                                   <xsl:value-of
                                                       select="$siaf-events/rdf:Description[rdfs:label = $event]/@rdf:about"
                                                   />
                                               </xsl:attribute>
                                           </RiC:affectedBy>
                                       </xsl:if>
                                       
                                   </xsl:for-each>
                                   <xsl:for-each
                                       select="eac:cpfDescription/eac:description/descendant::eac:mandate">
                                       
                                      
                                           <xsl:variable name="descNote" select="normalize-space(eac:descriptiveNote)"/>
                                           <xsl:variable name="citation" select="
                                               if (eac:citation[@xlink:href!=''])
                                               then (normalize-space(eac:citation/@xlink:href))
                                               else ('non')
                                               "/>
                                           <!--<xsl:variable name="link" select="eac:citation/@xlink:href"/>-->
                                           <xsl:if test="$siaf-mandates/rdf:Description[RiC:hasTitle=$descNote and rdf:type[@rdf:resource='http://www.ica.org/standards/RiC/ontology#Mandate']]">
                                               
                                               <xsl:choose>
                                                   <xsl:when test="$citation='non'">
                                                       <xsl:for-each select="$siaf-mandates/rdf:Description[RiC:hasTitle=$descNote and rdf:type[@rdf:resource='http://www.ica.org/standards/RiC/ontology#Mandate']]">
                                                           <RiC:conformsToRule>
                                                               <xsl:attribute name="rdf:resource">
                                                                   <xsl:value-of select="@rdf:about"/>
                                                                   
                                                               </xsl:attribute>
                                                               
                                                           </RiC:conformsToRule>
                                                       </xsl:for-each>
                                                       
                                                   </xsl:when>
                                                   <xsl:when test="$citation!='non'">
                                                       <!--<xsl:comment>citation pas vide</xsl:comment>-->
                                                       <xsl:for-each select="$siaf-mandates/rdf:Description[RiC:hasTitle=$descNote and rdf:type[@rdf:resource='http://www.ica.org/standards/RiC/ontology#Mandate']]">
                                                           
                                                           
                                                           <xsl:choose>
                                                               <xsl:when test="RiC:evidencedBy">
                                                                   <xsl:variable name="theEvidence" select="RiC:evidencedBy/@rdf:resource"/>
                                                            <!--       <xsl:comment>il y a un RiC:evidencedBy</xsl:comment>-->
                                                                   <xsl:if test="ancestor::rdf:RDF/rdf:Description[@rdf:about=$theEvidence and RiC:identifier=$citation]">
                                                                       
                                                                       <RiC:conformsToRule>
                                                                           <xsl:attribute name="rdf:resource">
                                                                               <xsl:value-of select="@rdf:about"/>
                                                                               
                                                                           </xsl:attribute>
                                                                           
                                                                       </RiC:conformsToRule>
                                                                   </xsl:if>
                                                                   
                                                                   
                                                                   
                                                               </xsl:when>
                                                           </xsl:choose>
                                                           
                                                        
                                                           
                                                       </xsl:for-each>    
                                                   </xsl:when>
                                               </xsl:choose>
                                               
                                               
                                               
                                     
                                               
                                           </xsl:if>
                                           
                                           
                      
                                       
                                       
                                   </xsl:for-each>
                                
                                   <xsl:for-each
                                       select="eac:cpfDescription/eac:description/descendant::eac:function[@localType = 'piaafFunction']">
                                       <xsl:variable name="functionName">
                                          <!-- <xsl:for-each select="eac:term">
                                               <xsl:value-of select="."/>
                                               <xsl:if test="position() != last()">
                                                   <xsl:text> -\- </xsl:text>
                                               </xsl:if>
                                           </xsl:for-each>-->
                                           <xsl:value-of select="eac:term[1]"/>
                                       </xsl:variable>
                                       <xsl:if
                                           test="$functions/rdf:Description[rdfs:label = $functionName]">
                                           <piaaf-onto:hasFunctionOfType>
                                               <xsl:attribute name="rdf:resource">
                                                   <xsl:value-of
                                                       select="$functions/rdf:Description[rdfs:label = $functionName]/@rdf:about"
                                                   />
                                               </xsl:attribute>
                                           </piaaf-onto:hasFunctionOfType>
                                       </xsl:if>
                                       
                                   </xsl:for-each>
                               
                               </xsl:when>
                               <xsl:when test="$hasGroupType!='non'">
                                   <!--<xsl:comment>GroupType=<xsl:value-of select="$hasGroupType"/></xsl:comment>
                                   -->
                                   <xsl:if test="$collection-EAC-SIAF/eac:eac-cpf[normalize-space(eac:control/eac:recordId)=$hasGroupType]">
                                
                                   
                                       <RiC:groupType rdf:resource="{concat('http://piaaf.demo.logilab.fr/resource/FRSIAF_group-type_', $hasGroupType)}"/>
                                 <xsl:variable name="entGTypeFile">
                                       <xsl:value-of select="concat('FRSIAF_group-type_', $hasGroupType)"/>
                                   </xsl:variable>
                                  
                                                             
                                   
                                   
                                   <!-- la question se pose ensuite de répartir les infos de description correctement -->
                                   <!-- on va faire des tests d'équivalence pour toutes les informations rédigées -->
                                   <!-- concernés : eac:function[not(@localType)]/eac:descriptiveNote ; eac:description/eac:biogHist/eac:p ; eac:description/eac:structureOrGenealogy-->
                                   
                                  
                                       <xsl:variable name="entGTypeFileMainEl" select="$collection-RDF-gtypes/rdf:RDF[ends-with(rdf:Description[1]/@rdf:about, concat('resource/', $entGTypeFile))]/rdf:Description[1]"/>
                                   
                                   <xsl:variable name="gTypeHistory" select="$entGTypeFileMainEl/RiC:history"/>
                                   <xsl:variable name="gTypeFunction" select="$entGTypeFileMainEl/RiC:description"/>
                                   <xsl:variable name="gTypeStruct" select="$entGTypeFileMainEl/RiC:noteOnStructureOrGenealogy"/>
                                  
                                   <xsl:choose>
                                       <xsl:when test="eac:cpfDescription/eac:description/eac:biogHist/*[not(self::eac:chronList) and normalize-space(.)!='']">
                                         <xsl:variable name="theEntHistory">
                                             <piaaf:theEntHistory>
                                                 <xsl:for-each select="eac:cpfDescription/eac:description/eac:biogHist/*[not(self::eac:chronList) and normalize-space(.)!='']">
                                                     <xsl:apply-templates></xsl:apply-templates>
                                                     <xsl:if test="position()!=last()">
                                                         <xsl:text> </xsl:text>
                                                     </xsl:if>
                                                 </xsl:for-each>
                                             </piaaf:theEntHistory>
                                         </xsl:variable>
                                          <!-- <xsl:copy-of select="$theEntHistory"/>
                                           <xsl:copy-of select="$gTypeHistory"/>-->
                                           <xsl:if test="normalize-space($gTypeHistory)!=normalize-space($theEntHistory/piaaf:theEntHistory)">
                                            <!--   <xsl:comment>deux historiques différents ; on sort donc l'historique de l'entité</xsl:comment>-->
                                               <RiC:history xml:lang="fr">
                                                   <xsl:value-of select="normalize-space($theEntHistory/piaaf:theEntHistory)"/>
                                               </RiC:history>
                                               
                                           </xsl:if>
                                       </xsl:when>
                                       <xsl:otherwise/>
                                   </xsl:choose>   
                                   <xsl:choose>
                                       <xsl:when test="eac:cpfDescription/eac:description/eac:function[not(@localType)]/*[not(self::eac:term) and normalize-space(.)!='']">
                                           <xsl:variable name="theEntFunction">
                                               <piaaf:theEntFunction>
                                                   <xsl:for-each select="eac:cpfDescription/eac:description/eac:function[not(@localType)]/*[not(self::eac:term) and normalize-space(.)!='']">
                                                       <xsl:apply-templates/>
                                                       <xsl:if test="position()!=last()">
                                                           <xsl:text> </xsl:text>
                                                       </xsl:if>
                                                   </xsl:for-each>
                                                 
                                               </piaaf:theEntFunction>
                                           </xsl:variable>
                                           <xsl:if test="normalize-space($gTypeFunction)!=normalize-space($theEntFunction/piaaf:theEntFunction)">
                                              <!-- <xsl:comment>deux notes sur la fonction différentes ; on sort donc cette note</xsl:comment>-->
                                               <RiC:description xml:lang="fr">
                                                   <xsl:value-of select="normalize-space($theEntFunction/piaaf:theEntFunction)"/>
                                               </RiC:description>
                                               
                                           </xsl:if>
                                       </xsl:when>
                                       <xsl:otherwise/>
                                   </xsl:choose>   
                                   <xsl:choose>
                                       <xsl:when test="eac:cpfDescription/eac:description/eac:structureOrGenealogy/*[normalize-space(.)!='']">
                                           <xsl:variable name="theEntStruct">
                                               <piaaf:theEntStructure>
                                                   <xsl:for-each select="eac:cpfDescription/eac:description/eac:structureOrGenealogy/*[normalize-space(.)!='']">
                                                      <xsl:apply-templates/>
                                                       <xsl:if test="position()!=last()">
                                                           <xsl:text> </xsl:text>
                                                       </xsl:if>
                                                   </xsl:for-each>
                                               </piaaf:theEntStructure>
                                           </xsl:variable>
                                      
                                           <xsl:if test="normalize-space($gTypeStruct)!=normalize-space($theEntStruct/piaaf:theEntStructure)">
                                               <!--<xsl:comment>deux notes d'organisation internes différentes ; on sort donc la note de l'entité</xsl:comment>-->
                                               <RiC:noteOnStructureOrGenealogy xml:lang="fr">
                                                   <xsl:value-of select="normalize-space($theEntStruct/piaaf:theEntStructure)"/>
                                               </RiC:noteOnStructureOrGenealogy>
                                               
                                           </xsl:if>
                                       </xsl:when>
                                       <xsl:otherwise/>
                                   </xsl:choose> 
                                   <xsl:for-each
                                       select="eac:cpfDescription/eac:description/eac:biogHist/eac:chronList/eac:chronItem[normalize-space(.) != '']">
                                       <xsl:variable name="event"
                                           select="concat(normalize-space(eac:date), ' : ', normalize-space(eac:event))"/>
                                       
                                       <xsl:for-each select="$siaf-events/rdf:Description[rdfs:label = $event]">
                                           
                                           <xsl:variable name="theTargEventURI" select="@rdf:about"/>
                                         
                                      
                                           <xsl:choose>
                                               <xsl:when test="not($entGTypeFileMainEl/descendant::RiC:affectedBy[normalize-space(@rdf:resource)=$theTargEventURI])">
                                                   <!-- -->
                                                 <!--  <xsl:comment> événement non présent dans la notice du group type</xsl:comment>-->
                                                   <RiC:affectedBy>
                                                       <xsl:attribute name="rdf:resource">
                                                           
                                                           <xsl:value-of
                                                               select="$theTargEventURI"
                                                           />
                                                       </xsl:attribute>
                                                   </RiC:affectedBy>
                                               </xsl:when>
                                               <xsl:otherwise>
                                                  <!-- <xsl:comment>événement présent dans la notice du group type</xsl:comment>-->
                                               </xsl:otherwise>
                                              
                                           </xsl:choose>
                                       </xsl:for-each>
                                       
                                   </xsl:for-each>
                                   <xsl:for-each
                                       select="eac:cpfDescription/eac:description/descendant::eac:mandate">
                                      <xsl:variable name="descNote"
                                           select="normalize-space(eac:descriptiveNote)"/>
                                   
                                       <xsl:variable name="citation" select="
                                           if (eac:citation[@xlink:href!=''])
                                           then (normalize-space(eac:citation/@xlink:href))
                                           else ('non')
                                           "/>
                                       <!--<xsl:variable name="link" select="eac:citation/@xlink:href"/>-->
                                       <xsl:if test="$siaf-mandates/rdf:Description[RiC:hasTitle=$descNote and rdf:type[@rdf:resource='http://www.ica.org/standards/RiC/ontology#Mandate']]">
                                           
                                           <xsl:choose>
                                               <xsl:when test="$citation='non'">
                                                   <xsl:for-each select="$siaf-mandates/rdf:Description[RiC:hasTitle=$descNote and rdf:type[@rdf:resource='http://www.ica.org/standards/RiC/ontology#Mandate']]">
                                                       <xsl:variable name="theTargMandateURI" select="@rdf:about"/>
                                                       <xsl:choose>
                                                           <xsl:when test="$entGTypeFileMainEl/RiC:conformsToRule[@rdf:resource=$theTargMandateURI]">  <!--<xsl:comment>mandate présent dans la notice du group type</xsl:comment>--></xsl:when>
                                                           <xsl:otherwise>
                                                               <RiC:conformsToRule>
                                                                   <xsl:attribute name="rdf:resource">
                                                                       <xsl:value-of
                                                                           select="$theTargMandateURI"/>
                                                                       
                                                                   </xsl:attribute>
                                                                   
                                                               </RiC:conformsToRule>
                                                           </xsl:otherwise>
                                                       </xsl:choose>
                                                   
                                                   </xsl:for-each>
                                                   
                                               </xsl:when>
                                               <xsl:when test="$citation!='non'">
                                                <!--   <xsl:comment>citation pas vide</xsl:comment>-->
                                                   <xsl:for-each select="$siaf-mandates/rdf:Description[RiC:hasTitle=$descNote and rdf:type[@rdf:resource='http://www.ica.org/standards/RiC/ontology#Mandate']]">
                                                       <xsl:variable name="theTargMandateURI" select="@rdf:about"/>
                                                       
                                                       <xsl:choose>
                                                           <xsl:when test="RiC:evidencedBy">
                                                               <xsl:variable name="theEvidence" select="RiC:evidencedBy/@rdf:resource"/>
                                                             
                                                               <xsl:if test="ancestor::rdf:RDF/rdf:Description[@rdf:about=$theEvidence and RiC:identifier=$citation]">
                                                                 <!--  <xsl:comment>il y a un RiC:evidencedBy</xsl:comment>-->
                                                                   <xsl:choose>
                                                                       <xsl:when test="$entGTypeFileMainEl/RiC:conformsToRule[@rdf:resource=$theTargMandateURI]"> <!-- <xsl:comment>mandate présent dans la notice du group type</xsl:comment>--></xsl:when>
                                                                       <xsl:otherwise>
                                                                           <RiC:conformsToRule>
                                                                               <xsl:attribute name="rdf:resource">
                                                                                   <xsl:value-of
                                                                                       select="$theTargMandateURI"/>
                                                                                   
                                                                               </xsl:attribute>
                                                                               
                                                                           </RiC:conformsToRule>
                                                                       </xsl:otherwise>
                                                                   </xsl:choose>
                                                                  
                                                               </xsl:if>
                                                               
                                                               
                                                               
                                                           </xsl:when>
                                                       </xsl:choose>
                                                
                                                       
                                                   </xsl:for-each>    
                                               </xsl:when>
                                           </xsl:choose>
                                           
                                    
                                           
                                       </xsl:if>
                                       
                                   </xsl:for-each>
                                   <xsl:for-each
                                       select="eac:cpfDescription/eac:description/descendant::eac:function[@localType = 'piaafFunction']">
                                       <xsl:variable name="functionName">
                                           <!--<xsl:for-each select="eac:term">
                                               <xsl:value-of select="."/>
                                               <xsl:if test="position() != last()">
                                                   <xsl:text> -\- </xsl:text>
                                               </xsl:if>
                                           </xsl:for-each>-->
                                           <xsl:value-of select="eac:term[1]"/>
                                       </xsl:variable>
                                       <xsl:if
                                           test="$functions/rdf:Description[rdfs:label = $functionName]">
                                           <xsl:variable name="theTargFunctionURI" select="$functions/rdf:Description[rdfs:label = $functionName]/@rdf:about"/>
                                           <xsl:choose>
                                               <xsl:when test="$entGTypeFileMainEl/piaaf-onto:hasFunctionOfType[@rdf:resource=$theTargFunctionURI]"></xsl:when>
                                               <xsl:otherwise>
                                                   <piaaf-onto:hasFunctionOfType>
                                                       <xsl:attribute name="rdf:resource">
                                                           <xsl:value-of
                                                               select="$theTargFunctionURI"
                                                           />
                                                       </xsl:attribute>
                                                   </piaaf-onto:hasFunctionOfType>
                                               </xsl:otherwise>
                                           </xsl:choose>
                                         
                                       </xsl:if>
                                       
                                   </xsl:for-each>
                                   </xsl:if>
                                   
                                   <xsl:if test="not($collection-EAC-SIAF/eac:eac-cpf[normalize-space(eac:control/eac:recordId)=$hasGroupType])">
                                   <!--    <xsl:comment>Le grouptype cible n'existe pas !!</xsl:comment>-->
                                   </xsl:if>
                               </xsl:when>
                              
                           </xsl:choose>
                        
                          
                            <xsl:for-each select="eac:cpfDescription/eac:description/eac:places/eac:place[normalize-space(eac:placeEntry)!='']">
                               
                                <xsl:variable name="placeName">
                                    <xsl:choose>
                                        <xsl:when test="eac:placeRole='Siège social'">
                                            <xsl:choose>
                                                <xsl:when test="parent::eac:places/eac:place[eac:placeRole='Siège']">
                                                    <xsl:value-of select="concat(normalize-space(parent::eac:places/eac:place[eac:placeRole='Siège']/eac:placeEntry), '. ', normalize-space(eac:placeEntry))"/>
                                                </xsl:when>
                                                <xsl:otherwise><xsl:value-of select="normalize-space(eac:placeEntry)"/></xsl:otherwise>
                                            </xsl:choose>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:value-of select="normalize-space(eac:placeEntry)"/>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </xsl:variable>
                                <xsl:if
                                    test="$siaf-places/rdf:Description[rdfs:label = $placeName]">
                            
                                    <RiC:hasLocationRelation>
                                        <xsl:attribute name="rdf:resource">
                                            <xsl:value-of
                                                select="concat('http://piaaf.demo.logilab.fr/resource/FRSIAF_LocationRelation_', $recId, '_', generate-id())"/>
                                            
                                        </xsl:attribute>
                                    </RiC:hasLocationRelation>
                                    
                                </xsl:if>
                            </xsl:for-each>
                          
                      

                           
                         
                            <xsl:for-each
                                select="eac:cpfDescription/eac:relations/eac:cpfRelation[@cpfRelationType = 'identity']">
                          
                                <xsl:variable name="lnk" select="normalize-space(@xlink:href)"/>
                                <owl:sameAs>
                                    <xsl:attribute name="rdf:resource">
                                        <xsl:if test="contains($lnk, 'bnf.fr')">
                                            <xsl:choose>
                                                <xsl:when
                                                  test="contains($lnk, 'catalogue.bnf.fr/ark:/12148/') and contains($lnk, '/PUBLIC')">
                                                  <xsl:text>http://data.bnf.fr/ark:/12148/</xsl:text>
                                                  <xsl:value-of
                                                  select="substring-before(substring-after($lnk, 'http://catalogue.bnf.fr/ark:/12148/'), '/PUBLIC')"/>
                                                    <xsl:choose>
                                                        <xsl:when test="$RiCClass='CorporateBody'"><xsl:text>#foaf:Organization</xsl:text></xsl:when>
                                                        <xsl:when test="$RiCClass='Person'"><xsl:text>#foaf:Person</xsl:text></xsl:when>
                                                    </xsl:choose>
                                                    
                                                </xsl:when>
                                                <xsl:when test="contains($lnk, 'catalogue.bnf.fr/ark:/12148/') and not(contains($lnk, '/PUBLIC'))">
                                                    <xsl:text>http://data.bnf.fr/ark:/12148/</xsl:text>
                                                    <xsl:value-of
                                                        select="substring-after($lnk, 'http://catalogue.bnf.fr/ark:/12148/')"/>
                                                    <xsl:choose>
                                                        <xsl:when test="$RiCClass='CorporateBody'"><xsl:text>#foaf:Organization</xsl:text></xsl:when>
                                                        <xsl:when test="$RiCClass='Person'"><xsl:text>#foaf:Person</xsl:text></xsl:when>
                                                    </xsl:choose>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:if>

                                    </xsl:attribute>
                                </owl:sameAs>
                            </xsl:for-each>
                            <!-- les ISNI -->
                            <xsl:if test="eac:cpfDescription/eac:identity/eac:entityId[@localType='ISNI' and normalize-space(.)!='']">
                                <xsl:variable name="ISNIValue" select="normalize-space(eac:cpfDescription/eac:identity/eac:entityId[@localType='ISNI' and normalize-space(.)!=''])"></xsl:variable>
                                <xsl:variable name="ISNIId">
                                    <xsl:choose>
                                        <xsl:when test="starts-with($ISNIValue, 'ISNI:')">
                                            <xsl:value-of select="replace(substring-after($ISNIValue, 'ISNI:'), ' ', '')"/>
                                        </xsl:when>
                                        <xsl:when test="starts-with($ISNIValue, 'ISNI :')">
                                            <xsl:value-of select="replace(substring-after($ISNIValue, 'ISNI :'), ' ', '')"/>
                                        </xsl:when>
                                        <xsl:when test="starts-with($ISNIValue, 'ISNI')">
                                            <xsl:value-of select="replace(substring-after($ISNIValue, 'ISNI'), ' ', '')"/>
                                        </xsl:when>
                                    </xsl:choose>
                                </xsl:variable>
                                <!-- http://isni.org/isni/0000000121032683-->
                                <isni:identifierValid><xsl:value-of select="$ISNIId"/></isni:identifierValid>
                        
                            </xsl:if>
                            
                            <!-- les relations cpfType autres que identité -->
                            <xsl:for-each
                                select="eac:cpfDescription/eac:relations/eac:cpfRelation[contains(@xlink:arcrole, 'isControlledBy')]">
                                <xsl:call-template name="outputObjectPropertyForRelation">
                                    <xsl:with-param name="link"
                                        select="
                                            if (normalize-space(@xlink:href) = '' or not(@xlink:href))
                                            then
                                                ('non')
                                            else
                                                (normalize-space(@xlink:href))
                                            "/>
                                    <xsl:with-param name="relEntry"
                                        select="normalize-space(eac:relationEntry)"/>
                                    <xsl:with-param name="recId" select="$recId"/>
                                    <xsl:with-param name="srcRelType" select="'isControlledBy'"/>
                                    <xsl:with-param name="arcToRelName"
                                        select="'agentControlledBy'"/>
                                    <xsl:with-param name="toAncRelArcName" select="'controlOnAgent'"/>
                                    <xsl:with-param name="relName" select="'AgentControlRelation'"/>
                                    <xsl:with-param name="relSecondArcName"
                                        select="'agentControlHeldBy'"/>
                                   <!-- <xsl:with-param name="shortcutRelName" select="'controlledBy'"/>-->
                                    <xsl:with-param name="relFromDate"
                                        select="   if (eac:dateRange/eac:fromDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:fromDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:fromDate))
                                        
                                        "/>
                                    <xsl:with-param name="relToDate"
                                        select="
                                        
                                        if (eac:dateRange/eac:toDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:toDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:toDate))
                                        
                                        
                                        
                                        
                                        "/>
                                  
                                    <xsl:with-param name="relDate"
                                        select="
                                        if (eac:date/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:date/@standardDate))
                                        else
                                        (normalize-space(eac:date))"
                                        
                                        
                                        >
                                        
                                    </xsl:with-param>
                               
                                    <xsl:with-param name="coll" select="$coll"/>
                                </xsl:call-template>


                            </xsl:for-each>
                            <xsl:for-each
                                select="eac:cpfDescription/eac:relations/eac:cpfRelation[contains(@xlink:arcrole, 'isDirectorOf')]">
                                <xsl:call-template name="outputObjectPropertyForRelation">
                                    <xsl:with-param name="link"
                                        select="
                                        if (normalize-space(@xlink:href) = '' or not(@xlink:href))
                                        then
                                        ('non')
                                        else
                                        (normalize-space(@xlink:href))
                                        "/>
                                    <xsl:with-param name="relEntry"
                                        select="normalize-space(eac:relationEntry)"/>
                                    <xsl:with-param name="recId" select="$recId"/>
                                    <xsl:with-param name="srcRelType" select="'isDirectorOf'"/>
                                    <xsl:with-param name="arcToRelName"
                                        select="'agentHasLeadershipRelation'"/>
                                    <xsl:with-param name="toAncRelArcName" select="'leadershipBy'"/>
                                    <xsl:with-param name="relName" select="'AgentLeadershipRelation'"/>
                                    <xsl:with-param name="relSecondArcName"
                                        select="'leadershipOn'"/>
                                 <!--   <xsl:with-param name="shortcutRelName" select="'controlledBy'"/>-->
                                    <xsl:with-param name="relFromDate"
                                        select="   if (eac:dateRange/eac:fromDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:fromDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:fromDate))
                                        
                                        "/>
                                    <xsl:with-param name="relToDate"
                                        select="
                                        
                                        if (eac:dateRange/eac:toDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:toDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:toDate))
                                        
                                        
                                        
                                        
                                        "/>
                                    
                                    <xsl:with-param name="relDate"
                                        select="
                                        if (eac:date/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:date/@standardDate))
                                        else
                                        (normalize-space(eac:date))"
                                        
                                        
                                        >
                                        
                                    </xsl:with-param>
                                   
                                    <xsl:with-param name="coll" select="$coll"/>
                                </xsl:call-template>
                                
                                
                            </xsl:for-each>
                            <xsl:for-each
                                select="eac:cpfDescription/eac:relations/eac:cpfRelation[contains(@xlink:arcrole, 'isFunctionallyLinkedTo')]">
                                <xsl:call-template name="outputObjectPropertyForRelation">
                                    <xsl:with-param name="link"
                                        select="
                                            if (normalize-space(@xlink:href) = '' or not(@xlink:href))
                                            then
                                                ('non')
                                            else
                                                (normalize-space(@xlink:href))
                                            "/>
                                    <xsl:with-param name="recId" select="$recId"/>
                                    <xsl:with-param name="relEntry"
                                        select="normalize-space(eac:relationEntry)"/>
                                    <xsl:with-param name="srcRelType"
                                        select="'isFunctionallyLinkedTo'"/>
                                    <xsl:with-param name="arcToRelName"
                                        select="'hasBusinessRelation'"/>
                                    <xsl:with-param name="toAncRelArcName"
                                        select="'businessRelationWith'"/>
                                    <xsl:with-param name="relName" select="'BusinessRelation'"/>
                                    <xsl:with-param name="relSecondArcName"
                                        select="'businessRelationWith'"/>
                                
                                    <xsl:with-param name="relFromDate"
                                        select="   if (eac:dateRange/eac:fromDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:fromDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:fromDate))
                                        
                                        "/>
                                    <xsl:with-param name="relToDate"
                                        select="
                                        
                                        if (eac:dateRange/eac:toDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:toDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:toDate))
                                        
                                        
                                        
                                        
                                        "/>
                               
                                    <xsl:with-param name="relDate"
                                        select="
                                        if (eac:date/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:date/@standardDate))
                                        else
                                        (normalize-space(eac:date))"
                                        
                                        
                                        >
                                        
                                    </xsl:with-param>
                           
                                    <xsl:with-param name="coll" select="$coll"/>
                                </xsl:call-template>

                            </xsl:for-each>
                            <xsl:for-each
                                select="eac:cpfDescription/eac:relations/eac:cpfRelation[contains(@xlink:arcrole, 'hasPart')]">
                                <xsl:call-template name="outputObjectPropertyForRelation">
                                    <xsl:with-param name="link"
                                        select="
                                            if (normalize-space(@xlink:href) = '' or not(@xlink:href))
                                            then
                                                ('non')
                                            else
                                                (normalize-space(@xlink:href))
                                            "/>
                                    <xsl:with-param name="recId" select="$recId"/>
                                    <xsl:with-param name="relEntry"
                                        select="normalize-space(eac:relationEntry)"/>
                                    <xsl:with-param name="srcRelType" select="'hasPart'"/>
                                    <xsl:with-param name="arcToRelName" select="'groupHasPart'"/>
                                    <xsl:with-param name="toAncRelArcName" select="'agentPartOf'"/>
                                    <xsl:with-param name="relName" select="'AgentWholePartRelation'"/>
                                    <xsl:with-param name="relSecondArcName" select="'wholeHasAgentPart'"/>
                               
                                    <xsl:with-param name="relFromDate"
                                        select="   if (eac:dateRange/eac:fromDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:fromDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:fromDate))
                                        
                                        "/>
                                    <xsl:with-param name="relToDate"
                                        select="
                                        
                                        if (eac:dateRange/eac:toDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:toDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:toDate))
                                        
                                        
                                        
                                        
                                        "/>
                               
                                    <xsl:with-param name="relDate"
                                        select="
                                        if (eac:date/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:date/@standardDate))
                                        else
                                        (normalize-space(eac:date))"
                                        
                                        
                                        >
                                        
                                    </xsl:with-param>
                                 
                                    <xsl:with-param name="coll" select="$coll"/>
                                </xsl:call-template>

                            </xsl:for-each>
                            <xsl:for-each
                                select="eac:cpfDescription/eac:relations/eac:cpfRelation[contains(@xlink:arcrole, 'isPartOf')]">
                                <xsl:call-template name="outputObjectPropertyForRelation">
                                    <xsl:with-param name="link"
                                        select="
                                            if (normalize-space(@xlink:href) = '' or not(@xlink:href))
                                            then
                                                ('non')
                                            else
                                                (normalize-space(@xlink:href))
                                            "/>
                                    <xsl:with-param name="recId" select="$recId"/>
                                    <xsl:with-param name="relEntry"
                                        select="normalize-space(eac:relationEntry)"/>
                                    <xsl:with-param name="srcRelType" select="'isPartOf'"/>
                                    <xsl:with-param name="arcToRelName"
                                        select="'agentHasWholePartRelation'"/>
                                    <xsl:with-param name="toAncRelArcName" select="'wholeHasAgentPart'"/>
                                    <xsl:with-param name="relName" select="'AgentWholePartRelation'"/>
                                    <xsl:with-param name="relSecondArcName" select="'agentPartOf'"/>  
                                    <xsl:with-param name="relFromDate"
                                        select="   if (eac:dateRange/eac:fromDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:fromDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:fromDate))
                                        
                                        "/>
                                    <xsl:with-param name="relToDate"
                                        select="
                                        
                                        if (eac:dateRange/eac:toDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:toDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:toDate))
                                        
                                        
                                        
                                        
                                        "/>
                               
                                    <xsl:with-param name="relDate"
                                        select="
                                        if (eac:date/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:date/@standardDate))
                                        else
                                        (normalize-space(eac:date))"
                                        
                                        
                                        >
                                        
                                    </xsl:with-param>   <xsl:with-param name="coll" select="$coll"/>
                                </xsl:call-template>

                            </xsl:for-each>
                            <xsl:for-each
                                select="eac:cpfDescription/eac:relations/eac:cpfRelation[@cpfRelationType = 'temporal-earlier']">
                                <xsl:call-template name="outputObjectPropertyForRelation">
                                    <xsl:with-param name="link"
                                        select="
                                            if (normalize-space(@xlink:href) = '' or not(@xlink:href))
                                            then
                                                ('non')
                                            else
                                                (normalize-space(@xlink:href))
                                            "/>
                                    <xsl:with-param name="recId" select="$recId"/>
                                    <xsl:with-param name="relEntry"
                                        select="normalize-space(eac:relationEntry)"/>
                                    <xsl:with-param name="srcRelType" select="'temporal-earlier'"/>
                                    <xsl:with-param name="arcToRelName" select="'agentFollows'"/>
                                    <xsl:with-param name="toAncRelArcName"
                                        select="'followingAgent'"/>
                                    <xsl:with-param name="relName" select="'AgentTemporalRelation'"/>
                                    <xsl:with-param name="relSecondArcName"
                                        select="'precedingAgent'"/>
                               
                                    <xsl:with-param name="relFromDate"
                                        select="   if (eac:dateRange/eac:fromDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:fromDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:fromDate))
                                        
                                        "/>
                                    <xsl:with-param name="relToDate"
                                        select="
                                        
                                        if (eac:dateRange/eac:toDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:toDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:toDate))
                                        
                                        
                                        
                                        
                                        "/>
                               
                                    <xsl:with-param name="relDate"
                                        select="
                                        if (eac:date/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:date/@standardDate))
                                        else
                                        (normalize-space(eac:date))"
                                        
                                        
                                        >
                                        
                                    </xsl:with-param>
                                
   <xsl:with-param name="coll" select="$coll"/>
                                </xsl:call-template>

                            </xsl:for-each>
                            <xsl:for-each
                                select="eac:cpfDescription/eac:relations/eac:cpfRelation[@cpfRelationType = 'temporal-later']">
                                <xsl:call-template name="outputObjectPropertyForRelation">
                                    <xsl:with-param name="link"
                                        select="
                                            if (normalize-space(@xlink:href) = '' or not(@xlink:href))
                                            then
                                                ('non')
                                            else
                                                (normalize-space(@xlink:href))
                                            "/>
                                    <xsl:with-param name="recId" select="$recId"/>
                                    <xsl:with-param name="relEntry"
                                        select="normalize-space(eac:relationEntry)"/>
                                    <xsl:with-param name="srcRelType" select="'temporal-later'"/>
                                    <xsl:with-param name="arcToRelName" select="'agentPrecedes'"/>
                                    <xsl:with-param name="toAncRelArcName"
                                        select="'precedingAgent'"/>
                                    <xsl:with-param name="relName" select="'AgentTemporalRelation'"/>
                                    <xsl:with-param name="relSecondArcName"
                                        select="'followingAgent'"/>
                                 
                                    <xsl:with-param name="relFromDate"
                                        select="   if (eac:dateRange/eac:fromDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:fromDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:fromDate))
                                        
                                        "/>
                                    <xsl:with-param name="relToDate"
                                        select="
                                        
                                        if (eac:dateRange/eac:toDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:toDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:toDate))
                                        
                                        
                                        
                                        
                                        "/>
                                
                                    <xsl:with-param name="relDate"
                                        select="
                                        if (eac:date/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:date/@standardDate))
                                        else
                                        (normalize-space(eac:date))"
                                        
                                        
                                        >
                                        
                                    </xsl:with-param>
                                 <xsl:with-param name="coll" select="$coll"/>
                                </xsl:call-template>

                            </xsl:for-each>
                            <xsl:for-each
                                select="eac:cpfDescription/eac:relations/eac:cpfRelation[@cpfRelationType = 'hierarchical-child' and (not(@xlink:arcrole) or @xlink:arcrole='') ]">
                                <xsl:call-template name="outputObjectPropertyForRelation">
                                    <xsl:with-param name="link"
                                        select="
                                        if (normalize-space(@xlink:href) = '' or not(@xlink:href))
                                        then
                                        ('non')
                                        else
                                        (normalize-space(@xlink:href))
                                        "/>
                                    <xsl:with-param name="recId" select="$recId"/>
                                    <xsl:with-param name="relEntry"
                                        select="normalize-space(eac:relationEntry)"/>
                                    <xsl:with-param name="srcRelType" select="'hierarchical-child'"/>
                                    <xsl:with-param name="arcToRelName" select="'agentIsSuperiorInHierarchicalRelation'"/>
                                    <xsl:with-param name="toAncRelArcName"
                                        select="'hasHierarchicalParent'"/>
                                    <xsl:with-param name="relName" select="'AgentHierarchicalRelation'"/>
                                    <xsl:with-param name="relSecondArcName"
                                        select="'hasHierarchicalChild'"/>
                               
                                    <xsl:with-param name="relFromDate"
                                        select="   if (eac:dateRange/eac:fromDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:fromDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:fromDate))
                                        
                                        "/>
                                    <xsl:with-param name="relToDate"
                                        select="
                                        
                                        if (eac:dateRange/eac:toDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:toDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:toDate))
                                        
                                        
                                        
                                        
                                        "/>
                                
                                    <xsl:with-param name="relDate"
                                        select="
                                        if (eac:date/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:date/@standardDate))
                                        else
                                        (normalize-space(eac:date))"
                                        
                                        
                                        >
                                        
                                    </xsl:with-param>
                              
                                    <xsl:with-param name="coll" select="$coll"/>
                                </xsl:call-template>
                                
                            </xsl:for-each>
                            <!-- Relations de provenance -->
                            
                            <xsl:for-each select="eac:cpfDescription/eac:relations/eac:resourceRelation[@resourceRelationType = 'creatorOf']">
                                
                                <xsl:call-template name="outputObjectPropertyForProvenanceRelation">
                                    <xsl:with-param name="link"
                                        select="
                                        if (normalize-space(@xlink:href) = '' or not(@xlink:href))
                                        then
                                        ('non')
                                        else
                                        (normalize-space(@xlink:href))
                                        "/>
                                    <xsl:with-param name="recId" select="$recId"/>
                                    <xsl:with-param name="relEntry"
                                        select="normalize-space(eac:relationEntry)"/>
                                   
                                  
                                  
                            
                                    <xsl:with-param name="relFromDate"
                                        select="   if (eac:dateRange/eac:fromDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:fromDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:fromDate))
                                        
                                        "/>
                                    <xsl:with-param name="relToDate"
                                        select="
                                        
                                        if (eac:dateRange/eac:toDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:toDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:toDate))
                                        
                                        
                                        
                                        
                                        "/>
                          
                                    <xsl:with-param name="relDate"
                                        select="
                                        if (eac:date/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:date/@standardDate))
                                        else
                                        (normalize-space(eac:date))"
                                        
                                        
                                        >
                                        
                                    </xsl:with-param>
                                  
                                    <xsl:with-param name="coll" select="$coll"/>
                                </xsl:call-template>
                            </xsl:for-each>
                            <!-- métadonnées -->

                            <RiC:describedBy>
                                <xsl:attribute name="rdf:resource">
                                    <xsl:value-of
                                        select="concat('http://piaaf.demo.logilab.fr/resource/FRSIAF_description_', $recId)"
                                    />
                                </xsl:attribute>
                            </RiC:describedBy>
                        </rdf:Description>
                        <!-- sortie des relations de catégorisation -->
                     
                        <xsl:for-each select="eac:cpfDescription/eac:identity/eac:nameEntry">
                            <xsl:variable name="theName" select="  if (count(eac:part)=1)
                                then (normalize-space(eac:part))
                                else (
                                concat(eac:part[@localType='nom'], ', ', eac:part[@localType='prenom'], ' (', eac:part[@localType='dates_biographiques'], ')')
                                )"/>
                            <rdf:Description>
                                <xsl:attribute name="rdf:about">
                                    <xsl:value-of
                                        select="concat('http://piaaf.demo.logilab.fr/resource/FRSIAF_DenominationRelation_', $recId, '_', generate-id())"/>

                                </xsl:attribute>
                                <rdf:type
                                    rdf:resource="http://www.ica.org/standards/RiC/ontology#DenominationRelation"/>
                                <xsl:if
                                    test="eac:useDates/eac:dateRange/eac:fromDate">

                                    <RiC:beginningDate>
                                        <!--<xsl:value-of
                                            select="eac:useDates/eac:dateRange/eac:fromDate"/>-->
                                        <xsl:call-template name="outputDate">
                                            <xsl:with-param name="stdDate" select="eac:useDates/eac:dateRange/eac:fromDate/@standardDate"></xsl:with-param>
                                            <xsl:with-param name="date" select="eac:useDates/eac:dateRange/eac:fromDate"></xsl:with-param>
                                        </xsl:call-template>
                                       <!-- <xsl:choose>
                                            
                                            <xsl:when test="string-length(eac:useDates/eac:dateRange/eac:fromDate/@standardDate)=4">
                                                <xsl:attribute name="rdf:datatype"> <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text></xsl:attribute>
                                                
                                            </xsl:when>
                                            <xsl:when test="string-length(eac:useDates/eac:dateRange/eac:fromDate/@standardDate)=7">
                                                <xsl:attribute name="rdf:datatype">
                                                    <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                                </xsl:attribute>
                                            </xsl:when>
                                            <xsl:when test="string-length(eac:useDates/eac:dateRange/eac:fromDate/@standardDate)=10">
                                                <xsl:attribute name="rdf:datatype">
                                                    <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                                </xsl:attribute>
                                            </xsl:when>
                                            <xsl:otherwise/>
                                        </xsl:choose>
                                        
                                        <xsl:value-of select="
                                            if (normalize-space(eac:useDates/eac:dateRange/eac:fromDate/@standardDate)!='')
                                            then (normalize-space(eac:useDates/eac:dateRange/eac:fromDate/@standardDate))
                                            else(
                                            if (normalize-space(eac:useDates/eac:dateRange/eac:fromDate)!='')
                                            then (normalize-space(eac:useDates/eac:dateRange/eac:fromDate))
                                            else()
                                            )
                                            "/>-->
                                    </RiC:beginningDate>
                                </xsl:if>
                                <xsl:if
                                    test="eac:useDates/eac:dateRange/eac:toDate">
                                    <RiC:endDate>
                                        <xsl:call-template name="outputDate">
                                            <xsl:with-param name="stdDate" select="eac:useDates/eac:dateRange/eac:toDate/@standardDate"></xsl:with-param>
                                            <xsl:with-param name="date" select="eac:useDates/eac:dateRange/eac:toDate"></xsl:with-param>
                                        </xsl:call-template>
                                    <!--    <xsl:choose>
                                            
                                            <xsl:when test="string-length(eac:useDates/eac:dateRange/eac:toDate/@standardDate)=4">
                                                <xsl:attribute name="rdf:datatype"> <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text></xsl:attribute>
                                                
                                            </xsl:when>
                                            <xsl:when test="string-length(eac:useDates/eac:dateRange/eac:toDate/@standardDate)=7">
                                                <xsl:attribute name="rdf:datatype">
                                                    <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                                </xsl:attribute>
                                            </xsl:when>
                                            <xsl:when test="string-length(eac:useDates/eac:dateRange/eac:toDate/@standardDate)=10">
                                                <xsl:attribute name="rdf:datatype">
                                                    <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                                </xsl:attribute>
                                            </xsl:when>
                                            <xsl:otherwise/>
                                        </xsl:choose>
                                        
                                        <!-\-  <RiC:endDate rdf:datatype="http://www.w3.org/2001/XMLSchema#dateTime">-\->
                                        <xsl:value-of select="
                                            if (normalize-space(eac:useDates/eac:dateRange/eac:toDate/@standardDate)!='')
                                            then (normalize-space(eac:useDates/eac:toDate/@standardDate))
                                            else(
                                            if (normalize-space(eac:useDates/eac:dateRange/eac:toDate)!='')
                                            then (normalize-space(eac:useDates/eac:dateRange/eac:toDate))
                                            else()
                                            )
                                            "/>-->
                                        
                                        
                                      <!--  <xsl:value-of select="eac:useDates/eac:dateRange/eac:toDate"
                                        />-->
                                    </RiC:endDate>

                                </xsl:if>
                                <RiC:certainty xml:lang="fr">certain</RiC:certainty>
                                <RiC:denominationOf>
                                    <xsl:attribute name="rdf:resource">
                                        <xsl:value-of
                                            select="$URI"
                                        />
                                    </xsl:attribute>
                                </RiC:denominationOf>
                                <RiC:denominatedBy>
                                        <xsl:choose>
                                           <xsl:when test="$siaf-names/rdf:Description[rdfs:label=$theName]">
                                               <xsl:attribute name="rdf:resource">
                                               <xsl:value-of select="$siaf-names/rdf:Description[rdfs:label=$theName]/@rdf:about"/>
                                               </xsl:attribute>
                                           </xsl:when> 
                                            <xsl:otherwise>
                                                <xsl:attribute name="xml:lang">fr</xsl:attribute>
                                                <xsl:comment>pas de nom trouvé dans le fichier des noms</xsl:comment>
                                                <xsl:value-of select="normalize-space(eac:part)"/>
                                                
                                            </xsl:otherwise>
                                        </xsl:choose>


                             

                                </RiC:denominatedBy>
                            </rdf:Description>

                        </xsl:for-each>


                        <!-- statuts juridiques -->


                        <xsl:for-each
                            select="eac:cpfDescription/eac:description/descendant::eac:legalStatus">
                            <xsl:variable name="lsName" select="normalize-space(eac:term)"/>
                            
                            
                         
                                <rdf:Description>
                                    <xsl:attribute name="rdf:about">
                                        <xsl:text>http://piaaf.demo.logilab.fr/resource/FRSIAF_TypeRelation_</xsl:text>
                                        <xsl:value-of select="$recId"/>
                                        <xsl:text>_</xsl:text>
                                        <xsl:value-of
                                            select="generate-id()"/>
                                    </xsl:attribute>
                                    <rdf:type
                                        rdf:resource="http://www.ica.org/standards/RiC/ontology#TypeRelation"/>
                                    <RiC:hasType>
                                        <xsl:choose>
                                            <xsl:when test="$legalsts/skos:Concept[skos:prefLabel = $lsName]">
                                        <xsl:attribute name="rdf:resource">
                                            
                                                    <xsl:value-of
                                                        select="$legalsts/skos:Concept[skos:prefLabel = $lsName]/@rdf:about"
                                                    />
                                            
                                            
                                            
                                          
                                        </xsl:attribute>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:attribute name="xml:lang">fr</xsl:attribute>
                                                <xsl:value-of select="normalize-space(eac:term)"/>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </RiC:hasType>
                                    <RiC:categorizationOf>
                                        <xsl:attribute name="rdf:resource">
                                            <xsl:value-of
                                                select="$URI"
                                            />
                                        </xsl:attribute>
                                    </RiC:categorizationOf>
                                    <xsl:if
                                        test="eac:dateRange/eac:fromDate">
                                        <RiC:beginningDate>
                                            <xsl:call-template name="outputDate">
                                                <xsl:with-param name="stdDate" select="eac:dateRange/eac:fromDate/@standardDate"></xsl:with-param>
                                                <xsl:with-param name="date" select="eac:dateRange/eac:fromDate"></xsl:with-param>
                                            </xsl:call-template>
                                           <!-- <xsl:choose>
                                                
                                                <xsl:when test="string-length(eac:dateRange/eac:fromDate/@standardDate)=4">
                                                    <xsl:attribute name="rdf:datatype"> <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text></xsl:attribute>
                                                    
                                                </xsl:when>
                                                <xsl:when test="string-length(eac:dateRange/eac:fromDate/@standardDate)=7">
                                                    <xsl:attribute name="rdf:datatype">
                                                        <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                                    </xsl:attribute>
                                                </xsl:when>
                                                <xsl:when test="string-length(eac:dateRange/eac:fromDate/@standardDate)=10">
                                                    <xsl:attribute name="rdf:datatype">
                                                        <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                                    </xsl:attribute>
                                                </xsl:when>
                                                <xsl:otherwise/>
                                            </xsl:choose>
                                            
                                            <xsl:value-of select="
                                                if (normalize-space(eac:dateRange/eac:fromDate/@standardDate)!='')
                                                then (normalize-space(eac:dateRange/eac:fromDate/@standardDate))
                                                else(
                                                if (normalize-space(eac:dateRange/eac:fromDate)!='')
                                                then (normalize-space(eac:dateRange/eac:fromDate))
                                                else()
                                                )
                                                "/>
                                            -->
                                            
                                        </RiC:beginningDate>
                                    </xsl:if>
                                        <xsl:if
                                            test="eac:dateRange/eac:toDate">
                                            
                                            <RiC:endDate>
                                                <xsl:call-template name="outputDate">
                                                    <xsl:with-param name="stdDate" select="eac:dateRange/eac:toDate/@standardDate"></xsl:with-param>
                                                    <xsl:with-param name="date" select="eac:dateRange/eac:toDate"></xsl:with-param>
                                                </xsl:call-template>
                                             <!--   <xsl:choose>
                                                    
                                                    <xsl:when test="string-length(eac:dateRange/eac:toDate/@standardDate)=4">
                                                        <xsl:attribute name="rdf:datatype"> <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text></xsl:attribute>
                                                        
                                                    </xsl:when>
                                                    <xsl:when test="string-length(eac:dateRange/eac:toDate/@standardDate)=7">
                                                        <xsl:attribute name="rdf:datatype">
                                                            <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                                        </xsl:attribute>
                                                    </xsl:when>
                                                    <xsl:when test="string-length(eac:dateRange/eac:toDate/@standardDate)=10">
                                                        <xsl:attribute name="rdf:datatype">
                                                            <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                                        </xsl:attribute>
                                                    </xsl:when>
                                                    <xsl:otherwise/>
                                                </xsl:choose>
                                                
                                                <!-\-  <RiC:endDate rdf:datatype="http://www.w3.org/2001/XMLSchema#dateTime">-\->
                                                <xsl:value-of select="
                                                    if (normalize-space(eac:dateRange/eac:toDate/@standardDate)!='')
                                                    then (normalize-space(eac:dateRange/eac:toDate/@standardDate))
                                                    else(
                                                    if (normalize-space(eac:dateRange/eac:toDate)!='')
                                                    then (normalize-space(eac:dateRange/eac:toDate))
                                                    else()
                                                    )
                                                    "/>-->
                                            </RiC:endDate>
                                        </xsl:if>
                                    
                               <!--     <xsl:if test="eac:date[normalize-space(.) != '']">
                                        <xsl:choose>
                                            <xsl:when test="not(contains(eac:date, '/'))">
                                                <RiC:beginningDate
                                                  rdf:datatype="http://www.w3.org/2001/XMLSchema#dateTime">
                                                  <xsl:value-of select="normalize-space(eac:date)"/>
                                                </RiC:beginningDate>
                                            </xsl:when>
                                        </xsl:choose>
                                    </xsl:if>-->
                                    <xsl:if test="eac:descriptiveNote">
                                        <RiC:description xml:lang="fr">
                                            <xsl:value-of select="eac:descriptiveNote"/>
                                        </RiC:description>
                                    </xsl:if>
                                </rdf:Description>
                            <!--</xsl:if>-->

                        </xsl:for-each>
                        
                        <xsl:for-each select="eac:cpfDescription/eac:description/eac:places/eac:place[normalize-space(eac:placeEntry)!='']">
                           
                            <xsl:variable name="placeName">
                                <xsl:choose>
                                    <xsl:when test="eac:placeRole='Siège social'">
                                        <xsl:choose>
                                            <xsl:when test="parent::eac:places/eac:place[eac:placeRole='Siège']">
                                                <xsl:value-of select="concat(normalize-space(parent::eac:places/eac:place[eac:placeRole='Siège']/eac:placeEntry), '. ', normalize-space(eac:placeEntry))"/>
                                            </xsl:when>
                                            <xsl:otherwise><xsl:value-of select="normalize-space(eac:placeEntry)"/></xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="normalize-space(eac:placeEntry)"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:variable>
                            <xsl:if
                                test="$siaf-places/rdf:Description[rdfs:label = $placeName]">
                                
                                <rdf:Description>
                                    <xsl:attribute name="rdf:about">
                                        <xsl:text>http://piaaf.demo.logilab.fr/resource/FRSIAF_LocationRelation_</xsl:text>
                                        <xsl:value-of select="$recId"/>
                                        <xsl:text>_</xsl:text>
                                        <xsl:value-of
                                            select="generate-id()"/>
                                    </xsl:attribute>
                                    <rdf:type
                                        rdf:resource="http://www.ica.org/standards/RiC/ontology#LocationRelation"/>
                                    <RiC:hasLocation>
                                        <xsl:attribute name="rdf:resource">
                                            <xsl:value-of
                                                select="$siaf-places/rdf:Description[rdfs:label = $placeName]/@rdf:about"
                                            />
                                        </xsl:attribute>
                                    </RiC:hasLocation>
                                    <RiC:locationOf>
                                        <xsl:attribute name="rdf:resource">
                                            <xsl:value-of
                                                select="$URI"
                                            />
                                        </xsl:attribute>
                                    </RiC:locationOf>
                                    <xsl:if
                                        test="eac:dateRange/eac:fromDate">
                                        <RiC:beginningDate>
                                            <xsl:call-template name="outputDate">
                                                <xsl:with-param name="stdDate" select="eac:dateRange/eac:fromDate/@standardDate"></xsl:with-param>
                                                <xsl:with-param name="date" select="eac:dateRange/eac:fromDate"></xsl:with-param>
                                            </xsl:call-template>
                                            <!-- <xsl:choose>
                                                
                                                <xsl:when test="string-length(eac:dateRange/eac:fromDate/@standardDate)=4">
                                                    <xsl:attribute name="rdf:datatype"> <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text></xsl:attribute>
                                                    
                                                </xsl:when>
                                                <xsl:when test="string-length(eac:dateRange/eac:fromDate/@standardDate)=7">
                                                    <xsl:attribute name="rdf:datatype">
                                                        <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                                    </xsl:attribute>
                                                </xsl:when>
                                                <xsl:when test="string-length(eac:dateRange/eac:fromDate/@standardDate)=10">
                                                    <xsl:attribute name="rdf:datatype">
                                                        <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                                    </xsl:attribute>
                                                </xsl:when>
                                                <xsl:otherwise/>
                                            </xsl:choose>
                                            
                                            <xsl:value-of select="
                                                if (normalize-space(eac:dateRange/eac:fromDate/@standardDate)!='')
                                                then (normalize-space(eac:dateRange/eac:fromDate/@standardDate))
                                                else(
                                                if (normalize-space(eac:dateRange/eac:fromDate)!='')
                                                then (normalize-space(eac:dateRange/eac:fromDate))
                                                else()
                                                )
                                                "/>
                                            -->
                                            
                                        </RiC:beginningDate>
                                    </xsl:if>
                                    <xsl:if
                                        test="eac:dateRange/eac:toDate">
                                        
                                        <RiC:endDate>
                                            <xsl:call-template name="outputDate">
                                                <xsl:with-param name="stdDate" select="eac:dateRange/eac:toDate/@standardDate"></xsl:with-param>
                                                <xsl:with-param name="date" select="eac:dateRange/eac:toDate"></xsl:with-param>
                                            </xsl:call-template>
                                            <!--   <xsl:choose>
                                                    
                                                    <xsl:when test="string-length(eac:dateRange/eac:toDate/@standardDate)=4">
                                                        <xsl:attribute name="rdf:datatype"> <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text></xsl:attribute>
                                                        
                                                    </xsl:when>
                                                    <xsl:when test="string-length(eac:dateRange/eac:toDate/@standardDate)=7">
                                                        <xsl:attribute name="rdf:datatype">
                                                            <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                                        </xsl:attribute>
                                                    </xsl:when>
                                                    <xsl:when test="string-length(eac:dateRange/eac:toDate/@standardDate)=10">
                                                        <xsl:attribute name="rdf:datatype">
                                                            <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                                        </xsl:attribute>
                                                    </xsl:when>
                                                    <xsl:otherwise/>
                                                </xsl:choose>
                                                
                                                <!-\-  <RiC:endDate rdf:datatype="http://www.w3.org/2001/XMLSchema#dateTime">-\->
                                                <xsl:value-of select="
                                                    if (normalize-space(eac:dateRange/eac:toDate/@standardDate)!='')
                                                    then (normalize-space(eac:dateRange/eac:toDate/@standardDate))
                                                    else(
                                                    if (normalize-space(eac:dateRange/eac:toDate)!='')
                                                    then (normalize-space(eac:dateRange/eac:toDate))
                                                    else()
                                                    )
                                                    "/>-->
                                        </RiC:endDate>
                                    </xsl:if>
                                    
                                   <!-- <xsl:if test="eac:date[normalize-space(.) != '']">
                                        <xsl:choose>
                                            <xsl:when test="not(contains(eac:date, '/'))">
                                                <RiC:beginningDate
                                                    rdf:datatype="http://www.w3.org/2001/XMLSchema#dateTime">
                                                    <xsl:value-of select="normalize-space(eac:date)"/>
                                                </RiC:beginningDate>
                                            </xsl:when>
                                        </xsl:choose>
                                    </xsl:if>-->
                                    <xsl:if test="eac:descriptiveNote[normalize-space()!='']">
                                        <RiC:description xml:lang="fr">
                                            <xsl:value-of select="eac:descriptiveNote"/>
                                        </RiC:description>
                                    </xsl:if>
                                </rdf:Description>
                                
                            </xsl:if>
                        </xsl:for-each>

                        <!-- la notice  (entité Description) -->
                        <rdf:Description>

                            <xsl:attribute name="rdf:about">
                                <xsl:value-of
                                    select="concat('http://piaaf.demo.logilab.fr/resource/FRSIAF_description_', $recId)"
                                />
                            </xsl:attribute>

                            <rdf:type
                                rdf:resource="http://www.ica.org/standards/RiC/ontology#Description"/>
                            <RiC:describes>
                                <xsl:attribute name="rdf:resource">
                                    <xsl:value-of
                                        select="$URI"
                                    />
                                </xsl:attribute>
                            </RiC:describes>
                            <xsl:if test="eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'created']">
                                <RiC:creationDate>
                                    <xsl:call-template name="outputDate">
                                        <xsl:with-param name="stdDate" select="eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'created']/eac:eventDateTime/@standardDateTime"></xsl:with-param>
                                        <xsl:with-param name="date" select="eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'created']/eac:eventDateTime"></xsl:with-param>
                                    </xsl:call-template>
                                   <!-- <xsl:choose>
                                        
                                        <xsl:when test="string-length(eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'created']/eac:eventDateTime/@standardDate)=4">
                                            <xsl:attribute name="rdf:datatype"> <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text></xsl:attribute>
                                            
                                        </xsl:when>
                                        <xsl:when test="string-length(eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'created']/eac:eventDateTime/@standardDate)=7">
                                            <xsl:attribute name="rdf:datatype">
                                                <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                            </xsl:attribute>
                                        </xsl:when>
                                        <xsl:when test="string-length(eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'created']/eac:eventDateTime/@standardDate)=10">
                                            <xsl:attribute name="rdf:datatype">
                                                <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                            </xsl:attribute>
                                        </xsl:when>
                                        <xsl:otherwise/>
                                    </xsl:choose>
                                    
                                    <!-\-  <RiC:endDate rdf:datatype="http://www.w3.org/2001/XMLSchema#dateTime">-\->
                                    <xsl:value-of select="
                                        if (normalize-space(eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'created']/eac:eventDateTime/@standardDate)!='')
                                        then (normalize-space(eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'created']/eac:eventDateTime/@standardDate))
                                        else(
                                        if (normalize-space(eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'created']/eac:eventDateTime)!='')
                                        then (normalize-space(eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'created']/eac:eventDateTime))
                                        else()
                                        )
                                        "/>
                                    
                                    -->
                                   <!-- <xsl:value-of
                                        select="eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'created']/eac:eventDateTime/@standardDateTime"/>-->
                                    
                                </RiC:creationDate>
                            </xsl:if>
                            <RiC:lastUpdateDate>
                              <!--  <xsl:value-of
                                    select="eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'revised'][last()]/eac:eventDateTime/@standardDateTime"
                                />-->
                                <xsl:call-template name="outputDate">
                                    <xsl:with-param name="stdDate" select="eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'revised'][last()]/eac:eventDateTime/@standardDateTime"></xsl:with-param>
                                    <xsl:with-param name="date" select="eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'revised'][last()]/eac:eventDateTime"></xsl:with-param>
                                </xsl:call-template>
                               <!-- <xsl:choose>
                                    
                                    <xsl:when test="string-length(eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'revised'][last()]/eac:eventDateTime/@standardDate)=4">
                                        <xsl:attribute name="rdf:datatype"> <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text></xsl:attribute>
                                        
                                    </xsl:when>
                                    <xsl:when test="string-length(eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'revised'][last()]/eac:eventDateTime/@standardDate)=7">
                                        <xsl:attribute name="rdf:datatype">
                                            <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                        </xsl:attribute>
                                    </xsl:when>
                                    <xsl:when test="string-length(eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'revised'][last()]/eac:eventDateTime/@standardDate)=10">
                                        <xsl:attribute name="rdf:datatype">
                                            <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                        </xsl:attribute>
                                    </xsl:when>
                                    <xsl:otherwise/>
                                </xsl:choose>
                                
                                <!-\-  <RiC:endDate rdf:datatype="http://www.w3.org/2001/XMLSchema#dateTime">-\->
                                <xsl:value-of select="
                                    if (normalize-space(eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'revised'][last()]/eac:eventDateTime/@standardDate)!='')
                                    then (normalize-space(eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'revised'][last()]/eac:eventDateTime/@standardDate))
                                    else(
                                    if (normalize-space(eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'revised'][last()]/eac:eventDateTime)!='')
                                    then (normalize-space(eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'revised'][last()]/eac:eventDateTime))
                                    else()
                                    )
                                    "/>
                                -->
                                
                              <!--  <xsl:value-of
                                    select="eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'revised'][last()]/eac:eventDateTime/@standardDateTime"/>-->
                            </RiC:lastUpdateDate>
                            <RiC:identifier>
                                <xsl:value-of select="eac:control/eac:recordId"/>
                            </RiC:identifier>
                            <RiC:authoredBy
                                rdf:resource="http://piaaf.demo.logilab.fr/resource/FRAN_corporate-body_005568"/>
                            <xsl:for-each select="eac:control/eac:sources/eac:source[normalize-space(eac:sourceEntry)!='']">
                                <RiC:evidencedBy xml:lang="fr">
                                    <xsl:value-of select="normalize-space(eac:sourceEntry)"/>
                                    <xsl:if test="@xlink:href[normalize-space(.) != '']">
                                        <xsl:value-of
                                            select="concat(' (', normalize-space(@xlink:href), ')')"
                                        />
                                    </xsl:if>
                                </RiC:evidencedBy>
                            </xsl:for-each>
                            <xsl:for-each select="eac:control/descendant::eac:conventionDeclaration">
                                <xsl:variable name="abbr" select="normalize-space(eac:abbreviation)"/>
                                <!--<xsl:variable name="link" select="eac:citation/@xlink:href"/>-->
                                <xsl:if
                                    test="$siaf-mandates/rdf:Description[rdfs:label = $abbr and rdf:type[@rdf:resource = 'http://www.ica.org/standards/RiC/ontology#Mandate']]">
                                    <RiC:conformsToRule>
                                        <xsl:attribute name="rdf:resource">
                                            <xsl:value-of
                                                select="$siaf-mandates/rdf:Description[rdfs:label = $abbr and rdf:type[@rdf:resource = 'http://www.ica.org/standards/RiC/ontology#Mandate']]/@rdf:about"/>

                                        </xsl:attribute>

                                    </RiC:conformsToRule>

                                </xsl:if>


                            </xsl:for-each>
                            <xsl:if test="eac:cpfDescription/eac:identity/eac:entityId[@localType='ISNI' and normalize-space(.)!='']">
                                <xsl:variable name="ISNIValue" select="normalize-space(eac:cpfDescription/eac:identity/eac:entityId[@localType='ISNI' and normalize-space(.)!=''])"></xsl:variable>
                                <xsl:variable name="ISNIId">
                                    <xsl:choose>
                                        <xsl:when test="starts-with($ISNIValue, 'ISNI:')">
                                            <xsl:value-of select="replace(substring-after($ISNIValue, 'ISNI:'), ' ', '')"/>
                                        </xsl:when>
                                        <xsl:when test="starts-with($ISNIValue, 'ISNI :')">
                                            <xsl:value-of select="replace(substring-after($ISNIValue, 'ISNI :'), ' ', '')"/>
                                        </xsl:when>
                                        <xsl:when test="starts-with($ISNIValue, 'ISNI')">
                                            <xsl:value-of select="replace(substring-after($ISNIValue, 'ISNI'), ' ', '')"/>
                                        </xsl:when>
                                    </xsl:choose>
                                </xsl:variable>
                                <!-- http://isni.org/isni/0000000121032683-->
                                <rdfs:seeAlso>
                                    <xsl:attribute name="rdf:resource">
                                        <xsl:value-of select="concat('http://isni.org/isni/', $ISNIId)"/>
                                    </xsl:attribute>
                                </rdfs:seeAlso>
                                <!--  <owl:sameAs>
                                    <xsl:attribute name="rdf:resource">
                                        
                                    </xsl:attribute>
                                </owl:sameAs>   -->
                            </xsl:if>
                            <xsl:for-each
                                select="eac:cpfDescription/eac:relations/eac:cpfRelation[@cpfRelationType = 'identity']">
                                <!-- identité -->
                                <!-- http://data.bnf.fr/ark:/12148/cb11863993z
                            http://catalogue.bnf.fr/ark:/12148/cb11863993z/PUBLIC
                            http://catalogue.bnf.fr/ark:/12148/cb11863993z/PUBLIC
                            http://data.bnf.fr/ark:/12148/cb11863993z#foaf:Organization-->
                                <!-- http://catalogue.bnf.fr/ark:/12148/cb11904421w -->
                                <!-- http://data.bnf.fr/ark:/12148/cb11862469g-->
                                <xsl:variable name="lnk" select="normalize-space(@xlink:href)"/>
                                <xsl:if test="$lnk!=''">
                                    <rdfs:seeAlso>
                                        <xsl:attribute name="rdf:resource">
                                            <xsl:if test="contains($lnk, 'bnf.fr')">
                                                <xsl:choose>
                                                    <xsl:when
                                                        test="contains($lnk, 'catalogue.bnf.fr/ark:/12148/') and contains($lnk, '/PUBLIC')">
                                                        <xsl:text>http://data.bnf.fr/ark:/12148/</xsl:text>
                                                        <xsl:value-of
                                                            select="substring-before(substring-after($lnk, '12148/'), '/PUBLIC')"/>
                                                        
                                                    </xsl:when>
                                                    <xsl:when test="contains($lnk, 'catalogue.bnf.fr/ark:/12148/') and not(contains($lnk, '/PUBLIC'))">
                                                        <xsl:text>http://data.bnf.fr/ark:/12148/</xsl:text>
                                                        <xsl:value-of
                                                            select="substring-after($lnk, '12148/')"/>
                                                        
                                                    </xsl:when>
                                                    <xsl:when test="contains($lnk, 'http://data.bnf.fr/ark:/12148/')">
                                                        <xsl:value-of select="$lnk"/>
                                                        
                                                    </xsl:when>
                                                </xsl:choose>
                                            </xsl:if>
                                            <!-- liens AN vers notices wikipedia -->
                                            <!-- exemple : http://fr.dbpedia.org/page/Henri_Labrouste, https://fr.wikipedia.org/wiki/Henri_Labrouste -->
                                            <xsl:if test="contains($lnk, 'wikipedia.org')">
                                                
                                                <xsl:value-of select="$lnk"/>
                                            </xsl:if>
                                            
                                        </xsl:attribute>
                                    </rdfs:seeAlso>
                                </xsl:if>
                            </xsl:for-each>
                         
                        </rdf:Description>

                    </rdf:RDF>
                </xsl:result-document>
            </xsl:for-each>





        </xsl:if>

<xsl:if test="$coll = 'FRBNF'">

            <xsl:for-each
                select="$collection-EAC-BnF/eac:eac-cpf">
                <xsl:variable name="recId" select="normalize-space(eac:control/eac:recordId)"/>
                <xsl:variable name="entType" select="eac:cpfDescription/eac:identity/eac:entityType"/>
                <xsl:variable name="entTypeName">
                    <xsl:choose>
                        <xsl:when test="$entType = 'corporateBody'">corporate-body</xsl:when>
                        <xsl:when test="$entType = 'person'">person</xsl:when>
                    </xsl:choose>
                </xsl:variable>
                <xsl:variable name="RiCClass">
                    <xsl:choose>
                        <xsl:when test="$entType = 'corporateBody'">CorporateBody</xsl:when>
                        <xsl:when test="$entType = 'person'">Person</xsl:when>
                    </xsl:choose>
                </xsl:variable>
                <xsl:variable name="theGoodRecId" select="substring-after($recId, 'DGEARC_')"/>
                
                <xsl:variable name="URI"
                    select="concat('http://piaaf.demo.logilab.fr/resource/', $coll, '_', $entTypeName, '_', $theGoodRecId)"/>
                
                <xsl:result-document method="xml" encoding="utf-8" indent="yes" href="rdf/agents/{$coll}_{$entTypeName}_{$theGoodRecId}.rdf">
                   
                    <rdf:RDF 
                        xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                        xmlns:owl="http://www.w3.org/2002/07/owl#"
                        xmlns:RiC="http://www.ica.org/standards/RiC/ontology#"
                        xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
                        xmlns:piaaf-onto="http://piaaf.demo.logilab.fr/ontology#"
                        xmlns:skos="http://www.w3.org/2004/02/skos/core#"
                        xmlns:isni="http://isni.org/ontology#">
                        <rdf:Description>
                            <xsl:attribute name="rdf:about">
                                <xsl:value-of select="$URI"/>
                            </xsl:attribute>
                            <rdf:type
                                rdf:resource="http://www.ica.org/standards/RiC/ontology#{$RiCClass}"/>
                            <rdfs:label xml:lang="fr">
                                <xsl:value-of select="
                                    if (count(eac:cpfDescription/eac:identity/eac:nameEntry[@localType = 'preferredFormForProject']/eac:part)=1)
                                    then (normalize-space(eac:cpfDescription/eac:identity/eac:nameEntry[@localType = 'preferredFormForProject']/eac:part))
                                    else (
                                    concat(eac:cpfDescription/eac:identity/eac:nameEntry[@localType = 'preferredFormForProject']/eac:part[@localType='nom'], ', ', eac:cpfDescription/eac:identity/eac:nameEntry[@localType = 'preferredFormForProject']/eac:part[@localType='prenom'], ' (', eac:cpfDescription/eac:identity/eac:nameEntry[@localType = 'preferredFormForProject']/eac:part[@localType='dates_biographiques'], ')')
                                    )
                                    
                                    
                                    "/>
                              
                            </rdfs:label>
                            <!-- relation vers chacun des noms -->
                            <xsl:for-each select="eac:cpfDescription/eac:identity/eac:nameEntry">
                                <RiC:hasDenominationRelation>
                                    <xsl:attribute name="rdf:resource">
                                        <xsl:value-of
                                            select="concat('http://piaaf.demo.logilab.fr/resource/FRBNF_DenominationRelation_', $theGoodRecId, '_', generate-id())"/>

                                    </xsl:attribute>
                                </RiC:hasDenominationRelation>
                            </xsl:for-each>
                            <!-- relation vers chacun des statuts juridiques -->
                            <xsl:for-each select="eac:cpfDescription/eac:description/descendant::eac:legalStatus">
                                <!--<xsl:choose>
                                    <xsl:when test="normalize-space(eac:term)='statut indéterminé à ce jour'">
                                        
                                    </xsl:when>
                                    <xsl:otherwise></xsl:otherwise>
                                </xsl:choose>-->
                                <RiC:hasTypeRelation>
                                    
                                    <xsl:attribute name="rdf:resource">
                                        <xsl:value-of
                                            select="concat('http://piaaf.demo.logilab.fr/resource/FRBNF_TypeRelation_', $theGoodRecId, '_', generate-id())"/>
                                        
                                    </xsl:attribute>
                                </RiC:hasTypeRelation>
                            </xsl:for-each>
                            <xsl:choose>
                                <xsl:when test="$entType='person'">
                                    <xsl:if test="eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate">
                                        <RiC:birthDate>
                                            <xsl:call-template name="outputDate">
                                                <xsl:with-param name="stdDate" select="eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate/@standardDate"></xsl:with-param>
                                                <xsl:with-param name="date" select="eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate"></xsl:with-param>
                                            </xsl:call-template>
                                            
                                            <!--  <xsl:choose>
                                            
                                            <xsl:when test="string-length(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate/@standardDate)=4">
                                                <xsl:attribute name="rdf:datatype"> <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text></xsl:attribute>
                                                
                                            </xsl:when>
                                            <xsl:when test="string-length(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate/@standardDate)=7">
                                                <xsl:attribute name="rdf:datatype">
                                                    <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                                </xsl:attribute>
                                            </xsl:when>
                                            <xsl:when test="string-length(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate/@standardDate)=10">
                                                <xsl:attribute name="rdf:datatype">
                                                    <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                                </xsl:attribute>
                                            </xsl:when>
                                            <xsl:otherwise/>
                                        </xsl:choose>
                                        
                                        <xsl:value-of select="
                                            if (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate/@standardDate)!='')
                                            then (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate/@standardDate))
                                            else(
                                            if (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate)!='')
                                            then (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate))
                                            else()
                                            )
                                            "/>
                                        -->
                                            
                                            
                                            <!--  <xsl:value-of
                                            select="eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate/@standardDate"
                                        />-->
                                        </RiC:birthDate>
                                    </xsl:if>
                                    <xsl:if
                                        test="eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate">
                                        <RiC:deathDate>
                                            <xsl:call-template name="outputDate">
                                                <xsl:with-param name="stdDate" select="eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate/@standardDate"></xsl:with-param>
                                                <xsl:with-param name="date" select="eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate"></xsl:with-param>
                                            </xsl:call-template>
                                            <!-- <xsl:choose>
                                                
                                                <xsl:when test="string-length(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate/@standardDate)=4">
                                                    <xsl:attribute name="rdf:datatype"> <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text></xsl:attribute>
                                                    
                                                </xsl:when>
                                                <xsl:when test="string-length(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate/@standardDate)=7">
                                                    <xsl:attribute name="rdf:datatype">
                                                        <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                                    </xsl:attribute>
                                                </xsl:when>
                                                <xsl:when test="string-length(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate/@standardDate)=10">
                                                    <xsl:attribute name="rdf:datatype">
                                                        <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                                    </xsl:attribute>
                                                </xsl:when>
                                                <xsl:otherwise/>
                                            </xsl:choose>
                                            
                                            <!-\-  <RiC:endDate rdf:datatype="http://www.w3.org/2001/XMLSchema#dateTime">-\->
                                            <xsl:value-of select="
                                                if (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate/@standardDate)!='')
                                                then (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate/@standardDate))
                                                else(
                                                if (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate)!='')
                                                then (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate))
                                                else()
                                                )
                                                "/>-->
                                            
                                            
                                            <!--      <xsl:value-of
                                                select="eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate/@standardDate"
                                            />-->
                                        </RiC:deathDate>
                                    </xsl:if>
                                </xsl:when>
                                <xsl:when test="$entType='corporateBody'">
                                    <xsl:if test="eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate">
                                        <RiC:beginningDate>
                                            <xsl:call-template name="outputDate">
                                                <xsl:with-param name="stdDate" select="eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate/@standardDate"></xsl:with-param>
                                                <xsl:with-param name="date" select="eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate"></xsl:with-param>
                                            </xsl:call-template>
                                            <!--     <xsl:choose>
                                            
                                            <xsl:when test="string-length(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate/@standardDate)=4">
                                                <xsl:attribute name="rdf:datatype"> <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text></xsl:attribute>
                                                
                                            </xsl:when>
                                            <xsl:when test="string-length(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate/@standardDate)=7">
                                                <xsl:attribute name="rdf:datatype">
                                                    <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                                </xsl:attribute>
                                            </xsl:when>
                                            <xsl:when test="string-length(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate/@standardDate)=10">
                                                <xsl:attribute name="rdf:datatype">
                                                    <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                                </xsl:attribute>
                                            </xsl:when>
                                            <xsl:otherwise/>
                                        </xsl:choose>
                                        
                                        <xsl:value-of select="
                                            if (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate/@standardDate)!='')
                                            then (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate/@standardDate))
                                            else(
                                            if (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate)!='')
                                            then (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate))
                                            else()
                                            )
                                            "/>
                                        -->
                                            
                                        </RiC:beginningDate>
                                    </xsl:if>
                                    <xsl:if
                                        test="eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate">
                                        <RiC:endDate>
                                            <xsl:call-template name="outputDate">
                                                <xsl:with-param name="stdDate" select="eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate/@standardDate"></xsl:with-param>
                                                <xsl:with-param name="date" select="eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate"></xsl:with-param>
                                            </xsl:call-template>
                                            <!--   <xsl:choose>
                                                
                                                <xsl:when test="string-length(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate/@standardDate)=4">
                                                    <xsl:attribute name="rdf:datatype"> <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text></xsl:attribute>
                                                    
                                                </xsl:when>
                                                <xsl:when test="string-length(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate/@standardDate)=7">
                                                    <xsl:attribute name="rdf:datatype">
                                                        <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                                    </xsl:attribute>
                                                </xsl:when>
                                                <xsl:when test="string-length(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate/@standardDate)=10">
                                                    <xsl:attribute name="rdf:datatype">
                                                        <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                                    </xsl:attribute>
                                                </xsl:when>
                                                <xsl:otherwise/>
                                            </xsl:choose>
                                            
                                            <!-\-  <RiC:endDate rdf:datatype="http://www.w3.org/2001/XMLSchema#dateTime">-\->
                                            <xsl:value-of select="
                                                if (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate/@standardDate)!='')
                                                then (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate/@standardDate))
                                                else(
                                                if (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate)!='')
                                                then (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate))
                                                else()
                                                )
                                                "/>
                                            -->
                                            
                                        </RiC:endDate>
                                    </xsl:if>
                                </xsl:when>
                            </xsl:choose>
                            
                     
                            <xsl:if
                                test="eac:cpfDescription/eac:description/eac:function[not(@localType)]/*[not(self::eac:term) and normalize-space(.)!='']">
                                
                                <RiC:description xml:lang="fr">
                                    <xsl:for-each select="eac:cpfDescription/eac:description/eac:function[not(@localType)]/*[not(self::eac:term) and normalize-space(.)!='']">
                                        <xsl:apply-templates></xsl:apply-templates>
                                        <xsl:if test="position()!=last()">
                                            <xsl:text> </xsl:text>
                                        </xsl:if>
                                    </xsl:for-each>
                                    
                                </RiC:description>
                                
                            </xsl:if>
                            <xsl:if
                                test="eac:cpfDescription/eac:description/eac:functions[eac:p]">
                                
                                <RiC:description xml:lang="fr">
                                    <xsl:for-each select="eac:cpfDescription/eac:description/eac:functions/eac:p[normalize-space(.)!='']">
                                        <xsl:value-of
                                            select="."
                                        />
                                        <xsl:if test="position()!=last()"><xsl:text> </xsl:text></xsl:if>
                                    </xsl:for-each>
                                </RiC:description>
                            </xsl:if>
                            <xsl:if test="eac:cpfDescription/eac:description/eac:biogHist/*[not(self::eac:chronList) and normalize-space(.)!='']">
                                <RiC:history xml:lang="fr">
                                    <xsl:for-each
                                        select="eac:cpfDescription/eac:description/eac:biogHist/*[not(self::eac:chronList) and normalize-space(.)!='']">
                                        <xsl:apply-templates/>
                                        <xsl:if test="position() != last()">
                                            <xsl:text> </xsl:text>
                                        </xsl:if>
                                    </xsl:for-each>
                                </RiC:history>
                                
                            </xsl:if>
                            <xsl:if
                                test="eac:cpfDescription/eac:description/eac:structureOrGenealogy[normalize-space(.) != '']">
                                <RiC:noteOnStructureOrGenealogy xml:lang="fr">
                                    <xsl:for-each select="eac:cpfDescription/eac:description/eac:structureOrGenealogy/*[normalize-space(.)!='']">
                                        <xsl:apply-templates/>
                                        <xsl:if test="position() != last()">
                                            <xsl:text> </xsl:text>
                                        </xsl:if>
                                    </xsl:for-each>
                                    
                                </RiC:noteOnStructureOrGenealogy>
                                
                            </xsl:if>
                                 
                               
                                   <xsl:for-each
                                       select="eac:cpfDescription/eac:description/descendant::eac:mandate">
                                       
                                      
                                           <xsl:variable name="cit" select="normalize-space(eac:citation)"/>
                                           <xsl:variable name="citation" select="
                                               if (eac:citation[@xlink:href!=''])
                                               then (normalize-space(eac:citation/@xlink:href))
                                               else ('non')
                                               "/>
                                           <!--<xsl:variable name="link" select="eac:citation/@xlink:href"/>-->
                                           <xsl:if test="$bnf-mandates/rdf:Description[RiC:hasTitle=$cit and rdf:type[@rdf:resource='http://www.ica.org/standards/RiC/ontology#Mandate']]">
                                               
                                               <xsl:choose>
                                                   <xsl:when test="$citation='non'">
                                                       <xsl:for-each select="$bnf-mandates/rdf:Description[RiC:hasTitle=$cit and rdf:type[@rdf:resource='http://www.ica.org/standards/RiC/ontology#Mandate']]">
                                                           <RiC:conformsToRule>
                                                               <xsl:attribute name="rdf:resource">
                                                                   <xsl:value-of select="@rdf:about"/>
                                                                   
                                                               </xsl:attribute>
                                                               
                                                           </RiC:conformsToRule>
                                                       </xsl:for-each>
                                                       
                                                   </xsl:when>
                                                   <xsl:when test="$citation!='non'">
                                                     <!--  <xsl:comment>citation pas vide</xsl:comment>-->
                                                       <xsl:for-each select="$bnf-mandates/rdf:Description[RiC:hasTitle=$cit and rdf:type[@rdf:resource='http://www.ica.org/standards/RiC/ontology#Mandate']]">
                                                           
                                                           
                                                           <xsl:choose>
                                                               <xsl:when test="RiC:evidencedBy">
                                                                   <xsl:variable name="theEvidence" select="RiC:evidencedBy/@rdf:resource"/>
                                                                  <!-- <xsl:comment>il y a un RiC:evidencedBy</xsl:comment>-->
                                                                   <xsl:if test="ancestor::rdf:RDF/rdf:Description[@rdf:about=$theEvidence and RiC:identifier=$citation]">
                                                                       
                                                                       <RiC:conformsToRule>
                                                                           <xsl:attribute name="rdf:resource">
                                                                               <xsl:value-of select="@rdf:about"/>
                                                                               
                                                                           </xsl:attribute>
                                                                           
                                                                       </RiC:conformsToRule>
                                                                   </xsl:if>
                                                                   
                                                                   
                                                                   
                                                               </xsl:when>
                                                           </xsl:choose>
                                                           
                                                    
                                                           
                                                       </xsl:for-each>    
                                                   </xsl:when>
                                               </xsl:choose>
                                               
                                         
                                               
                                               
                                               
                                           </xsl:if>
                                           
                                    
                                       
                                       
                                   </xsl:for-each>
                                 
                                   <xsl:for-each
                                       select="eac:cpfDescription/eac:description/descendant::eac:function[@localType = 'piaafFunction']">
                                       <xsl:variable name="functionName">
                                        <!--   <xsl:for-each select="eac:term">
                                               <xsl:value-of select="."/>
                                               <xsl:if test="position() != last()">
                                                   <xsl:text> -\- </xsl:text>
                                               </xsl:if>
                                           </xsl:for-each>-->
                                           <xsl:value-of select="eac:term[1]"/>
                                       </xsl:variable>
                                       <xsl:if
                                           test="$functions/rdf:Description[rdfs:label = $functionName]">
                                           <piaaf-onto:hasFunctionOfType>
                                               <xsl:attribute name="rdf:resource">
                                                   <xsl:value-of
                                                       select="$functions/rdf:Description[rdfs:label = $functionName]/@rdf:about"
                                                   />
                                               </xsl:attribute>
                                           </piaaf-onto:hasFunctionOfType>
                                       </xsl:if>
                                       
                                   </xsl:for-each>
                                 
                            
                            
                        
                          
                            <!--<xsl:for-each select="eac:cpfDescription/eac:description/eac:places/eac:place[normalize-space(eac:placeEntry)!='']">
                                <!-\-  <xsl:choose>
                                    <xsl:when test="eac:placeRole='Siège social'">
                                        <xsl:choose>
                                            <xsl:when test="parent::eac:places/eac:place[eac:placeRole='Siège']">
                                                <xsl:value-of select="concat(normalize-space(parent::eac:places/eac:place[eac:placeRole='Siège']/eac:placeEntry), '. ', current-grouping-key())"/>
                                            </xsl:when>
                                            <xsl:otherwise><xsl:value-of select="current-grouping-key()"/></xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="current-grouping-key()"/>
                                    </xsl:otherwise>
                                </xsl:choose>-\->
                                <xsl:variable name="placeName">
                                    <xsl:choose>
                                        <xsl:when test="eac:placeRole='Siège social'">
                                            <xsl:choose>
                                                <xsl:when test="parent::eac:places/eac:place[eac:placeRole='Siège']">
                                                    <xsl:value-of select="concat(normalize-space(parent::eac:places/eac:place[eac:placeRole='Siège']/eac:placeEntry), '. ', normalize-space(eac:placeEntry))"/>
                                                </xsl:when>
                                                <xsl:otherwise><xsl:value-of select="normalize-space(eac:placeEntry)"/></xsl:otherwise>
                                            </xsl:choose>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:value-of select="normalize-space(eac:placeEntry)"/>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </xsl:variable>
                                <xsl:if
                                    test="$bnf-places/rdf:Description[rdfs:label = $placeName]">
                                    <!-\-<RiC:hasPlace>
                                               <xsl:attribute name="rdf:resource">
                                                   <xsl:value-of
                                                       select="$siaf-places/rdf:Description[rdfs:label = $placeName]/@rdf:about"
                                                   />
                                               </xsl:attribute>
                                           </RiC:hasPlace>-\->
                                    <!-\- ici il faut une relation n-aire comme pour les statuts juridiques -\->
                                    <RiC:hasLocationRelation>
                                        <xsl:attribute name="rdf:resource">
                                            <xsl:value-of
                                                select="concat('http://www.piaaf.net/relations/FRBNF_LocationRelation_', $theGoodRecId, '_', generate-id())"/>
                                            
                                        </xsl:attribute>
                                    </RiC:hasLocationRelation>
                                    
                                </xsl:if>
                            </xsl:for-each>-->
                          
                      

                           
                         
                            <xsl:for-each
                                select="eac:cpfDescription/eac:relations/eac:cpfRelation[@cpfRelationType = 'identity']">
                                <!-- identité -->
                                <!-- http://data.bnf.fr/ark:/12148/cb11863993z
                            http://catalogue.bnf.fr/ark:/12148/cb11863993z/PUBLIC
                            http://catalogue.bnf.fr/ark:/12148/cb11863993z/PUBLIC
                            http://data.bnf.fr/ark:/12148/cb11863993z#foaf:Organization-->
                                <!-- http://catalogue.bnf.fr/ark:/12148/cb11904421w -->
                                <!-- http://data.bnf.fr/ark:/12148/cb11862469g-->
                                <xsl:variable name="lnk" select="normalize-space(@xlink:href)"/>
                                <owl:sameAs>
                                    <xsl:attribute name="rdf:resource">
                                        <xsl:if test="contains($lnk, 'bnf.fr')">
                                            <xsl:choose>
                                                <xsl:when
                                                  test="contains($lnk, 'catalogue.bnf.fr/ark:/12148/') and contains($lnk, '/PUBLIC')">
                                                  <xsl:text>http://data.bnf.fr/ark:/12148/</xsl:text>
                                                  <xsl:value-of
                                                  select="substring-before(substring-after($lnk, 'http://catalogue.bnf.fr/ark:/12148/'), '/PUBLIC')"/>
                                                    <xsl:choose>
                                                        <xsl:when test="$RiCClass='CorporateBody'"><xsl:text>#foaf:Organization</xsl:text></xsl:when>
                                                        <xsl:when test="$RiCClass='Person'"><xsl:text>#foaf:Person</xsl:text></xsl:when>
                                                    </xsl:choose>
                                                    
                                                </xsl:when>
                                                <xsl:when test="contains($lnk, 'catalogue.bnf.fr/ark:/12148/') and not(contains($lnk, '/PUBLIC'))">
                                                    <xsl:text>http://data.bnf.fr/ark:/12148/</xsl:text>
                                                    <xsl:value-of
                                                        select="substring-after($lnk, 'http://catalogue.bnf.fr/ark:/12148/')"/>
                                                    <xsl:choose>
                                                        <xsl:when test="$RiCClass='CorporateBody'"><xsl:text>#foaf:Organization</xsl:text></xsl:when>
                                                        <xsl:when test="$RiCClass='Person'"><xsl:text>#foaf:Person</xsl:text></xsl:when>
                                                    </xsl:choose>
                                                </xsl:when>
                                                <xsl:when test="contains($lnk, 'http://data.bnf.fr/ark:/12148/')">
                                                    <xsl:value-of select="$lnk"/>
                                                    <xsl:choose>
                                                        <xsl:when test="$RiCClass='CorporateBody'"><xsl:text>#foaf:Organization</xsl:text></xsl:when>
                                                        <xsl:when test="$RiCClass='Person'"><xsl:text>#foaf:Person</xsl:text></xsl:when>
                                                    </xsl:choose>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:if>

                                    </xsl:attribute>
                                </owl:sameAs>
                            </xsl:for-each>
                            <!-- les ISNI -->
                           
                                <!--<xsl:if test="eac:cpfDescription/eac:identity/eac:entityId[starts-with(normalize-space(.), 'ISNI')]">-->
                            <xsl:for-each select="eac:cpfDescription/eac:identity/eac:entityId[starts-with(normalize-space(.), 'ISNI')]">
                                        <xsl:variable name="ISNIValue" select="normalize-space(.)"></xsl:variable>
                            <!--    <xsl:variable name="ISNIValue" select="normalize-space(eac:cpfDescription/eac:identity/eac:entityId[starts-with(normalize-space(.), 'ISNI')])"></xsl:variable>-->
                                <xsl:variable name="ISNIId">
                                    <xsl:choose>
                                        <xsl:when test="starts-with($ISNIValue, 'ISNI:')">
                                            <xsl:value-of select="replace(substring-after($ISNIValue, 'ISNI:'), ' ', '')"/>
                                        </xsl:when>
                                        <xsl:when test="starts-with($ISNIValue, 'ISNI :')">
                                            <xsl:value-of select="replace(substring-after($ISNIValue, 'ISNI :'), ' ', '')"/>
                                        </xsl:when>
                                        <xsl:when test="starts-with($ISNIValue, 'ISNI')">
                                            <xsl:value-of select="replace(substring-after($ISNIValue, 'ISNI'), ' ', '')"/>
                                        </xsl:when>
                                    </xsl:choose>
                                </xsl:variable>
                                <!-- http://isni.org/isni/0000000121032683-->
                                <isni:identifierValid><xsl:value-of select="$ISNIId"/></isni:identifierValid>
                                
                            
                                    </xsl:for-each>
                            <!--</xsl:if>-->
                            
                            <!-- les relations cpfType autres que identité -->
                            <xsl:for-each
                                select="eac:cpfDescription/eac:relations/eac:cpfRelation[contains(@xlink:arcrole, 'isControlledBy')]">
                                <xsl:call-template name="outputObjectPropertyForRelation">
                                    <xsl:with-param name="link"
                                        select="
                                            if (normalize-space(@xlink:href) = '' or not(@xlink:href))
                                            then
                                                ('non')
                                            else
                                                (normalize-space(@xlink:href))
                                            "/>
                                    <xsl:with-param name="relEntry"
                                        select="normalize-space(eac:relationEntry)"/>
                                    <xsl:with-param name="recId" select="$theGoodRecId"/>
                                    <xsl:with-param name="srcRelType" select="'isControlledBy'"/>
                                    <xsl:with-param name="arcToRelName"
                                        select="'agentControlledBy'"/>
                                    <xsl:with-param name="toAncRelArcName" select="'controlOnAgent'"/>
                                    <xsl:with-param name="relName" select="'AgentControlRelation'"/>
                                    <xsl:with-param name="relSecondArcName"
                                        select="'agentControlHeldBy'"/>
                                  
                                    <xsl:with-param name="relFromDate"
                                        select="   if (eac:dateRange/eac:fromDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:fromDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:fromDate))
                                        
                                        "/>
                                    <xsl:with-param name="relToDate"
                                        select="
                                        
                                        if (eac:dateRange/eac:toDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:toDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:toDate))
                                        
                                        
                                        
                                        
                                        "/>
                                        
                                    <xsl:with-param name="relDate"
                                        select="
                                        if (eac:date/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:date/@standardDate))
                                        else
                                        (normalize-space(eac:date))"
                                        
                                        
                                        >
                                        
                                    </xsl:with-param>
                                 
                                    <xsl:with-param name="coll" select="$coll"/>
                                </xsl:call-template>


                            </xsl:for-each>
                            <xsl:for-each
                                select="eac:cpfDescription/eac:relations/eac:cpfRelation[contains(@xlink:arcrole, 'isDirectorOf')]">
                                <xsl:call-template name="outputObjectPropertyForRelation">
                                    <xsl:with-param name="link"
                                        select="
                                        if (normalize-space(@xlink:href) = '' or not(@xlink:href))
                                        then
                                        ('non')
                                        else
                                        (normalize-space(@xlink:href))
                                        "/>
                                    <xsl:with-param name="relEntry"
                                        select="normalize-space(eac:relationEntry)"/>
                                    <xsl:with-param name="recId" select="$theGoodRecId"/>
                                    <xsl:with-param name="srcRelType" select="'isDirectorOf'"/>
                                    <xsl:with-param name="arcToRelName"
                                        select="'agentHasLeadershipRelation'"/>
                                    <xsl:with-param name="toAncRelArcName" select="'leadershipBy'"/>
                                    <xsl:with-param name="relName" select="'AgentLeadershipRelation'"/>
                                    <xsl:with-param name="relSecondArcName"
                                        select="'leadershipOn'"/>
                                 <!--   <xsl:with-param name="shortcutRelName" select="'controlledBy'"/>-->
                                    <xsl:with-param name="relFromDate"
                                        select="   if (eac:dateRange/eac:fromDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:fromDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:fromDate))
                                        
                                        "/>
                                    <xsl:with-param name="relToDate"
                                        select="
                                        
                                        if (eac:dateRange/eac:toDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:toDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:toDate))
                                        
                                        
                                        
                                        
                                        "/>
                                    <!--<xsl:with-param name="relNote"
                                        select="normalize-space(eac:descriptiveNote/eac:p)"/>
                                    -->
                                    <xsl:with-param name="relDate"
                                        select="
                                        if (eac:date/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:date/@standardDate))
                                        else
                                        (normalize-space(eac:date))"
                                        
                                        
                                        >
                                        
                                    </xsl:with-param>
                                    <xsl:with-param name="coll" select="$coll"/>
                                </xsl:call-template>
                                
                                
                            </xsl:for-each>
                            <xsl:for-each
                                select="eac:cpfDescription/eac:relations/eac:cpfRelation[contains(@xlink:arcrole, 'isDirectedBy')]">
                                <xsl:call-template name="outputObjectPropertyForRelation">
                                    <xsl:with-param name="link"
                                        select="
                                        if (normalize-space(@xlink:href) = '' or not(@xlink:href))
                                        then
                                        ('non')
                                        else
                                        (normalize-space(@xlink:href))
                                        "/>
                                    <xsl:with-param name="relEntry"
                                        select="normalize-space(eac:relationEntry)"/>
                                    <xsl:with-param name="recId" select="$theGoodRecId"/>
                                    <xsl:with-param name="srcRelType" select="'isDirectedBy'"/>
                                    <xsl:with-param name="arcToRelName"
                                        select="'leadBy'"/>
                                    <xsl:with-param name="toAncRelArcName" select="'leadershipOn'"/>
                                    <xsl:with-param name="relName" select="'AgentLeadershipRelation'"/>
                                    <xsl:with-param name="relSecondArcName"
                                        select="'leadershipBy'"/>
                                    <!--   <xsl:with-param name="shortcutRelName" select="'controlledBy'"/>-->
                                    <xsl:with-param name="relFromDate"
                                        select="   if (eac:dateRange/eac:fromDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:fromDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:fromDate))
                                        
                                        "/>
                                    <xsl:with-param name="relToDate"
                                        select="
                                        
                                        if (eac:dateRange/eac:toDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:toDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:toDate))
                                        
                                        
                                        
                                        
                                        "/>
                                    <!--<xsl:with-param name="relNote"
                                        select="normalize-space(eac:descriptiveNote/eac:p)"/>
                                    -->
                                    <xsl:with-param name="relDate"
                                        select="
                                        if (eac:date/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:date/@standardDate))
                                        else
                                        (normalize-space(eac:date))"
                                        
                                        
                                        >
                                        
                                    </xsl:with-param>
                                    <xsl:with-param name="coll" select="$coll"/>
                                </xsl:call-template>
                                
                                
                            </xsl:for-each>
                            <xsl:for-each
                                select="eac:cpfDescription/eac:relations/eac:cpfRelation[contains(@xlink:arcrole, 'isFunctionallyLinkedTo')]">
                                <xsl:call-template name="outputObjectPropertyForRelation">
                                    <xsl:with-param name="link"
                                        select="
                                            if (normalize-space(@xlink:href) = '' or not(@xlink:href))
                                            then
                                                ('non')
                                            else
                                                (normalize-space(@xlink:href))
                                            "/>
                                    <xsl:with-param name="recId" select="$theGoodRecId"/>
                                    <xsl:with-param name="relEntry"
                                        select="normalize-space(eac:relationEntry)"/>
                                    <xsl:with-param name="srcRelType"
                                        select="'isFunctionallyLinkedTo'"/>
                                    <xsl:with-param name="arcToRelName"
                                        select="'hasBusinessRelation'"/>
                                    <xsl:with-param name="toAncRelArcName"
                                        select="'businessRelationWith'"/>
                                    <xsl:with-param name="relName" select="'BusinessRelation'"/>
                                    <xsl:with-param name="relSecondArcName"
                                        select="'businessRelationWith'"/>
                                 <!--   <xsl:with-param name="shortcutRelName"
                                        select="'functionallyLinkedTo'"/>-->
                                    <xsl:with-param name="relFromDate"
                                        select="   if (eac:dateRange/eac:fromDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:fromDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:fromDate))
                                        
                                        "/>
                                    <xsl:with-param name="relToDate"
                                        select="
                                        
                                        if (eac:dateRange/eac:toDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:toDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:toDate))
                                        
                                        
                                        
                                        
                                        "/>
                              <!--      <xsl:with-param name="relNote"
                                        select="normalize-space(eac:descriptiveNote/eac:p)"/>-->
                                    <xsl:with-param name="relDate"
                                        select="
                                        if (eac:date/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:date/@standardDate))
                                        else
                                        (normalize-space(eac:date))"
                                        
                                        
                                        >
                                        
                                    </xsl:with-param>
                                    <xsl:with-param name="coll" select="$coll"/>
                                </xsl:call-template>

                            </xsl:for-each>
                            <xsl:for-each
                                select="eac:cpfDescription/eac:relations/eac:cpfRelation[contains(@xlink:arcrole, 'hasPart')]">
                                <xsl:call-template name="outputObjectPropertyForRelation">
                                    <xsl:with-param name="link"
                                        select="
                                            if (normalize-space(@xlink:href) = '' or not(@xlink:href))
                                            then
                                                ('non')
                                            else
                                                (normalize-space(@xlink:href))
                                            "/>
                                    <xsl:with-param name="recId" select="$theGoodRecId"/>
                                    <xsl:with-param name="relEntry"
                                        select="normalize-space(eac:relationEntry)"/>
                                    <xsl:with-param name="srcRelType" select="'hasPart'"/>
                                    <xsl:with-param name="arcToRelName" select="'groupHasPart'"/>
                                    <xsl:with-param name="toAncRelArcName" select="'agentPartOf'"/>
                                    <xsl:with-param name="relName" select="'AgentWholePartRelation'"/>
                                    <xsl:with-param name="relSecondArcName" select="'wholeHasAgentPart'"/>
                                <!--    <xsl:with-param name="shortcutRelName" select="'integrates'"/>-->
                                    <xsl:with-param name="relFromDate"
                                        select="   if (eac:dateRange/eac:fromDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:fromDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:fromDate))
                                        
                                        "/>
                                    <xsl:with-param name="relToDate"
                                        select="
                                        
                                        if (eac:dateRange/eac:toDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:toDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:toDate))
                                        
                                        
                                        
                                        
                                        "/>
                                    <xsl:with-param name="relDate"
                                        select="
                                        if (eac:date/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:date/@standardDate))
                                        else
                                        (normalize-space(eac:date))"
                                        
                                        
                                        >
                                        
                                    </xsl:with-param>
                                   <!-- <xsl:with-param name="relNote"
                                        select="normalize-space(eac:descriptiveNote/eac:p)"/>-->
                                    <xsl:with-param name="coll" select="$coll"/>
                                </xsl:call-template>

                            </xsl:for-each>
                            <xsl:for-each
                                select="eac:cpfDescription/eac:relations/eac:cpfRelation[contains(@xlink:arcrole, 'isPartOf')]">
                                <xsl:call-template name="outputObjectPropertyForRelation">
                                    <xsl:with-param name="link"
                                        select="
                                            if (normalize-space(@xlink:href) = '' or not(@xlink:href))
                                            then
                                                ('non')
                                            else
                                                (normalize-space(@xlink:href))
                                            "/>
                                    <xsl:with-param name="recId" select="$theGoodRecId"/>
                                    <xsl:with-param name="relEntry"
                                        select="normalize-space(eac:relationEntry)"/>
                                    <xsl:with-param name="srcRelType" select="'isPartOf'"/>
                                    <xsl:with-param name="arcToRelName"
                                        select="'agentHasWholePartRelation'"/>
                                    <xsl:with-param name="toAncRelArcName" select="'wholeHasAgentPart'"/>
                                    <xsl:with-param name="relName" select="'AgentWholePartRelation'"/>
                                    <xsl:with-param name="relSecondArcName" select="'agentPartOf'"/>
                                  <!--  <xsl:with-param name="shortcutRelName" select="'integratedIn'"/>-->
                                    <xsl:with-param name="relFromDate"
                                        select="   if (eac:dateRange/eac:fromDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:fromDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:fromDate))
                                        
                                        "/>
                                    <xsl:with-param name="relToDate"
                                        select="
                                        
                                        if (eac:dateRange/eac:toDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:toDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:toDate))
                                        
                                        
                                        
                                        
                                        "/>
                                    <xsl:with-param name="relDate"
                                        select="
                                        if (eac:date/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:date/@standardDate))
                                        else
                                        (normalize-space(eac:date))"
                                        
                                        
                                        >
                                        
                                    </xsl:with-param>
                               <!--     <xsl:with-param name="relNote"
                                        select="normalize-space(eac:descriptiveNote/eac:p)"/>
                                        
-->   <xsl:with-param name="coll" select="$coll"/>
                                </xsl:call-template>

                            </xsl:for-each>
                            <xsl:for-each
                                select="eac:cpfDescription/eac:relations/eac:cpfRelation[@cpfRelationType = 'temporal-earlier']">
                                <xsl:call-template name="outputObjectPropertyForRelation">
                                    <xsl:with-param name="link"
                                        select="
                                            if (normalize-space(@xlink:href) = '' or not(@xlink:href))
                                            then
                                                ('non')
                                            else
                                                (normalize-space(@xlink:href))
                                            "/>
                                    <xsl:with-param name="recId" select="$theGoodRecId"/>
                                    <xsl:with-param name="relEntry"
                                        select="normalize-space(eac:relationEntry)"/>
                                    <xsl:with-param name="srcRelType" select="'temporal-earlier'"/>
                                    <xsl:with-param name="arcToRelName" select="'agentFollows'"/>
                                    <xsl:with-param name="toAncRelArcName"
                                        select="'followingAgent'"/>
                                    <xsl:with-param name="relName" select="'AgentTemporalRelation'"/>
                                    <xsl:with-param name="relSecondArcName"
                                        select="'precedingAgent'"/>
                                  <!--  <xsl:with-param name="shortcutRelName" select="'successorOf'"/>-->
                                    <xsl:with-param name="relFromDate"
                                        select="   if (eac:dateRange/eac:fromDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:fromDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:fromDate))
                                        
                                        "/>
                                    <xsl:with-param name="relToDate"
                                        select="
                                        
                                        if (eac:dateRange/eac:toDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:toDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:toDate))
                                        
                                        
                                        
                                        
                                        "/>
                                    <xsl:with-param name="relDate"
                                        select="
                                        if (eac:date/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:date/@standardDate))
                                        else
                                        (normalize-space(eac:date))"
                                        
                                        
                                        >
                                        
                                    </xsl:with-param>
                                 <!--   <xsl:with-param name="relNote"
                                        select="normalize-space(eac:descriptiveNote/eac:p)"/>
-->   <xsl:with-param name="coll" select="$coll"/>
                                </xsl:call-template>

                            </xsl:for-each>
                            <xsl:for-each
                                select="eac:cpfDescription/eac:relations/eac:cpfRelation[@cpfRelationType = 'temporal-later']">
                                <xsl:call-template name="outputObjectPropertyForRelation">
                                    <xsl:with-param name="link"
                                        select="
                                            if (normalize-space(@xlink:href) = '' or not(@xlink:href))
                                            then
                                                ('non')
                                            else
                                                (normalize-space(@xlink:href))
                                            "/>
                                    <xsl:with-param name="recId" select="$theGoodRecId"/>
                                    <xsl:with-param name="relEntry"
                                        select="normalize-space(eac:relationEntry)"/>
                                    <xsl:with-param name="srcRelType" select="'temporal-later'"/>
                                    <xsl:with-param name="arcToRelName" select="'agentPrecedes'"/>
                                    <xsl:with-param name="toAncRelArcName"
                                        select="'precedingAgent'"/>
                                    <xsl:with-param name="relName" select="'AgentTemporalRelation'"/>
                                    <xsl:with-param name="relSecondArcName"
                                        select="'followingAgent'"/>
                                  <!--  <xsl:with-param name="shortcutRelName" select="'predecessorOf'"/>-->
                                    <xsl:with-param name="relFromDate"
                                        select="   if (eac:dateRange/eac:fromDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:fromDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:fromDate))
                                        
                                        "/>
                                    <xsl:with-param name="relToDate"
                                        select="
                                        
                                        if (eac:dateRange/eac:toDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:toDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:toDate))
                                        
                                        
                                        
                                        
                                        "/>
                                    <xsl:with-param name="relDate"
                                        select="
                                        if (eac:date/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:date/@standardDate))
                                        else
                                        (normalize-space(eac:date))"
                                        
                                        
                                        >
                                        
                                    </xsl:with-param>
                                 <!--   <xsl:with-param name="relNote"
                                        select="normalize-space(eac:descriptiveNote/eac:p)"/>
-->   <xsl:with-param name="coll" select="$coll"/>
                                </xsl:call-template>

                            </xsl:for-each>
                            <xsl:for-each
                                select="eac:cpfDescription/eac:relations/eac:cpfRelation[@cpfRelationType = 'hierarchical-child' and (not(@xlink:arcrole) or @xlink:arcrole='')]">
                                <xsl:call-template name="outputObjectPropertyForRelation">
                                    <xsl:with-param name="link"
                                        select="
                                        if (normalize-space(@xlink:href) = '' or not(@xlink:href))
                                        then
                                        ('non')
                                        else
                                        (normalize-space(@xlink:href))
                                        "/>
                                    <xsl:with-param name="recId" select="$theGoodRecId"/>
                                    <xsl:with-param name="relEntry"
                                        select="normalize-space(eac:relationEntry)"/>
                                    <xsl:with-param name="srcRelType" select="'hierarchical-child'"/>
                                    <xsl:with-param name="arcToRelName" select="'agentIsSuperiorInHierarchicalRelation'"/>
                                    <xsl:with-param name="toAncRelArcName"
                                        select="'hasHierarchicalParent'"/>
                                    <xsl:with-param name="relName" select="'AgentHierarchicalRelation'"/>
                                    <xsl:with-param name="relSecondArcName"
                                        select="'hasHierarchicalChild'"/>
                                <!--    <xsl:with-param name="shortcutRelName" select="'predecessorOf'"/>-->
                                    <xsl:with-param name="relFromDate"
                                        select="   if (eac:dateRange/eac:fromDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:fromDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:fromDate))
                                        
                                        "/>
                                    <xsl:with-param name="relToDate"
                                        select="
                                        
                                        if (eac:dateRange/eac:toDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:toDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:toDate))
                                        
                                        
                                        
                                        
                                        "/>
                                    <xsl:with-param name="relDate"
                                        select="
                                        if (eac:date/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:date/@standardDate))
                                        else
                                        (normalize-space(eac:date))"
                                        
                                        
                                        >
                                        
                                    </xsl:with-param>
                                  <!--  <xsl:with-param name="relNote"
                                        select="normalize-space(eac:descriptiveNote/eac:p)"/>-->
                                    <xsl:with-param name="coll" select="$coll"/>  
                                </xsl:call-template>
                                
                            </xsl:for-each>
                            <xsl:for-each
                                select="eac:cpfDescription/eac:relations/eac:cpfRelation[@cpfRelationType = 'hierarchical-parent'  and (not(@xlink:arcrole) or @xlink:arcrole='')]">
                                <xsl:call-template name="outputObjectPropertyForRelation">
                                    <xsl:with-param name="link"
                                        select="
                                        if (normalize-space(@xlink:href) = '' or not(@xlink:href))
                                        then
                                        ('non')
                                        else
                                        (normalize-space(@xlink:href))
                                        "/>
                                    <xsl:with-param name="recId" select="$theGoodRecId"/>
                                    <xsl:with-param name="relEntry"
                                        select="normalize-space(eac:relationEntry)"/>
                                    <xsl:with-param name="srcRelType" select="'hierarchical-parent'"/>
                                    <xsl:with-param name="arcToRelName" select="'agentIsInferiorInHierarchicalRelation'"/>
                                    <xsl:with-param name="toAncRelArcName"
                                        select="'hasHierarchicalChild'"/>
                                    <xsl:with-param name="relName" select="'AgentHierarchicalRelation'"/>
                                    <xsl:with-param name="relSecondArcName"
                                        select="'hasHierarchicalParent'"/>
                                    <!--    <xsl:with-param name="shortcutRelName" select="'predecessorOf'"/>-->
                                    <xsl:with-param name="relFromDate"
                                        select="   if (eac:dateRange/eac:fromDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:fromDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:fromDate))
                                        
                                        "/>
                                    <xsl:with-param name="relToDate"
                                        select="
                                        
                                        if (eac:dateRange/eac:toDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:toDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:toDate))
                                        
                                        
                                        
                                        
                                        "/>
                                    <xsl:with-param name="relDate"
                                        select="
                                        if (eac:date/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:date/@standardDate))
                                        else
                                        (normalize-space(eac:date))"
                                        
                                        
                                        >
                                        
                                    </xsl:with-param>
                                    <!--  <xsl:with-param name="relNote"
                                        select="normalize-space(eac:descriptiveNote/eac:p)"/>-->
                                    <xsl:with-param name="coll" select="$coll"/> 
                                </xsl:call-template>
                                
                            </xsl:for-each>
                            <xsl:for-each
                                select="eac:cpfDescription/eac:relations/eac:cpfRelation[@cpfRelationType = 'associative'  and (not(@xlink:arcrole) or @xlink:arcrole='')]">
                                <xsl:call-template name="outputObjectPropertyForRelation">
                                    <xsl:with-param name="link"
                                        select="
                                        if (normalize-space(@xlink:href) = '' or not(@xlink:href))
                                        then
                                        ('non')
                                        else
                                        (normalize-space(@xlink:href))
                                        "/>
                                    <xsl:with-param name="recId" select="$theGoodRecId"/>
                                    <xsl:with-param name="relEntry"
                                        select="normalize-space(eac:relationEntry)"/>
                                    <xsl:with-param name="srcRelType" select="'associative'"/>
                                    <xsl:with-param name="arcToRelName" select="'associatedWithRelation'"/>
                                    <xsl:with-param name="toAncRelArcName"
                                        select="'relationAssociates'"/>
                                    <xsl:with-param name="relName" select="'SocialRelation'"/>
                                    <xsl:with-param name="relSecondArcName"
                                        select="'relationAssociates'"/>
                                  
                                    <xsl:with-param name="relFromDate"
                                        select="   if (eac:dateRange/eac:fromDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:fromDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:fromDate))
                                        
                                        "/>
                                    <xsl:with-param name="relToDate"
                                        select="
                                        
                                        if (eac:dateRange/eac:toDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:toDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:toDate))
                                        
                                        
                                        
                                        
                                        "/>
                                    <xsl:with-param name="relDate"
                                        select="
                                        if (eac:date/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:date/@standardDate))
                                        else
                                        (normalize-space(eac:date))"
                                        
                                        
                                        >
                                        
                                    </xsl:with-param>
                               
                                    <xsl:with-param name="coll" select="$coll"/>  
                                </xsl:call-template>
                                
                            </xsl:for-each>
                            <xsl:for-each select="eac:cpfDescription/eac:relations/eac:resourceRelation[@resourceRelationType = 'creatorOf']">
                                
                                <xsl:call-template name="outputObjectPropertyForProvenanceRelation">
                                    <xsl:with-param name="link"
                                        select="
                                        if (normalize-space(@xlink:href) = '' or not(@xlink:href))
                                        then
                                        ('non')
                                        else
                                        (normalize-space(@xlink:href))
                                        "/>
                                    <xsl:with-param name="recId" select="$recId"/>
                                    <xsl:with-param name="relEntry"
                                        select="normalize-space(eac:relationEntry)"/>
                                    
                                    
                                    
                                 
                                    <xsl:with-param name="relFromDate"
                                        select="   if (eac:dateRange/eac:fromDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:fromDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:fromDate))
                                        
                                        "/>
                                    <xsl:with-param name="relToDate"
                                        select="
                                        
                                        if (eac:dateRange/eac:toDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:toDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:toDate))
                                        
                                        
                                        
                                        
                                        "/>
                                 
                                    <xsl:with-param name="relDate"
                                        select="
                                        if (eac:date/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:date/@standardDate))
                                        else
                                        (normalize-space(eac:date))"
                                        
                                        
                                        >
                                        
                                    </xsl:with-param>
                                   
                                    <xsl:with-param name="coll" select="$coll"/>
                                </xsl:call-template>
                            </xsl:for-each>
                            <!-- métadonnées -->

                            <RiC:describedBy>
                                <xsl:attribute name="rdf:resource">
                                    <xsl:value-of
                                        select="concat('http://piaaf.demo.logilab.fr/resource/FRBNF_description_', $theGoodRecId)"
                                    />
                                </xsl:attribute>
                            </RiC:describedBy>
                        </rdf:Description>
                        <!-- sortie des relations de catégorisation -->
                       
                        <xsl:for-each select="eac:cpfDescription/eac:identity/eac:nameEntry">
                            <xsl:variable name="theName" select="  if (count(eac:part)=1)
                                then (normalize-space(eac:part))
                                else (
                                concat(eac:part[@localType='nom'], ', ', eac:part[@localType='prenom'], ' (', eac:part[@localType='dates_biographiques'], ')')
                                )"/>
                            <rdf:Description>
                                <xsl:attribute name="rdf:about">
                                    <xsl:value-of
                                        select="concat('http://piaaf.demo.logilab.fr/resource/FRBNF_DenominationRelation_', $theGoodRecId, '_', generate-id())"/>

                                </xsl:attribute>
                                <rdf:type
                                    rdf:resource="http://www.ica.org/standards/RiC/ontology#DenominationRelation"/>
                                <xsl:if
                                    test="eac:useDates/eac:dateRange/eac:fromDate">
                                    
                                    <RiC:beginningDate>
                                        <!--<xsl:value-of
                                            select="eac:useDates/eac:dateRange/eac:fromDate"/>-->
                                        <xsl:call-template name="outputDate">
                                            <xsl:with-param name="stdDate" select="eac:useDates/eac:dateRange/eac:fromDate/@standardDate"></xsl:with-param>
                                            <xsl:with-param name="date" select="eac:useDates/eac:dateRange/eac:fromDate"></xsl:with-param>
                                        </xsl:call-template>
                                        <!-- <xsl:choose>
                                            
                                            <xsl:when test="string-length(eac:useDates/eac:dateRange/eac:fromDate/@standardDate)=4">
                                                <xsl:attribute name="rdf:datatype"> <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text></xsl:attribute>
                                                
                                            </xsl:when>
                                            <xsl:when test="string-length(eac:useDates/eac:dateRange/eac:fromDate/@standardDate)=7">
                                                <xsl:attribute name="rdf:datatype">
                                                    <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                                </xsl:attribute>
                                            </xsl:when>
                                            <xsl:when test="string-length(eac:useDates/eac:dateRange/eac:fromDate/@standardDate)=10">
                                                <xsl:attribute name="rdf:datatype">
                                                    <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                                </xsl:attribute>
                                            </xsl:when>
                                            <xsl:otherwise/>
                                        </xsl:choose>
                                        
                                        <xsl:value-of select="
                                            if (normalize-space(eac:useDates/eac:dateRange/eac:fromDate/@standardDate)!='')
                                            then (normalize-space(eac:useDates/eac:dateRange/eac:fromDate/@standardDate))
                                            else(
                                            if (normalize-space(eac:useDates/eac:dateRange/eac:fromDate)!='')
                                            then (normalize-space(eac:useDates/eac:dateRange/eac:fromDate))
                                            else()
                                            )
                                            "/>-->
                                    </RiC:beginningDate>
                                </xsl:if>
                                <xsl:if
                                    test="eac:useDates/eac:dateRange/eac:toDate">
                                    <RiC:endDate>
                                        <xsl:call-template name="outputDate">
                                            <xsl:with-param name="stdDate" select="eac:useDates/eac:dateRange/eac:toDate/@standardDate"></xsl:with-param>
                                            <xsl:with-param name="date" select="eac:useDates/eac:dateRange/eac:toDate"></xsl:with-param>
                                        </xsl:call-template>
                                        <!--    <xsl:choose>
                                            
                                            <xsl:when test="string-length(eac:useDates/eac:dateRange/eac:toDate/@standardDate)=4">
                                                <xsl:attribute name="rdf:datatype"> <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text></xsl:attribute>
                                                
                                            </xsl:when>
                                            <xsl:when test="string-length(eac:useDates/eac:dateRange/eac:toDate/@standardDate)=7">
                                                <xsl:attribute name="rdf:datatype">
                                                    <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                                </xsl:attribute>
                                            </xsl:when>
                                            <xsl:when test="string-length(eac:useDates/eac:dateRange/eac:toDate/@standardDate)=10">
                                                <xsl:attribute name="rdf:datatype">
                                                    <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                                </xsl:attribute>
                                            </xsl:when>
                                            <xsl:otherwise/>
                                        </xsl:choose>
                                        
                                        <!-\-  <RiC:endDate rdf:datatype="http://www.w3.org/2001/XMLSchema#dateTime">-\->
                                        <xsl:value-of select="
                                            if (normalize-space(eac:useDates/eac:dateRange/eac:toDate/@standardDate)!='')
                                            then (normalize-space(eac:useDates/eac:toDate/@standardDate))
                                            else(
                                            if (normalize-space(eac:useDates/eac:dateRange/eac:toDate)!='')
                                            then (normalize-space(eac:useDates/eac:dateRange/eac:toDate))
                                            else()
                                            )
                                            "/>-->
                                        
                                        
                                        <!--  <xsl:value-of select="eac:useDates/eac:dateRange/eac:toDate"
                                        />-->
                                    </RiC:endDate>
                                    
                                </xsl:if>
                                <RiC:certainty xml:lang="fr">certain</RiC:certainty>
                                <RiC:denominationOf>
                                    <xsl:attribute name="rdf:resource">
                                        <xsl:value-of
                                            select="$URI"
                                        />
                                    </xsl:attribute>
                                </RiC:denominationOf>
                                <RiC:denominatedBy>
                                        <xsl:choose>
                                           <xsl:when test="$bnf-names/rdf:Description[rdfs:label=$theName]">
                                               <xsl:attribute name="rdf:resource">
                                               <xsl:value-of select="$bnf-names/rdf:Description[rdfs:label=$theName]/@rdf:about"/>
                                               </xsl:attribute>
                                           </xsl:when> 
                                            <xsl:otherwise>
                                                <xsl:attribute name="xml:lang">fr</xsl:attribute>
                                                <xsl:comment>pas de nom trouvé dans le fichier des noms</xsl:comment>
                                                <xsl:value-of select="normalize-space(eac:part)"/>
                                                
                                            </xsl:otherwise>
                                        </xsl:choose>


                             

                                </RiC:denominatedBy>
                            </rdf:Description>

                        </xsl:for-each>


                        <!-- statuts juridiques -->


                        <xsl:for-each
                            select="eac:cpfDescription/eac:description/descendant::eac:legalStatus">
                            <xsl:variable name="lsName" select="normalize-space(eac:term)"/>
                            
                            
                           <!-- <xsl:if test="$legalsts/skos:Concept[skos:prefLabel = $lsName]">-->
                                <rdf:Description>
                                    <xsl:attribute name="rdf:about">
                                        <xsl:text>http://piaaf.demo.logilab.fr/resource/FRBNF_TypeRelation_</xsl:text>
                                        <xsl:value-of select="$theGoodRecId"/>
                                        <xsl:text>_</xsl:text>
                                        <xsl:value-of
                                            select="generate-id()"/>
                                    </xsl:attribute>
                                    <rdf:type
                                        rdf:resource="http://www.ica.org/standards/RiC/ontology#TypeRelation"/>
                                    <RiC:hasType>
                                        <xsl:choose>
                                            <xsl:when test="$legalsts/skos:Concept[skos:prefLabel = $lsName]">
                                        <xsl:attribute name="rdf:resource">
                                            
                                                    <xsl:value-of
                                                        select="$legalsts/skos:Concept[skos:prefLabel = $lsName]/@rdf:about"
                                                    />
                                            
                                            
                                            
                                          
                                        </xsl:attribute>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:attribute name="xml:lang">fr</xsl:attribute>
                                                <xsl:value-of select="normalize-space(eac:term)"/>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </RiC:hasType>
                                    <RiC:categorizationOf>
                                        <xsl:attribute name="rdf:resource">
                                            <xsl:value-of
                                                select="$URI"
                                            />
                                        </xsl:attribute>
                                    </RiC:categorizationOf>
                                    <xsl:if
                                        test="eac:dateRange/eac:fromDate">
                                        <RiC:beginningDate>
                                            <xsl:call-template name="outputDate">
                                                <xsl:with-param name="stdDate" select="eac:dateRange/eac:fromDate/@standardDate"></xsl:with-param>
                                                <xsl:with-param name="date" select="eac:dateRange/eac:fromDate"></xsl:with-param>
                                            </xsl:call-template>
                                            <!-- <xsl:choose>
                                                
                                                <xsl:when test="string-length(eac:dateRange/eac:fromDate/@standardDate)=4">
                                                    <xsl:attribute name="rdf:datatype"> <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text></xsl:attribute>
                                                    
                                                </xsl:when>
                                                <xsl:when test="string-length(eac:dateRange/eac:fromDate/@standardDate)=7">
                                                    <xsl:attribute name="rdf:datatype">
                                                        <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                                    </xsl:attribute>
                                                </xsl:when>
                                                <xsl:when test="string-length(eac:dateRange/eac:fromDate/@standardDate)=10">
                                                    <xsl:attribute name="rdf:datatype">
                                                        <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                                    </xsl:attribute>
                                                </xsl:when>
                                                <xsl:otherwise/>
                                            </xsl:choose>
                                            
                                            <xsl:value-of select="
                                                if (normalize-space(eac:dateRange/eac:fromDate/@standardDate)!='')
                                                then (normalize-space(eac:dateRange/eac:fromDate/@standardDate))
                                                else(
                                                if (normalize-space(eac:dateRange/eac:fromDate)!='')
                                                then (normalize-space(eac:dateRange/eac:fromDate))
                                                else()
                                                )
                                                "/>
                                            -->
                                            
                                        </RiC:beginningDate>
                                    </xsl:if>
                                    <xsl:if
                                        test="eac:dateRange/eac:toDate">
                                        
                                        <RiC:endDate>
                                            <xsl:call-template name="outputDate">
                                                <xsl:with-param name="stdDate" select="eac:dateRange/eac:toDate/@standardDate"></xsl:with-param>
                                                <xsl:with-param name="date" select="eac:dateRange/eac:toDate"></xsl:with-param>
                                            </xsl:call-template>
                                            <!--   <xsl:choose>
                                                    
                                                    <xsl:when test="string-length(eac:dateRange/eac:toDate/@standardDate)=4">
                                                        <xsl:attribute name="rdf:datatype"> <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text></xsl:attribute>
                                                        
                                                    </xsl:when>
                                                    <xsl:when test="string-length(eac:dateRange/eac:toDate/@standardDate)=7">
                                                        <xsl:attribute name="rdf:datatype">
                                                            <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                                        </xsl:attribute>
                                                    </xsl:when>
                                                    <xsl:when test="string-length(eac:dateRange/eac:toDate/@standardDate)=10">
                                                        <xsl:attribute name="rdf:datatype">
                                                            <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                                        </xsl:attribute>
                                                    </xsl:when>
                                                    <xsl:otherwise/>
                                                </xsl:choose>
                                                
                                                <!-\-  <RiC:endDate rdf:datatype="http://www.w3.org/2001/XMLSchema#dateTime">-\->
                                                <xsl:value-of select="
                                                    if (normalize-space(eac:dateRange/eac:toDate/@standardDate)!='')
                                                    then (normalize-space(eac:dateRange/eac:toDate/@standardDate))
                                                    else(
                                                    if (normalize-space(eac:dateRange/eac:toDate)!='')
                                                    then (normalize-space(eac:dateRange/eac:toDate))
                                                    else()
                                                    )
                                                    "/>-->
                                        </RiC:endDate>
                                    </xsl:if>
                                  <!--  <xsl:if test="eac:date[normalize-space(.) != '']">
                                        <xsl:choose>
                                            <xsl:when test="not(contains(eac:date, '/'))">
                                                <RiC:beginningDate
                                                  rdf:datatype="http://www.w3.org/2001/XMLSchema#dateTime">
                                                  <xsl:value-of select="normalize-space(eac:date)"/>
                                                </RiC:beginningDate>
                                            </xsl:when>
                                        </xsl:choose>
                                    </xsl:if>-->
                                    <xsl:if test="eac:descriptiveNote">
                                        <RiC:description xml:lang="fr">
                                            <xsl:value-of select="eac:descriptiveNote"/>
                                        </RiC:description>
                                    </xsl:if>
                                </rdf:Description>
                            <!--</xsl:if>-->

                        </xsl:for-each>
                        
                        <!--<xsl:for-each select="eac:cpfDescription/eac:description/eac:places/eac:place[normalize-space(eac:placeEntry)!='']">
                           
                            <xsl:variable name="placeName">
                                <xsl:choose>
                                    <xsl:when test="eac:placeRole='Siège social'">
                                        <xsl:choose>
                                            <xsl:when test="parent::eac:places/eac:place[eac:placeRole='Siège']">
                                                <xsl:value-of select="concat(normalize-space(parent::eac:places/eac:place[eac:placeRole='Siège']/eac:placeEntry), '. ', normalize-space(eac:placeEntry))"/>
                                            </xsl:when>
                                            <xsl:otherwise><xsl:value-of select="normalize-space(eac:placeEntry)"/></xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="normalize-space(eac:placeEntry)"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:variable>
                            <xsl:if
                                test="$bnf-places/rdf:Description[rdfs:label = $placeName]">
                                
                                <rdf:Description>
                                    <xsl:attribute name="rdf:about">
                                        <xsl:text>http://www.piaaf.net/relations/FRBNF_LocationRelation_</xsl:text>
                                        <xsl:value-of select="$theGoodRecId"/>
                                        <xsl:text>_</xsl:text>
                                        <xsl:value-of
                                            select="generate-id()"/>
                                    </xsl:attribute>
                                    <rdf:type
                                        rdf:resource="http://www.ica.org/standards/RiC/ontology#LocationRelation"/>
                                    <RiC:hasLocation>
                                        <xsl:attribute name="rdf:resource">
                                            <xsl:value-of
                                                select="$bnf-places/rdf:Description[rdfs:label = $placeName]/@rdf:about"
                                            />
                                        </xsl:attribute>
                                    </RiC:hasLocation>
                                    <RiC:locationOf>
                                        <xsl:attribute name="rdf:resource">
                                            <xsl:value-of
                                                select="$URI"
                                            />
                                        </xsl:attribute>
                                    </RiC:locationOf>
                                    <xsl:if
                                        test="eac:dateRange/eac:fromDate">
                                        <RiC:beginningDate>
                                            <xsl:call-template name="outputDate">
                                                <xsl:with-param name="stdDate" select="eac:dateRange/eac:fromDate/@standardDate"></xsl:with-param>
                                                <xsl:with-param name="date" select="eac:dateRange/eac:fromDate"></xsl:with-param>
                                            </xsl:call-template>
                                            <!-\- <xsl:choose>
                                                
                                                <xsl:when test="string-length(eac:dateRange/eac:fromDate/@standardDate)=4">
                                                    <xsl:attribute name="rdf:datatype"> <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text></xsl:attribute>
                                                    
                                                </xsl:when>
                                                <xsl:when test="string-length(eac:dateRange/eac:fromDate/@standardDate)=7">
                                                    <xsl:attribute name="rdf:datatype">
                                                        <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                                    </xsl:attribute>
                                                </xsl:when>
                                                <xsl:when test="string-length(eac:dateRange/eac:fromDate/@standardDate)=10">
                                                    <xsl:attribute name="rdf:datatype">
                                                        <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                                    </xsl:attribute>
                                                </xsl:when>
                                                <xsl:otherwise/>
                                            </xsl:choose>
                                            
                                            <xsl:value-of select="
                                                if (normalize-space(eac:dateRange/eac:fromDate/@standardDate)!='')
                                                then (normalize-space(eac:dateRange/eac:fromDate/@standardDate))
                                                else(
                                                if (normalize-space(eac:dateRange/eac:fromDate)!='')
                                                then (normalize-space(eac:dateRange/eac:fromDate))
                                                else()
                                                )
                                                "/>
                                            -\->
                                            
                                        </RiC:beginningDate>
                                    </xsl:if>
                                    <xsl:if
                                        test="eac:dateRange/eac:toDate">
                                        
                                        <RiC:endDate>
                                            <xsl:call-template name="outputDate">
                                                <xsl:with-param name="stdDate" select="eac:dateRange/eac:toDate/@standardDate"></xsl:with-param>
                                                <xsl:with-param name="date" select="eac:dateRange/eac:toDate"></xsl:with-param>
                                            </xsl:call-template>
                                            <!-\-   <xsl:choose>
                                                    
                                                    <xsl:when test="string-length(eac:dateRange/eac:toDate/@standardDate)=4">
                                                        <xsl:attribute name="rdf:datatype"> <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text></xsl:attribute>
                                                        
                                                    </xsl:when>
                                                    <xsl:when test="string-length(eac:dateRange/eac:toDate/@standardDate)=7">
                                                        <xsl:attribute name="rdf:datatype">
                                                            <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                                        </xsl:attribute>
                                                    </xsl:when>
                                                    <xsl:when test="string-length(eac:dateRange/eac:toDate/@standardDate)=10">
                                                        <xsl:attribute name="rdf:datatype">
                                                            <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                                        </xsl:attribute>
                                                    </xsl:when>
                                                    <xsl:otherwise/>
                                                </xsl:choose>
                                                
                                                <!-\\-  <RiC:endDate rdf:datatype="http://www.w3.org/2001/XMLSchema#dateTime">-\\->
                                                <xsl:value-of select="
                                                    if (normalize-space(eac:dateRange/eac:toDate/@standardDate)!='')
                                                    then (normalize-space(eac:dateRange/eac:toDate/@standardDate))
                                                    else(
                                                    if (normalize-space(eac:dateRange/eac:toDate)!='')
                                                    then (normalize-space(eac:dateRange/eac:toDate))
                                                    else()
                                                    )
                                                    "/>-\->
                                        </RiC:endDate>
                                    </xsl:if>
                                   <!-\- <xsl:if test="eac:date[normalize-space(.) != '']">
                                        <xsl:choose>
                                            <xsl:when test="not(contains(eac:date, '/'))">
                                                <RiC:beginningDate
                                                    rdf:datatype="http://www.w3.org/2001/XMLSchema#dateTime">
                                                    <xsl:value-of select="normalize-space(eac:date)"/>
                                                </RiC:beginningDate>
                                            </xsl:when>
                                        </xsl:choose>
                                    </xsl:if>-\->
                                    <xsl:if test="eac:descriptiveNote[normalize-space()!='']">
                                        <RiC:description xml:lang="fr">
                                            <xsl:value-of select="eac:descriptiveNote"/>
                                        </RiC:description>
                                    </xsl:if>
                                </rdf:Description>
                                
                            </xsl:if>
                        </xsl:for-each>-->

                        <!-- la notice (entité Description) -->
                        <rdf:Description>

                            <xsl:attribute name="rdf:about">
                                <xsl:value-of
                                    select="concat('http://piaaf.demo.logilab.fr/resource/FRBNF_description_', $theGoodRecId)"
                                />
                            </xsl:attribute>

                            <rdf:type
                                rdf:resource="http://www.ica.org/standards/RiC/ontology#Description"/>
                            <RiC:describes>
                                <xsl:attribute name="rdf:resource">
                                    <xsl:value-of
                                        select="$URI"
                                    />
                                </xsl:attribute>
                            </RiC:describes>
                            <xsl:if test="eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'created']">
                                <RiC:creationDate>
                                    <xsl:call-template name="outputDate">
                                        <xsl:with-param name="stdDate" select="eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'created']/eac:eventDateTime/@standardDateTime"></xsl:with-param>
                                        <xsl:with-param name="date" select="eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'created']/eac:eventDateTime"></xsl:with-param>
                                    </xsl:call-template>
                                    <!-- <xsl:choose>
                                        
                                        <xsl:when test="string-length(eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'created']/eac:eventDateTime/@standardDate)=4">
                                            <xsl:attribute name="rdf:datatype"> <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text></xsl:attribute>
                                            
                                        </xsl:when>
                                        <xsl:when test="string-length(eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'created']/eac:eventDateTime/@standardDate)=7">
                                            <xsl:attribute name="rdf:datatype">
                                                <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                            </xsl:attribute>
                                        </xsl:when>
                                        <xsl:when test="string-length(eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'created']/eac:eventDateTime/@standardDate)=10">
                                            <xsl:attribute name="rdf:datatype">
                                                <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                            </xsl:attribute>
                                        </xsl:when>
                                        <xsl:otherwise/>
                                    </xsl:choose>
                                    
                                    <!-\-  <RiC:endDate rdf:datatype="http://www.w3.org/2001/XMLSchema#dateTime">-\->
                                    <xsl:value-of select="
                                        if (normalize-space(eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'created']/eac:eventDateTime/@standardDate)!='')
                                        then (normalize-space(eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'created']/eac:eventDateTime/@standardDate))
                                        else(
                                        if (normalize-space(eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'created']/eac:eventDateTime)!='')
                                        then (normalize-space(eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'created']/eac:eventDateTime))
                                        else()
                                        )
                                        "/>
                                    
                                    -->
                                    <!-- <xsl:value-of
                                        select="eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'created']/eac:eventDateTime/@standardDateTime"/>-->
                                    
                                </RiC:creationDate>
                            </xsl:if>
                            <RiC:lastUpdateDate>
                                <!--  <xsl:value-of
                                    select="eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'revised'][last()]/eac:eventDateTime/@standardDateTime"
                                />-->
                                <xsl:call-template name="outputDate">
                                    <xsl:with-param name="stdDate" select="eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'revised'][last()]/eac:eventDateTime/@standardDateTime"></xsl:with-param>
                                    <xsl:with-param name="date" select="eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'revised'][last()]/eac:eventDateTime"></xsl:with-param>
                                </xsl:call-template>
                                <!-- <xsl:choose>
                                    
                                    <xsl:when test="string-length(eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'revised'][last()]/eac:eventDateTime/@standardDate)=4">
                                        <xsl:attribute name="rdf:datatype"> <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text></xsl:attribute>
                                        
                                    </xsl:when>
                                    <xsl:when test="string-length(eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'revised'][last()]/eac:eventDateTime/@standardDate)=7">
                                        <xsl:attribute name="rdf:datatype">
                                            <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                        </xsl:attribute>
                                    </xsl:when>
                                    <xsl:when test="string-length(eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'revised'][last()]/eac:eventDateTime/@standardDate)=10">
                                        <xsl:attribute name="rdf:datatype">
                                            <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                        </xsl:attribute>
                                    </xsl:when>
                                    <xsl:otherwise/>
                                </xsl:choose>
                                
                                <!-\-  <RiC:endDate rdf:datatype="http://www.w3.org/2001/XMLSchema#dateTime">-\->
                                <xsl:value-of select="
                                    if (normalize-space(eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'revised'][last()]/eac:eventDateTime/@standardDate)!='')
                                    then (normalize-space(eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'revised'][last()]/eac:eventDateTime/@standardDate))
                                    else(
                                    if (normalize-space(eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'revised'][last()]/eac:eventDateTime)!='')
                                    then (normalize-space(eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'revised'][last()]/eac:eventDateTime))
                                    else()
                                    )
                                    "/>
                                -->
                                
                                <!--  <xsl:value-of
                                    select="eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'revised'][last()]/eac:eventDateTime/@standardDateTime"/>-->
                            </RiC:lastUpdateDate>
                            
                            <RiC:identifier>
                                <xsl:value-of select="eac:control/eac:recordId"/>
                            </RiC:identifier>
                          <!--  <RiC:authoredBy
                                rdf:resource="http://www.piaaf.net/agents/FRAN_corporate-body_005568"/>-->
                            <RiC:authoredBy rdf:resource="http://piaaf.demo.logilab.fr/resource/FRBNF_corporate-body_0036"/>
                            <xsl:for-each select="eac:control/eac:sources/eac:source[normalize-space(eac:sourceEntry)!='']">
                                <RiC:evidencedBy xml:lang="fr">
                                    <xsl:value-of select="normalize-space(eac:sourceEntry)"/>
                                    <xsl:if test="@xlink:href[normalize-space(.) != '']">
                                        <xsl:value-of
                                            select="concat(' (', normalize-space(@xlink:href), ')')"
                                        />
                                    </xsl:if>
                                </RiC:evidencedBy>
                            </xsl:for-each>
                            <xsl:for-each select="eac:control/descendant::eac:conventionDeclaration">
                                <xsl:variable name="abbr" select="normalize-space(eac:abbreviation)"/>
                                <!--<xsl:variable name="link" select="eac:citation/@xlink:href"/>-->
                                <xsl:if
                                    test="$bnf-mandates/rdf:Description[rdfs:label = $abbr and rdf:type[@rdf:resource = 'http://www.ica.org/standards/RiC/ontology#Mandate']]">
                                    <RiC:conformsToRule>
                                        <xsl:attribute name="rdf:resource">
                                            <xsl:value-of
                                                select="$bnf-mandates/rdf:Description[rdfs:label = $abbr and rdf:type[@rdf:resource = 'http://www.ica.org/standards/RiC/ontology#Mandate']]/@rdf:about"/>

                                        </xsl:attribute>

                                    </RiC:conformsToRule>

                                </xsl:if>


                            </xsl:for-each>
                            <xsl:for-each select="eac:cpfDescription/eac:identity/eac:entityId[starts-with(normalize-space(.), 'ISNI')]">
                              
                                    <xsl:variable name="ISNIValue" select="normalize-space(.)"></xsl:variable>
                                    <xsl:variable name="ISNIId">
                                        <xsl:choose>
                                            <xsl:when test="starts-with($ISNIValue, 'ISNI:')">
                                                <xsl:value-of select="replace(substring-after($ISNIValue, 'ISNI:'), ' ', '')"/>
                                            </xsl:when>
                                            <xsl:when test="starts-with($ISNIValue, 'ISNI :')">
                                                <xsl:value-of select="replace(substring-after($ISNIValue, 'ISNI :'), ' ', '')"/>
                                            </xsl:when>
                                            <xsl:when test="starts-with($ISNIValue, 'ISNI')">
                                                <xsl:value-of select="replace(substring-after($ISNIValue, 'ISNI'), ' ', '')"/>
                                            </xsl:when>
                                        </xsl:choose>
                                    </xsl:variable>
                                    <!-- http://isni.org/isni/0000000121032683-->
                                    <rdfs:seeAlso>
                                        <xsl:attribute name="rdf:resource">
                                            <xsl:value-of select="concat('http://isni.org/isni/', $ISNIId)"/>
                                        </xsl:attribute>
                                    </rdfs:seeAlso>
                                    <!--  <owl:sameAs>
                                    <xsl:attribute name="rdf:resource">
                                        
                                    </xsl:attribute>
                                </owl:sameAs>   -->
                                </xsl:for-each>
                          
                            
                              
                            <xsl:for-each
                                select="eac:cpfDescription/eac:relations/eac:cpfRelation[@cpfRelationType = 'identity']">
                                <!-- identité -->
                                <!-- http://data.bnf.fr/ark:/12148/cb11863993z
                            http://catalogue.bnf.fr/ark:/12148/cb11863993z/PUBLIC
                            http://catalogue.bnf.fr/ark:/12148/cb11863993z/PUBLIC
                            http://data.bnf.fr/ark:/12148/cb11863993z#foaf:Organization-->
                                <!-- http://catalogue.bnf.fr/ark:/12148/cb11904421w -->
                                <!-- http://data.bnf.fr/ark:/12148/cb11862469g-->
                                <xsl:variable name="lnk" select="normalize-space(@xlink:href)"/>
                                <xsl:if test="$lnk!=''">
                                <rdfs:seeAlso>
                                    <xsl:attribute name="rdf:resource">
                                        <xsl:if test="contains($lnk, 'bnf.fr')">
                                            <xsl:choose>
                                                <xsl:when
                                                    test="contains($lnk, 'catalogue.bnf.fr/ark:/12148/') and contains($lnk, '/PUBLIC')">
                                                    <xsl:text>http://data.bnf.fr/ark:/12148/</xsl:text>
                                                    <xsl:value-of
                                                        select="substring-before(substring-after($lnk, '12148/'), '/PUBLIC')"/>
                                                    
                                                </xsl:when>
                                                <xsl:when test="contains($lnk, 'catalogue.bnf.fr/ark:/12148/') and not(contains($lnk, '/PUBLIC'))">
                                                    <xsl:text>http://data.bnf.fr/ark:/12148/</xsl:text>
                                                    <xsl:value-of
                                                        select="substring-after($lnk, '12148/')"/>
                                                    
                                                </xsl:when>
                                                <xsl:when test="contains($lnk, 'http://data.bnf.fr/ark:/12148/')">
                                                    <xsl:value-of select="$lnk"/>
                                                    
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:if>
                                        <!-- liens AN vers notices wikipedia -->
                                        <!-- exemple : http://fr.dbpedia.org/page/Henri_Labrouste, https://fr.wikipedia.org/wiki/Henri_Labrouste -->
                                        <xsl:if test="contains($lnk, 'wikipedia.org')">
                                            
                                            <xsl:value-of select="$lnk"/>
                                        </xsl:if>
                                        
                                    </xsl:attribute>
                                </rdfs:seeAlso>
                                </xsl:if>
                            </xsl:for-each>
                            
                       
                        </rdf:Description>

                    </rdf:RDF>
                </xsl:result-document>
            </xsl:for-each>





        </xsl:if>
<xsl:if test="$coll = 'FRAN'">

            <xsl:for-each
                select="$collection-EAC-AN/eac:eac-cpf">
                <xsl:variable name="recId" select="normalize-space(eac:control/eac:recordId)"/>
                <xsl:variable name="entType" select="eac:cpfDescription/eac:identity/eac:entityType"/>
                <xsl:variable name="entTypeName">
                    <xsl:choose>
                        <xsl:when test="$entType = 'corporateBody'">corporate-body</xsl:when>
                        <xsl:when test="$entType = 'person'">person</xsl:when>
                    </xsl:choose>
                </xsl:variable>
                <xsl:variable name="RiCClass">
                    <xsl:choose>
                        <xsl:when test="$entType = 'corporateBody'">CorporateBody</xsl:when>
                        <xsl:when test="$entType = 'person'">Person</xsl:when>
                    </xsl:choose>
                </xsl:variable>
                <xsl:variable name="theGoodRecId" select="substring-after($recId, 'FRAN_NP_')"/>
                
                <xsl:variable name="URI"
                    select="concat('http://piaaf.demo.logilab.fr/resource/', $coll, '_', $entTypeName, '_', $theGoodRecId)"/>
                
                <xsl:result-document method="xml" encoding="utf-8" indent="yes" href="rdf/agents/{$coll}_{$entTypeName}_{$theGoodRecId}.rdf">

                    <rdf:RDF
                        xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                        xmlns:owl="http://www.w3.org/2002/07/owl#"
                        xmlns:RiC="http://www.ica.org/standards/RiC/ontology#"
                        xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
                        xmlns:piaaf-onto="http://piaaf.demo.logilab.fr/ontology#"
                        xmlns:skos="http://www.w3.org/2004/02/skos/core#"
                        xmlns:isni="http://isni.org/ontology#">
                        <rdf:Description>
                            <xsl:attribute name="rdf:about">
                                <xsl:value-of select="$URI"/>
                            </xsl:attribute>
                            <rdf:type
                                rdf:resource="http://www.ica.org/standards/RiC/ontology#{$RiCClass}"/>
                            <rdfs:label xml:lang="fr">
                                <xsl:value-of select="
                                    if (count(eac:cpfDescription/eac:identity/eac:nameEntry[@localType = 'preferredFormForProject']/eac:part)=1)
                                    then (normalize-space(eac:cpfDescription/eac:identity/eac:nameEntry[@localType = 'preferredFormForProject']/eac:part))
                                    else (
                                    concat(eac:cpfDescription/eac:identity/eac:nameEntry[@localType = 'preferredFormForProject']/eac:part[@localType='nom'], ', ', eac:cpfDescription/eac:identity/eac:nameEntry[@localType = 'preferredFormForProject']/eac:part[@localType='prenom'], ' (', eac:cpfDescription/eac:identity/eac:nameEntry[@localType = 'preferredFormForProject']/eac:part[@localType='dates_biographiques'], ')')
                                    )
                                    
                                    
                                    "/>
                          
                            </rdfs:label>
                            <!-- relation vers chacun des noms -->
                            <xsl:for-each select="eac:cpfDescription/eac:identity/eac:nameEntry">
                                <RiC:hasDenominationRelation>
                                    <xsl:attribute name="rdf:resource">
                                        <xsl:value-of
                                            select="concat('http://piaaf.demo.logilab.fr/resource/FRAN_DenominationRelation_', $theGoodRecId, '_', generate-id())"/>

                                    </xsl:attribute>
                                </RiC:hasDenominationRelation>
                            </xsl:for-each>
                            <!-- relation vers chacun des statuts juridiques -->
                            <xsl:for-each select="eac:cpfDescription/eac:description/descendant::eac:legalStatus">
                                <RiC:hasTypeRelation>
                                    <xsl:attribute name="rdf:resource">
                                        <xsl:value-of
                                            select="concat('http://piaaf.demo.logilab.fr/resource/FRAN_TypeRelation_', $theGoodRecId, '_', generate-id())"/>
                                        
                                    </xsl:attribute>
                                </RiC:hasTypeRelation>
                            </xsl:for-each>
                            <xsl:choose>
                                <xsl:when test="$entType='person'">
                                    <xsl:if test="eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate">
                                        <RiC:birthDate>
                                            <xsl:call-template name="outputDate">
                                                <xsl:with-param name="stdDate" select="eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate/@standardDate"></xsl:with-param>
                                                <xsl:with-param name="date" select="eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate"></xsl:with-param>
                                            </xsl:call-template>
                                            
                                            <!--  <xsl:choose>
                                            
                                            <xsl:when test="string-length(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate/@standardDate)=4">
                                                <xsl:attribute name="rdf:datatype"> <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text></xsl:attribute>
                                                
                                            </xsl:when>
                                            <xsl:when test="string-length(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate/@standardDate)=7">
                                                <xsl:attribute name="rdf:datatype">
                                                    <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                                </xsl:attribute>
                                            </xsl:when>
                                            <xsl:when test="string-length(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate/@standardDate)=10">
                                                <xsl:attribute name="rdf:datatype">
                                                    <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                                </xsl:attribute>
                                            </xsl:when>
                                            <xsl:otherwise/>
                                        </xsl:choose>
                                        
                                        <xsl:value-of select="
                                            if (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate/@standardDate)!='')
                                            then (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate/@standardDate))
                                            else(
                                            if (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate)!='')
                                            then (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate))
                                            else()
                                            )
                                            "/>
                                        -->
                                            
                                            
                                            <!--  <xsl:value-of
                                            select="eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate/@standardDate"
                                        />-->
                                        </RiC:birthDate>
                                    </xsl:if>
                                    <xsl:if
                                        test="eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate">
                                        <RiC:deathDate>
                                            <xsl:call-template name="outputDate">
                                                <xsl:with-param name="stdDate" select="eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate/@standardDate"></xsl:with-param>
                                                <xsl:with-param name="date" select="eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate"></xsl:with-param>
                                            </xsl:call-template>
                                            <!-- <xsl:choose>
                                                
                                                <xsl:when test="string-length(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate/@standardDate)=4">
                                                    <xsl:attribute name="rdf:datatype"> <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text></xsl:attribute>
                                                    
                                                </xsl:when>
                                                <xsl:when test="string-length(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate/@standardDate)=7">
                                                    <xsl:attribute name="rdf:datatype">
                                                        <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                                    </xsl:attribute>
                                                </xsl:when>
                                                <xsl:when test="string-length(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate/@standardDate)=10">
                                                    <xsl:attribute name="rdf:datatype">
                                                        <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                                    </xsl:attribute>
                                                </xsl:when>
                                                <xsl:otherwise/>
                                            </xsl:choose>
                                            
                                            <!-\-  <RiC:endDate rdf:datatype="http://www.w3.org/2001/XMLSchema#dateTime">-\->
                                            <xsl:value-of select="
                                                if (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate/@standardDate)!='')
                                                then (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate/@standardDate))
                                                else(
                                                if (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate)!='')
                                                then (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate))
                                                else()
                                                )
                                                "/>-->
                                            
                                            
                                            <!--      <xsl:value-of
                                                select="eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate/@standardDate"
                                            />-->
                                        </RiC:deathDate>
                                    </xsl:if>
                                </xsl:when>
                                <xsl:when test="$entType='corporateBody'">
                                    <xsl:if test="eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate">
                                        <RiC:beginningDate>
                                            <xsl:call-template name="outputDate">
                                                <xsl:with-param name="stdDate" select="eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate/@standardDate"></xsl:with-param>
                                                <xsl:with-param name="date" select="eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate"></xsl:with-param>
                                            </xsl:call-template>
                                            <!--     <xsl:choose>
                                            
                                            <xsl:when test="string-length(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate/@standardDate)=4">
                                                <xsl:attribute name="rdf:datatype"> <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text></xsl:attribute>
                                                
                                            </xsl:when>
                                            <xsl:when test="string-length(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate/@standardDate)=7">
                                                <xsl:attribute name="rdf:datatype">
                                                    <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                                </xsl:attribute>
                                            </xsl:when>
                                            <xsl:when test="string-length(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate/@standardDate)=10">
                                                <xsl:attribute name="rdf:datatype">
                                                    <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                                </xsl:attribute>
                                            </xsl:when>
                                            <xsl:otherwise/>
                                        </xsl:choose>
                                        
                                        <xsl:value-of select="
                                            if (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate/@standardDate)!='')
                                            then (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate/@standardDate))
                                            else(
                                            if (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate)!='')
                                            then (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:fromDate))
                                            else()
                                            )
                                            "/>
                                        -->
                                            
                                        </RiC:beginningDate>
                                    </xsl:if>
                                    <xsl:if
                                        test="eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate">
                                        <RiC:endDate>
                                            <xsl:call-template name="outputDate">
                                                <xsl:with-param name="stdDate" select="eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate/@standardDate"></xsl:with-param>
                                                <xsl:with-param name="date" select="eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate"></xsl:with-param>
                                            </xsl:call-template>
                                            <!--   <xsl:choose>
                                                
                                                <xsl:when test="string-length(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate/@standardDate)=4">
                                                    <xsl:attribute name="rdf:datatype"> <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text></xsl:attribute>
                                                    
                                                </xsl:when>
                                                <xsl:when test="string-length(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate/@standardDate)=7">
                                                    <xsl:attribute name="rdf:datatype">
                                                        <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                                    </xsl:attribute>
                                                </xsl:when>
                                                <xsl:when test="string-length(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate/@standardDate)=10">
                                                    <xsl:attribute name="rdf:datatype">
                                                        <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                                    </xsl:attribute>
                                                </xsl:when>
                                                <xsl:otherwise/>
                                            </xsl:choose>
                                            
                                            <!-\-  <RiC:endDate rdf:datatype="http://www.w3.org/2001/XMLSchema#dateTime">-\->
                                            <xsl:value-of select="
                                                if (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate/@standardDate)!='')
                                                then (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate/@standardDate))
                                                else(
                                                if (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate)!='')
                                                then (normalize-space(eac:cpfDescription/eac:description/eac:existDates/eac:dateRange/eac:toDate))
                                                else()
                                                )
                                                "/>
                                            -->
                                            
                                        </RiC:endDate>
                                    </xsl:if>
                                </xsl:when>
                            </xsl:choose>
                            
                      
                            <xsl:if
                                test="eac:cpfDescription/eac:description/eac:function[not(@localType)]/*[not(self::eac:term) and normalize-space(.)!='']">
                                
                                <RiC:description xml:lang="fr">
                                    <xsl:for-each select="eac:cpfDescription/eac:description/eac:function[not(@localType)]/*[not(self::eac:term) and normalize-space(.)!='']">
                                        <xsl:apply-templates></xsl:apply-templates>
                                        <xsl:if test="position()!=last()">
                                            <xsl:text> </xsl:text>
                                        </xsl:if>
                                    </xsl:for-each>
                                    
                                </RiC:description>
                                
                            </xsl:if>
                            <xsl:if
                                test="eac:cpfDescription/eac:description/eac:functions[eac:p]">
                                
                                <RiC:description xml:lang="fr">
                                    <xsl:for-each select="eac:cpfDescription/eac:description/eac:functions/eac:p[normalize-space(.)!='']">
                                        <xsl:value-of
                                            select="."
                                        />
                                        <xsl:if test="position()!=last()"><xsl:text> </xsl:text></xsl:if>
                                    </xsl:for-each>
                                </RiC:description>
                            </xsl:if>
                            <xsl:if test="eac:cpfDescription/eac:description/eac:biogHist/*[not(self::eac:chronList) and normalize-space(.)!='']">
                                <RiC:history xml:lang="fr">
                                    <xsl:for-each
                                        select="eac:cpfDescription/eac:description/eac:biogHist/*[not(self::eac:chronList) and normalize-space(.)!='']">
                                        <xsl:apply-templates/>
                                        <xsl:if test="position() != last()">
                                            <xsl:text> </xsl:text>
                                        </xsl:if>
                                    </xsl:for-each>
                                </RiC:history>
                                
                            </xsl:if>
                            <xsl:if
                                test="eac:cpfDescription/eac:description/eac:structureOrGenealogy[normalize-space(.) != '']">
                                <RiC:noteOnStructureOrGenealogy xml:lang="fr">
                                    <xsl:for-each select="eac:cpfDescription/eac:description/eac:structureOrGenealogy/*[normalize-space(.)!='']">
                                        <xsl:apply-templates/>
                                        <xsl:if test="position() != last()">
                                            <xsl:text> </xsl:text>
                                        </xsl:if>
                                    </xsl:for-each>
                                    
                                </RiC:noteOnStructureOrGenealogy>
                                
                            </xsl:if>
                            
                                
                                   <xsl:for-each
                                       select="eac:cpfDescription/eac:description/descendant::eac:mandate[normalize-space(eac:citation)!='']">
                                       
                                      
                                           <xsl:variable name="descNote" select="normalize-space(eac:descriptiveNote)"/>
                                       <xsl:variable name="cit" select="
                                           
                                           if (contains(eac:citation, '('))
                                           then (if (starts-with(normalize-space(eac:citation), '-'))
                                           then (normalize-space(substring-before(substring-after(eac:citation, '-'), '(')))
                                           else(
                                           
                                           normalize-space(substring-before(eac:citation, '('))
                                           )
                                           
                                           )
                                           else(
                                           if (contains(eac:citation, ':'))
                                           
                                           then (if (starts-with(normalize-space(eac:citation), '-'))
                                           then (normalize-space(substring-before(substring-after(eac:citation, '-'), ':')))
                                           else(
                                           
                                           normalize-space(substring-before(eac:citation, ':'))
                                           ))
                                           else(
                                           if (starts-with(normalize-space(eac:citation), '-'))
                                           then (normalize-space(substring-after(eac:citation, '-')))
                                           else(
                                           
                                           if (ends-with(normalize-space(eac:citation), ';'))
                                           then (normalize-space(substring(normalize-space(eac:citation), 1, string-length(normalize-space(eac:citation)) -1)))
                                           else(
                                           
                                           if (ends-with(normalize-space(eac:citation), '.'))
                                           then (normalize-space(substring(normalize-space(eac:citation), 1, string-length(normalize-space(eac:citation)) -1)))
                                           else(
                                           
                                           normalize-space(eac:citation)
                                           )
                                           
                                           )
                                           
                                           )
                                           )
                                           )"/>
                                        
                                           <xsl:if test="$an-mandates/rdf:Description[RiC:hasTitle=$cit and rdf:type[@rdf:resource='http://www.ica.org/standards/RiC/ontology#Mandate']]">
                                               
                                            
                                               <RiC:conformsToRule>
                                                                           <xsl:attribute name="rdf:resource">
                                                                               <xsl:value-of select="$an-mandates/rdf:Description[RiC:hasTitle=$cit and rdf:type[@rdf:resource='http://www.ica.org/standards/RiC/ontology#Mandate']]/@rdf:about"/>
                                                                               
                                                                           </xsl:attribute>
                                                                           
                                                                   </RiC:conformsToRule>
                                                           
                                               
                                               
                                               
                                               
                                           </xsl:if>
                                           
                                           
                                       
                                       
                                       
                                       
                                 
                                       
                                       
                                   </xsl:for-each>
                                  <!-- <xsl:for-each select="eac:cpfDescription/eac:description/descendant::eac:function[not(@localType) and contains(eac:term/@vocabularySource, ' http://data.culture.fr/thesaurus"></xsl:for-each>-->
                                  
                                   <xsl:for-each
                                       select="eac:cpfDescription/eac:description/descendant::eac:function[@localType = 'piaafFunction']">
                                       <xsl:variable name="functionName">
                                         <!--  <xsl:for-each select="eac:term">
                                               <xsl:value-of select="."/>
                                               <xsl:if test="position() != last()">
                                                   <xsl:text> -\- </xsl:text>
                                               </xsl:if>
                                           </xsl:for-each>-->
                                           <xsl:value-of select="eac:term[1]"/>
                                       </xsl:variable>
                                       <xsl:if
                                           test="$functions/rdf:Description[rdfs:label = $functionName]">
                                           <piaaf-onto:hasFunctionOfType>
                                               <xsl:attribute name="rdf:resource">
                                                   <xsl:value-of
                                                       select="$functions/rdf:Description[rdfs:label = $functionName]/@rdf:about"
                                                   />
                                               </xsl:attribute>
                                           </piaaf-onto:hasFunctionOfType>
                                       </xsl:if>
                                       
                                   </xsl:for-each>
                                 
                               
                            
                        
                        <xsl:for-each select="eac:cpfDescription/eac:description/eac:places/eac:place[normalize-space(eac:placeEntry)!='']">
                                
                                <xsl:variable name="placeName">
                                    <xsl:choose>
                                        <xsl:when test="eac:placeRole='Siège social'">
                                            <xsl:choose>
                                                <xsl:when test="parent::eac:places/eac:place[eac:placeRole='Siège']">
                                                    <xsl:value-of select="concat(normalize-space(parent::eac:places/eac:place[eac:placeRole='Siège']/eac:placeEntry), '. ', normalize-space(eac:placeEntry))"/>
                                                </xsl:when>
                                                <xsl:otherwise><xsl:value-of select="normalize-space(eac:placeEntry)"/></xsl:otherwise>
                                            </xsl:choose>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:value-of select="normalize-space(eac:placeEntry)"/>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </xsl:variable>
                                <xsl:if
                                    test="$an-places/rdf:Description[rdfs:label = $placeName]">
                                    
                                    <RiC:hasLocationRelation>
                                        <xsl:attribute name="rdf:resource">
                                            <xsl:value-of
                                                select="concat('http://piaaf.demo.logilab.fr/resource/FRAN_LocationRelation_', $theGoodRecId, '_', generate-id())"/>
                                            
                                        </xsl:attribute>
                                    </RiC:hasLocationRelation>
                                    
                                </xsl:if>
                            </xsl:for-each>
                          
                      

                           
                         
                            <xsl:for-each
                                select="eac:cpfDescription/eac:relations/eac:cpfRelation[@cpfRelationType = 'identity']">
                                <!-- identité -->
                                <!-- http://data.bnf.fr/ark:/12148/cb11863993z
                            http://catalogue.bnf.fr/ark:/12148/cb11863993z/PUBLIC
                            http://catalogue.bnf.fr/ark:/12148/cb11863993z/PUBLIC
                            http://data.bnf.fr/ark:/12148/cb11863993z#foaf:Organization-->
                                <!-- http://catalogue.bnf.fr/ark:/12148/cb11904421w -->
                                <!-- http://data.bnf.fr/ark:/12148/cb11862469g-->
                                <xsl:variable name="lnk" select="normalize-space(@xlink:href)"/>
                                <owl:sameAs>
                                    <xsl:attribute name="rdf:resource">
                                        <xsl:if test="contains($lnk, 'bnf.fr')">
                                            <xsl:choose>
                                                <xsl:when
                                                  test="contains($lnk, 'catalogue.bnf.fr/ark:/12148/') and contains($lnk, '/PUBLIC')">
                                                  <xsl:text>http://data.bnf.fr/ark:/12148/</xsl:text>
                                                  <xsl:value-of
                                                  select="substring-before(substring-after($lnk, 'http://catalogue.bnf.fr/ark:/12148/'), '/PUBLIC')"/>
                                                    <xsl:choose>
                                                        <xsl:when test="$RiCClass='CorporateBody'"><xsl:text>#foaf:Organization</xsl:text></xsl:when>
                                                        <xsl:when test="$RiCClass='Person'"><xsl:text>#foaf:Person</xsl:text></xsl:when>
                                                    </xsl:choose> 
                                                </xsl:when>
                                                <xsl:when test="contains($lnk, 'catalogue.bnf.fr/ark:/12148/') and not(contains($lnk, '/PUBLIC'))">
                                                    <xsl:text>http://data.bnf.fr/ark:/12148/</xsl:text>
                                                    <xsl:value-of
                                                        select="substring-after($lnk, 'http://catalogue.bnf.fr/ark:/12148/')"/>
                                                    <xsl:choose>
                                                        <xsl:when test="$RiCClass='CorporateBody'"><xsl:text>#foaf:Organization</xsl:text></xsl:when>
                                                        <xsl:when test="$RiCClass='Person'"><xsl:text>#foaf:Person</xsl:text></xsl:when>
                                                    </xsl:choose>
                                                </xsl:when>
                                                <xsl:when test="contains($lnk, 'http://data.bnf.fr/ark:/12148/')">
                                                    <xsl:value-of select="$lnk"/>
                                                    <xsl:choose>
                                                        <xsl:when test="$RiCClass='CorporateBody'"><xsl:text>#foaf:Organization</xsl:text></xsl:when>
                                                        <xsl:when test="$RiCClass='Person'"><xsl:text>#foaf:Person</xsl:text></xsl:when>
                                                    </xsl:choose>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:if>
                                                                              <!-- liens AN vers notices wikipedia -->
                                        <!-- exemple : http://fr.dbpedia.org/page/Henri_Labrouste, https://fr.wikipedia.org/wiki/Henri_Labrouste -->
                                                                                <xsl:if test="contains($lnk, 'wikipedia.org')">
                                            <xsl:text>http://fr.dbpedia.org/resource/</xsl:text>
                                            <xsl:value-of select="substring-after($lnk, 'https://fr.wikipedia.org/wiki/')"/>
                                        </xsl:if>

                                    </xsl:attribute>
                                </owl:sameAs>
                            </xsl:for-each>
                            <!-- les ISNI -->
                            <xsl:if test="eac:cpfDescription/eac:identity/eac:entityId[starts-with(normalize-space(.), 'ISNI')]">
                                <xsl:variable name="ISNIValue" select="normalize-space(eac:cpfDescription/eac:identity/eac:entityId[starts-with(normalize-space(.), 'ISNI')])"></xsl:variable>
                                <xsl:variable name="ISNIId">
                                    <xsl:choose>
                                        <xsl:when test="starts-with($ISNIValue, 'ISNI:')">
                                            <xsl:value-of select="replace(substring-after($ISNIValue, 'ISNI:'), ' ', '')"/>
                                        </xsl:when>
                                        <xsl:when test="starts-with($ISNIValue, 'ISNI :')">
                                            <xsl:value-of select="replace(substring-after($ISNIValue, 'ISNI :'), ' ', '')"/>
                                        </xsl:when>
                                        <xsl:when test="starts-with($ISNIValue, 'ISNI')">
                                            <xsl:value-of select="replace(substring-after($ISNIValue, 'ISNI'), ' ', '')"/>
                                        </xsl:when>
                                    </xsl:choose>
                                </xsl:variable>
                                <!-- http://isni.org/isni/0000000121032683-->
                                <isni:identifierValid><xsl:value-of select="$ISNIId"/></isni:identifierValid>
                                
                             
                            </xsl:if>
                            
                            <!-- les relations cpfType autres que identité -->
                            <xsl:for-each
                                select="eac:cpfDescription/eac:relations/eac:cpfRelation[contains(@xlink:arcrole, 'isControlledBy')]">
                                <xsl:call-template name="outputObjectPropertyForRelation">
                                    <xsl:with-param name="link"
                                        select="
                                            if (normalize-space(@xlink:href) = '' or not(@xlink:href))
                                            then
                                                ('non')
                                            else
                                                (normalize-space(@xlink:href))
                                            "/>
                                    <xsl:with-param name="relEntry"
                                        select="normalize-space(eac:relationEntry)"/>
                                    <xsl:with-param name="recId" select="$theGoodRecId"/>
                                    <xsl:with-param name="srcRelType" select="'isControlledBy'"/>
                                    <xsl:with-param name="arcToRelName"
                                        select="'agentControlledBy'"/>
                                    <xsl:with-param name="toAncRelArcName" select="'controlOnAgent'"/>
                                    <xsl:with-param name="relName" select="'AgentControlRelation'"/>
                                    <xsl:with-param name="relSecondArcName"
                                        select="'agentControlHeldBy'"/>
                                   <!-- <xsl:with-param name="shortcutRelName" select="'controlledBy'"/>-->
                                  <!--  <xsl:with-param name="relFromDate"
                                        select="eac:dateRange/eac:fromDate/@standardDate"/>
                                    -->
                                    <xsl:with-param name="relFromDate"
                                        select="   if (eac:dateRange/eac:fromDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:fromDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:fromDate))
                                        
                                        "/>
                                    <xsl:with-param name="relToDate"
                                        select="
                                        
                                        if (eac:dateRange/eac:toDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:toDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:toDate))
                                        
                                        
                                        
                                        
                                        "/>
                                        
                                    <xsl:with-param name="relDate"
                                        select="
                                        if (eac:date/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:date/@standardDate))
                                        else
                                        (normalize-space(eac:date))"
                                        
                                        
                                        >
                                        
                                    </xsl:with-param>
                                   <!-- <xsl:with-param name="relNote"
                                        select="normalize-space(eac:descriptiveNote/eac:p)"/>-->
                                    <xsl:with-param name="coll" select="$coll"/>
                                </xsl:call-template>


                            </xsl:for-each>
                            <xsl:for-each
                                select="eac:cpfDescription/eac:relations/eac:cpfRelation[contains(@xlink:arcrole, 'isAssociatedWithForItsControl')]">
                                <xsl:call-template name="outputObjectPropertyForRelation">
                                    <xsl:with-param name="link"
                                        select="
                                        if (normalize-space(@xlink:href) = '' or not(@xlink:href))
                                        then
                                        ('non')
                                        else
                                        (normalize-space(@xlink:href))
                                        "/>
                                    <xsl:with-param name="relEntry"
                                        select="normalize-space(eac:relationEntry)"/>
                                    <xsl:with-param name="recId" select="$theGoodRecId"/>
                                    <xsl:with-param name="srcRelType" select="'isAssociatedWithForItsControl'"/>
                                    <xsl:with-param name="arcToRelName"
                                        select="'agentControlledBy'"/>
                                    <xsl:with-param name="toAncRelArcName" select="'controlOnAgent'"/>
                                    <xsl:with-param name="relName" select="'AgentControlRelation'"/>
                                    <xsl:with-param name="relSecondArcName"
                                        select="'agentControlHeldBy'"/>
                                    <!-- <xsl:with-param name="shortcutRelName" select="'controlledBy'"/>-->
                                    <!--  <xsl:with-param name="relFromDate"
                                        select="eac:dateRange/eac:fromDate/@standardDate"/>
                                    -->
                                    <xsl:with-param name="relFromDate"
                                        select="   if (eac:dateRange/eac:fromDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:fromDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:fromDate))
                                        
                                        "/>
                                    <xsl:with-param name="relToDate"
                                        select="
                                        
                                        if (eac:dateRange/eac:toDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:toDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:toDate))
                                        
                                        
                                        
                                        
                                        "/>
                                    
                                    <xsl:with-param name="relDate"
                                        select="
                                        if (eac:date/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:date/@standardDate))
                                        else
                                        (normalize-space(eac:date))"
                                        
                                        
                                        >
                                        
                                    </xsl:with-param>
                                    <!-- <xsl:with-param name="relNote"
                                        select="normalize-space(eac:descriptiveNote/eac:p)"/>-->
                                    <xsl:with-param name="coll" select="$coll"/>
                                </xsl:call-template>
                                
                                
                            </xsl:for-each>
                            <xsl:for-each
                                select="eac:cpfDescription/eac:relations/eac:cpfRelation[contains(@xlink:arcrole, 'controls')]">
                                <xsl:call-template name="outputObjectPropertyForRelation">
                                    <xsl:with-param name="link"
                                        select="
                                        if (normalize-space(@xlink:href) = '' or not(@xlink:href))
                                        then
                                        ('non')
                                        else
                                        (normalize-space(@xlink:href))
                                        "/>
                                    <xsl:with-param name="relEntry"
                                        select="normalize-space(eac:relationEntry)"/>
                                    <xsl:with-param name="recId" select="$theGoodRecId"/>
                                    <xsl:with-param name="srcRelType" select="'controls'"/>
                                    <xsl:with-param name="arcToRelName"
                                        select="'hasAgentControlRelation'"/>
                                    <xsl:with-param name="toAncRelArcName" select="'agentControlHeldBy'"/>
                                    <xsl:with-param name="relName" select="'AgentControlRelation'"/>
                                    <xsl:with-param name="relSecondArcName"
                                        select="'controlOnAgent'"/>
                                    <!-- <xsl:with-param name="shortcutRelName" select="'controlledBy'"/>-->
                                    <!--  <xsl:with-param name="relFromDate"
                                        select="eac:dateRange/eac:fromDate/@standardDate"/>
                                    -->
                                    <xsl:with-param name="relFromDate"
                                        select="   if (eac:dateRange/eac:fromDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:fromDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:fromDate))
                                        
                                        "/>
                                    <xsl:with-param name="relToDate"
                                        select="
                                        
                                        if (eac:dateRange/eac:toDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:toDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:toDate))
                                        
                                        
                                        
                                        
                                        "/>
                                    
                                    <xsl:with-param name="relDate"
                                        select="
                                        if (eac:date/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:date/@standardDate))
                                        else
                                        (normalize-space(eac:date))"
                                        
                                        
                                        >
                                        
                                    </xsl:with-param>
                                    <!-- <xsl:with-param name="relNote"
                                        select="normalize-space(eac:descriptiveNote/eac:p)"/>-->
                                    <xsl:with-param name="coll" select="$coll"/>
                                </xsl:call-template>
                                
                                
                            </xsl:for-each>
                            <xsl:for-each
                                select="eac:cpfDescription/eac:relations/eac:cpfRelation[contains(@xlink:arcrole, 'hasMember')]">
                                <xsl:call-template name="outputObjectPropertyForRelation">
                                    <xsl:with-param name="link"
                                        select="
                                        if (normalize-space(@xlink:href) = '' or not(@xlink:href))
                                        then
                                        ('non')
                                        else
                                        (normalize-space(@xlink:href))
                                        "/>
                                    <xsl:with-param name="relEntry"
                                        select="normalize-space(eac:relationEntry)"/>
                                    <xsl:with-param name="recId" select="$theGoodRecId"/>
                                    <xsl:with-param name="srcRelType" select="'hasMember'"/>
                                    <xsl:with-param name="arcToRelName"
                                        select="'groupHasMember'"/>
                                    <xsl:with-param name="toAncRelArcName" select="'agentMembershipIn'"/>
                                    <xsl:with-param name="relName" select="'AgentMembershipRelation'"/>
                                    <xsl:with-param name="relSecondArcName"
                                        select="'hasAgentMember'"/>
                                    <!-- <xsl:with-param name="shortcutRelName" select="'controlledBy'"/>-->
                                    <!--  <xsl:with-param name="relFromDate"
                                        select="eac:dateRange/eac:fromDate/@standardDate"/>
                                    -->
                                    <xsl:with-param name="relFromDate"
                                        select="   if (eac:dateRange/eac:fromDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:fromDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:fromDate))
                                        
                                        "/>
                                    <xsl:with-param name="relToDate"
                                        select="
                                        
                                        if (eac:dateRange/eac:toDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:toDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:toDate))
                                        
                                        
                                        
                                        
                                        "/>
                                    
                                    <xsl:with-param name="relDate"
                                        select="
                                        if (eac:date/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:date/@standardDate))
                                        else
                                        (normalize-space(eac:date))"
                                        
                                        
                                        >
                                        
                                    </xsl:with-param>
                                    <!-- <xsl:with-param name="relNote"
                                        select="normalize-space(eac:descriptiveNote/eac:p)"/>-->
                                    <xsl:with-param name="coll" select="$coll"/>
                                </xsl:call-template>
                                
                                
                            </xsl:for-each>
                            <xsl:for-each
                                select="eac:cpfDescription/eac:relations/eac:cpfRelation[contains(@xlink:arcrole, 'isMemberOf')]">
                                <xsl:call-template name="outputObjectPropertyForRelation">
                                    <xsl:with-param name="link"
                                        select="
                                        if (normalize-space(@xlink:href) = '' or not(@xlink:href))
                                        then
                                        ('non')
                                        else
                                        (normalize-space(@xlink:href))
                                        "/>
                                    <xsl:with-param name="relEntry"
                                        select="normalize-space(eac:relationEntry)"/>
                                    <xsl:with-param name="recId" select="$theGoodRecId"/>
                                    <xsl:with-param name="srcRelType" select="'isMemberOf'"/>
                                    <xsl:with-param name="arcToRelName"
                                        select="'agentHasMembershipRelation'"/>
                                    <xsl:with-param name="toAncRelArcName" select="'hasAgentMember'"/>
                                    <xsl:with-param name="relName" select="'AgentMembershipRelation'"/>
                                    <xsl:with-param name="relSecondArcName"
                                        select="'agentMembershipIn'"/>
                                    <!-- <xsl:with-param name="shortcutRelName" select="'controlledBy'"/>-->
                                    <!--  <xsl:with-param name="relFromDate"
                                        select="eac:dateRange/eac:fromDate/@standardDate"/>
                                    -->
                                    <xsl:with-param name="relFromDate"
                                        select="   if (eac:dateRange/eac:fromDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:fromDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:fromDate))
                                        
                                        "/>
                                    <xsl:with-param name="relToDate"
                                        select="
                                        
                                        if (eac:dateRange/eac:toDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:toDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:toDate))
                                        
                                        
                                        
                                        
                                        "/>
                                    
                                    <xsl:with-param name="relDate"
                                        select="
                                        if (eac:date/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:date/@standardDate))
                                        else
                                        (normalize-space(eac:date))"
                                        
                                        
                                        >
                                        
                                    </xsl:with-param>
                                    <!-- <xsl:with-param name="relNote"
                                        select="normalize-space(eac:descriptiveNote/eac:p)"/>-->
                                    <xsl:with-param name="coll" select="$coll"/>
                                </xsl:call-template>
                                
                                
                            </xsl:for-each>
                            <xsl:for-each
                                select="eac:cpfDescription/eac:relations/eac:cpfRelation[contains(@xlink:arcrole, 'hasEmployee')]">
                                <xsl:call-template name="outputObjectPropertyForRelation">
                                    <xsl:with-param name="link"
                                        select="
                                        if (normalize-space(@xlink:href) = '' or not(@xlink:href))
                                        then
                                        ('non')
                                        else
                                        (normalize-space(@xlink:href))
                                        "/>
                                    <xsl:with-param name="relEntry"
                                        select="normalize-space(eac:relationEntry)"/>
                                    <xsl:with-param name="recId" select="$theGoodRecId"/>
                                    <xsl:with-param name="srcRelType" select="'hasEmployee'"/>
                                    <xsl:with-param name="arcToRelName"
                                        select="'groupHasMember'"/>
                                    <xsl:with-param name="toAncRelArcName" select="'agentMembershipIn'"/>
                                    <xsl:with-param name="relName" select="'AgentMembershipRelation'"/>
                                    <xsl:with-param name="relSecondArcName"
                                        select="'hasAgentMember'"/>
                                    <!-- <xsl:with-param name="shortcutRelName" select="'controlledBy'"/>-->
                                    <!--  <xsl:with-param name="relFromDate"
                                        select="eac:dateRange/eac:fromDate/@standardDate"/>
                                    -->
                                    <xsl:with-param name="relFromDate"
                                        select="   if (eac:dateRange/eac:fromDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:fromDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:fromDate))
                                        
                                        "/>
                                    <xsl:with-param name="relToDate"
                                        select="
                                        
                                        if (eac:dateRange/eac:toDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:toDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:toDate))
                                        
                                        
                                        
                                        
                                        "/>
                                    
                                    <xsl:with-param name="relDate"
                                        select="
                                        if (eac:date/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:date/@standardDate))
                                        else
                                        (normalize-space(eac:date))"
                                        
                                        
                                        >
                                        
                                    </xsl:with-param>
                                    <!-- <xsl:with-param name="relNote"
                                        select="normalize-space(eac:descriptiveNote/eac:p)"/>-->
                                    <xsl:with-param name="coll" select="$coll"/>
                                </xsl:call-template>
                                
                                
                            </xsl:for-each>
                            <xsl:for-each
                                select="eac:cpfDescription/eac:relations/eac:cpfRelation[contains(@xlink:arcrole, 'isEmployeeOf')]">
                                <xsl:call-template name="outputObjectPropertyForRelation">
                                    <xsl:with-param name="link"
                                        select="
                                        if (normalize-space(@xlink:href) = '' or not(@xlink:href))
                                        then
                                        ('non')
                                        else
                                        (normalize-space(@xlink:href))
                                        "/>
                                    <xsl:with-param name="relEntry"
                                        select="normalize-space(eac:relationEntry)"/>
                                    <xsl:with-param name="recId" select="$theGoodRecId"/>
                                    <xsl:with-param name="srcRelType" select="'isEmployeeOf'"/>
                                    <xsl:with-param name="arcToRelName"
                                        select="'agentHasMembershipRelation'"/>
                                    <xsl:with-param name="toAncRelArcName" select="'hasAgentMember'"/>
                                    <xsl:with-param name="relName" select="'AgentMembershipRelation'"/>
                                    <xsl:with-param name="relSecondArcName"
                                        select="'agentMembershipIn'"/>
                                    <!-- <xsl:with-param name="shortcutRelName" select="'controlledBy'"/>-->
                                    <!--  <xsl:with-param name="relFromDate"
                                        select="eac:dateRange/eac:fromDate/@standardDate"/>
                                    -->
                                    <xsl:with-param name="relFromDate"
                                        select="   if (eac:dateRange/eac:fromDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:fromDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:fromDate))
                                        
                                        "/>
                                    <xsl:with-param name="relToDate"
                                        select="
                                        
                                        if (eac:dateRange/eac:toDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:toDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:toDate))
                                        
                                        
                                        
                                        
                                        "/>
                                    
                                    <xsl:with-param name="relDate"
                                        select="
                                        if (eac:date/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:date/@standardDate))
                                        else
                                        (normalize-space(eac:date))"
                                        
                                        
                                        >
                                        
                                    </xsl:with-param>
                                    <!-- <xsl:with-param name="relNote"
                                        select="normalize-space(eac:descriptiveNote/eac:p)"/>-->
                                    <xsl:with-param name="coll" select="$coll"/>
                                </xsl:call-template>
                                
                                
                            </xsl:for-each>
                            <xsl:for-each
                                select="eac:cpfDescription/eac:relations/eac:cpfRelation[contains(@xlink:arcrole, 'isDirectorOf')]">
                                <xsl:call-template name="outputObjectPropertyForRelation">
                                    <xsl:with-param name="link"
                                        select="
                                        if (normalize-space(@xlink:href) = '' or not(@xlink:href))
                                        then
                                        ('non')
                                        else
                                        (normalize-space(@xlink:href))
                                        "/>
                                    <xsl:with-param name="relEntry"
                                        select="normalize-space(eac:relationEntry)"/>
                                    <xsl:with-param name="recId" select="$theGoodRecId"/>
                                    <xsl:with-param name="srcRelType" select="'isDirectorOf'"/>
                                    <xsl:with-param name="arcToRelName"
                                        select="'agentHasLeadershipRelation'"/>
                                    <xsl:with-param name="toAncRelArcName" select="'leadershipBy'"/>
                                    <xsl:with-param name="relName" select="'AgentLeadershipRelation'"/>
                                    <xsl:with-param name="relSecondArcName"
                                        select="'leadershipOn'"/>
                                 <!--   <xsl:with-param name="shortcutRelName" select="'controlledBy'"/>-->
                                    <xsl:with-param name="relFromDate"
                                        select="   if (eac:dateRange/eac:fromDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:fromDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:fromDate))
                                        
                                        "/>
                                    <xsl:with-param name="relToDate"
                                        select="
                                        
                                        if (eac:dateRange/eac:toDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:toDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:toDate))
                                        
                                        
                                        
                                        
                                        "/>
                                    <!--<xsl:with-param name="relNote"
                                        select="normalize-space(eac:descriptiveNote/eac:p)"/>
                                    -->
                                    <xsl:with-param name="relDate"
                                        select="
                                        if (eac:date/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:date/@standardDate))
                                        else
                                        (normalize-space(eac:date))"
                                        
                                        
                                        >
                                        
                                    </xsl:with-param>
                                    <xsl:with-param name="coll" select="$coll"/>
                                </xsl:call-template>
                                
                                
                            </xsl:for-each>
                            <xsl:for-each
                                select="eac:cpfDescription/eac:relations/eac:cpfRelation[contains(@xlink:arcrole, 'isDirectedBy')]">
                                <xsl:call-template name="outputObjectPropertyForRelation">
                                    <xsl:with-param name="link"
                                        select="
                                        if (normalize-space(@xlink:href) = '' or not(@xlink:href))
                                        then
                                        ('non')
                                        else
                                        (normalize-space(@xlink:href))
                                        "/>
                                    <xsl:with-param name="relEntry"
                                        select="normalize-space(eac:relationEntry)"/>
                                    <xsl:with-param name="recId" select="$theGoodRecId"/>
                                    <xsl:with-param name="srcRelType" select="'isDirectedBy'"/>
                                    <xsl:with-param name="arcToRelName"
                                        select="'leadBy'"/>
                                    <xsl:with-param name="toAncRelArcName" select="'leadershipOn'"/>
                                    <xsl:with-param name="relName" select="'AgentLeadershipRelation'"/>
                                    <xsl:with-param name="relSecondArcName"
                                        select="'leadershipBy'"/>
                                    <!--   <xsl:with-param name="shortcutRelName" select="'controlledBy'"/>-->
                                    <xsl:with-param name="relFromDate"
                                        select="   if (eac:dateRange/eac:fromDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:fromDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:fromDate))
                                        
                                        "/>
                                    <xsl:with-param name="relToDate"
                                        select="
                                        
                                        if (eac:dateRange/eac:toDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:toDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:toDate))
                                        
                                        
                                        
                                        
                                        "/>
                                    <!--<xsl:with-param name="relNote"
                                        select="normalize-space(eac:descriptiveNote/eac:p)"/>
                                    -->
                                    <xsl:with-param name="relDate"
                                        select="
                                        if (eac:date/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:date/@standardDate))
                                        else
                                        (normalize-space(eac:date))"
                                        
                                        
                                        >
                                        
                                    </xsl:with-param>
                                    <xsl:with-param name="coll" select="$coll"/>
                                </xsl:call-template>
                                
                                
                            </xsl:for-each>
                            <xsl:for-each
                                select="eac:cpfDescription/eac:relations/eac:cpfRelation[contains(@xlink:arcrole, 'isFunctionallyLinkedTo')]">
                                <xsl:call-template name="outputObjectPropertyForRelation">
                                    <xsl:with-param name="link"
                                        select="
                                            if (normalize-space(@xlink:href) = '' or not(@xlink:href))
                                            then
                                                ('non')
                                            else
                                                (normalize-space(@xlink:href))
                                            "/>
                                    <xsl:with-param name="recId" select="$theGoodRecId"/>
                                    <xsl:with-param name="relEntry"
                                        select="normalize-space(eac:relationEntry)"/>
                                    <xsl:with-param name="srcRelType"
                                        select="'isFunctionallyLinkedTo'"/>
                                    <xsl:with-param name="arcToRelName"
                                        select="'hasBusinessRelation'"/>
                                    <xsl:with-param name="toAncRelArcName"
                                        select="'businessRelationWith'"/>
                                    <xsl:with-param name="relName" select="'BusinessRelation'"/>
                                    <xsl:with-param name="relSecondArcName"
                                        select="'businessRelationWith'"/>
                                 <!--   <xsl:with-param name="shortcutRelName"
                                        select="'functionallyLinkedTo'"/>-->
                                    <xsl:with-param name="relFromDate"
                                        select="   if (eac:dateRange/eac:fromDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:fromDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:fromDate))
                                        
                                        "/>
                                    <xsl:with-param name="relToDate"
                                        select="
                                        
                                        if (eac:dateRange/eac:toDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:toDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:toDate))
                                        
                                        
                                        
                                        
                                        "/>
                              <!--      <xsl:with-param name="relNote"
                                        select="normalize-space(eac:descriptiveNote/eac:p)"/>-->
                                    <xsl:with-param name="relDate"
                                        select="
                                        if (eac:date/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:date/@standardDate))
                                        else
                                        (normalize-space(eac:date))"
                                        
                                        
                                        >
                                        
                                    </xsl:with-param>
                                    <xsl:with-param name="coll" select="$coll"/>
                                </xsl:call-template>

                            </xsl:for-each>
                            <xsl:for-each
                                select="eac:cpfDescription/eac:relations/eac:cpfRelation[@cpfRelationtype= 'family']">
                                <xsl:call-template name="outputObjectPropertyForRelation">
                                    <xsl:with-param name="link"
                                        select="
                                        if (normalize-space(@xlink:href) = '' or not(@xlink:href))
                                        then
                                        ('non')
                                        else
                                        (normalize-space(@xlink:href))
                                        "/>
                                    <xsl:with-param name="recId" select="$theGoodRecId"/>
                                    <xsl:with-param name="relEntry"
                                        select="normalize-space(eac:relationEntry)"/>
                                    <xsl:with-param name="srcRelType"
                                        select="'family'"/>
                                    <xsl:with-param name="arcToRelName"
                                        select="'hasFamilyRelation'"/>
                                    <xsl:with-param name="toAncRelArcName"
                                        select="'familyRelationWith'"/>
                                    <xsl:with-param name="relName" select="'FamilyRelation'"/>
                                    <xsl:with-param name="relSecondArcName"
                                        select="'familyRelationWith'"/>
                                    <!--   <xsl:with-param name="shortcutRelName"
                                        select="'functionallyLinkedTo'"/>-->
                                    <xsl:with-param name="relFromDate"
                                        select="   if (eac:dateRange/eac:fromDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:fromDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:fromDate))
                                        
                                        "/>
                                    <xsl:with-param name="relToDate"
                                        select="
                                        
                                        if (eac:dateRange/eac:toDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:toDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:toDate))
                                        
                                        
                                        
                                        
                                        "/>
                                    <!--      <xsl:with-param name="relNote"
                                        select="normalize-space(eac:descriptiveNote/eac:p)"/>-->
                                    <xsl:with-param name="relDate"
                                        select="
                                        if (eac:date/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:date/@standardDate))
                                        else
                                        (normalize-space(eac:date))"
                                        
                                        
                                        >
                                        
                                    </xsl:with-param>
                                    <xsl:with-param name="coll" select="$coll"/>
                                </xsl:call-template>
                                
                            </xsl:for-each>
                            <xsl:for-each
                                select="eac:cpfDescription/eac:relations/eac:cpfRelation[contains(@xlink:arcrole, 'knows')]">
                                <xsl:call-template name="outputObjectPropertyForRelation">
                                    <xsl:with-param name="link"
                                        select="
                                        if (normalize-space(@xlink:href) = '' or not(@xlink:href))
                                        then
                                        ('non')
                                        else
                                        (normalize-space(@xlink:href))
                                        "/>
                                    <xsl:with-param name="recId" select="$theGoodRecId"/>
                                    <xsl:with-param name="relEntry"
                                        select="normalize-space(eac:relationEntry)"/>
                                    <xsl:with-param name="srcRelType"
                                        select="'knows'"/>
                                    <xsl:with-param name="arcToRelName"
                                        select="'hasSocialRelation'"/>
                                    <xsl:with-param name="toAncRelArcName"
                                        select="'relationAssociates'"/>
                                    <xsl:with-param name="relName" select="'SocialRelation'"/>
                                    <xsl:with-param name="relSecondArcName"
                                        select="'relationAssociates'"/>
                                    <!--   <xsl:with-param name="shortcutRelName"
                                        select="'functionallyLinkedTo'"/>-->
                                    <xsl:with-param name="relFromDate"
                                        select="   if (eac:dateRange/eac:fromDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:fromDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:fromDate))
                                        
                                        "/>
                                    <xsl:with-param name="relToDate"
                                        select="
                                        
                                        if (eac:dateRange/eac:toDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:toDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:toDate))
                                        
                                        
                                        
                                        
                                        "/>
                                    <!--      <xsl:with-param name="relNote"
                                        select="normalize-space(eac:descriptiveNote/eac:p)"/>-->
                                    <xsl:with-param name="relDate"
                                        select="
                                        if (eac:date/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:date/@standardDate))
                                        else
                                        (normalize-space(eac:date))"
                                        
                                        
                                        >
                                        
                                    </xsl:with-param>
                                    <xsl:with-param name="coll" select="$coll"/>
                                </xsl:call-template>
                                
                            </xsl:for-each>
                            <xsl:for-each
                                select="eac:cpfDescription/eac:relations/eac:cpfRelation[contains(@xlink:arcrole, 'hasPart')]">
                                <xsl:call-template name="outputObjectPropertyForRelation">
                                    <xsl:with-param name="link"
                                        select="
                                            if (normalize-space(@xlink:href) = '' or not(@xlink:href))
                                            then
                                                ('non')
                                            else
                                                (normalize-space(@xlink:href))
                                            "/>
                                    <xsl:with-param name="recId" select="$theGoodRecId"/>
                                    <xsl:with-param name="relEntry"
                                        select="normalize-space(eac:relationEntry)"/>
                                    <xsl:with-param name="srcRelType" select="'hasPart'"/>
                                    <xsl:with-param name="arcToRelName" select="'groupHasPart'"/>
                                    <xsl:with-param name="toAncRelArcName" select="'agentPartOf'"/>
                                    <xsl:with-param name="relName" select="'AgentWholePartRelation'"/>
                                    <xsl:with-param name="relSecondArcName" select="'wholeHasAgentPart'"/>
                                <!--    <xsl:with-param name="shortcutRelName" select="'integrates'"/>-->
                                    <xsl:with-param name="relFromDate"
                                        select="   if (eac:dateRange/eac:fromDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:fromDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:fromDate))
                                        
                                        "/>
                                    <xsl:with-param name="relToDate"
                                        select="
                                        
                                        if (eac:dateRange/eac:toDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:toDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:toDate))
                                        
                                        
                                        
                                        
                                        "/>
                                    <xsl:with-param name="relDate"
                                        select="
                                        if (eac:date/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:date/@standardDate))
                                        else
                                        (normalize-space(eac:date))"
                                        
                                        
                                        >
                                        
                                    </xsl:with-param>
                                   <!-- <xsl:with-param name="relNote"
                                        select="normalize-space(eac:descriptiveNote/eac:p)"/>-->
                                    <xsl:with-param name="coll" select="$coll"/>
                                </xsl:call-template>

                            </xsl:for-each>
                            <xsl:for-each
                                select="eac:cpfDescription/eac:relations/eac:cpfRelation[contains(@xlink:arcrole, 'isPartOf')]">
                                <xsl:call-template name="outputObjectPropertyForRelation">
                                    <xsl:with-param name="link"
                                        select="
                                            if (normalize-space(@xlink:href) = '' or not(@xlink:href))
                                            then
                                                ('non')
                                            else
                                                (normalize-space(@xlink:href))
                                            "/>
                                    <xsl:with-param name="recId" select="$theGoodRecId"/>
                                    <xsl:with-param name="relEntry"
                                        select="normalize-space(eac:relationEntry)"/>
                                    <xsl:with-param name="srcRelType" select="'isPartOf'"/>
                                    <xsl:with-param name="arcToRelName"
                                        select="'agentHasWholePartRelation'"/>
                                    <xsl:with-param name="toAncRelArcName" select="'wholeHasAgentPart'"/>
                                    <xsl:with-param name="relName" select="'AgentWholePartRelation'"/>
                                    <xsl:with-param name="relSecondArcName" select="'agentPartOf'"/>
                                  <!--  <xsl:with-param name="shortcutRelName" select="'integratedIn'"/>-->
                                    <xsl:with-param name="relFromDate"
                                        select="   if (eac:dateRange/eac:fromDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:fromDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:fromDate))
                                        
                                        "/>
                                    <xsl:with-param name="relToDate"
                                        select="
                                        
                                        if (eac:dateRange/eac:toDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:toDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:toDate))
                                        
                                        
                                        
                                        
                                        "/>
                                    <xsl:with-param name="relDate"
                                        select="
                                        if (eac:date/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:date/@standardDate))
                                        else
                                        (normalize-space(eac:date))"
                                        
                                        
                                        >
                                        
                                    </xsl:with-param>
                               <!--     <xsl:with-param name="relNote"
                                        select="normalize-space(eac:descriptiveNote/eac:p)"/>
                                        
-->   <xsl:with-param name="coll" select="$coll"/>
                                </xsl:call-template>

                            </xsl:for-each>
                            <xsl:for-each
                                select="eac:cpfDescription/eac:relations/eac:cpfRelation[@cpfRelationType = 'temporal-earlier']">
                                <xsl:call-template name="outputObjectPropertyForRelation">
                                    <xsl:with-param name="link"
                                        select="
                                            if (normalize-space(@xlink:href) = '' or not(@xlink:href))
                                            then
                                                ('non')
                                            else
                                                (normalize-space(@xlink:href))
                                            "/>
                                    <xsl:with-param name="recId" select="$theGoodRecId"/>
                                    <xsl:with-param name="relEntry"
                                        select="normalize-space(eac:relationEntry)"/>
                                    <xsl:with-param name="srcRelType" select="'temporal-earlier'"/>
                                    <xsl:with-param name="arcToRelName" select="'agentFollows'"/>
                                    <xsl:with-param name="toAncRelArcName"
                                        select="'followingAgent'"/>
                                    <xsl:with-param name="relName" select="'AgentTemporalRelation'"/>
                                    <xsl:with-param name="relSecondArcName"
                                        select="'precedingAgent'"/>
                                  <!--  <xsl:with-param name="shortcutRelName" select="'successorOf'"/>-->
                                    <xsl:with-param name="relFromDate"
                                        select="   if (eac:dateRange/eac:fromDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:fromDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:fromDate))
                                        
                                        "/>
                                    <xsl:with-param name="relToDate"
                                        select="
                                        
                                        if (eac:dateRange/eac:toDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:toDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:toDate))
                                        
                                        
                                        
                                        
                                        "/>
                                    <xsl:with-param name="relDate"
                                        select="
                                        if (eac:date/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:date/@standardDate))
                                        else
                                        (normalize-space(eac:date))"
                                        
                                        
                                        >
                                        
                                    </xsl:with-param>
                                 <!--   <xsl:with-param name="relNote"
                                        select="normalize-space(eac:descriptiveNote/eac:p)"/>
-->   <xsl:with-param name="coll" select="$coll"/>
                                </xsl:call-template>

                            </xsl:for-each>
                            <xsl:for-each
                                select="eac:cpfDescription/eac:relations/eac:cpfRelation[@cpfRelationType = 'temporal-later']">
                                <xsl:call-template name="outputObjectPropertyForRelation">
                                    <xsl:with-param name="link"
                                        select="
                                            if (normalize-space(@xlink:href) = '' or not(@xlink:href))
                                            then
                                                ('non')
                                            else
                                                (normalize-space(@xlink:href))
                                            "/>
                                    <xsl:with-param name="recId" select="$theGoodRecId"/>
                                    <xsl:with-param name="relEntry"
                                        select="normalize-space(eac:relationEntry)"/>
                                    <xsl:with-param name="srcRelType" select="'temporal-later'"/>
                                    <xsl:with-param name="arcToRelName" select="'agentPrecedes'"/>
                                    <xsl:with-param name="toAncRelArcName"
                                        select="'precedingAgent'"/>
                                    <xsl:with-param name="relName" select="'AgentTemporalRelation'"/>
                                    <xsl:with-param name="relSecondArcName"
                                        select="'followingAgent'"/>
                                  <!--  <xsl:with-param name="shortcutRelName" select="'predecessorOf'"/>-->
                                    <xsl:with-param name="relFromDate"
                                        select="   if (eac:dateRange/eac:fromDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:fromDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:fromDate))
                                        
                                        "/>
                                    <xsl:with-param name="relToDate"
                                        select="
                                        
                                        if (eac:dateRange/eac:toDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:toDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:toDate))
                                        
                                        
                                        
                                        
                                        "/>
                                    <xsl:with-param name="relDate"
                                        select="
                                        if (eac:date/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:date/@standardDate))
                                        else
                                        (normalize-space(eac:date))"
                                        
                                        
                                        >
                                        
                                    </xsl:with-param>
                                 <!--   <xsl:with-param name="relNote"
                                        select="normalize-space(eac:descriptiveNote/eac:p)"/>
-->   <xsl:with-param name="coll" select="$coll"/>
                                </xsl:call-template>

                            </xsl:for-each>
                            <xsl:for-each
                                select="eac:cpfDescription/eac:relations/eac:cpfRelation[@cpfRelationType = 'hierarchical-child' and (not(@xlink:arcrole) or @xlink:arcrole='')]">
                                <xsl:call-template name="outputObjectPropertyForRelation">
                                    <xsl:with-param name="link"
                                        select="
                                        if (normalize-space(@xlink:href) = '' or not(@xlink:href))
                                        then
                                        ('non')
                                        else
                                        (normalize-space(@xlink:href))
                                        "/>
                                    <xsl:with-param name="recId" select="$theGoodRecId"/>
                                    <xsl:with-param name="relEntry"
                                        select="normalize-space(eac:relationEntry)"/>
                                    <xsl:with-param name="srcRelType" select="'hierarchical-child'"/>
                                    <xsl:with-param name="arcToRelName" select="'agentIsSuperiorInHierarchicalRelation'"/>
                                    <xsl:with-param name="toAncRelArcName"
                                        select="'hasHierarchicalParent'"/>
                                    <xsl:with-param name="relName" select="'AgentHierarchicalRelation'"/>
                                    <xsl:with-param name="relSecondArcName"
                                        select="'hasHierarchicalChild'"/>
                                <!--    <xsl:with-param name="shortcutRelName" select="'predecessorOf'"/>-->
                                    <xsl:with-param name="relFromDate"
                                        select="   if (eac:dateRange/eac:fromDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:fromDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:fromDate))
                                        
                                        "/>
                                    <xsl:with-param name="relToDate"
                                        select="
                                        
                                        if (eac:dateRange/eac:toDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:toDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:toDate))
                                        
                                        
                                        
                                        
                                        "/>
                                    <xsl:with-param name="relDate"
                                        select="
                                        if (eac:date/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:date/@standardDate))
                                        else
                                        (normalize-space(eac:date))"
                                        
                                        
                                        >
                                        
                                    </xsl:with-param>
                                  <!--  <xsl:with-param name="relNote"
                                        select="normalize-space(eac:descriptiveNote/eac:p)"/>-->
                                    <xsl:with-param name="coll" select="$coll"/>  
                                </xsl:call-template>
                                
                            </xsl:for-each>
                            <xsl:for-each
                                select="eac:cpfDescription/eac:relations/eac:cpfRelation[@cpfRelationType = 'hierarchical-parent'  and (not(@xlink:arcrole) or @xlink:arcrole='')]">
                                <xsl:call-template name="outputObjectPropertyForRelation">
                                    <xsl:with-param name="link"
                                        select="
                                        if (normalize-space(@xlink:href) = '' or not(@xlink:href))
                                        then
                                        ('non')
                                        else
                                        (normalize-space(@xlink:href))
                                        "/>
                                    <xsl:with-param name="recId" select="$theGoodRecId"/>
                                    <xsl:with-param name="relEntry"
                                        select="normalize-space(eac:relationEntry)"/>
                                    <xsl:with-param name="srcRelType" select="'hierarchical-parent'"/>
                                    <xsl:with-param name="arcToRelName" select="'agentIsInferiorInHierarchicalRelation'"/>
                                    <xsl:with-param name="toAncRelArcName"
                                        select="'hasHierarchicalChild'"/>
                                    <xsl:with-param name="relName" select="'AgentHierarchicalRelation'"/>
                                    <xsl:with-param name="relSecondArcName"
                                        select="'hasHierarchicalParent'"/>
                                    <!--    <xsl:with-param name="shortcutRelName" select="'predecessorOf'"/>-->
                                    <xsl:with-param name="relFromDate"
                                        select="   if (eac:dateRange/eac:fromDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:fromDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:fromDate))
                                        
                                        "/>
                                    <xsl:with-param name="relToDate"
                                        select="
                                        
                                        if (eac:dateRange/eac:toDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:toDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:toDate))
                                        
                                        
                                        
                                        
                                        "/>
                                    <xsl:with-param name="relDate"
                                        select="
                                        if (eac:date/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:date/@standardDate))
                                        else
                                        (normalize-space(eac:date))"
                                        
                                        
                                        >
                                        
                                    </xsl:with-param>
                                    <!--  <xsl:with-param name="relNote"
                                        select="normalize-space(eac:descriptiveNote/eac:p)"/>-->
                                    <xsl:with-param name="coll" select="$coll"/> 
                                </xsl:call-template>
                                
                            </xsl:for-each>
                            <xsl:for-each
                                select="eac:cpfDescription/eac:relations/eac:cpfRelation[@cpfRelationType = 'associative'  and (not(@xlink:arcrole) or @xlink:arcrole='')]">
                                <xsl:call-template name="outputObjectPropertyForRelation">
                                    <xsl:with-param name="link"
                                        select="
                                        if (normalize-space(@xlink:href) = '' or not(@xlink:href))
                                        then
                                        ('non')
                                        else
                                        (normalize-space(@xlink:href))
                                        "/>
                                    <xsl:with-param name="recId" select="$theGoodRecId"/>
                                    <xsl:with-param name="relEntry"
                                        select="normalize-space(eac:relationEntry)"/>
                                    <xsl:with-param name="srcRelType" select="'associative'"/>
                                    <xsl:with-param name="arcToRelName" select="'associatedWithRelation'"/>
                                    <xsl:with-param name="toAncRelArcName"
                                        select="'relationAssociates'"/>
                                    <xsl:with-param name="relName" select="'SocialRelation'"/>
                                    <xsl:with-param name="relSecondArcName"
                                        select="'relationAssociates'"/>
                                    <!--    <xsl:with-param name="shortcutRelName" select="'predecessorOf'"/>-->
                                    <xsl:with-param name="relFromDate"
                                        select="   if (eac:dateRange/eac:fromDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:fromDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:fromDate))
                                        
                                        "/>
                                    <xsl:with-param name="relToDate"
                                        select="
                                        
                                        if (eac:dateRange/eac:toDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:toDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:toDate))
                                        
                                        
                                        
                                        
                                        "/>
                                    <xsl:with-param name="relDate"
                                        select="
                                        if (eac:date/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:date/@standardDate))
                                        else
                                        (normalize-space(eac:date))"
                                        
                                        
                                        >
                                        
                                    </xsl:with-param>
                                    <!--  <xsl:with-param name="relNote"
                                        select="normalize-space(eac:descriptiveNote/eac:p)"/>-->
                                    <xsl:with-param name="coll" select="$coll"/>  
                                </xsl:call-template>
                                
                            </xsl:for-each>
                            <xsl:for-each select="eac:cpfDescription/eac:relations/eac:resourceRelation[@resourceRelationType = 'creatorOf']">
                                
                                <xsl:call-template name="outputObjectPropertyForProvenanceRelation">
                                    <xsl:with-param name="link"
                                        select="
                                        if (normalize-space(@xlink:href) = '' or not(@xlink:href))
                                        then
                                        ('non')
                                        else
                                        (normalize-space(@xlink:href))
                                        "/>
                                    <xsl:with-param name="recId" select="$recId"/>
                                    <xsl:with-param name="relEntry"
                                        select="normalize-space(eac:relationEntry)"/>
                                    
                                    
                                    
                                    <!--    <xsl:with-param name="shortcutRelName" select="'predecessorOf'"/>-->
                                    <xsl:with-param name="relFromDate"
                                        select="   if (eac:dateRange/eac:fromDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:fromDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:fromDate))
                                        
                                        "/>
                                    <xsl:with-param name="relToDate"
                                        select="
                                        
                                        if (eac:dateRange/eac:toDate/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:dateRange/eac:toDate/@standardDate))
                                        else
                                        (normalize-space(eac:dateRange/eac:toDate))
                                        
                                        
                                        
                                        
                                        "/>
                                    <!--<xsl:with-param name="relNote"
                                        select="normalize-space(eac:descriptiveNote/eac:p)"/>
                                    -->
                                    <xsl:with-param name="relDate"
                                        select="
                                        if (eac:date/@standardDate[normalize-space(.) != ''])
                                        then
                                        (normalize-space(eac:date/@standardDate))
                                        else
                                        (normalize-space(eac:date))"
                                        
                                        
                                        >
                                        
                                    </xsl:with-param>
                                    <!--  <xsl:with-param name="relNote"
                                        select="normalize-space(eac:descriptiveNote/eac:p)"/>-->
                                    <xsl:with-param name="coll" select="$coll"/>
                                </xsl:call-template>
                            </xsl:for-each>
                            <!-- métadonnées -->

                            <RiC:describedBy>
                                <xsl:attribute name="rdf:resource">
                                    <xsl:value-of
                                        select="concat('http://piaaf.demo.logilab.fr/resource/FRAN_description_', $theGoodRecId)"
                                    />
                                </xsl:attribute>
                            </RiC:describedBy>
                        </rdf:Description>
                        <!-- sortie des relations de catégorisation -->
                      
                        <xsl:for-each select="eac:cpfDescription/eac:identity/eac:nameEntry">
                            <xsl:variable name="theName" select="  if (count(eac:part)=1)
                                then (normalize-space(eac:part))
                                else (
                                concat(eac:part[@localType='nom'], ', ', eac:part[@localType='prenom'], ' (', eac:part[@localType='dates_biographiques'], ')')
                                )"/>
                            <rdf:Description>
                                <xsl:attribute name="rdf:about">
                                    <xsl:value-of
                                        select="concat('http://piaaf.demo.logilab.fr/resource/FRAN_DenominationRelation_', $theGoodRecId, '_', generate-id())"/>

                                </xsl:attribute>
                                <rdf:type
                                    rdf:resource="http://www.ica.org/standards/RiC/ontology#DenominationRelation"/>
                                <xsl:if
                                    test="eac:useDates/eac:dateRange/eac:fromDate">
                                    
                                    <RiC:beginningDate>
                                        <!--<xsl:value-of
                                            select="eac:useDates/eac:dateRange/eac:fromDate"/>-->
                                        <xsl:call-template name="outputDate">
                                            <xsl:with-param name="stdDate" select="eac:useDates/eac:dateRange/eac:fromDate/@standardDate"></xsl:with-param>
                                            <xsl:with-param name="date" select="eac:useDates/eac:dateRange/eac:fromDate"></xsl:with-param>
                                        </xsl:call-template>
                                        <!-- <xsl:choose>
                                            
                                            <xsl:when test="string-length(eac:useDates/eac:dateRange/eac:fromDate/@standardDate)=4">
                                                <xsl:attribute name="rdf:datatype"> <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text></xsl:attribute>
                                                
                                            </xsl:when>
                                            <xsl:when test="string-length(eac:useDates/eac:dateRange/eac:fromDate/@standardDate)=7">
                                                <xsl:attribute name="rdf:datatype">
                                                    <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                                </xsl:attribute>
                                            </xsl:when>
                                            <xsl:when test="string-length(eac:useDates/eac:dateRange/eac:fromDate/@standardDate)=10">
                                                <xsl:attribute name="rdf:datatype">
                                                    <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                                </xsl:attribute>
                                            </xsl:when>
                                            <xsl:otherwise/>
                                        </xsl:choose>
                                        
                                        <xsl:value-of select="
                                            if (normalize-space(eac:useDates/eac:dateRange/eac:fromDate/@standardDate)!='')
                                            then (normalize-space(eac:useDates/eac:dateRange/eac:fromDate/@standardDate))
                                            else(
                                            if (normalize-space(eac:useDates/eac:dateRange/eac:fromDate)!='')
                                            then (normalize-space(eac:useDates/eac:dateRange/eac:fromDate))
                                            else()
                                            )
                                            "/>-->
                                    </RiC:beginningDate>
                                </xsl:if>
                                <xsl:if
                                    test="eac:useDates/eac:dateRange/eac:toDate">
                                    <RiC:endDate>
                                        <xsl:call-template name="outputDate">
                                            <xsl:with-param name="stdDate" select="eac:useDates/eac:dateRange/eac:toDate/@standardDate"></xsl:with-param>
                                            <xsl:with-param name="date" select="eac:useDates/eac:dateRange/eac:toDate"></xsl:with-param>
                                        </xsl:call-template>
                                        <!--    <xsl:choose>
                                            
                                            <xsl:when test="string-length(eac:useDates/eac:dateRange/eac:toDate/@standardDate)=4">
                                                <xsl:attribute name="rdf:datatype"> <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text></xsl:attribute>
                                                
                                            </xsl:when>
                                            <xsl:when test="string-length(eac:useDates/eac:dateRange/eac:toDate/@standardDate)=7">
                                                <xsl:attribute name="rdf:datatype">
                                                    <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                                </xsl:attribute>
                                            </xsl:when>
                                            <xsl:when test="string-length(eac:useDates/eac:dateRange/eac:toDate/@standardDate)=10">
                                                <xsl:attribute name="rdf:datatype">
                                                    <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                                </xsl:attribute>
                                            </xsl:when>
                                            <xsl:otherwise/>
                                        </xsl:choose>
                                        
                                        <!-\-  <RiC:endDate rdf:datatype="http://www.w3.org/2001/XMLSchema#dateTime">-\->
                                        <xsl:value-of select="
                                            if (normalize-space(eac:useDates/eac:dateRange/eac:toDate/@standardDate)!='')
                                            then (normalize-space(eac:useDates/eac:toDate/@standardDate))
                                            else(
                                            if (normalize-space(eac:useDates/eac:dateRange/eac:toDate)!='')
                                            then (normalize-space(eac:useDates/eac:dateRange/eac:toDate))
                                            else()
                                            )
                                            "/>-->
                                        
                                        
                                        <!--  <xsl:value-of select="eac:useDates/eac:dateRange/eac:toDate"
                                        />-->
                                    </RiC:endDate>
                                    
                                </xsl:if>
                                <RiC:certainty xml:lang="fr">certain</RiC:certainty>
                                <RiC:denominationOf>
                                    <xsl:attribute name="rdf:resource">
                                        <xsl:value-of
                                            select="$URI"
                                        />
                                    </xsl:attribute>
                                </RiC:denominationOf>
                                <RiC:denominatedBy>
                                        <xsl:choose>
                                           <xsl:when test="$an-names/rdf:Description[rdfs:label=$theName]">
                                               <xsl:attribute name="rdf:resource">
                                               <xsl:value-of select="$an-names/rdf:Description[rdfs:label=$theName]/@rdf:about"/>
                                               </xsl:attribute>
                                           </xsl:when> 
                                            <xsl:otherwise>
                                                <xsl:attribute name="xml:lang">fr</xsl:attribute>
                                                <xsl:comment>pas de nom trouvé dans le fichier des noms</xsl:comment>
                                                <xsl:value-of select="normalize-space(eac:part)"/>
                                                
                                            </xsl:otherwise>
                                        </xsl:choose>


                             

                                </RiC:denominatedBy>
                            </rdf:Description>

                        </xsl:for-each>


                        <!-- statuts juridiques -->


                        <xsl:for-each
                            select="eac:cpfDescription/eac:description/descendant::eac:legalStatus">
                            <xsl:variable name="lsName" select="normalize-space(eac:term)"/>
                            
                            
                           <!-- <xsl:if test="$legalsts/skos:Concept[skos:prefLabel = $lsName]">-->
                                <rdf:Description>
                                    <xsl:attribute name="rdf:about">
                                        <xsl:text>http://piaaf.demo.logilab.fr/resource/FRAN_TypeRelation_</xsl:text>
                                        <xsl:value-of select="$theGoodRecId"/>
                                        <xsl:text>_</xsl:text>
                                        <xsl:value-of
                                            select="generate-id()"/>
                                    </xsl:attribute>
                                    <rdf:type
                                        rdf:resource="http://www.ica.org/standards/RiC/ontology#TypeRelation"/>
                                    <RiC:hasType>
                                        <xsl:choose>
                                            <xsl:when test="$legalsts/skos:Concept[skos:prefLabel = $lsName]">
                                        <xsl:attribute name="rdf:resource">
                                            
                                                    <xsl:value-of
                                                        select="$legalsts/skos:Concept[skos:prefLabel = $lsName]/@rdf:about"
                                                    />
                                            
                                            
                                            
                                          
                                        </xsl:attribute>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:attribute name="xml:lang">fr</xsl:attribute>
                                                <xsl:value-of select="normalize-space(eac:term)"/>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </RiC:hasType>
                                    <RiC:categorizationOf>
                                        <xsl:attribute name="rdf:resource">
                                            <xsl:value-of
                                                select="$URI"
                                            />
                                        </xsl:attribute>
                                    </RiC:categorizationOf>
                                    <xsl:if
                                        test="eac:dateRange/eac:fromDate">
                                        <RiC:beginningDate>
                                            <xsl:call-template name="outputDate">
                                                <xsl:with-param name="stdDate" select="eac:dateRange/eac:fromDate/@standardDate"></xsl:with-param>
                                                <xsl:with-param name="date" select="eac:dateRange/eac:fromDate"></xsl:with-param>
                                            </xsl:call-template>
                                            <!-- <xsl:choose>
                                                
                                                <xsl:when test="string-length(eac:dateRange/eac:fromDate/@standardDate)=4">
                                                    <xsl:attribute name="rdf:datatype"> <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text></xsl:attribute>
                                                    
                                                </xsl:when>
                                                <xsl:when test="string-length(eac:dateRange/eac:fromDate/@standardDate)=7">
                                                    <xsl:attribute name="rdf:datatype">
                                                        <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                                    </xsl:attribute>
                                                </xsl:when>
                                                <xsl:when test="string-length(eac:dateRange/eac:fromDate/@standardDate)=10">
                                                    <xsl:attribute name="rdf:datatype">
                                                        <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                                    </xsl:attribute>
                                                </xsl:when>
                                                <xsl:otherwise/>
                                            </xsl:choose>
                                            
                                            <xsl:value-of select="
                                                if (normalize-space(eac:dateRange/eac:fromDate/@standardDate)!='')
                                                then (normalize-space(eac:dateRange/eac:fromDate/@standardDate))
                                                else(
                                                if (normalize-space(eac:dateRange/eac:fromDate)!='')
                                                then (normalize-space(eac:dateRange/eac:fromDate))
                                                else()
                                                )
                                                "/>
                                            -->
                                            
                                        </RiC:beginningDate>
                                    </xsl:if>
                                    <xsl:if
                                        test="eac:dateRange/eac:toDate">
                                        
                                        <RiC:endDate>
                                            <xsl:call-template name="outputDate">
                                                <xsl:with-param name="stdDate" select="eac:dateRange/eac:toDate/@standardDate"></xsl:with-param>
                                                <xsl:with-param name="date" select="eac:dateRange/eac:toDate"></xsl:with-param>
                                            </xsl:call-template>
                                            <!--   <xsl:choose>
                                                    
                                                    <xsl:when test="string-length(eac:dateRange/eac:toDate/@standardDate)=4">
                                                        <xsl:attribute name="rdf:datatype"> <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text></xsl:attribute>
                                                        
                                                    </xsl:when>
                                                    <xsl:when test="string-length(eac:dateRange/eac:toDate/@standardDate)=7">
                                                        <xsl:attribute name="rdf:datatype">
                                                            <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                                        </xsl:attribute>
                                                    </xsl:when>
                                                    <xsl:when test="string-length(eac:dateRange/eac:toDate/@standardDate)=10">
                                                        <xsl:attribute name="rdf:datatype">
                                                            <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                                        </xsl:attribute>
                                                    </xsl:when>
                                                    <xsl:otherwise/>
                                                </xsl:choose>
                                                
                                                <!-\-  <RiC:endDate rdf:datatype="http://www.w3.org/2001/XMLSchema#dateTime">-\->
                                                <xsl:value-of select="
                                                    if (normalize-space(eac:dateRange/eac:toDate/@standardDate)!='')
                                                    then (normalize-space(eac:dateRange/eac:toDate/@standardDate))
                                                    else(
                                                    if (normalize-space(eac:dateRange/eac:toDate)!='')
                                                    then (normalize-space(eac:dateRange/eac:toDate))
                                                    else()
                                                    )
                                                    "/>-->
                                        </RiC:endDate>
                                    </xsl:if>
                                    <xsl:if test="eac:descriptiveNote">
                                        <RiC:description xml:lang="fr">
                                            <xsl:value-of select="eac:descriptiveNote"/>
                                        </RiC:description>
                                    </xsl:if>
                                </rdf:Description>
                            <!--</xsl:if>-->

                        </xsl:for-each>
                        
                       <xsl:for-each select="eac:cpfDescription/eac:description/eac:places/eac:place[normalize-space(eac:placeEntry)!='']">
                           
                            <xsl:variable name="placeName">
                                <xsl:choose>
                                    <xsl:when test="eac:placeRole='Siège social'">
                                        <xsl:choose>
                                            <xsl:when test="parent::eac:places/eac:place[eac:placeRole='Siège']">
                                                <xsl:value-of select="concat(normalize-space(parent::eac:places/eac:place[eac:placeRole='Siège']/eac:placeEntry), '. ', normalize-space(eac:placeEntry))"/>
                                            </xsl:when>
                                            <xsl:otherwise><xsl:value-of select="normalize-space(eac:placeEntry)"/></xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="normalize-space(eac:placeEntry)"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:variable>
                            <xsl:if
                                test="$an-places/rdf:Description[rdfs:label = $placeName]">
                                
                                <rdf:Description>
                                    <xsl:attribute name="rdf:about">
                                        <xsl:text>http://piaaf.demo.logilab.fr/resource/FRAN_LocationRelation_</xsl:text>
                                        <xsl:value-of select="$theGoodRecId"/>
                                        <xsl:text>_</xsl:text>
                                        <xsl:value-of
                                            select="generate-id()"/>
                                    </xsl:attribute>
                                    <rdf:type
                                        rdf:resource="http://www.ica.org/standards/RiC/ontology#LocationRelation"/>
                                    <RiC:hasLocation>
                                        <xsl:attribute name="rdf:resource">
                                            <xsl:value-of
                                                select="$an-places/rdf:Description[rdfs:label = $placeName]/@rdf:about"
                                            />
                                        </xsl:attribute>
                                    </RiC:hasLocation>
                                    <RiC:locationOf>
                                        <xsl:attribute name="rdf:resource">
                                            <xsl:value-of
                                                select="$URI"
                                            />
                                        </xsl:attribute>
                                    </RiC:locationOf>
                                    <xsl:if
                                        test="eac:dateRange/eac:fromDate">
                                        <RiC:beginningDate>
                                            <xsl:call-template name="outputDate">
                                                <xsl:with-param name="stdDate" select="eac:dateRange/eac:fromDate/@standardDate"></xsl:with-param>
                                                <xsl:with-param name="date" select="eac:dateRange/eac:fromDate"></xsl:with-param>
                                            </xsl:call-template>
                                            <!-- <xsl:choose>
                                                
                                                <xsl:when test="string-length(eac:dateRange/eac:fromDate/@standardDate)=4">
                                                    <xsl:attribute name="rdf:datatype"> <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text></xsl:attribute>
                                                    
                                                </xsl:when>
                                                <xsl:when test="string-length(eac:dateRange/eac:fromDate/@standardDate)=7">
                                                    <xsl:attribute name="rdf:datatype">
                                                        <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                                    </xsl:attribute>
                                                </xsl:when>
                                                <xsl:when test="string-length(eac:dateRange/eac:fromDate/@standardDate)=10">
                                                    <xsl:attribute name="rdf:datatype">
                                                        <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                                    </xsl:attribute>
                                                </xsl:when>
                                                <xsl:otherwise/>
                                            </xsl:choose>
                                            
                                            <xsl:value-of select="
                                                if (normalize-space(eac:dateRange/eac:fromDate/@standardDate)!='')
                                                then (normalize-space(eac:dateRange/eac:fromDate/@standardDate))
                                                else(
                                                if (normalize-space(eac:dateRange/eac:fromDate)!='')
                                                then (normalize-space(eac:dateRange/eac:fromDate))
                                                else()
                                                )
                                                "/>
                                            -->
                                            
                                        </RiC:beginningDate>
                                    </xsl:if>
                                    <xsl:if
                                        test="eac:dateRange/eac:toDate">
                                        
                                        <RiC:endDate>
                                            <xsl:call-template name="outputDate">
                                                <xsl:with-param name="stdDate" select="eac:dateRange/eac:toDate/@standardDate"></xsl:with-param>
                                                <xsl:with-param name="date" select="eac:dateRange/eac:toDate"></xsl:with-param>
                                            </xsl:call-template>
                                            <!--   <xsl:choose>
                                                    
                                                    <xsl:when test="string-length(eac:dateRange/eac:toDate/@standardDate)=4">
                                                        <xsl:attribute name="rdf:datatype"> <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text></xsl:attribute>
                                                        
                                                    </xsl:when>
                                                    <xsl:when test="string-length(eac:dateRange/eac:toDate/@standardDate)=7">
                                                        <xsl:attribute name="rdf:datatype">
                                                            <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                                        </xsl:attribute>
                                                    </xsl:when>
                                                    <xsl:when test="string-length(eac:dateRange/eac:toDate/@standardDate)=10">
                                                        <xsl:attribute name="rdf:datatype">
                                                            <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                                        </xsl:attribute>
                                                    </xsl:when>
                                                    <xsl:otherwise/>
                                                </xsl:choose>
                                                
                                                <!-\-  <RiC:endDate rdf:datatype="http://www.w3.org/2001/XMLSchema#dateTime">-\->
                                                <xsl:value-of select="
                                                    if (normalize-space(eac:dateRange/eac:toDate/@standardDate)!='')
                                                    then (normalize-space(eac:dateRange/eac:toDate/@standardDate))
                                                    else(
                                                    if (normalize-space(eac:dateRange/eac:toDate)!='')
                                                    then (normalize-space(eac:dateRange/eac:toDate))
                                                    else()
                                                    )
                                                    "/>-->
                                        </RiC:endDate>
                                    </xsl:if>
                                    <xsl:if test="eac:descriptiveNote[normalize-space()!='']">
                                        <RiC:description xml:lang="fr">
                                            <xsl:value-of select="eac:descriptiveNote"/>
                                        </RiC:description>
                                    </xsl:if>
                                </rdf:Description>
                                
                            </xsl:if>
                        </xsl:for-each>

                      
                        <rdf:Description>

                            <xsl:attribute name="rdf:about">
                                <xsl:value-of
                                    select="concat('http://piaaf.demo.logilab.fr/resource/FRAN_description_', $theGoodRecId)"
                                />
                            </xsl:attribute>

                            <rdf:type
                                rdf:resource="http://www.ica.org/standards/RiC/ontology#Description"/>
                            <RiC:describes>
                                <xsl:attribute name="rdf:resource">
                                    <xsl:value-of
                                        select="$URI"
                                    />
                                </xsl:attribute>
                            </RiC:describes>
                            <xsl:if test="eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'created']">
                                <RiC:creationDate>
                                    <xsl:call-template name="outputDate">
                                        <xsl:with-param name="stdDate" select="eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'created']/eac:eventDateTime/@standardDateTime"></xsl:with-param>
                                        <xsl:with-param name="date" select="eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'created']/eac:eventDateTime"></xsl:with-param>
                                    </xsl:call-template>
                                    <!-- <xsl:choose>
                                        
                                        <xsl:when test="string-length(eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'created']/eac:eventDateTime/@standardDate)=4">
                                            <xsl:attribute name="rdf:datatype"> <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text></xsl:attribute>
                                            
                                        </xsl:when>
                                        <xsl:when test="string-length(eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'created']/eac:eventDateTime/@standardDate)=7">
                                            <xsl:attribute name="rdf:datatype">
                                                <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                            </xsl:attribute>
                                        </xsl:when>
                                        <xsl:when test="string-length(eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'created']/eac:eventDateTime/@standardDate)=10">
                                            <xsl:attribute name="rdf:datatype">
                                                <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                            </xsl:attribute>
                                        </xsl:when>
                                        <xsl:otherwise/>
                                    </xsl:choose>
                                    
                                    <!-\-  <RiC:endDate rdf:datatype="http://www.w3.org/2001/XMLSchema#dateTime">-\->
                                    <xsl:value-of select="
                                        if (normalize-space(eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'created']/eac:eventDateTime/@standardDate)!='')
                                        then (normalize-space(eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'created']/eac:eventDateTime/@standardDate))
                                        else(
                                        if (normalize-space(eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'created']/eac:eventDateTime)!='')
                                        then (normalize-space(eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'created']/eac:eventDateTime))
                                        else()
                                        )
                                        "/>
                                    
                                    -->
                                    <!-- <xsl:value-of
                                        select="eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'created']/eac:eventDateTime/@standardDateTime"/>-->
                                    
                                </RiC:creationDate>
                            </xsl:if>
                            <RiC:lastUpdateDate>
                                <!--  <xsl:value-of
                                    select="eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'revised'][last()]/eac:eventDateTime/@standardDateTime"
                                />-->
                                <xsl:call-template name="outputDate">
                                    <xsl:with-param name="stdDate" select="eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'revised'][last()]/eac:eventDateTime/@standardDateTime"></xsl:with-param>
                                    <xsl:with-param name="date" select="eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'revised'][last()]/eac:eventDateTime"></xsl:with-param>
                                </xsl:call-template>
                                <!-- <xsl:choose>
                                    
                                    <xsl:when test="string-length(eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'revised'][last()]/eac:eventDateTime/@standardDate)=4">
                                        <xsl:attribute name="rdf:datatype"> <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text></xsl:attribute>
                                        
                                    </xsl:when>
                                    <xsl:when test="string-length(eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'revised'][last()]/eac:eventDateTime/@standardDate)=7">
                                        <xsl:attribute name="rdf:datatype">
                                            <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                        </xsl:attribute>
                                    </xsl:when>
                                    <xsl:when test="string-length(eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'revised'][last()]/eac:eventDateTime/@standardDate)=10">
                                        <xsl:attribute name="rdf:datatype">
                                            <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                        </xsl:attribute>
                                    </xsl:when>
                                    <xsl:otherwise/>
                                </xsl:choose>
                                
                                <!-\-  <RiC:endDate rdf:datatype="http://www.w3.org/2001/XMLSchema#dateTime">-\->
                                <xsl:value-of select="
                                    if (normalize-space(eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'revised'][last()]/eac:eventDateTime/@standardDate)!='')
                                    then (normalize-space(eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'revised'][last()]/eac:eventDateTime/@standardDate))
                                    else(
                                    if (normalize-space(eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'revised'][last()]/eac:eventDateTime)!='')
                                    then (normalize-space(eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'revised'][last()]/eac:eventDateTime))
                                    else()
                                    )
                                    "/>
                                -->
                                
                                <!--  <xsl:value-of
                                    select="eac:control/eac:maintenanceHistory/eac:maintenanceEvent[eac:eventType = 'revised'][last()]/eac:eventDateTime/@standardDateTime"/>-->
                            </RiC:lastUpdateDate>
                            <RiC:identifier>
                                <xsl:value-of select="eac:control/eac:recordId"/>
                            </RiC:identifier>
                          <!--  <RiC:authoredBy
                                rdf:resource="http://www.piaaf.net/agents/FRAN_corporate-body_005568"/>-->
                            <RiC:authoredBy rdf:resource="http://piaaf.demo.logilab.fr/resource/FRAN_corporate-body_005061"/>
                            <xsl:for-each select="eac:control/eac:sources/eac:source[normalize-space(eac:sourceEntry)!='']">
                                <RiC:evidencedBy xml:lang="fr">
                                    <xsl:value-of select="normalize-space(eac:sourceEntry)"/>
                                    <xsl:if test="@xlink:href[normalize-space(.) != '']">
                                        <xsl:value-of
                                            select="concat(' (', normalize-space(@xlink:href), ')')"
                                        />
                                    </xsl:if>
                                </RiC:evidencedBy>
                            </xsl:for-each>
                          
                            <RiC:conformsToRule rdf:resource="http://piaaf.demo.logilab.fr/resource/FRAN_mandate_rl003"/>
                            <RiC:conformsToRule rdf:resource="http://piaaf.demo.logilab.fr/resource/FRAN_mandate_rl004"/>
                            <xsl:choose>
                                <xsl:when test="$entType='corporateBody'"> <RiC:conformsToRule rdf:resource="http://piaaf.demo.logilab.fr/resource/FRAN_mandate_rl001"/></xsl:when>
                                <xsl:when test="$entType='person'"><RiC:conformsToRule rdf:resource="http://piaaf.demo.logilab.fr/resource/FRAN_mandate_rl002"/></xsl:when>
                            </xsl:choose>
                            <xsl:if test="eac:cpfDescription/eac:identity/eac:entityId[starts-with(normalize-space(.), 'ISNI')]">
                                <xsl:variable name="ISNIValue" select="normalize-space(eac:cpfDescription/eac:identity/eac:entityId[starts-with(normalize-space(.), 'ISNI')])"></xsl:variable>
                                <xsl:variable name="ISNIId">
                                    <xsl:choose>
                                        <xsl:when test="starts-with($ISNIValue, 'ISNI:')">
                                            <xsl:value-of select="replace(substring-after($ISNIValue, 'ISNI:'), ' ', '')"/>
                                        </xsl:when>
                                        <xsl:when test="starts-with($ISNIValue, 'ISNI :')">
                                            <xsl:value-of select="replace(substring-after($ISNIValue, 'ISNI :'), ' ', '')"/>
                                        </xsl:when>
                                        <xsl:when test="starts-with($ISNIValue, 'ISNI')">
                                            <xsl:value-of select="replace(substring-after($ISNIValue, 'ISNI'), ' ', '')"/>
                                        </xsl:when>
                                    </xsl:choose>
                                </xsl:variable>
                                <!-- http://isni.org/isni/0000000121032683-->
                                <rdfs:seeAlso>
                                    <xsl:attribute name="rdf:resource">
                                        <xsl:value-of select="concat('http://isni.org/isni/', $ISNIId)"/>
                                    </xsl:attribute>
                                </rdfs:seeAlso>
                                <!--  <owl:sameAs>
                                    <xsl:attribute name="rdf:resource">
                                        
                                    </xsl:attribute>
                                </owl:sameAs>   -->
                            </xsl:if>
                          
                          <rdfs:seeAlso>
                                <xsl:attribute name="rdf:resource">
                                    <!-- https://www.siv.archives-nationales.culture.gouv.fr/siv/NP/-->
                                    <xsl:value-of
                                        select="concat('https://www.siv.archives-nationales.culture.gouv.fr/siv/NP/', $recId)"
                                    />
                                </xsl:attribute>
                            </rdfs:seeAlso>
                            <xsl:for-each
                                select="eac:cpfDescription/eac:relations/eac:cpfRelation[@cpfRelationType = 'identity']">
                                <!-- identité -->
                                <!-- http://data.bnf.fr/ark:/12148/cb11863993z
                            http://catalogue.bnf.fr/ark:/12148/cb11863993z/PUBLIC
                            http://catalogue.bnf.fr/ark:/12148/cb11863993z/PUBLIC
                            http://data.bnf.fr/ark:/12148/cb11863993z#foaf:Organization-->
                                <!-- http://catalogue.bnf.fr/ark:/12148/cb11904421w -->
                                <!-- http://data.bnf.fr/ark:/12148/cb11862469g-->
                                <xsl:variable name="lnk" select="normalize-space(@xlink:href)"/>
                                <xsl:if test="$lnk!=''">
                                    <rdfs:seeAlso>
                                        <xsl:attribute name="rdf:resource">
                                            <xsl:if test="contains($lnk, 'bnf.fr')">
                                                <xsl:choose>
                                                    <xsl:when
                                                        test="contains($lnk, 'catalogue.bnf.fr/ark:/12148/') and contains($lnk, '/PUBLIC')">
                                                        <xsl:text>http://data.bnf.fr/ark:/12148/</xsl:text>
                                                        <xsl:value-of
                                                            select="substring-before(substring-after($lnk, '12148/'), '/PUBLIC')"/>
                                                        
                                                    </xsl:when>
                                                    <xsl:when test="contains($lnk, 'catalogue.bnf.fr/ark:/12148/') and not(contains($lnk, '/PUBLIC'))">
                                                        <xsl:text>http://data.bnf.fr/ark:/12148/</xsl:text>
                                                        <xsl:value-of
                                                            select="substring-after($lnk, '12148/')"/>
                                                        
                                                    </xsl:when>
                                                    <xsl:when test="contains($lnk, 'http://data.bnf.fr/ark:/12148/')">
                                                        <xsl:value-of select="$lnk"/>
                                                        
                                                    </xsl:when>
                                                </xsl:choose>
                                            </xsl:if>
                                            <!-- liens AN vers notices wikipedia -->
                                            <!-- exemple : http://fr.dbpedia.org/page/Henri_Labrouste, https://fr.wikipedia.org/wiki/Henri_Labrouste -->
                                            <xsl:if test="contains($lnk, 'wikipedia.org')">
                                                
                                                <xsl:value-of select="$lnk"/>
                                            </xsl:if>
                                            
                                        </xsl:attribute>
                                    </rdfs:seeAlso>
                                </xsl:if>
                            </xsl:for-each>
                        </rdf:Description>

                    </rdf:RDF>
                </xsl:result-document>
            </xsl:for-each>





        </xsl:if>

    </xsl:template>
    <xsl:template name="outputObjectPropertyForRelation">
        <xsl:param name="recId"/>
        <xsl:param name="link"/>
        <xsl:param name="relType"/>
        <xsl:param name="arcToRelName"/>
        <xsl:param name="relName"/>
        <xsl:param name="relSecondArcName"/>
        <xsl:param name="srcRelType"/>
     <!--   <xsl:param name="shortcutRelName"/>-->
        <xsl:param name="relFromDate"/>
        <xsl:param name="relToDate"/>
        <xsl:param name="relDate"/>
     <!--   <xsl:param name="relNote"/>-->
        <xsl:param name="relEntry"/>
        <xsl:param name="toAncRelArcName"/>
        <xsl:param name="coll"/>
    
        <xsl:comment>$link=
            <xsl:value-of select="$link"/></xsl:comment>
        
        <xsl:variable name="relFile" select="
            if ($coll='FRSIAF')
            then ( document('rdf/relations/FRSIAF_relations-between-agents.rdf')/rdf:RDF)
            else (
                if ($coll='FRBNF')
                then ( document('rdf/relations/FRBNF_relations-between-agents.rdf')/rdf:RDF )
                else(if ($coll='FRAN')
                then ( document('rdf/relations/FRAN_relations-between-agents.rdf')/rdf:RDF )
                else())
            
            )
            
            
            
            ">
           
        </xsl:variable>
       
           
                <!-- plusieurs cas -->
                <!-- cas 1 : pas de cible, juste le nom de l'entité -->
                <xsl:choose>
                    <xsl:when test="$link = 'non'">
                        <!-- pas d'URL cible. On cherche une relation qui a pour cible un literal -->
                        <xsl:if
                            test="
                                $relFile/rdf:Description[
                                ends-with(rdf:type/@rdf:resource, $relName)
                                and
                                child::*[local-name() = $toAncRelArcName and ends-with(@rdf:resource, concat('_', $recId))]
                                and
                                child::*[local-name() = $relSecondArcName and normalize-space(.) = $relEntry]
                                ]">
                            <xsl:element name="RiC:{$arcToRelName}">
                                
                                <xsl:attribute name="rdf:resource">
                            <xsl:variable name="theTargetRel"
                                select="
                                    $relFile/rdf:Description[
                                    ends-with(rdf:type/@rdf:resource, $relName)
                                    and
                                    child::*[local-name() = $toAncRelArcName and ends-with(@rdf:resource, concat('_', $recId))]
                                    and
                                    child::*[local-name() = $relSecondArcName and normalize-space(.)= $relEntry]
                                    ]"/>
                            <xsl:choose>
                                <xsl:when
                                    test="boolean($relFromDate) and boolean($relToDate) = false()">
                                    <xsl:if
                                        test="$theTargetRel/RiC:beginningDate = $relFromDate and not($theTargetRel/RiC:endDate)">
                                        <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                    </xsl:if>
                                </xsl:when>
                                <xsl:when
                                    test="boolean($relFromDate) = false() and boolean($relToDate)">
                                    <xsl:if
                                        test="not($theTargetRel/RiC:beginningDate) and $theTargetRel/RiC:endDate = $relToDate">
                                        <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                    </xsl:if>
                                </xsl:when>
                                <xsl:when test="boolean($relFromDate) and boolean($relToDate)">
                                    <xsl:if
                                        test="$theTargetRel/RiC:beginningDate = $relFromDate and $theTargetRel/RiC:endDate = $relToDate">
                                        <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                    </xsl:if>
                                </xsl:when>
                                <xsl:when
                                    test="boolean($relFromDate) = false() and boolean($relToDate) = false()">
                                    <xsl:if
                                        test="not($theTargetRel/RiC:beginningDate) and not($theTargetRel/RiC:endDate)">
                                        <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                    </xsl:if>
                                </xsl:when>
                                <xsl:when
                                    test="boolean($relDate)">
                                    <xsl:if
                                        test="boolean($theTargetRel/RiC:date)=$relDate">
                                        <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                    </xsl:if>
                                </xsl:when>
                                <xsl:otherwise>AREVOIR</xsl:otherwise>
                            
                            </xsl:choose>
                                </xsl:attribute>
                                <xsl:comment>$theTargetRel/@rdf:about=<xsl:value-of select="$relFile/rdf:Description[
                                ends-with(rdf:type/@rdf:resource, $relName)
                                and
                                child::*[local-name() = $toAncRelArcName and ends-with(@rdf:resource, concat('_', $recId))]
                                and
                                child::*[local-name() = $relSecondArcName and normalize-space(.) = $relEntry]
                                ]/@rdf:about"/></xsl:comment>
                                <xsl:comment>$relToDate=<xsl:value-of select="$relToDate"/></xsl:comment>
                            </xsl:element>
                        </xsl:if>
                    </xsl:when>
                    <xsl:when test="$link != 'non'">

                        <!-- il y a une URL pour la cible.  -->
                        <xsl:choose>
                            <xsl:when
                                test="
                                starts-with($link, 'FRAN_') and $relFile/rdf:Description[
                                ends-with(rdf:type/@rdf:resource, $relName)
                                and
                                child::*[local-name() = $toAncRelArcName and ends-with(@rdf:resource, concat('_', $recId))]
                                and
                                child::*[local-name() = $relSecondArcName and contains(@rdf:nodeID, substring-after($link, 'FRAN_NP_'))]
                                
                                
                                ]">
                                
                                <xsl:for-each select="$relFile/rdf:Description[
                                    ends-with(rdf:type/@rdf:resource, $relName)
                                    and
                                    child::*[local-name() = $toAncRelArcName and ends-with(@rdf:resource, concat('_', $recId))]
                                    and
                                    child::*[local-name() = $relSecondArcName and contains(@rdf:nodeID, substring-after($link, 'FRAN_NP_'))]
                                    
                                    
                                    ]">
                                    <xsl:variable name="theTargetRel"
                                        select="."/>
                                    
                                    <xsl:choose>
                                        <xsl:when
                                            test="boolean($relFromDate) and boolean($relToDate) = false()">
                                            <xsl:if
                                                test="$theTargetRel/RiC:beginningDate = $relFromDate and not($theTargetRel/RiC:endDate)"><xsl:element name="RiC:{$arcToRelName}">
                                                    
                                                    <xsl:attribute name="rdf:resource">
                                                        <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                                    </xsl:attribute></xsl:element>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:when
                                            test="boolean($relFromDate) = false() and boolean($relToDate)">
                                            <xsl:if
                                                test="not($theTargetRel/RiC:beginningDate) and $theTargetRel/RiC:endDate = $relToDate"><xsl:element name="RiC:{$arcToRelName}">
                                                    
                                                    <xsl:attribute name="rdf:resource">
                                                        <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                                    </xsl:attribute>
                                                </xsl:element>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:when test="boolean($relFromDate) and boolean($relToDate)">
                                            <xsl:if
                                                test="$theTargetRel/RiC:beginningDate = $relFromDate and $theTargetRel/RiC:endDate = $relToDate">
                                                <xsl:element name="RiC:{$arcToRelName}">
                                                    
                                                    <xsl:attribute name="rdf:resource">
                                                        <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                                    </xsl:attribute>
                                                </xsl:element>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:when
                                            test="boolean($relFromDate) = false() and boolean($relToDate) = false()">
                                            <xsl:if
                                                test="not($theTargetRel/RiC:beginningDate) and not($theTargetRel/RiC:endDate)">
                                                <xsl:element name="RiC:{$arcToRelName}">
                                                    
                                                    <xsl:attribute name="rdf:resource">
                                                        <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                                    </xsl:attribute>
                                                </xsl:element>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:when
                                            test="boolean($relDate)">
                                            <xsl:if
                                                test="boolean($theTargetRel/RiC:date)=$relDate">
                                                <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:otherwise>AREVOIR</xsl:otherwise>
                                    </xsl:choose>
                                    
                                </xsl:for-each>
                                
                                
                                
                                
                            </xsl:when>
                            <xsl:when
                                test="
                                starts-with($link, 'FRAN_') and $relFile/rdf:Description[
                                ends-with(rdf:type/@rdf:resource, $relName)
                                and
                                child::*[local-name() = $toAncRelArcName and ends-with(@rdf:resource, concat('_', $recId))]
                                and
                                child::*[local-name() = $relSecondArcName and ends-with(@rdf:resource, concat('_', substring-after($link, 'FRAN_NP_')))]
                                
                                
                                ]">
                                
                                <xsl:for-each select="$relFile/rdf:Description[
                                    ends-with(rdf:type/@rdf:resource, $relName)
                                    and
                                    child::*[local-name() = $toAncRelArcName and ends-with(@rdf:resource, concat('_', $recId))]
                                    and
                                    child::*[local-name() = $relSecondArcName and ends-with(@rdf:resource, concat('_', substring-after($link, 'FRAN_NP_')))]
                                    
                                    
                                    ]">
                                    <xsl:variable name="theTargetRel"
                                        select="."/>
                                    
                                    <xsl:choose>
                                        <xsl:when
                                            test="boolean($relFromDate) and boolean($relToDate) = false()">
                                            <xsl:if
                                                test="$theTargetRel/RiC:beginningDate = $relFromDate and not($theTargetRel/RiC:endDate)"><xsl:element name="RiC:{$arcToRelName}">
                                                    
                                                    <xsl:attribute name="rdf:resource">
                                                        <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                                    </xsl:attribute></xsl:element>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:when
                                            test="boolean($relFromDate) = false() and boolean($relToDate)">
                                            <xsl:if
                                                test="not($theTargetRel/RiC:beginningDate) and $theTargetRel/RiC:endDate = $relToDate"><xsl:element name="RiC:{$arcToRelName}">
                                                    
                                                    <xsl:attribute name="rdf:resource">
                                                        <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                                    </xsl:attribute>
                                                </xsl:element>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:when test="boolean($relFromDate) and boolean($relToDate)">
                                            <xsl:if
                                                test="$theTargetRel/RiC:beginningDate = $relFromDate and $theTargetRel/RiC:endDate = $relToDate">
                                                <xsl:element name="RiC:{$arcToRelName}">
                                                    
                                                    <xsl:attribute name="rdf:resource">
                                                        <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                                    </xsl:attribute>
                                                </xsl:element>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:when
                                            test="boolean($relFromDate) = false() and boolean($relToDate) = false()">
                                            <xsl:if
                                                test="not($theTargetRel/RiC:beginningDate) and not($theTargetRel/RiC:endDate)">
                                                <xsl:element name="RiC:{$arcToRelName}">
                                                    
                                                    <xsl:attribute name="rdf:resource">
                                                        <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                                    </xsl:attribute>
                                                </xsl:element>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:when
                                            test="boolean($relDate)">
                                            <xsl:if
                                                test="boolean($theTargetRel/RiC:date)=$relDate">
                                                <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:otherwise>AREVOIR</xsl:otherwise>
                                    </xsl:choose>
                                    
                                </xsl:for-each>
                                
                                
                                
                                
                            </xsl:when>
                            
                            
                            
                            <xsl:when
                                test="
                                starts-with($link, 'FRAD') and $relFile/rdf:Description[
                                    ends-with(rdf:type/@rdf:resource, $relName)
                                    and
                                    child::*[local-name() = $toAncRelArcName and ends-with(@rdf:resource, concat('_', $recId))]
                                    and
                                    child::*[local-name() = $relSecondArcName and ends-with(@rdf:resource, $link)]
                                    
                                    
                                    ]">
                                <xsl:for-each select="$relFile/rdf:Description[
                                    ends-with(rdf:type/@rdf:resource, $relName)
                                    and
                                    child::*[local-name() = $toAncRelArcName and ends-with(@rdf:resource, concat('_', $recId))]
                                    and
                                    child::*[local-name() = $relSecondArcName and ends-with(@rdf:resource, $link)]
                                    
                                    
                                    ]">
                                
                                <!--<xsl:element name="RiC:{$arcToRelName}">
                                    
                                    <xsl:attribute name="rdf:resource">-->
                                <xsl:variable name="theTargetRel"
                                    select="."/>
                                <xsl:choose>
                                    <xsl:when
                                        test="boolean($relFromDate) and boolean($relToDate) = false()">
                                        <xsl:if
                                            test="$theTargetRel/RiC:beginningDate = $relFromDate and not($theTargetRel/RiC:endDate)">
                                            <xsl:element name="RiC:{$arcToRelName}">
                                                
                                                <xsl:attribute name="rdf:resource">
                                            
                                            <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                                </xsl:attribute>
                                            </xsl:element>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when
                                        test="boolean($relFromDate) = false() and boolean($relToDate)">
                                        <xsl:if
                                            test="not($theTargetRel/RiC:beginningDate) and $theTargetRel/RiC:endDate = $relToDate">
                                            <xsl:element name="RiC:{$arcToRelName}">
                                                
                                                <xsl:attribute name="rdf:resource">
                                                    
                                                    <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                                </xsl:attribute>
                                            </xsl:element>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when test="boolean($relFromDate) and boolean($relToDate)">
                                        <xsl:if
                                            test="$theTargetRel/RiC:beginningDate = $relFromDate and $theTargetRel/RiC:endDate = $relToDate">
                                            <xsl:element name="RiC:{$arcToRelName}">
                                                
                                                <xsl:attribute name="rdf:resource">
                                                    
                                                    <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                                </xsl:attribute>
                                            </xsl:element>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when
                                        test="boolean($relFromDate) = false() and boolean($relToDate) = false()">
                                        <xsl:if
                                            test="not($theTargetRel/RiC:beginningDate) and not($theTargetRel/RiC:endDate)">
                                            <xsl:element name="RiC:{$arcToRelName}">
                                                
                                                <xsl:attribute name="rdf:resource">
                                                    
                                                    <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                                </xsl:attribute>
                                            </xsl:element>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when
                                        test="boolean($relDate)">
                                        <xsl:if
                                            test="boolean($theTargetRel/RiC:date)=$relDate">
                                            <xsl:element name="RiC:{$arcToRelName}">
                                                
                                                <xsl:attribute name="rdf:resource">
                                                    
                                                    <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                                </xsl:attribute>
                                            </xsl:element>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:otherwise>AREVOIR</xsl:otherwise>
                                </xsl:choose>
                                  <!--  </xsl:attribute>
                                </xsl:element>-->
                                </xsl:for-each>
                            </xsl:when>
                            <xsl:when
                                test="
                                starts-with($link, 'FRAD') and $relFile/rdf:Description[
                                ends-with(rdf:type/@rdf:resource, $relName)
                                and
                                child::*[local-name() = $toAncRelArcName and ends-with(@rdf:resource, concat('_', $recId))]
                                and
                                child::*[local-name() = $relSecondArcName and contains(@rdf:nodeID, $link)]
                                
                                
                                ]">
                                
                                <xsl:for-each select="$relFile/rdf:Description[
                                    ends-with(rdf:type/@rdf:resource, $relName)
                                    and
                                    child::*[local-name() = $toAncRelArcName and ends-with(@rdf:resource, concat('_', $recId))]
                                    and
                                    child::*[local-name() = $relSecondArcName and contains(@rdf:nodeID, $link)]
                                    
                                    
                                    ]">
                                    <xsl:variable name="theTargetRel"
                                        select="."/>
                                    
                                    <xsl:choose>
                                        <xsl:when
                                            test="boolean($relFromDate) and boolean($relToDate) = false()">
                                            <xsl:if
                                                test="$theTargetRel/RiC:beginningDate = $relFromDate and not($theTargetRel/RiC:endDate)"><xsl:element name="RiC:{$arcToRelName}">
                                                    
                                                    <xsl:attribute name="rdf:resource">
                                                        <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                                    </xsl:attribute></xsl:element>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:when
                                            test="boolean($relFromDate) = false() and boolean($relToDate)">
                                            <xsl:if
                                                test="not($theTargetRel/RiC:beginningDate) and $theTargetRel/RiC:endDate = $relToDate"><xsl:element name="RiC:{$arcToRelName}">
                                                    
                                                    <xsl:attribute name="rdf:resource">
                                                        <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                                    </xsl:attribute>
                                                </xsl:element>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:when test="boolean($relFromDate) and boolean($relToDate)">
                                            <xsl:if
                                                test="$theTargetRel/RiC:beginningDate = $relFromDate and $theTargetRel/RiC:endDate = $relToDate">
                                                <xsl:element name="RiC:{$arcToRelName}">
                                                    
                                                    <xsl:attribute name="rdf:resource">
                                                        <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                                    </xsl:attribute>
                                                </xsl:element>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:when
                                            test="boolean($relFromDate) = false() and boolean($relToDate) = false()">
                                            <xsl:if
                                                test="not($theTargetRel/RiC:beginningDate) and not($theTargetRel/RiC:endDate)">
                                                <xsl:element name="RiC:{$arcToRelName}">
                                                    
                                                    <xsl:attribute name="rdf:resource">
                                                        <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                                    </xsl:attribute>
                                                </xsl:element>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:when
                                            test="boolean($relDate)">
                                            <xsl:if
                                                test="boolean($theTargetRel/RiC:date)=$relDate">
                                                <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:otherwise>AREVOIR</xsl:otherwise>
                                    </xsl:choose>
                                    
                                </xsl:for-each>
                                
                                
                                
                                
                            </xsl:when>
                            <xsl:when
                                    test="
                                    starts-with($link, 'FRBNF_') and $relFile/rdf:Description[
                                    ends-with(rdf:type/@rdf:resource, $relName)
                                    and
                                    child::*[local-name() = $toAncRelArcName and ends-with(@rdf:resource, concat('_', $recId))]
                                    and
                                    child::*[local-name() = $relSecondArcName and contains(@rdf:nodeID, substring-after($link, 'DGEARC_'))]
                                    
                                    
                                    ]">
                                
                                <xsl:for-each select="$relFile/rdf:Description[
                                    ends-with(rdf:type/@rdf:resource, $relName)
                                    and
                                    child::*[local-name() = $toAncRelArcName and ends-with(@rdf:resource, concat('_', $recId))]
                                    and
                                    child::*[local-name() = $relSecondArcName and contains(@rdf:nodeID, substring-after($link, 'DGEARC_'))]
                                    
                                    
                                    ]">
                                    <xsl:variable name="theTargetRel"
                                        select="."/>
                                   
                                            <xsl:choose>
                                                <xsl:when
                                                    test="boolean($relFromDate) and boolean($relToDate) = false()">
                                                    <xsl:if
                                                        test="$theTargetRel/RiC:beginningDate = $relFromDate and not($theTargetRel/RiC:endDate)"><xsl:element name="RiC:{$arcToRelName}">
                                                            
                                                            <xsl:attribute name="rdf:resource">
                                                                <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                                            </xsl:attribute></xsl:element>
                                                    </xsl:if>
                                                </xsl:when>
                                                <xsl:when
                                                    test="boolean($relFromDate) = false() and boolean($relToDate)">
                                                    <xsl:if
                                                        test="not($theTargetRel/RiC:beginningDate) and $theTargetRel/RiC:endDate = $relToDate"><xsl:element name="RiC:{$arcToRelName}">
                                                            
                                                            <xsl:attribute name="rdf:resource">
                                                                <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                                            </xsl:attribute>
                                                        </xsl:element>
                                                    </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($relFromDate) and boolean($relToDate)">
                                                    <xsl:if
                                                        test="$theTargetRel/RiC:beginningDate = $relFromDate and $theTargetRel/RiC:endDate = $relToDate">
                                                        <xsl:element name="RiC:{$arcToRelName}">
                                                            
                                                            <xsl:attribute name="rdf:resource">
                                                                <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                                            </xsl:attribute>
                                                        </xsl:element>
                                                    </xsl:if>
                                                </xsl:when>
                                                <xsl:when
                                                    test="boolean($relFromDate) = false() and boolean($relToDate) = false()">
                                                    <xsl:if
                                                        test="not($theTargetRel/RiC:beginningDate) and not($theTargetRel/RiC:endDate)">
                                                        <xsl:element name="RiC:{$arcToRelName}">
                                                            
                                                            <xsl:attribute name="rdf:resource">
                                                                <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                                            </xsl:attribute>
                                                        </xsl:element>
                                                    </xsl:if>
                                                </xsl:when>
                                                <xsl:when
                                                    test="boolean($relDate)">
                                                    <xsl:if
                                                        test="boolean($theTargetRel/RiC:date)=$relDate">
                                                        <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                                    </xsl:if>
                                                </xsl:when>
                                                <xsl:otherwise>AREVOIR</xsl:otherwise>
                                            </xsl:choose>
                                    
                                </xsl:for-each>
                                
                                
                                
                                   
                            </xsl:when>
                            <xsl:when
                                test="
                                starts-with($link, 'FRBNF_') and $relFile/rdf:Description[
                                ends-with(rdf:type/@rdf:resource, $relName)
                                and
                                child::*[local-name() = $toAncRelArcName and ends-with(@rdf:resource, concat('_', $recId))]
                                and
                                child::*[local-name() = $relSecondArcName and ends-with(@rdf:resource, concat('_', substring-after($link, 'DGEARC_')))]
                                
                                
                                ]">
                                
                                <xsl:for-each select="$relFile/rdf:Description[
                                    ends-with(rdf:type/@rdf:resource, $relName)
                                    and
                                    child::*[local-name() = $toAncRelArcName and ends-with(@rdf:resource, concat('_', $recId))]
                                    and
                                    child::*[local-name() = $relSecondArcName and ends-with(@rdf:resource, concat('_', substring-after($link, 'DGEARC_')))]
                                    
                                    
                                    ]">
                                    <xsl:variable name="theTargetRel"
                                        select="."/>
                                    
                                            <xsl:choose>
                                                <xsl:when
                                                    test="boolean($relFromDate) and boolean($relToDate) = false()">
                                                    <xsl:if
                                                        test="$theTargetRel/RiC:beginningDate = $relFromDate and not($theTargetRel/RiC:endDate)"><xsl:element name="RiC:{$arcToRelName}">
                                                            
                                                            <xsl:attribute name="rdf:resource">
                                                        <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                                            </xsl:attribute></xsl:element>
                                                    </xsl:if>
                                                </xsl:when>
                                                <xsl:when
                                                    test="boolean($relFromDate) = false() and boolean($relToDate)">
                                                    <xsl:if
                                                        test="not($theTargetRel/RiC:beginningDate) and $theTargetRel/RiC:endDate = $relToDate"><xsl:element name="RiC:{$arcToRelName}">
                                                            
                                                            <xsl:attribute name="rdf:resource">
                                                        <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                                            </xsl:attribute>
                                                        </xsl:element>
                                                    </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($relFromDate) and boolean($relToDate)">
                                                    <xsl:if
                                                        test="$theTargetRel/RiC:beginningDate = $relFromDate and $theTargetRel/RiC:endDate = $relToDate">
                                                        <xsl:element name="RiC:{$arcToRelName}">
                                                            
                                                            <xsl:attribute name="rdf:resource">
                                                        <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                                            </xsl:attribute>
                                                        </xsl:element>
                                                    </xsl:if>
                                                </xsl:when>
                                                <xsl:when
                                                    test="boolean($relFromDate) = false() and boolean($relToDate) = false()">
                                                    <xsl:if
                                                        test="not($theTargetRel/RiC:beginningDate) and not($theTargetRel/RiC:endDate)">
                                                        <xsl:element name="RiC:{$arcToRelName}">
                                                            
                                                            <xsl:attribute name="rdf:resource">
                                                        <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                                            </xsl:attribute>
                                                        </xsl:element>
                                                    </xsl:if>
                                                </xsl:when>
                                                <xsl:when
                                                    test="boolean($relDate)">
                                                    <xsl:if
                                                        test="boolean($theTargetRel/RiC:date)=$relDate">
                                                        <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                                    </xsl:if>
                                                </xsl:when>
                                                <xsl:otherwise>AREVOIR</xsl:otherwise>
                                            </xsl:choose>
                                    
                                </xsl:for-each>
                                
                                
                                
                                
                            </xsl:when>
                            
                            <xsl:when
                                test="
                                starts-with($link, 'http://data.bnf.fr/ark:/12148/') and $relFile/rdf:Description[
                                ends-with(rdf:type/@rdf:resource, $relName)
                                and
                                child::*[local-name() = $toAncRelArcName and ends-with(@rdf:resource, concat('_', $recId))]
                                and
                                child::*[local-name() = $relSecondArcName and starts-with(@rdf:resource, $link)]
                                
                                
                                ]">
                                
                                <xsl:for-each select="$relFile/rdf:Description[
                                    ends-with(rdf:type/@rdf:resource, $relName)
                                    and
                                    child::*[local-name() = $toAncRelArcName and ends-with(@rdf:resource, concat('_', $recId))]
                                    and
                                    child::*[local-name() = $relSecondArcName and starts-with(@rdf:resource, 'http://data.bnf.fr/ark:/12148/')]
                                    
                                    
                                    ]">
                                    <xsl:variable name="theTargetRel"
                                        select="."/>
                                    
                                    <xsl:choose>
                                        <xsl:when
                                            test="boolean($relFromDate) and boolean($relToDate) = false()">
                                            <xsl:if
                                                test="$theTargetRel/RiC:beginningDate = $relFromDate and not($theTargetRel/RiC:endDate)"><xsl:element name="RiC:{$arcToRelName}">
                                                    
                                                    <xsl:attribute name="rdf:resource">
                                                        <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                                    </xsl:attribute></xsl:element>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:when
                                            test="boolean($relFromDate) = false() and boolean($relToDate)">
                                            <xsl:if
                                                test="not($theTargetRel/RiC:beginningDate) and $theTargetRel/RiC:endDate = $relToDate"><xsl:element name="RiC:{$arcToRelName}">
                                                    
                                                    <xsl:attribute name="rdf:resource">
                                                        <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                                    </xsl:attribute>
                                                </xsl:element>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:when test="boolean($relFromDate) and boolean($relToDate)">
                                            <xsl:if
                                                test="$theTargetRel/RiC:beginningDate = $relFromDate and $theTargetRel/RiC:endDate = $relToDate">
                                                <xsl:element name="RiC:{$arcToRelName}">
                                                    
                                                    <xsl:attribute name="rdf:resource">
                                                        <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                                    </xsl:attribute>
                                                </xsl:element>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:when
                                            test="boolean($relFromDate) = false() and boolean($relToDate) = false()">
                                            <xsl:if
                                                test="not($theTargetRel/RiC:beginningDate) and not($theTargetRel/RiC:endDate)">
                                                <xsl:element name="RiC:{$arcToRelName}">
                                                    
                                                    <xsl:attribute name="rdf:resource">
                                                        <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                                    </xsl:attribute>
                                                </xsl:element>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:when
                                            test="boolean($relDate)">
                                            <xsl:if
                                                test="boolean($theTargetRel/RiC:date)=$relDate">
                                                <xsl:value-of select="$theTargetRel/@rdf:about"/>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:otherwise>AREVOIR</xsl:otherwise>
                                    </xsl:choose>
                                    
                                </xsl:for-each>
                                
                                
                                
                                
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:text>problème :</xsl:text>
                                <xsl:text>$recId=</xsl:text>
                                <xsl:value-of select="$recId"/>
                                <xsl:text> $relName=</xsl:text>
                                <xsl:value-of select="$relName"/>
                                <xsl:text>; $arcToRelName=</xsl:text>
                                <xsl:value-of select="$arcToRelName"/>
                                <xsl:text> ; $toAncRelArcName=</xsl:text>
                                <xsl:value-of select="$toAncRelArcName"/>
                                <xsl:text> ; $relSecondArcName=</xsl:text>
                                <xsl:value-of select="$relSecondArcName"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                </xsl:choose>
        <!--<xsl:if test="
            starts-with($link, 'FRAN_') and $relFile/rdf:Description[
            ends-with(rdf:type/@rdf:resource, $relName)
            and
            child::*[local-name() = $toAncRelArcName and ends-with(@rdf:resource, concat('_', $recId))]
            and
            child::*[local-name() = $relSecondArcName and ends-with(@rdf:resource, concat('_', substring-after($link, 'FRAN_NP_')))]
            
            
            ]">
            <xsl:text> OUI</xsl:text>
        </xsl:if>-->





    </xsl:template>
   <xsl:template name="outputObjectPropertyForProvenanceRelation">
        <xsl:param name="coll"></xsl:param>
        <xsl:param name="link"></xsl:param>
        <xsl:param name="recId"></xsl:param>
        <xsl:param name="relEntry"></xsl:param>
        <xsl:param name="relDate"></xsl:param>
        <xsl:param name="relFromDate"></xsl:param>
        <xsl:param name="relToDate"></xsl:param>
        <xsl:choose>
            <xsl:when test="$link='non'">
                <xsl:choose>
                    <xsl:when test="$relEntry=''"></xsl:when>
                    <xsl:when test="$relEntry!=''">
                        
                        <xsl:choose>
                            <xsl:when test="$coll='FRSIAF'">
                                <xsl:if test="$siaf-prov-rels/rdf:Description[ends-with(rdf:type/@rdf:resource, 'ArchivalProvenanceRelation') and
                                    ends-with(RiC:provenanceEntity/@rdf:resource,$recId)
                                    and 
                                    RiC:originatedObject=$relEntry
                                    ]
                                    ">
                                    <RiC:hasProvenanceRelation>
                                        <xsl:attribute name="rdf:resource">
                                            <xsl:value-of select="$siaf-prov-rels/rdf:Description[ends-with(rdf:type/@rdf:resource, 'ArchivalProvenanceRelation') and
                                                ends-with(RiC:provenanceEntity/@rdf:resource,$recId)
                                                and 
                                                RiC:originatedObject=$relEntry
                                                ]/@rdf:about"/>
                                        </xsl:attribute>
                                        
                                    </RiC:hasProvenanceRelation>
                                </xsl:if>
                                
                            </xsl:when>
                            <xsl:when test="$coll='FRBNF'">
                                <xsl:if test="$bnf-prov-rels/rdf:Description[ends-with(rdf:type/@rdf:resource, 'ArchivalProvenanceRelation') and
                                    ends-with(RiC:provenanceEntity/@rdf:resource, concat('_', substring-after($recId, 'DGEARC_')))
                                    and 
                                    RiC:originatedObject=$relEntry
                                    ]
                                    ">
                                    <RiC:hasProvenanceRelation>
                                        <xsl:attribute name="rdf:resource">
                                            <xsl:value-of select="$bnf-prov-rels/rdf:Description[ends-with(rdf:type/@rdf:resource, 'ArchivalProvenanceRelation') and
                                                ends-with(RiC:provenanceEntity/@rdf:resource, concat('_', substring-after($recId, 'DGEARC_')))
                                                and 
                                                RiC:originatedObject=$relEntry
                                                ]/@rdf:about"/>
                                        </xsl:attribute>
                                        
                                    </RiC:hasProvenanceRelation>
                                </xsl:if>
                                
                            </xsl:when>
                            <xsl:when test="$coll='FRAN'">
                                <xsl:if test="$an-prov-rels/rdf:Description[ends-with(rdf:type/@rdf:resource, 'ArchivalProvenanceRelation') and
                                    ends-with(RiC:provenanceEntity/@rdf:resource, concat('_', substring-after($recId, 'FRAN_NP_')))
                                    and 
                                    RiC:originatedObject=$relEntry
                                    ]
                                    ">
                                    <RiC:hasProvenanceRelation>
                                        <xsl:attribute name="rdf:resource">
                                            <xsl:value-of select="$an-prov-rels/rdf:Description[ends-with(rdf:type/@rdf:resource, 'ArchivalProvenanceRelation') and
                                                ends-with(RiC:provenanceEntity/@rdf:resource, concat('_', substring-after($recId, 'FRAN_NP_')))
                                                and 
                                                RiC:originatedObject=$relEntry
                                                ]/@rdf:about"/>
                                        </xsl:attribute>
                                        
                                    </RiC:hasProvenanceRelation>
                                </xsl:if>
                                
                            </xsl:when>
                        </xsl:choose>
                    </xsl:when>
                </xsl:choose>
            </xsl:when>
            <xsl:when test="$link!='non'">
             
                <xsl:choose>
                    <xsl:when test="$coll='FRSIAF'">
                        
                        <xsl:choose>
                            <xsl:when test="$siaf-prov-rels/rdf:Description[ends-with(rdf:type/@rdf:resource, 'ArchivalProvenanceRelation') and
                                ends-with(RiC:provenanceEntity/@rdf:resource, $recId)
                                and 
                                RiC:originatedObject/@rdf:resource
                                ]
                                ">
                                <xsl:for-each select="$siaf-prov-rels/rdf:Description[ends-with(rdf:type/@rdf:resource, 'ArchivalProvenanceRelation') and
                                    ends-with(RiC:provenanceEntity/@rdf:resource, $recId)
                                    and 
                                    RiC:originatedObject/@rdf:resource
                                    ]">
                                    <!-- http://piaaf.demo.logilab.fr/resource/FRBNF_record_cb437901278-->
                                    <!-- $link='http://catalogue.bnf.fr/ark:/12148/cb437901278'-->
                                  <!--  <xsl:variable name="theObjectURI" select="RiC:originatedObject/@rdf:resource"/>
                                 
                                    <xsl:choose>
                                        <xsl:when test="ends-with($theObjectURI, concat($link, '_top'))">-->
                                            <RiC:hasProvenanceRelation>
                                                <xsl:attribute name="rdf:resource">
                                                    <xsl:value-of select="@rdf:about"/>
                                                </xsl:attribute>
                                                
                                            </RiC:hasProvenanceRelation>
                                        <!--</xsl:when>
                                    </xsl:choose>-->
                                </xsl:for-each>
                                
                                
                                
                                
                            </xsl:when>
                          <!--  <xsl:when test="$siaf-prov-rels/rdf:Description[ends-with(rdf:type/@rdf:resource, 'ArchivalProvenanceRelation') and
                                ends-with(RiC:provenanceEntity/@rdf:resource, $recId)
                                and 
                                RiC:originatedObject/@rdf:nodeID
                                ]
                                ">
                               
                                <xsl:for-each select="$siaf-prov-rels/rdf:Description[ends-with(rdf:type/@rdf:resource, 'ArchivalProvenanceRelation') and
                                    ends-with(RiC:provenanceEntity/@rdf:resource, $recId)
                                    and 
                                    RiC:originatedObject/@rdf:nodeID
                                    ]">
                                    <xsl:variable name="theObjectURI" select="RiC:originatedObject/@rdf:nodeID"/>
                                  <!-\-  <xsl:text>@rdf:nodeID=</xsl:text><xsl:value-of select="$theObjectURI"/>-\->
                                    <xsl:choose>
                                        <xsl:when test="ends-with($theObjectURI, substring-after($link, 'http://catalogue.bnf.fr/ark:/12148/'))">
                                            <RiC:hasProvenanceRelation>
                                                <xsl:attribute name="rdf:resource">
                                                    <xsl:value-of select="@rdf:about"/>
                                                </xsl:attribute>
                                                
                                            </RiC:hasProvenanceRelation>
                                        </xsl:when>
                                    </xsl:choose>
                                </xsl:for-each>
                                
                                
                                
                                
                            </xsl:when>-->
                        </xsl:choose>
                      
                       
                        
                    </xsl:when>
                    <xsl:when test="$coll='FRBNF'">
                        <xsl:if test="$bnf-prov-rels/rdf:Description[ends-with(rdf:type/@rdf:resource, 'ArchivalProvenanceRelation') and
                            ends-with(RiC:provenanceEntity/@rdf:resource, concat('_', substring-after($recId, 'DGEARC_')))
                           and
                            RiC:originatedObject/@rdf:resource
                            ]
                            ">
                            <xsl:for-each select="$bnf-prov-rels/rdf:Description[ends-with(rdf:type/@rdf:resource, 'ArchivalProvenanceRelation') and
                                ends-with(RiC:provenanceEntity/@rdf:resource, concat('_', substring-after($recId, 'DGEARC_')))
                                and
                                RiC:originatedObject/@rdf:resource
                                ]">
                             <!--   <xsl:if test="contains(rdfs:label, $relEntry)">-->
                                    <RiC:hasProvenanceRelation>
                                        <xsl:attribute name="rdf:resource">
                                            <xsl:value-of select="@rdf:about"/>
                                        </xsl:attribute>
                                        
                                    </RiC:hasProvenanceRelation>
                                <!--</xsl:if>-->
                            
                            </xsl:for-each>
                          
                        </xsl:if>
                        
                    </xsl:when>
                    <xsl:when test="$coll='FRAN'">
                     <!--   <xsl:variable name="reducedLink" select="substring-after($link, 'FRAN_IR')"/>-->
                        <xsl:choose>
                           
                            <xsl:when test="$an-prov-rels/rdf:Description[ends-with(rdf:type/@rdf:resource, 'ArchivalProvenanceRelation') and
                                ends-with(RiC:provenanceEntity/@rdf:resource, concat('_', substring-after($recId, 'FRAN_NP_')))
                                and
                                RiC:originatedObject/@rdf:resource
                                ]">
                                <xsl:for-each select="$an-prov-rels/rdf:Description[ends-with(rdf:type/@rdf:resource, 'ArchivalProvenanceRelation') and
                                    ends-with(RiC:provenanceEntity/@rdf:resource, concat('_', substring-after($recId, 'FRAN_NP_')))
                                    and
                                    RiC:originatedObject/@rdf:resource
                                    ]
                                    ">
                                    <xsl:variable name="theObjectURI" select="RiC:originatedObject/@rdf:resource"/>
                                   <!-- <xsl:text>@rdf:nodeID=</xsl:text><xsl:value-of select="$theObjectURI"/>-->
                                   
                                  <!--  <xsl:choose>
                                        <xsl:when test="contains($link, '#')">
                                           <!-\- <xsl:if test="ends-with($theObjectURI, concat(substring-before(substring-after($link, 'FRAN_'), '#'), '-top'))">-\->
                                            <xsl:if test="ends-with($theObjectURI, concat('_', substring-before($reducedLink, '#'), '-top'))">
                                                <RiC:hasProvenanceRelation>
                                                    <xsl:attribute name="rdf:resource">
                                                        <xsl:value-of select="@rdf:about"/>
                                                    </xsl:attribute>
                                                </RiC:hasProvenanceRelation>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:when test="not(contains($link, '#'))">-->
                                            <!-- http://piaaf.demo.logilab.fr/resource/FRAN_record-set_016801-top-->
                                            <!-- $link=''FRAN_IR_016801''-->
                                           <!-- <xsl:if test="ends-with($theObjectURI, concat('_', $reducedLink, '-top'))">-->
                                                <RiC:hasProvenanceRelation>
                                                    <xsl:attribute name="rdf:resource">
                                                        <xsl:value-of select="@rdf:about"/>
                                                    </xsl:attribute>
                                                </RiC:hasProvenanceRelation>
                                            <!--</xsl:if>-->
                                    <!--    </xsl:when>
                                    </xsl:choose>
                                    -->
                                </xsl:for-each>
                            </xsl:when>
                         <!--   <xsl:when test="$an-prov-rels/rdf:Description[ends-with(rdf:type/@rdf:resource, 'ArchivalProvenanceRelation') and
                                ends-with(RiC:provenanceEntity/@rdf:resource, concat('_', substring-after($recId, 'FRAN_NP_')))
                                and
                                RiC:originatedObject/@rdf:nodeID
                                ]">
                                <xsl:for-each select="$an-prov-rels/rdf:Description[ends-with(rdf:type/@rdf:resource, 'ArchivalProvenanceRelation') and
                                    ends-with(RiC:provenanceEntity/@rdf:resource, concat('_', substring-after($recId, 'FRAN_NP_')))
                                    and
                                    RiC:originatedObject/@rdf:nodeID
                                    ]
                                    ">
                                    <xsl:variable name="theObjectURI" select="RiC:originatedObject/@rdf:nodeID"/>
                                    <xsl:choose>
                                        <xsl:when test="contains($link, '#')">
                                            <xsl:if test="ends-with($theObjectURI, concat(substring-before(substring-after($link, 'FRAN_'), '#'), '-top'))">
                                                <RiC:hasProvenanceRelation>
                                                    <xsl:attribute name="rdf:resource">
                                                        <xsl:value-of select="@rdf:about"/>
                                                    </xsl:attribute>
                                                </RiC:hasProvenanceRelation>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:when test="not(contains($link, '#'))">
                                            <xsl:if test="ends-with($theObjectURI, concat(substring-after($link, 'FRAN_'), '-top'))">
                                                <RiC:hasProvenanceRelation>
                                                    <xsl:attribute name="rdf:resource">
                                                        <xsl:value-of select="@rdf:about"/>
                                                    </xsl:attribute>
                                                </RiC:hasProvenanceRelation>
                                            </xsl:if>
                                        </xsl:when>
                                    </xsl:choose>
                                    
                                </xsl:for-each>
                            </xsl:when>
                            -->
                        </xsl:choose>
                       
                            
                     
                        
                        
                    </xsl:when>
                </xsl:choose>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template name="outputDate">
        <xsl:param name="date"/>
        <xsl:param name="stdDate"/>
        
       <xsl:choose>
            
            <xsl:when test="string-length($stdDate)=4">
                <xsl:attribute name="rdf:datatype"> <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text></xsl:attribute>
                
            </xsl:when>
            <xsl:when test="string-length($stdDate)=7">
                <xsl:attribute name="rdf:datatype">
                    <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                </xsl:attribute>
            </xsl:when>
            <xsl:when test="string-length($stdDate)=10">
                <xsl:attribute name="rdf:datatype">
                    <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                </xsl:attribute>
            </xsl:when>
            <xsl:otherwise/>
        </xsl:choose>
        
        <xsl:value-of select="
            if (normalize-space($stdDate)!='')
            then (normalize-space($stdDate))
            else(
            if (normalize-space($date)!='')
            then (normalize-space($date))
            else()
            )
            "/>
       
        
        
        
    </xsl:template>
</xsl:stylesheet>
