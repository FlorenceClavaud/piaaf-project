<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:owl="http://www.w3.org/2002/07/owl#"
    xmlns:RiC="http://www.ica.org/standards/RiC/ontology#" xmlns:isni="http://isni.org/ontology#"
    xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:eac="urn:isbn:1-931666-33-4" xmlns="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:piaaf="http://www.piaaf.net" xmlns:piaaf-onto="http://piaaf.demo.logilab.fr/ontology#"
    xmlns:iso-thes="http://purl.org/iso25964/skos-thes#"
    xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:foaf="http://xmlns.com/foaf/0.1/"
    xmlns:dct="http://purl.org/dc/terms/" xmlns:xl="http://www.w3.org/2008/05/skos-xl#"
    xmlns:ginco="http://data.culture.fr/thesaurus/ginco/ns/"
    exclude-result-prefixes="xs xd eac iso-thes rdf dct xl piaaf xlink skos foaf ginco dc isni"
    version="2.0">
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> July 4, 2017, checked and updated Dec. 12, 2017</xd:p>
            <xd:p><xd:b>Author:</xd:b> Florence Clavaud (Archives nationales)</xd:p>
            <xd:p>étape 14 : génération d'un fichier RDF pour les instruments de recherche pour
                chacun des corpus</xd:p>
            <xd:p>Une ressource RDF de type Finding Aid est liée au Record Set de niveau "top"
                qu'elle décrit</xd:p>
            <xd:p/>
        </xd:desc>
    </xd:doc>
    <xsl:param name="coll">FRSIAF</xsl:param>


    <xsl:variable name="chemin-IR-AN"
        select="concat('fichiers-def-2/IR-EAD/AN/', '?select=*.xml;recurse=yes;on-error=warning')"/>
    <xsl:variable name="collection-IR-AN" select="collection($chemin-IR-AN)"/>

    <xsl:variable name="chemin-IR-SIAF"
        select="concat('fichiers-def-2/IR-EAD/SIAF/', '?select=*.xml;recurse=yes;on-error=warning')"/>
    <xsl:variable name="collection-IR-SIAF" select="collection($chemin-IR-SIAF)"/>
    <xsl:variable name="chemin-IR-BNF"
        select="concat('fichiers-def-2/IR-EAD/BnF/', '?select=*.xml;recurse=yes;on-error=warning')"/>
    <xsl:variable name="collection-IR-BnF" select="collection($chemin-IR-BNF)"/>



    <xsl:template match="/piaaf:vide">

        <xsl:if test="$coll = 'FRBNF'">





            <xsl:for-each select="$collection-IR-BnF/ead">
                <xsl:variable name="IRId">
                    <xsl:value-of
                        select="substring-after(archdesc/otherfindaid/p[extref]/extref/@href, 'ark:/12148/')"
                    />
                </xsl:variable>
                <xsl:variable name="IRTitle">
                    <xsl:value-of
                        select="
                            
                            if (not(eadheader/filedesc/titlestmt/subtitle))
                            then
                                (
                                
                                normalize-space(eadheader/filedesc/titlestmt/titleproper))
                            
                            else
                                (
                                concat(normalize-space(eadheader/filedesc/titlestmt/titleproper), ' - ', normalize-space(eadheader/filedesc/titlestmt/subtitle))
                                
                                )"
                    />
                </xsl:variable>
               <!-- <xsl:variable name="authors">

                    <xsl:for-each select="eadheader/filedesc/titlestmt/author">
                        <author>
                            <xsl:value-of select="normalize-space(.)"/>
                        </author>
                    </xsl:for-each>
                </xsl:variable>-->
                <xsl:variable name="publisher">
                    <xsl:value-of
                        select="normalize-space(eadheader/filedesc/publicationstmt/publisher)"/>
                </xsl:variable>

                <xsl:variable name="unitType">
                    <xsl:choose>
                        <xsl:when test="archdesc/@level = 'item'">record</xsl:when>
                        <xsl:otherwise>record-set</xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:variable name="unitClass">
                    <xsl:choose>
                        <xsl:when test="$unitType = 'record'">Record</xsl:when>
                        <xsl:otherwise>RecordSet</xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>


                <xsl:result-document method="xml" encoding="utf-8" indent="yes" href="{concat('rdf/finding-aids/FRBNF_finding-aid_', $IRId, '.rdf')}">
                    
                 

                    <rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                        xmlns:owl="http://www.w3.org/2002/07/owl#"
                        xmlns:RiC="http://www.ica.org/standards/RiC/ontology#"
                        xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
                        xmlns:piaaf-onto="http://piaaf.demo.logilab.fr/ontology#"
                        xmlns:skos="http://www.w3.org/2004/02/skos/core#">


                        <rdf:Description>
                            <xsl:attribute name="rdf:about">
                                <xsl:value-of
                                    select="concat('http://piaaf.demo.logilab.fr/resource/FRBNF_finding-aid_', $IRId)"
                                />
                            </xsl:attribute>

                            <rdf:type
                                rdf:resource="http://www.ica.org/standards/RiC/ontology#FindingAid"/>

                            <RiC:hasTitle xml:lang="fr">
                                <xsl:value-of select="$IRTitle"/>
                            </RiC:hasTitle>
                            <rdfs:label xml:lang="fr">
                                <xsl:value-of select="$IRTitle"/>
                            </rdfs:label>
                            <xsl:for-each select="eadheader/filedesc/titlestmt/author">
                                <RiC:authoredBy xml:lang="fr">
                                    <xsl:value-of select="normalize-space(.)"/>
                                </RiC:authoredBy>
                            </xsl:for-each>
                            <RiC:publishedBy rdf:resource="http://piaaf.demo.logilab.fr/resource/FRBNF_corporate-body_0036">
                                <!--<xsl:value-of select="$publisher"/>-->
                               
                            </RiC:publishedBy>
                            <xsl:if test="eadheader/filedesc/publicationstmt/date">
                                <RiC:publicationDate>
                                    <xsl:value-of
                                        select="normalize-space(eadheader/filedesc/publicationstmt/date)"
                                    />
                                </RiC:publicationDate>
                            </xsl:if>
                            <xsl:if test="eadheader/revisiondesc/change">
                                <RiC:lastUpdateDate>
                                    <xsl:value-of
                                        select="normalize-space(eadheader/revisiondesc/change[1]/date)"
                                    />
                                </RiC:lastUpdateDate>
                            </xsl:if>
                            <RiC:identifiedBy>
                                <xsl:value-of select="$IRId"/>
                            </RiC:identifiedBy>

                            <!--  <RiC:language></RiC:language>-->
                            <RiC:describes>
                                <xsl:attribute name="rdf:resource">
                                    <xsl:value-of
                                        select="concat('http://piaaf.demo.logilab.fr/resource/FRBNF_', $unitType, '_', $IRId, '-top')"
                                    /></xsl:attribute>

                            </RiC:describes>
                            <rdfs:seeAlso>
                                <xsl:attribute name="rdf:resource">
                                    <!--<xsl:value-of select="concat('http://archivesetmanuscrits.bnf.fr/ark:/12148/', )"/>-->
                                    <xsl:text>http://archivesetmanuscrits.bnf.fr/ark:/12148/</xsl:text>
                                    <xsl:value-of select="$IRId"/>

                                </xsl:attribute>
                            </rdfs:seeAlso>
                        </rdf:Description>







                    </rdf:RDF>
                </xsl:result-document>
            </xsl:for-each>
        </xsl:if>


        <xsl:if test="$coll = 'FRAN'">

            <xsl:for-each select="$collection-IR-AN/ead">
                <!-- replace($IR_id, '_', '') -->
                <xsl:variable name="IR_id">
                    <xsl:choose>
                        <xsl:when
                            test="archdesc/otherfindaid/p[extref]/extref/@href">
                            <xsl:value-of
                                select="substring-after(archdesc/otherfindaid/p[extref]/extref/@href, 'FRAN_')"
                            />
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of
                                select="substring-after(eadheader/eadid, 'FRAN_')"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:variable name="IRId">
                    
                    <xsl:choose>
                        <xsl:when
                            test="archdesc/otherfindaid/p[extref]/extref/@href">
                            <xsl:value-of
                                select="replace($IR_id, '_', '')"
                            />
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of
                                select="replace($IR_id, '_', '')"/>
                        </xsl:otherwise>
                    </xsl:choose>
                    
                   
                   
                
                </xsl:variable>
                <xsl:variable name="topRecordSetId">
                    <xsl:choose>
                        <xsl:when
                            test="archdesc/otherfindaid/p[extref]/extref/@href">
                            <xsl:value-of
                                select="substring-after(archdesc/otherfindaid/p[extref]/extref/@href, 'FRAN_IR_')"
                            />
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of
                                select="replace(substring-after(eadheader/eadid, 'FRAN_IR_'), '_', '')"
                            />
                        </xsl:otherwise>
                    </xsl:choose>
                    
                </xsl:variable>
                <xsl:variable name="IRTitle">
                    <xsl:value-of
                        select="
                        
                        if (not(eadheader/filedesc/titlestmt/subtitle))
                        then
                        (
                        
                        normalize-space(eadheader/filedesc/titlestmt/titleproper))
                        
                        else
                        (
                        concat(normalize-space(eadheader/filedesc/titlestmt/titleproper), ' - ', normalize-space(eadheader/filedesc/titlestmt/subtitle))
                        
                        )"
                    />
                </xsl:variable>
              <!--  <xsl:variable name="authors">
                    
                    <xsl:for-each select="eadheader/filedesc/titlestmt/author">
                        <author>
                            <xsl:value-of select="normalize-space(.)"/>
                        </author>
                    </xsl:for-each>
                </xsl:variable>-->
                <xsl:variable name="publisher">
                    <xsl:value-of
                        select="normalize-space(eadheader/filedesc/publicationstmt/publisher)"/>
                </xsl:variable>
                
                <xsl:variable name="unitType">
                    <xsl:choose>
                        <xsl:when test="archdesc/@level = 'item'">record</xsl:when>
                        <xsl:otherwise>record-set</xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:variable name="unitClass">
                    <xsl:choose>
                        <xsl:when test="$unitType = 'record'">Record</xsl:when>
                        <xsl:otherwise>RecordSet</xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                
                
                <xsl:result-document method="xml" encoding="utf-8" indent="yes" href="{concat('rdf/finding-aids/FRAN_finding-aid_', $IRId ,'.rdf')}">
                  
                    <rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                        xmlns:owl="http://www.w3.org/2002/07/owl#"
                        xmlns:RiC="http://www.ica.org/standards/RiC/ontology#"
                        xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
                        xmlns:piaaf-onto="http://piaaf.demo.logilab.fr/ontology#"
                        xmlns:skos="http://www.w3.org/2004/02/skos/core#">
                        
                        
                        <rdf:Description>
                            <xsl:attribute name="rdf:about">
                                <xsl:value-of
                                    select="concat('http://piaaf.demo.logilab.fr/resource/FRAN_finding-aid_', $IRId)"
                                />
                            </xsl:attribute>
                            
                            <rdf:type
                                rdf:resource="http://www.ica.org/standards/RiC/ontology#FindingAid"/>
                            
                            <RiC:hasTitle xml:lang="fr">
                                <xsl:value-of select="$IRTitle"/>
                            </RiC:hasTitle>
                            <rdfs:label xml:lang="fr">
                                <xsl:value-of select="$IRTitle"/>
                            </rdfs:label>
                            <xsl:for-each select="eadheader/filedesc/titlestmt/author">
                                <RiC:authoredBy xml:lang="fr">
                                    <xsl:value-of select="normalize-space(.)"/>
                                </RiC:authoredBy>
                            </xsl:for-each>
                           
                            <RiC:publishedBy rdf:resource="http://piaaf.demo.logilab.fr/resource/FRAN_corporate-body_005061">
                               <!-- <xsl:value-of select="$publisher"/>-->
                            </RiC:publishedBy>
                            <xsl:if test="eadheader/filedesc/publicationstmt/date">
                                <RiC:publicationDate>
                                    <xsl:value-of
                                        select="normalize-space(eadheader/filedesc/publicationstmt/date)"
                                    />
                                </RiC:publicationDate>
                            </xsl:if>
                            <xsl:if test="eadheader/revisiondesc/change">
                                <RiC:lastUpdateDate>
                                    <xsl:value-of
                                        select="normalize-space(eadheader/revisiondesc/change[1]/date)"
                                    />
                                </RiC:lastUpdateDate>
                            </xsl:if>
                            <RiC:identifiedBy>
                                <xsl:value-of select="$IR_id"/>
                            </RiC:identifiedBy>
                            
                            <!--  <RiC:language></RiC:language>-->
                            <RiC:describes>
                                <xsl:attribute name="rdf:resource">
                                    <xsl:value-of
                                        select="concat('http://piaaf.demo.logilab.fr/resource/FRAN_', $unitType, '_', $topRecordSetId, '-top')"
                                    /></xsl:attribute>
                                
                            </RiC:describes>
                           <!-- <rdfs:seeAlso>
                                <xsl:attribute name="rdf:resource">
                                    <!-\-<xsl:value-of select="concat('http://archivesetmanuscrits.bnf.fr/ark:/12148/', )"/>-\->
                                    <xsl:text>http://archivesetmanuscrits.bnf.fr/ark:/12148/</xsl:text>
                                    <xsl:value-of select="$IRId"/>
                                    
                                </xsl:attribute>
                            </rdfs:seeAlso>-->
                            <xsl:if test="not(contains($IR_id, 'protoLD'))">
                                <rdfs:seeAlso>
                                    <xsl:attribute name="rdf:resource">
                                        <!--<xsl:value-of select="concat('http://archivesetmanuscrits.bnf.fr/ark:/12148/', )"/>-->
                                        <xsl:text>https://www.siv.archives-nationales.culture.gouv.fr/siv/</xsl:text>
                                        <xsl:value-of
                                            select="
                                            
                                            concat('IR/FRAN_IR_', $topRecordSetId)
                                           
                                            "/>
                                        
                                    </xsl:attribute>
                                </rdfs:seeAlso>
                            </xsl:if>
                        </rdf:Description>
                        
                        
                        
                        
                        
                        
                        
                    </rdf:RDF>
                </xsl:result-document>
            </xsl:for-each>
         
            
        </xsl:if>

        <xsl:if test="$coll = 'FRSIAF'">
            <xsl:for-each select="$collection-IR-SIAF/ead">
                <xsl:variable name="IR_id">
                    
                    <xsl:value-of select="normalize-space(eadheader/eadid)"/>
                    
                </xsl:variable>
                <xsl:variable name="IRId">
                    
                    <xsl:value-of select="replace($IR_id, '_', '')"/>
                    
                    
                </xsl:variable>
              
                <xsl:variable name="IRTitle">
                    <xsl:value-of
                        select="
                        
                        if (not(eadheader/filedesc/titlestmt/subtitle))
                        then
                        (
                        
                        normalize-space(eadheader/filedesc/titlestmt/titleproper))
                        
                        else
                        (
                        concat(normalize-space(eadheader/filedesc/titlestmt/titleproper), ' - ', normalize-space(eadheader/filedesc/titlestmt/subtitle))
                        
                        )"
                    />
                </xsl:variable>
              <!--  <xsl:variable name="authors">
                    
                    <xsl:for-each select="eadheader/filedesc/titlestmt/author">
                        <author>
                            <xsl:value-of select="normalize-space(.)"/>
                        </author>
                    </xsl:for-each>
                </xsl:variable>-->
                <xsl:variable name="publisher">
                    <xsl:value-of
                        select="normalize-space(eadheader/filedesc/publicationstmt/publisher)"/>
                </xsl:variable>
                
                <xsl:variable name="unitType">
                    <xsl:choose>
                        <xsl:when test="archdesc/@level = 'item'">record</xsl:when>
                        <xsl:otherwise>record-set</xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:variable name="unitClass">
                    <xsl:choose>
                        <xsl:when test="$unitType = 'record'">Record</xsl:when>
                        <xsl:otherwise>RecordSet</xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                
                
                <xsl:result-document method="xml" encoding="utf-8" indent="yes" href="{concat('rdf/finding-aids/FRSIAF_finding-aid_', $IRId, '.rdf')}">
                   
                    
                    <rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                        xmlns:owl="http://www.w3.org/2002/07/owl#"
                        xmlns:RiC="http://www.ica.org/standards/RiC/ontology#"
                        xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
                        xmlns:piaaf-onto="http://piaaf.demo.logilab.fr/ontology#"
                        xmlns:skos="http://www.w3.org/2004/02/skos/core#">
                        
                        
                        <rdf:Description>
                            <xsl:attribute name="rdf:about">
                                <xsl:value-of
                                    select="concat('http://piaaf.demo.logilab.fr/resource/FRSIAF_finding-aid_', $IRId)"
                                />
                            </xsl:attribute>
                            
                            <rdf:type
                                rdf:resource="http://www.ica.org/standards/RiC/ontology#FindingAid"/>
                            
                            <RiC:hasTitle xml:lang="fr">
                                <xsl:value-of select="$IRTitle"/>
                            </RiC:hasTitle>
                            <rdfs:label xml:lang="fr">
                                <xsl:value-of select="$IRTitle"/>
                            </rdfs:label>
                            <xsl:for-each select="eadheader/filedesc/titlestmt/author">
                                <RiC:authoredBy xml:lang="fr">
                                    <xsl:value-of select="normalize-space(.)"/>
                                </RiC:authoredBy>
                            </xsl:for-each>
                               
                           
                            <RiC:publishedBy rdf:resource="http://piaaf.demo.logilab.fr/resource/FRSIAF_corporate-body_FRAD33_FR78422804100033_000000371">
                                <!--<xsl:value-of select="$publisher"/>-->
                            </RiC:publishedBy>
                            <xsl:if test="eadheader/filedesc/publicationstmt/date">
                                <RiC:publicationDate>
                                    <xsl:value-of
                                        select="normalize-space(eadheader/filedesc/publicationstmt/date)"
                                    />
                                </RiC:publicationDate>
                            </xsl:if>
                            <xsl:if test="eadheader/revisiondesc/change">
                                <RiC:lastUpdateDate>
                                    <xsl:value-of
                                        select="normalize-space(eadheader/revisiondesc/change[1]/date)"
                                    />
                                </RiC:lastUpdateDate>
                            </xsl:if>
                            <RiC:identifiedBy>
                                <xsl:value-of select="$IRId"/>
                            </RiC:identifiedBy>
                            
                            <!--  <RiC:language></RiC:language>-->
                            <RiC:describes>
                                <xsl:attribute name="rdf:resource">
                                    <xsl:value-of
                                        select="concat('http://piaaf.demo.logilab.fr/resource/FRSIAF_', $unitType, '_', $IRId, '-top')"
                                    /></xsl:attribute>
                                
                            </RiC:describes>
                            <xsl:if test="$IR_id!='FRAD033_BV_0111'">
                            <rdfs:seeAlso>
                                <xsl:attribute name="rdf:resource">
                                    <!--<xsl:value-of select="concat('http://archivesetmanuscrits.bnf.fr/ark:/12148/', )"/>-->
                                    <!--<xsl:text>http://gael.gironde.fr/ead.html?id=</xsl:text>-->
                                    
                                    <xsl:text>http://gael.gironde.fr/ead.html?id=FRAD033_IR_4T&amp;c=FRAD033_IR_4T_e0000002</xsl:text>
                                    
                                   <!-- <xsl:value-of select="$IR_id"/>-->
                                   
                                    
                                    
                                </xsl:attribute>
                            </rdfs:seeAlso>
                            </xsl:if>
                        </rdf:Description>
                        
                        
                        
                        
                        
                        
                        
                    </rdf:RDF>
                </xsl:result-document>
            </xsl:for-each>

           
        </xsl:if>



    </xsl:template>

</xsl:stylesheet>
