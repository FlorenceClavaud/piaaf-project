<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:piaaf="http://www.piaaf.net"
   
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:eac="urn:isbn:1-931666-33-4"
    xmlns="urn:isbn:1-931666-33-4"
   
    
    exclude-result-prefixes="xs xd eac piaaf"
    version="2.0"
    
    >
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b>June 23, 2017</xd:p>
            <xd:p><xd:b>Author:</xd:b> Florence</xd:p>
            <xd:p>Sémantisation : étape O : si nécessaire (ici c'est le cas puisque les fichiers source ne les fournissent pas), génération d'identifiants (@xml:id) pour chacun des éléments XML dont seront dérivées des ressources RDF</xd:p>
        </xd:desc>
    </xd:doc>
    <!--  -->
    
    
 
    
    <xsl:variable name="chemin-EAC-BnF">
        <xsl:value-of
            select="concat('fichiers-def-src/notices-EAC/BnF/', '?select=*.xml;recurse=yes;on-error=warning')"
        />
    </xsl:variable>
    <xsl:variable name="collection-EAC-BnF" select="collection($chemin-EAC-BnF)"/>
    <xsl:variable name="chemin-EAC-BnF-2">
        <xsl:value-of
            select="'fichiers-def-2/notices-EAC/BnF/'"
        />
    </xsl:variable>
 
    <xsl:variable name="chemin-EAC-SIAF">
        <xsl:value-of
            select="concat('fichiers-def-src/notices-EAC/SIAF/', '?select=*.xml;recurse=yes;on-error=warning')"
        />
    </xsl:variable>
    <xsl:variable name="collection-EAC-SIAF" select="collection($chemin-EAC-SIAF)"/>
    <xsl:variable name="chemin-EAC-SIAF-2">
        <xsl:value-of
            select="'fichiers-def-2/notices-EAC/SIAF/'"
        />
    </xsl:variable>
    <xsl:variable name="chemin-EAC-AN">
        <xsl:value-of
            select="concat('fichiers-def-src/notices-EAC/AN/', '?select=*.xml;recurse=yes;on-error=warning')"
        />
    </xsl:variable>
    <xsl:variable name="collection-EAC-AN" select="collection($chemin-EAC-AN)"/>
    <xsl:variable name="chemin-EAC-AN-2">
        <xsl:value-of
            select="'fichiers-def-2/notices-EAC/AN/'"
        />
    </xsl:variable>
    <xsl:template match="/piaaf:vide">
        
        <xsl:for-each select="$collection-EAC-BnF">
            <xsl:variable name="myId" select="/eac:eac-cpf/eac:control/eac:recordId"/>
            <xsl:result-document href="{$chemin-EAC-BnF-2}{$myId}.xml" method="xml" encoding="utf-8" indent="yes">
                <xsl:apply-templates/>
            </xsl:result-document>
          
            
        </xsl:for-each>
        <xsl:for-each select="$collection-EAC-SIAF">
            <xsl:variable name="myId" select="/eac:eac-cpf/eac:control/eac:recordId"/>
            <xsl:result-document href="{$chemin-EAC-SIAF-2}{$myId}.xml" method="xml" encoding="utf-8" indent="yes">
                <xsl:apply-templates/>
            </xsl:result-document>
            
            
        </xsl:for-each>
        <xsl:for-each select="$collection-EAC-AN">
            <xsl:variable name="myId" select="/eac:eac-cpf/eac:control/eac:recordId"/>
            <xsl:result-document href="{$chemin-EAC-AN-2}{$myId}.xml" method="xml" encoding="utf-8" indent="yes">
                <xsl:apply-templates/>
            </xsl:result-document>
            
            
        </xsl:for-each>
    </xsl:template>
    
    <xsl:template match="* | text() | @* | comment() | processing-instruction()">
        <xsl:copy>
            <xsl:apply-templates select="* | text() | @*| comment() | processing-instruction()"/>
        </xsl:copy>
    </xsl:template>
    <xsl:template match="eac:identity/eac:nameEntry">
        <nameEntry>
            <xsl:copy-of select="@*"/>
            <xsl:attribute name="xml:id">
                <xsl:call-template name="generate-identifier">
                    <xsl:with-param name="resource">agent-name</xsl:with-param>
                    <xsl:with-param name="prov">
                        <xsl:choose>
                            <xsl:when test="starts-with(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FRAN_NP_')">FRAN</xsl:when>
                            <xsl:when test="starts-with(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FRBNF_NP_protoLD_')">FRBNF</xsl:when>
                            <xsl:otherwise>FRSIAF</xsl:otherwise>
                        </xsl:choose>
                    </xsl:with-param>
                    <xsl:with-param name="recNumber">
                        <xsl:choose>
                            <xsl:when test="starts-with(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FRAN_NP_')">
                                <xsl:value-of select="normalize-space(substring-after(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FRAN_NP_'))"/>
                            </xsl:when>
                            <xsl:when test="starts-with(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FRBNF_NP_protoLD_')">
                                <xsl:value-of select="normalize-space(substring-after(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'DGEARC_'))"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="normalize-space(ancestor::eac:eac-cpf/eac:control/eac:recordId)"/>
                            </xsl:otherwise>
                            
                        </xsl:choose>
                    </xsl:with-param>
                    <xsl:with-param name="vocabSource">no</xsl:with-param>
                </xsl:call-template>
            </xsl:attribute>
               <xsl:apply-templates/> 
            
        </nameEntry>
    </xsl:template>
    <xsl:template match="eac:chronItem[ancestor::eac:description]">
        <chronItem>
            <xsl:copy-of select="@*"/>
            <xsl:attribute name="xml:id">
                <xsl:call-template name="generate-identifier">
                    <xsl:with-param name="resource">event</xsl:with-param>
                    <xsl:with-param name="prov">
                        <xsl:choose>
                            <xsl:when test="starts-with(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FRAN_NP_')">FRAN</xsl:when>
                            <xsl:when test="starts-with(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FRBNF_NP_protoLD_')">FRBNF</xsl:when>
                            <xsl:otherwise>FRSIAF</xsl:otherwise>
                        </xsl:choose>
                    </xsl:with-param>
                    <xsl:with-param name="recNumber">
                        <xsl:choose>
                            <xsl:when test="starts-with(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FRAN_NP_')">
                                <xsl:value-of select="normalize-space(substring-after(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FRAN_NP_'))"/>
                            </xsl:when>
                            <xsl:when test="starts-with(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FRBNF_NP_protoLD_')">
                                <xsl:value-of select="normalize-space(substring-after(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'DGEARC_'))"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="normalize-space(ancestor::eac:eac-cpf/eac:control/eac:recordId)"/>
                            </xsl:otherwise>
                            
                        </xsl:choose>
                    </xsl:with-param>
                    <xsl:with-param name="vocabSource">no</xsl:with-param>
                </xsl:call-template>
            </xsl:attribute>
            <xsl:apply-templates/> 
            
        </chronItem>
        
    </xsl:template>
    <xsl:template match="eac:mandate">
        <mandate>
            <xsl:copy-of select="@*"/>
            <xsl:attribute name="xml:id">
                <xsl:call-template name="generate-identifier">
                    <xsl:with-param name="resource">mandate</xsl:with-param>
                    <xsl:with-param name="prov">
                        <xsl:choose>
                            <xsl:when test="starts-with(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FRAN_NP_')">FRAN</xsl:when>
                            <xsl:when test="starts-with(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FRBNF_NP_protoLD_')">FRBNF</xsl:when>
                            <xsl:otherwise>FRSIAF</xsl:otherwise>
                        </xsl:choose>
                    </xsl:with-param>
                    <xsl:with-param name="recNumber">
                        <xsl:choose>
                            <xsl:when test="starts-with(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FRAN_NP_')">
                                <xsl:value-of select="normalize-space(substring-after(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FRAN_NP_'))"/>
                            </xsl:when>
                            <xsl:when test="starts-with(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FRBNF_NP_protoLD_')">
                                <xsl:value-of select="normalize-space(substring-after(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'DGEARC_'))"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="normalize-space(ancestor::eac:eac-cpf/eac:control/eac:recordId)"/>
                            </xsl:otherwise>
                            
                        </xsl:choose>
                    </xsl:with-param>
                    <xsl:with-param name="vocabSource">no</xsl:with-param>
                </xsl:call-template>
            </xsl:attribute>
            <xsl:apply-templates/> 
            
        </mandate>
        
    </xsl:template>
    <xsl:template match="eac:mandate/eac:citation">
        <citation>
            <xsl:copy-of select="@*"/>
            <xsl:attribute name="xml:id">
                <xsl:call-template name="generate-identifier">
                    <xsl:with-param name="resource">record</xsl:with-param>
                    <xsl:with-param name="prov">
                        <xsl:choose>
                            <xsl:when test="starts-with(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FRAN_NP_')">FRAN</xsl:when>
                            <xsl:when test="starts-with(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FRBNF_NP_protoLD_')">FRBNF</xsl:when>
                            <xsl:otherwise>FRSIAF</xsl:otherwise>
                        </xsl:choose>
                    </xsl:with-param>
                    <xsl:with-param name="recNumber">
                        <xsl:choose>
                            <xsl:when test="starts-with(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FRAN_NP_')">
                                <xsl:value-of select="normalize-space(substring-after(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FRAN_NP_'))"/>
                            </xsl:when>
                            <xsl:when test="starts-with(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FRBNF_NP_protoLD_')">
                                <xsl:value-of select="normalize-space(substring-after(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'DGEARC_'))"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="normalize-space(ancestor::eac:eac-cpf/eac:control/eac:recordId)"/>
                            </xsl:otherwise>
                            
                        </xsl:choose>
                    </xsl:with-param>
                    <xsl:with-param name="vocabSource">no</xsl:with-param>
                </xsl:call-template>
            </xsl:attribute>
            <xsl:apply-templates/> 
            
        </citation>
    </xsl:template>
    <xsl:template match="eac:control/eac:conventionDeclaration">
        <conventionDeclaration>
            <xsl:copy-of select="@*"/>
            <xsl:attribute name="xml:id">
                <xsl:call-template name="generate-identifier">
                    <xsl:with-param name="resource">mandate</xsl:with-param>
                    <xsl:with-param name="prov">
                        <xsl:choose>
                            <xsl:when test="starts-with(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FRAN_NP_')">FRAN</xsl:when>
                            <xsl:when test="starts-with(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FRBNF_NP_protoLD_')">FRBNF</xsl:when>
                            <xsl:otherwise>FRSIAF</xsl:otherwise>
                        </xsl:choose>
                    </xsl:with-param>
                    <xsl:with-param name="recNumber">
                        <xsl:choose>
                            <xsl:when test="starts-with(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FRAN_NP_')">
                                <xsl:value-of select="normalize-space(substring-after(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FRAN_NP_'))"/>
                            </xsl:when>
                            <xsl:when test="starts-with(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FRBNF_NP_protoLD_')">
                                <xsl:value-of select="normalize-space(substring-after(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'DGEARC_'))"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="normalize-space(ancestor::eac:eac-cpf/eac:control/eac:recordId)"/>
                            </xsl:otherwise>
                            
                        </xsl:choose>
                    </xsl:with-param>
                    <xsl:with-param name="vocabSource">no</xsl:with-param>
                </xsl:call-template>
            </xsl:attribute>
            <xsl:apply-templates/> 
            
        </conventionDeclaration>
        
    </xsl:template>
    <xsl:template match="eac:description/eac:places/eac:place">
        <place>
            <xsl:copy-of select="@*"/>
            <xsl:attribute name="xml:id">
                <xsl:call-template name="generate-identifier">
                    <xsl:with-param name="resource">place</xsl:with-param>
                    <xsl:with-param name="prov">
                        <xsl:choose>
                            <xsl:when test="starts-with(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FRAN_NP_')">FRAN</xsl:when>
                            <xsl:when test="starts-with(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FRBNF_NP_protoLD_')">FRBNF</xsl:when>
                            <xsl:otherwise>FRSIAF</xsl:otherwise>
                        </xsl:choose>
                    </xsl:with-param>
                    <xsl:with-param name="recNumber">
                        <xsl:choose>
                            <xsl:when test="starts-with(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FRAN_NP_')">
                                <xsl:value-of select="normalize-space(substring-after(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FRAN_NP_'))"/>
                            </xsl:when>
                            <xsl:when test="starts-with(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FRBNF_NP_protoLD_')">
                                <xsl:value-of select="normalize-space(substring-after(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'DGEARC_'))"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="normalize-space(ancestor::eac:eac-cpf/eac:control/eac:recordId)"/>
                            </xsl:otherwise>
                            
                        </xsl:choose>
                    </xsl:with-param>
                    <xsl:with-param name="vocabSource">no</xsl:with-param>
                </xsl:call-template>
            </xsl:attribute>
            <xsl:apply-templates/> 
            
        </place>
        
    </xsl:template>
    <xsl:template match="eac:relations/eac:cpfRelation">
        <cpfRelation>
            <xsl:copy-of select="@*"/>
            <xsl:attribute name="xml:id">
                <xsl:call-template name="generate-identifier">
                    <xsl:with-param name="resource">relation</xsl:with-param>
                    <xsl:with-param name="prov">
                        <xsl:choose>
                            <xsl:when test="starts-with(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FRAN_NP_')">FRAN</xsl:when>
                            <xsl:when test="starts-with(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FRBNF_NP_protoLD_')">FRBNF</xsl:when>
                            <xsl:otherwise>FRSIAF</xsl:otherwise>
                        </xsl:choose>
                    </xsl:with-param>
                    <xsl:with-param name="recNumber">
                        <xsl:choose>
                            <xsl:when test="starts-with(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FRAN_NP_')">
                                <xsl:value-of select="normalize-space(substring-after(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FRAN_NP_'))"/>
                            </xsl:when>
                            <xsl:when test="starts-with(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FRBNF_NP_protoLD_')">
                                <xsl:value-of select="normalize-space(substring-after(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'DGEARC_'))"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="normalize-space(ancestor::eac:eac-cpf/eac:control/eac:recordId)"/>
                            </xsl:otherwise>
                            
                        </xsl:choose>
                    </xsl:with-param>
                    <xsl:with-param name="vocabSource">no</xsl:with-param>
                </xsl:call-template>
            </xsl:attribute>
            <xsl:apply-templates/> 
            
        </cpfRelation>
    </xsl:template>
        <xsl:template match="eac:relations/eac:resourceRelation">
            <resourceRelation>
                <xsl:copy-of select="@*"/>
                <xsl:attribute name="xml:id">
                    <xsl:call-template name="generate-identifier">
                        <xsl:with-param name="resource">relation</xsl:with-param>
                        <xsl:with-param name="prov">
                            <xsl:choose>
                                <xsl:when test="starts-with(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FRAN_NP_')">FRAN</xsl:when>
                                <xsl:when test="starts-with(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FRBNF_NP_protoLD_')">FRBNF</xsl:when>
                                <xsl:otherwise>FRSIAF</xsl:otherwise>
                            </xsl:choose>
                        </xsl:with-param>
                        <xsl:with-param name="recNumber">
                            <xsl:choose>
                                <xsl:when test="starts-with(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FRAN_NP_')">
                                    <xsl:value-of select="normalize-space(substring-after(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FRAN_NP_'))"/>
                                </xsl:when>
                                <xsl:when test="starts-with(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FRBNF_NP_protoLD_')">
                                    <xsl:value-of select="normalize-space(substring-after(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'DGEARC_'))"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="normalize-space(ancestor::eac:eac-cpf/eac:control/eac:recordId)"/>
                                </xsl:otherwise>
                                
                            </xsl:choose>
                        </xsl:with-param>
                        <xsl:with-param name="vocabSource">no</xsl:with-param>
                    </xsl:call-template>
                </xsl:attribute>
                <xsl:apply-templates/> 
                
            </resourceRelation>
    </xsl:template>
    <xsl:template match="eac:legalStatus[ancestor::eac:description]">
        <legalStatus>
            <xsl:copy-of select="@*"/>
            <xsl:attribute name="xml:id">
                
                <xsl:call-template name="generate-identifier">
                    <xsl:with-param name="resource">legal-status</xsl:with-param>
                    <xsl:with-param name="prov">
                        <xsl:choose>
                            <xsl:when test="starts-with(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FRAN_NP_')">FRAN</xsl:when>
                            <xsl:when test="starts-with(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FRBNF_NP_protoLD_')">FRBNF</xsl:when>
                            <xsl:otherwise>FRSIAF</xsl:otherwise>
                        </xsl:choose>
                    </xsl:with-param>
                    <xsl:with-param name="recNumber">
                        <xsl:choose>
                            <xsl:when test="starts-with(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FRAN_NP_')">
                                <xsl:value-of select="normalize-space(substring-after(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FRAN_NP_'))"/>
                            </xsl:when>
                            <xsl:when test="starts-with(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FRBNF_NP_protoLD_')">
                                <xsl:value-of select="normalize-space(substring-after(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'DGEARC_'))"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="normalize-space(ancestor::eac:eac-cpf/eac:control/eac:recordId)"/>
                            </xsl:otherwise>
                            
                        </xsl:choose>
                    </xsl:with-param>
                    <xsl:with-param name="vocabSource">
                        <xsl:choose>
                            <xsl:when test="eac:term/@vocabularySource!=''">
                                <xsl:value-of select="normalize-space(eac:term/@vocabularySource)"/>
                            </xsl:when>
                            <xsl:otherwise>no</xsl:otherwise>
                        </xsl:choose>
                    </xsl:with-param>
                </xsl:call-template>
            </xsl:attribute>
            <xsl:apply-templates/> 
            
        </legalStatus>
    </xsl:template>
    <xsl:template match="eac:description/eac:function[@localType='piaafFunction']">
        <function>
            <xsl:copy-of select="@*"/>
            <xsl:attribute name="xml:id">
                
                <xsl:call-template name="generate-identifier">
                    <xsl:with-param name="resource">function</xsl:with-param>
                    <xsl:with-param name="prov">
                        <xsl:choose>
                            <xsl:when test="starts-with(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FRAN_NP_')">FRAN</xsl:when>
                            <xsl:when test="starts-with(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FRBNF_NP_protoLD_')">FRBNF</xsl:when>
                            <xsl:otherwise>FRSIAF</xsl:otherwise>
                        </xsl:choose>
                    </xsl:with-param>
                    <xsl:with-param name="recNumber">
                        <xsl:choose>
                            <xsl:when test="starts-with(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FRAN_NP_')">
                                <xsl:value-of select="normalize-space(substring-after(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FRAN_NP_'))"/>
                            </xsl:when>
                            <xsl:when test="starts-with(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FRBNF_NP_protoLD_')">
                                <xsl:value-of select="normalize-space(substring-after(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'DGEARC_'))"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="normalize-space(ancestor::eac:eac-cpf/eac:control/eac:recordId)"/>
                            </xsl:otherwise>
                            
                        </xsl:choose>
                    </xsl:with-param>
                    <xsl:with-param name="vocabSource">no</xsl:with-param>
                </xsl:call-template>
            </xsl:attribute>
            <xsl:apply-templates/> 
            
        </function>
    </xsl:template>
    <xsl:template name="generate-identifier">
        <xsl:param name="resource"/>
        <xsl:param name="prov"/>
        <xsl:param name="recNumber"/>
        <xsl:param name="vocabSource"/>
        <xsl:variable name="resourceCode">
            <xsl:choose>
                <xsl:when test="$resource='agent-name'">an</xsl:when>
                <xsl:when test="$resource='event'">ev</xsl:when>
                <xsl:when test="$resource='mandate'">ma</xsl:when>
                <xsl:when test="$resource='place'">pl</xsl:when>
                <xsl:when test="$resource='relation'">re</xsl:when>
                <xsl:when test="$resource='legal-status'">le</xsl:when>
                <xsl:when test="$resource='function'">fu</xsl:when>
                <xsl:when test="$resource='record'">re</xsl:when>
            </xsl:choose>
        </xsl:variable>
        
        <xsl:choose>
            <xsl:when test="$vocabSource!='no'">
                <xsl:value-of select="concat($prov, '_', $resourceCode, '_', $recNumber, '-', $vocabSource)"/>
            </xsl:when>
            <xsl:otherwise>  <xsl:value-of select="concat($prov, '_', $resourceCode, '_', $recNumber, '-', generate-id())"/></xsl:otherwise>
        </xsl:choose>
        
      
    </xsl:template>
</xsl:stylesheet>