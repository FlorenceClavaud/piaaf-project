<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:owl="http://www.w3.org/2002/07/owl#"
    xmlns:RiC="http://www.ica.org/standards/RiC/ontology#" xmlns:isni="http://isni.org/ontology#"
    xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:eac="urn:isbn:1-931666-33-4" xmlns="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:piaaf="http://www.piaaf.net" xmlns:piaaf-onto="http://piaaf.demo.logilab.fr/ontology#"
    xmlns:iso-thes="http://purl.org/iso25964/skos-thes#"
    xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:foaf="http://xmlns.com/foaf/0.1/"
    xmlns:dct="http://purl.org/dc/terms/" xmlns:xl="http://www.w3.org/2008/05/skos-xl#"
    xmlns:ginco="http://data.culture.fr/thesaurus/ginco/ns/"
    exclude-result-prefixes="xs xd eac iso-thes rdf dct xl piaaf xlink piaaf-onto skos foaf ginco dc"
    version="2.0">
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> July 4, 2017, checked and updated Dec. 12, 2017</xd:p>
            <xd:p><xd:b>Author:</xd:b> Florence Clavaud (Archives nationales)</xd:p>
            <xd:p>étape 12 : génération d'un fichier RDF pour chaque Record Set ou Record décrit
                dans les fichiers EAD</xd:p>
            <xd:p>Les fichiers incluent les ressources RDF de type Description
                correspondantes</xd:p>
            <xd:p>Après réflexion et tests, choix de s'arrêter à 3 niveaux de profondeur dans
                dsc</xd:p>
            <xd:p>Pour déterminer si on affaire à un Record ou à un RSet c'est compliqué...</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:param name="coll">FRBNF</xsl:param>
    <xsl:variable name="chemin-EAC-BnF">
        <xsl:value-of
            select="concat('fichiers-def-2/notices-EAC/BnF/', '?select=*.xml;recurse=yes;on-error=warning')"
        />
    </xsl:variable>
    <xsl:variable name="collection-EAC-BnF" select="collection($chemin-EAC-BnF)"/>
    <xsl:variable name="chemin-EAC-SIAF">
        <xsl:value-of
            select="concat('fichiers-def-2/notices-EAC/SIAF/', '?select=*.xml;recurse=yes;on-error=warning')"
        />
    </xsl:variable>
    <xsl:variable name="collection-EAC-SIAF" select="collection($chemin-EAC-SIAF)"/>
    <xsl:variable name="chemin-EAC-AN">
        <xsl:value-of
            select="concat('fichiers-def-2/notices-EAC/AN/', '?select=*.xml;recurse=yes;on-error=warning')"
        />
    </xsl:variable>
    <xsl:variable name="collection-EAC-AN" select="collection($chemin-EAC-AN)"/>
    <xsl:variable name="apos" select="'&#x2bc;'"/>

    <xsl:variable name="chemin-IR-AN"
        select="concat('fichiers-def-2/IR-EAD/AN/', '?select=*.xml;recurse=yes;on-error=warning')"/>
    <xsl:variable name="collection-IR-AN" select="collection($chemin-IR-AN)"/>

    <xsl:variable name="chemin-IR-SIAF"
        select="concat('fichiers-def-2/IR-EAD/SIAF/', '?select=*.xml;recurse=yes;on-error=warning')"/>
    <xsl:variable name="collection-IR-SIAF" select="collection($chemin-IR-SIAF)"/>
    <xsl:variable name="chemin-IR-BNF"
        select="concat('fichiers-def-2/IR-EAD/BnF/', '?select=*.xml;recurse=yes;on-error=warning')"/>
    <xsl:variable name="collection-IR-BnF" select="collection($chemin-IR-BNF)"/>

    <xsl:variable name="siaf-prov-rels"
        select="document('rdf/relations/FRSIAF_provenance-relations.rdf')/rdf:RDF"/>
    <xsl:variable name="bnf-prov-rels"
        select="document('rdf/relations/FRBNF_provenance-relations.rdf')/rdf:RDF"/>
    <xsl:variable name="an-prov-rels"
        select="document('rdf/relations/FRAN_provenance-relations.rdf')/rdf:RDF"/>

    <xsl:variable name="recordSetTypes"
        select="document('rdf/ontology/RiC-ontology/RiC-O-recordSetTypes.rdf')/rdf:RDF"/>
    <xsl:variable name="chemin-agents"
        select="concat('rdf/agents/', '?select=*.rdf;recurse=yes;on-error=warning')"/>
    <xsl:variable name="collection-agents" select="collection($chemin-agents)"/>

    <xsl:template match="/piaaf:vide">
        <!-- les RecordSets et Records de la BNF-->
        <xsl:if test="$coll = 'FRBNF'">
            <xsl:for-each
                select="$collection-IR-BnF/ead/archdesc | $collection-IR-BnF/ead/archdesc/dsc/descendant::c[count(ancestor::c) &lt; 3][normalize-space(did) != '']">
                <xsl:variable name="topRecordSetId">
                    <xsl:value-of
                        select="substring-after(ancestor-or-self::archdesc/otherfindaid/p[extref]/extref/@href, 'ark:/12148/')"
                    />
                </xsl:variable>
                <xsl:variable name="myId"
                    select="
                        if (self::archdesc)
                        then
                            (concat($topRecordSetId, '-top'))
                        else
                            (
                            concat($topRecordSetId, '-c', @id))
                        "/>

                <xsl:variable name="unitType">
                    <xsl:choose>
                        <xsl:when test="@level = 'item'">record</xsl:when>
                        <xsl:otherwise>record-set</xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:variable name="unitClass">
                    <xsl:choose>
                        <xsl:when test="$unitType = 'record'">Record</xsl:when>
                        <xsl:otherwise>RecordSet</xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:variable name="outputFileUrl"
                    select="concat('rdf/', $unitType, 's/', $coll, '_', $unitType, '_', $myId, '.rdf')"/>
                <xsl:result-document method="xml" encoding="utf-8" indent="yes"
                    href="{$outputFileUrl}">

                    <rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                        xmlns:owl="http://www.w3.org/2002/07/owl#"
                        xmlns:RiC="http://www.ica.org/standards/RiC/ontology#"
                        xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
                        xmlns:piaaf-onto="http://piaaf.demo.logilab.fr/ontology#"
                        xmlns:skos="http://www.w3.org/2004/02/skos/core#">
                        <rdf:Description>
                            <xsl:variable name="uri"
                                select="concat('http://piaaf.demo.logilab.fr/resource/', $coll, '_', $unitType, '_', $myId)"/>
                            <xsl:attribute name="rdf:about">
                                <xsl:value-of select="$uri"/>
                            </xsl:attribute>
                            <!-- <rdf:type
                                rdf:resource="http://www.ica.org/standards/RiC/ontology#RecordSet"/>-->
                            <rdf:type>
                                <xsl:attribute name="rdf:resource">
                                    <xsl:value-of
                                        select="concat('http://www.ica.org/standards/RiC/ontology#', $unitClass)"
                                    />
                                </xsl:attribute>
                            </rdf:type>
                            <xsl:if test="@level[normalize-space(.) != 'item']">
                                <xsl:variable name="lev"
                                    select="
                                        if (@level != 'otherlevel')
                                        then
                                            @level
                                        else
                                            (@otherlevel)"/>
                                <xsl:if
                                    test="$recordSetTypes/owl:NamedIndividual[rdf:type/@rdf:resource = 'http://www.w3.org/2004/02/skos/core#Concept' and skos:prefLabel = $lev]">
                                    <RiC:recordSetType>
                                        <xsl:attribute name="rdf:resource">
                                            <xsl:value-of
                                                select="$recordSetTypes/owl:NamedIndividual[rdf:type/@rdf:resource = 'http://www.w3.org/2004/02/skos/core#Concept' and skos:prefLabel = $lev]/@rdf:about"
                                            />
                                        </xsl:attribute>
                                    </RiC:recordSetType>
                                </xsl:if>
                                <xsl:if
                                    test="not($recordSetTypes/owl:NamedIndividual[rdf:type/@rdf:resource = 'http://www.w3.org/2004/02/skos/core#Concept' and skos:prefLabel = $lev])">
                                    <RiC:recordSetType xml:lang="fr">
                                        <xsl:value-of select="$lev"/>
                                    </RiC:recordSetType>
                                </xsl:if>
                            </xsl:if>
                            <xsl:if test="normalize-space(did/unittitle) != ''">
                                <rdfs:label xml:lang="fr">
                                    <xsl:value-of select="did/unittitle"/>
                                </rdfs:label>
                                <RiC:hasTitle xml:lang="fr">
                                    <xsl:value-of select="did/unittitle"/>
                                </RiC:hasTitle>
                            </xsl:if>
                            <xsl:if test="normalize-space(did/unittitle) = ''">
                                <xsl:choose>
                                    <xsl:when test="did/unitid[normalize-space(.) != '']">
                                        <rdfs:label xml:lang="fr">
                                            <xsl:value-of
                                                select="normalize-space(did/unitid[normalize-space(.) != ''][1])"
                                            />
                                        </rdfs:label>
                                        <RiC:hasTitle xml:lang="fr">
                                            <xsl:value-of
                                                select="normalize-space(did/unitid[normalize-space(.) != ''][1])"
                                            />
                                        </RiC:hasTitle>
                                    </xsl:when>
                                    <xsl:when test="did/unitdate[normalize-space(.) != '']">
                                        <rdfs:label xml:lang="fr">
                                            <xsl:value-of
                                                select="normalize-space(did/unitdate[normalize-space(.) != ''][1])"
                                            />
                                        </rdfs:label>
                                        <RiC:hasTitle xml:lang="fr">
                                            <xsl:value-of
                                                select="normalize-space(did/unitdate[normalize-space(.) != ''][1])"
                                            />
                                        </RiC:hasTitle>
                                    </xsl:when>
                                    <xsl:otherwise/>
                                </xsl:choose>
                            </xsl:if>
                            <xsl:for-each select="did/unitid[normalize-space(.) != '']">
                                <RiC:identifiedBy>
                                    <xsl:value-of select="normalize-space(.)"/>
                                </RiC:identifiedBy>
                            </xsl:for-each>

                            <xsl:for-each
                                select="did/unitdate[normalize-space(.) != '' or normalize-space(@normal) != '']">
                                <xsl:choose>
                                    <xsl:when test="normalize-space(@normal) != ''">
                                        <xsl:choose>
                                            <xsl:when test="not(contains(@normal, '/'))">
                                                <RiC:date>
                                                  <xsl:call-template name="outputDate">
                                                  <xsl:with-param name="date"
                                                  select="normalize-space(.)"/>
                                                  <xsl:with-param name="stdDate"
                                                  select="normalize-space(@normal)"/>
                                                  </xsl:call-template>
                                                </RiC:date>
                                            </xsl:when>

                                            <xsl:when test="contains(@normal, '/')">
                                                <xsl:analyze-string
                                                  select="normalize-space(@normal)"
                                                  regex="^([0-9]{{4}})(-[01][1-9])(-[0-3][1-9])/([0-9]{{4}})(-[01][1-9])(-[0-3][1-9])$">


                                                  <xsl:matching-substring>
                                                  <RiC:beginningDate>
                                                  <xsl:attribute name="rdf:datatype">
                                                  <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                                  </xsl:attribute>
                                                  <xsl:value-of
                                                  select="concat(regex-group(1), regex-group(2), regex-group(3))"
                                                  />
                                                  </RiC:beginningDate>
                                                  <RiC:endDate>
                                                  <xsl:attribute name="rdf:datatype">
                                                  <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                                  </xsl:attribute>
                                                  <xsl:value-of
                                                  select="concat(regex-group(4), regex-group(5), regex-group(6))"
                                                  />
                                                  </RiC:endDate>

                                                  </xsl:matching-substring>
                                                  <xsl:non-matching-substring>
                                                  <xsl:analyze-string select="."
                                                  regex="^([0-9]{{4}})(-[01][1-9])/([0-9]{{4}})(-[01][1-9])$">
                                                  <xsl:matching-substring>
                                                  <RiC:beginningDate>
                                                  <xsl:attribute name="rdf:datatype">
                                                  <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                                  </xsl:attribute>
                                                  <xsl:value-of
                                                  select="concat(regex-group(1), regex-group(2))"/>
                                                  </RiC:beginningDate>
                                                  <RiC:endDate>
                                                  <xsl:attribute name="rdf:datatype">
                                                  <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                                  </xsl:attribute>
                                                  <xsl:value-of
                                                  select="concat(regex-group(3), regex-group(4))"/>
                                                  </RiC:endDate>
                                                  </xsl:matching-substring>
                                                  <xsl:non-matching-substring>
                                                  <xsl:analyze-string select="."
                                                  regex="^([0-9]{{4}})/([0-9]{{4}})$">
                                                  <xsl:matching-substring>
                                                  <RiC:beginningDate>
                                                  <xsl:attribute name="rdf:datatype">
                                                  <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text>
                                                  </xsl:attribute>
                                                  <xsl:value-of select="regex-group(1)"/>
                                                  </RiC:beginningDate>
                                                  <RiC:endDate>
                                                  <xsl:attribute name="rdf:datatype">
                                                  <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text>
                                                  </xsl:attribute>
                                                  <xsl:value-of select="regex-group(2)"/>
                                                  </RiC:endDate>
                                                  </xsl:matching-substring>
                                                  <xsl:non-matching-substring>
                                                  <RiC:date>
                                                  <xsl:value-of select="."/>
                                                  </RiC:date>

                                                  </xsl:non-matching-substring>
                                                  </xsl:analyze-string>
                                                  </xsl:non-matching-substring>
                                                  </xsl:analyze-string>
                                                  </xsl:non-matching-substring>
                                                </xsl:analyze-string>
                                            </xsl:when>



                                        </xsl:choose>


                                    </xsl:when>
                                    <xsl:otherwise>
                                        <RiC:date>
                                            <xsl:value-of select="normalize-space(.)"/>
                                        </RiC:date>

                                    </xsl:otherwise>
                                </xsl:choose>

                            </xsl:for-each>




                            <xsl:if
                                test="did/origination[normalize-space(.) != ''] or ancestor::*[did/origination[normalize-space(.) != '']]">
                                <xsl:choose>
                                    <xsl:when test="did/origination[normalize-space(.) != '']">
                                        <xsl:for-each
                                            select="did/origination[normalize-space(.) != '']">
                                            <xsl:variable name="theOrigEl" select="."/>

                                            <xsl:choose>
                                                <xsl:when test="$theOrigEl/*[@authfilenumber]">
                                                  <xsl:for-each
                                                  select="$theOrigEl/*[@authfilenumber]">
                                                  <xsl:variable name="origId"
                                                  select="substring-after(@authfilenumber, 'DGEARC_')"/>

                                                  <xsl:variable name="origType">
                                                  <xsl:choose>
                                                  <xsl:when test="self::persname">person</xsl:when>
                                                  <xsl:when test="self::corpname"
                                                  >corporate-body</xsl:when>
                                                  <xsl:when test="self::famname">family</xsl:when>
                                                  </xsl:choose>
                                                  </xsl:variable>
                                                  <xsl:variable name="origUri"
                                                  select="concat('http://piaaf.demo.logilab.fr/resource/FRBNF_', $origType, '_', $origId)"/>
                                                  <xsl:choose>
                                                  <xsl:when
                                                  test="
                                                                    $bnf-prov-rels/rdf:Description[rdf:type/@rdf:resource = 'http://www.ica.org/standards/RiC/ontology#ArchivalProvenanceRelation' and
                                                                    
                                                                    RiC:provenanceEntity/@rdf:resource = $origUri
                                                                    ]">

                                                  <xsl:for-each
                                                  select="
                                                                        $bnf-prov-rels/rdf:Description[rdf:type/@rdf:resource = 'http://www.ica.org/standards/RiC/ontology#ArchivalProvenanceRelation' and
                                                                        
                                                                        RiC:provenanceEntity/@rdf:resource = $origUri
                                                                        ]">

                                                  <xsl:variable name="origObjectShortUri">
                                                  <xsl:value-of
                                                  select="
                                                                                if (
                                                                                ends-with(RiC:originatedObject/@rdf:resource, '-top')
                                                                                
                                                                                )
                                                                                
                                                                                then
                                                                                    (
                                                                                    
                                                                                    substring-before(RiC:originatedObject/@rdf:resource, '-top')
                                                                                    
                                                                                    
                                                                                    
                                                                                    )
                                                                                
                                                                                else
                                                                                    (
                                                                                    RiC:originatedObject/@rdf:resource
                                                                                    
                                                                                    
                                                                                    )
                                                                                
                                                                                
                                                                                
                                                                                "/>

                                                  </xsl:variable>
                                                  <xsl:comment>§origObjectShortUri=<xsl:value-of select="$origObjectShortUri"/></xsl:comment>

                                                  <xsl:choose>
                                                  <xsl:when test="$unitType = 'record-set'">
                                                  <xsl:if test="contains($uri, $origObjectShortUri)">
                                                  <!--<xsl:if test="$origObjectShortUri=$topRecordSetId">-->
                                                  <RiC:originatedBy>
                                                  <xsl:attribute name="rdf:resource">
                                                  <xsl:value-of
                                                  select="
                                                                                                @rdf:about
                                                                                                "
                                                  />
                                                  </xsl:attribute>
                                                  </RiC:originatedBy>




                                                  </xsl:if>
                                                  </xsl:when>
                                                  <xsl:when test="$unitType = 'record'">
                                                  <xsl:variable name="theObjectUri"
                                                  select="substring-after($origObjectShortUri, 'record-set')"/>
                                                  <xsl:if test="contains($uri, $theObjectUri)">
                                                  <!--<xsl:if test="$origObjectShortUri=$topRecordSetId">-->
                                                  <RiC:originatedBy>
                                                  <xsl:attribute name="rdf:resource">
                                                  <xsl:value-of
                                                  select="
                                                                                                @rdf:about
                                                                                                "
                                                  />
                                                  </xsl:attribute>
                                                  </RiC:originatedBy>




                                                  </xsl:if>
                                                  </xsl:when>
                                                  </xsl:choose>






                                                  </xsl:for-each>


                                                  </xsl:when>
                                                  <xsl:otherwise>
                                                  <xsl:comment><xsl:text>Attention, la cible du sous-élément </xsl:text>
                                                <xsl:value-of select="name()"/>
                                                    <xsl:text> (</xsl:text>
                                                    <xsl:value-of select="@authfilenumber"/>
                                                    <xsl:text>) de origination ne correspond à aucune relation de provenance préétablie</xsl:text>
                                                </xsl:comment>
                                                  <xsl:comment>
                                                    <xsl:text>$origId=</xsl:text>
                                                    <xsl:value-of select="$origId"/>
                                                    <xsl:text> ; $origType=</xsl:text>
                                                   <xsl:value-of select="$origType"/>
                                                      <xsl:text> ; $origUri=</xsl:text>
                                                   <xsl:value-of select="$origUri"/>
                                                      <xsl:text> ; $uri=</xsl:text>
                                                   <xsl:value-of select="$uri"/>
                                                </xsl:comment>
                                                  <RiC:createdBy rdf:resource="{$origUri}"
                                                  > </RiC:createdBy>
                                                  </xsl:otherwise>
                                                  </xsl:choose>




                                                  <!--<xsl:when test="$bnf-prov-rels/rdf:Description[rdf:type/@rdf:resource='http://www.ica.org/standards/RiC/ontology#RecordsProvenanceRelation' and 
                                                contains($uri, RiC:originatedObject/@rdf:resource)
                                                
                                                and 
                                                RiC:provenanceEntity/@rdf:resource=$origUri
                                                ]">  <RiC:originatedBy>
                                                    <xsl:attribute name="rdf:resource">
                                                        <xsl:value-of select="$bnf-prov-rels/rdf:Description[rdf:type/@rdf:resource='http://www.ica.org/standards/RiC/ontology#RecordsProvenanceRelation' and 
                                                            
                                                            contains($uri, RiC:originatedObject/@rdf:resource)
                                                            
                                                            and 
                                                            RiC:provenanceEntity/@rdf:resource=$origUri
                                                            ]/@rdf:about"/>
                                                    </xsl:attribute>
                                                </RiC:originatedBy>
                                            </xsl:when>-->





                                                  </xsl:for-each>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                  <RiC:noteOnProvenance xml:lang="fr">
                                                  <xsl:value-of select="$theOrigEl"/>
                                                  </RiC:noteOnProvenance>
                                                </xsl:otherwise>
                                            </xsl:choose>


                                        </xsl:for-each>

                                    </xsl:when>
                                    <xsl:when
                                        test="ancestor::*[did/origination[normalize-space(.) != '']]">
                                        <xsl:for-each
                                            select="ancestor::*[did/origination[normalize-space(.) != '']][1]/did/origination[normalize-space(.) != '']">
                                            <xsl:variable name="theOrigEl" select="."/>

                                            <xsl:choose>
                                                <xsl:when test="$theOrigEl/*[@authfilenumber]">
                                                  <xsl:for-each
                                                  select="$theOrigEl/*[@authfilenumber]">
                                                  <xsl:variable name="origId"
                                                  select="substring-after(@authfilenumber, 'DGEARC_')"/>

                                                  <xsl:variable name="origType">
                                                  <xsl:choose>
                                                  <xsl:when test="self::persname">person</xsl:when>
                                                  <xsl:when test="self::corpname"
                                                  >corporate-body</xsl:when>
                                                  <xsl:when test="self::famname">family</xsl:when>
                                                  </xsl:choose>
                                                  </xsl:variable>
                                                  <xsl:variable name="origUri"
                                                      select="concat('http://piaaf.demo.logilab.fr/resource/', $origType, '_', $origId)"/>
                                                  <xsl:choose>
                                                  <xsl:when
                                                  test="
                                                                    $bnf-prov-rels/rdf:Description[rdf:type/@rdf:resource = 'http://www.ica.org/standards/RiC/ontology#ArchivalProvenanceRelation' and
                                                                    
                                                                    RiC:provenanceEntity/@rdf:resource = $origUri
                                                                    ]">

                                                  <xsl:for-each
                                                  select="
                                                  $bnf-prov-rels/rdf:Description[rdf:type/@rdf:resource = 'http://www.ica.org/standards/RiC/ontology#ArchivalProvenanceRelation' and
                                                                        
                                                                        RiC:provenanceEntity/@rdf:resource = $origUri
                                                                        ]">

                                                  <xsl:variable name="origObjectShortUri">
                                                  <xsl:value-of
                                                  select="
                                                                                if (
                                                                                ends-with(RiC:originatedObject/@rdf:resource, '-top')
                                                                                
                                                                                )
                                                                                
                                                                                then
                                                                                    (
                                                                                    
                                                                                    substring-before(RiC:originatedObject/@rdf:resource, '-top')
                                                                                    
                                                                                    
                                                                                    
                                                                                    )
                                                                                
                                                                                else
                                                                                    (
                                                                                    RiC:originatedObject/@rdf:resource
                                                                                    
                                                                                    
                                                                                    )
                                                                                
                                                                                
                                                                                
                                                                                "/>

                                                  </xsl:variable>

                                                  <xsl:choose>
                                                  <xsl:when test="$unitType = 'record-set'">
                                                  <xsl:if test="contains($uri, $origObjectShortUri)">
                                                  <!--<xsl:if test="$origObjectShortUri=$topRecordSetId">-->
                                                  <RiC:originatedBy>
                                                  <xsl:attribute name="rdf:resource">
                                                  <xsl:value-of
                                                  select="
                                                                                                @rdf:about
                                                                                                "
                                                  />
                                                  </xsl:attribute>
                                                  </RiC:originatedBy>




                                                  </xsl:if>
                                                  </xsl:when>
                                                  <xsl:when test="$unitType = 'record'">
                                                  <xsl:variable name="theObjectUri"
                                                  select="substring-after($origObjectShortUri, 'record-set')"/>
                                                  <xsl:if test="contains($uri, $theObjectUri)">
                                                  <!--<xsl:if test="$origObjectShortUri=$topRecordSetId">-->
                                                  <RiC:originatedBy>
                                                  <xsl:attribute name="rdf:resource">
                                                  <xsl:value-of
                                                  select="
                                                                                                @rdf:about
                                                                                                "
                                                  />
                                                  </xsl:attribute>
                                                  </RiC:originatedBy>




                                                  </xsl:if>
                                                  </xsl:when>
                                                  </xsl:choose>

                                                  </xsl:for-each>


                                                  </xsl:when>
                                                  <xsl:otherwise>
                                                  <xsl:comment><xsl:text>Attention, la cible du sous-élément </xsl:text>
                                                <xsl:value-of select="name()"/>
                                                    <xsl:text> (</xsl:text>
                                                    <xsl:value-of select="@authfilenumber"/>
                                                    <xsl:text>) de origination ne correspond à aucune relation de provenance préétablie</xsl:text>
                                                </xsl:comment>
                                                  <xsl:comment>
                                                    <xsl:text>$origId=</xsl:text>
                                                    <xsl:value-of select="$origId"/>
                                                    <xsl:text> ; $origType=</xsl:text>
                                                   <xsl:value-of select="$origType"/>
                                                      <xsl:text> ; $origUri=</xsl:text>
                                                   <xsl:value-of select="$origUri"/>
                                                      <xsl:text> ; $uri=</xsl:text>
                                                   <xsl:value-of select="$uri"/>
                                                </xsl:comment>
                                                      <RiC:createdBy rdf:resource="{$origUri}"
                                                          > </RiC:createdBy>
                                                  </xsl:otherwise>
                                                  </xsl:choose>




                                                  <!--<xsl:when test="$bnf-prov-rels/rdf:Description[rdf:type/@rdf:resource='http://www.ica.org/standards/RiC/ontology#RecordsProvenanceRelation' and 
                                                contains($uri, RiC:originatedObject/@rdf:resource)
                                                
                                                and 
                                                RiC:provenanceEntity/@rdf:resource=$origUri
                                                ]">  <RiC:originatedBy>
                                                    <xsl:attribute name="rdf:resource">
                                                        <xsl:value-of select="$bnf-prov-rels/rdf:Description[rdf:type/@rdf:resource='http://www.ica.org/standards/RiC/ontology#RecordsProvenanceRelation' and 
                                                            
                                                            contains($uri, RiC:originatedObject/@rdf:resource)
                                                            
                                                            and 
                                                            RiC:provenanceEntity/@rdf:resource=$origUri
                                                            ]/@rdf:about"/>
                                                    </xsl:attribute>
                                                </RiC:originatedBy>
                                            </xsl:when>-->





                                                  </xsl:for-each>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                  <RiC:noteOnProvenance xml:lang="fr">
                                                  <xsl:value-of select="$theOrigEl"/>
                                                  </RiC:noteOnProvenance>
                                                </xsl:otherwise>
                                            </xsl:choose>


                                        </xsl:for-each>
                                    </xsl:when>
                                </xsl:choose>
                                <!--<xsl:variable name="theOrigEl"
                                    select="
                                        if (did/origination[normalize-space(.) != ''])
                                        then
                                            (did/origination[normalize-space(.) != ''])
                                        else
                                            (
                                            
                                            if (ancestor::*[did/origination[normalize-space(.) != '']])
                                            then
                                                (ancestor::*[did/origination[normalize-space(.) != ''][1]]/did/origination)
                                            else
                                                ()
                                            
                                            )
                                        
                                        "> </xsl:variable>-->


                            </xsl:if>

                            <xsl:if
                                test="did/repository[normalize-space(.) != ''] or ancestor::*[did/repository[normalize-space(.) != '']]">
                                <xsl:variable name="theReposEl"
                                    select="
                                        if (did/repository[normalize-space(.) != ''])
                                        then
                                            (did/repository[normalize-space(.) != ''])
                                        else
                                            (
                                            
                                            if (ancestor::*[did/repository[normalize-space(.) != '']])
                                            then
                                                (ancestor::*[did/repository[normalize-space(.) != ''][1]]/did/repository)
                                            else
                                                ()
                                            
                                            )
                                        
                                        "> </xsl:variable>

                                <xsl:choose>
                                    <xsl:when test="$theReposEl/*[@authfilenumber]">
                                        <xsl:for-each select="$theReposEl/*[@authfilenumber]">
                                            <xsl:variable name="reposId"
                                                select="substring-after(@authfilenumber, 'DGEARC_')"/>
                                            <RiC:heldBy>
                                                <xsl:attribute name="rdf:resource">
                                                  <xsl:value-of
                                                      select="concat('http://piaaf.demo.logilab.fr/resource/FRBNF_corporate-body_', $reposId)"
                                                  />
                                                </xsl:attribute>
                                            </RiC:heldBy>
                                        </xsl:for-each>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <piaaf-onto:noteOnRepository xml:lang="fr">
                                            <xsl:value-of select="$theReposEl"/>
                                        </piaaf-onto:noteOnRepository>
                                    </xsl:otherwise>
                                </xsl:choose>

                            </xsl:if>
                            <xsl:if test="scopecontent[normalize-space(.) != '']">
                                <RiC:scopeAndContent xml:lang="fr">
                                    <xsl:for-each select="scopecontent/*">
                                        <xsl:apply-templates/>
                                        <xsl:if test="position() != last()">
                                            <xsl:text> </xsl:text>
                                        </xsl:if>
                                    </xsl:for-each>
                                </RiC:scopeAndContent>
                            </xsl:if>

                            <!--<scopecontent>
                                    <head>Architecte responsable :</head>
                                    <p>
                                        <persname authfilenumber="ark:/12148/cb12442581n "
                                            normal="Pascal, Jean-Louis (1837-1920)" role="0990" source="OPP">Jean-Louis
                                            Pascal (1837-1920)</persname>
                                    </p>
                                </scopecontent>-->
                            <!-- http://data.bnf.fr/ark:/12148/cb12442581n-->
                            <!-- http://data.bnf.fr/ark:/12148/cb12442581n#foaf:Person-->
                            <!-- http://data.bnf.fr/ark:/12148/cb16224627d#foaf:Organization-->
                            <xsl:for-each
                                select="scopecontent/p/persname[normalize-space(@authfilenumber) != '' and normalize-space(@role) = '0990']">
                                <RiC:authoredBy>
                                    <xsl:attribute name="rdf:resource">
                                        <xsl:value-of
                                            select="concat('http://data.bnf.fr/', normalize-space(@authfilenumber), '#foaf:Person')"
                                        />
                                    </xsl:attribute>
                                </RiC:authoredBy>
                            </xsl:for-each>
                            <xsl:for-each
                                select="scopecontent/p/corpname[normalize-space(@authfilenumber) != '' and normalize-space(@role) = '0990']">
                                <RiC:authoredBy>
                                    <xsl:attribute name="rdf:resource">
                                        <xsl:value-of
                                            select="concat('http://data.bnf.fr/', normalize-space(@authfilenumber), '#foaf:Organization')"
                                        />
                                    </xsl:attribute>
                                </RiC:authoredBy>
                            </xsl:for-each>

                            <xsl:if test="did/physdesc/extent[normalize-space(.) != '']">

                                <xsl:for-each select="did/physdesc/extent[normalize-space(.) != '']">
                                    <RiC:physicalOrLogicalExtent xml:lang="fr">
                                        <xsl:apply-templates/>
                                        <xsl:if test="position() != last()">
                                            <xsl:text> </xsl:text>
                                        </xsl:if>
                                    </RiC:physicalOrLogicalExtent>
                                </xsl:for-each>

                            </xsl:if>
                            <xsl:if test="custodhist[normalize-space(.) != '']">
                                <RiC:history xml:lang="fr">
                                    <xsl:for-each select="custodhist/*">
                                        <xsl:apply-templates/>
                                        <xsl:if test="position() != last()">
                                            <xsl:text> </xsl:text>
                                        </xsl:if>
                                    </xsl:for-each>
                                </RiC:history>
                            </xsl:if>
                            <xsl:for-each select="acqinfo[normalize-space(.) != '']">
                                <RiC:noteOnAcquisition xml:lang="fr">
                                    <xsl:for-each select="*">
                                        <xsl:apply-templates/>
                                        <xsl:if test="position() != last()">
                                            <xsl:text> </xsl:text>
                                        </xsl:if>
                                    </xsl:for-each>
                                </RiC:noteOnAcquisition>
                            </xsl:for-each>
                            <xsl:if test="arrangement[normalize-space(.) != '']">
                                <RiC:arrangement xml:lang="fr">
                                    <xsl:for-each select="arrangement/*">
                                        <xsl:apply-templates/>
                                        <xsl:if test="position() != last()">
                                            <xsl:text> </xsl:text>
                                        </xsl:if>
                                    </xsl:for-each>
                                </RiC:arrangement>
                            </xsl:if>
                            <xsl:if test="processinfo[normalize-space(.) != '']">
                                <RiC:noteOnProcessApplied xml:lang="fr">
                                    <xsl:for-each select="processinfo/*">
                                        <xsl:apply-templates/>
                                        <xsl:if test="position() != last()">
                                            <xsl:text> </xsl:text>
                                        </xsl:if>
                                    </xsl:for-each>
                                </RiC:noteOnProcessApplied>
                            </xsl:if>
                            <xsl:if test="accruals[normalize-space(.) != '']">
                                <RiC:accruals xml:lang="fr">
                                    <xsl:for-each select="accruals/*">
                                        <xsl:apply-templates/>
                                        <xsl:if test="position() != last()">
                                            <xsl:text> </xsl:text>
                                        </xsl:if>
                                    </xsl:for-each>
                                </RiC:accruals>
                            </xsl:if>
                            <xsl:if test="accessrestrict[normalize-space(.) != '']">
                                <RiC:conditionsOfAccess xml:lang="fr">
                                    <xsl:for-each select="accessrestrict/*">
                                        <xsl:apply-templates/>
                                        <xsl:if test="position() != last()">
                                            <xsl:text> </xsl:text>
                                        </xsl:if>
                                    </xsl:for-each>
                                </RiC:conditionsOfAccess>
                            </xsl:if>
                            <xsl:for-each
                                select="controlaccess/genreform[normalize-space(.) != ''] | physdesc/genreform[normalize-space(.) != '']">
                                <RiC:contentType xml:lang="fr">
                                    <xsl:value-of select="normalize-space(.)"/>
                                </RiC:contentType>

                            </xsl:for-each>
                            <xsl:for-each select="controlaccess/persname[normalize-space(.) != '']">
                                <RiC:subject>
                                    <xsl:variable name="targ" select="@authfilenumber"/>
                                    <xsl:choose>
                                        <xsl:when test="$targ">
                                            <xsl:variable name="targId"
                                                select="substring-after($targ, 'DGEARC_')"/>
                                            <xsl:choose>
                                                <xsl:when
                                                  test="$collection-agents/rdf:RDF/rdf:Description[rdf:type/@rdf:resource = 'http://www.ica.org/standards/RiC/ontology#Person' and substring-after(@rdf:about, 'person_') = $targId]">
                                                  <xsl:attribute name="rdf:resource">
                                                  <xsl:value-of
                                                  select="$collection-agents/rdf:RDF/rdf:Description[rdf:type/@rdf:resource = 'http://www.ica.org/standards/RiC/ontology#Person' and substring-after(@rdf:about, 'person_') = $targId]/@rdf:about"
                                                  />
                                                  </xsl:attribute>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                  <xsl:attribute name="xml:lang">fr</xsl:attribute>
                                                  <xsl:value-of select="."/>
                                                </xsl:otherwise>
                                            </xsl:choose>

                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:attribute name="xml:lang">fr</xsl:attribute>
                                            <xsl:value-of select="."/>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </RiC:subject>

                            </xsl:for-each>
                            <xsl:for-each select="controlaccess/corpname[normalize-space(.) != '']">
                                <RiC:subject>
                                    <xsl:variable name="targ" select="@authfilenumber"/>
                                    <xsl:choose>
                                        <xsl:when test="normalize-space($targ) != ''">
                                            <xsl:variable name="targId"
                                                select="substring-after($targ, 'DGEARC_')"/>
                                            <xsl:choose>
                                                <xsl:when
                                                  test="$collection-agents/rdf:RDF/rdf:Description[rdf:type/@rdf:resource = 'http://www.ica.org/standards/RiC/ontology#CorporateBody' and substring-after(@rdf:about, 'corporate-body_') = $targId]">
                                                  <xsl:attribute name="rdf:resource">
                                                  <xsl:value-of
                                                  select="$collection-agents/rdf:RDF/rdf:Description[rdf:type/@rdf:resource = 'http://www.ica.org/standards/RiC/ontology#CorporateBody' and substring-after(@rdf:about, 'corporate-body_') = $targId]/@rdf:about"
                                                  />
                                                  </xsl:attribute>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                  <xsl:attribute name="xml:lang">fr</xsl:attribute>
                                                  <xsl:value-of select="."/>
                                                </xsl:otherwise>
                                            </xsl:choose>

                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:attribute name="xml:lang">fr</xsl:attribute>
                                            <xsl:value-of select="."/>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </RiC:subject>

                            </xsl:for-each>
                            <xsl:for-each select="relatedmaterial[normalize-space(.) != '']">
                                <xsl:choose>
                                    <xsl:when
                                        test="extref[starts-with(@href, 'http://gallica.bnf.fr/ark:')]">
                                        <xsl:for-each
                                            select="extref[starts-with(@href, 'http://gallica.bnf.fr/ark:')]">
                                            <RiC:isRelatedWith>
                                                <xsl:attribute name="rdf:resource">
                                                  <xsl:value-of select="@href"/>
                                                </xsl:attribute>
                                            </RiC:isRelatedWith>
                                        </xsl:for-each>


                                    </xsl:when>
                                    <xsl:when
                                        test="extref[starts-with(@href, 'http://webapp.bnf.fr/')]">
                                        <xsl:for-each
                                            select="extref[starts-with(@href, 'http://webapp.bnf.fr/')]">
                                            <RiC:isRelatedWith>
                                                <xsl:attribute name="rdf:resource">
                                                  <xsl:value-of select="@href"/>
                                                </xsl:attribute>
                                            </RiC:isRelatedWith>
                                        </xsl:for-each>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <RiC:noteOnRelatedMaterial xml:lang="fr">
                                            <xsl:for-each select="*">
                                                <xsl:apply-templates/>
                                                <xsl:if test="position != last()">
                                                  <xsl:text> </xsl:text>
                                                </xsl:if>
                                            </xsl:for-each>

                                        </RiC:noteOnRelatedMaterial>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:for-each>
                            <xsl:for-each select="dao[normalize-space(@href) != '']">
                                <RiC:digitalCopy>
                                    <xsl:attribute name="rdf:resource">
                                        <xsl:value-of select="@href"/>
                                    </xsl:attribute>
                                </RiC:digitalCopy>

                            </xsl:for-each>
                            <!-- liens reliant la ressource à celles qui en sont "membres"-->
                            <!-- pour l'instant on crée des relations directes, mais en théorie il faudrait des relations n-aires-->
                            <xsl:if
                                test="self::archdesc[dsc/c] | self::c[count(ancestor::c) &lt; 2 and c]">
                                <xsl:for-each
                                    select="child::dsc/c | self::c[count(ancestor::c) &lt; 2 and c]/c">
                                    <xsl:variable name="childId" select="@id"/>
                                    <xsl:variable name="theChildUnitId"
                                        select="concat($topRecordSetId, '-c', $childId)"/>
                                    <xsl:variable name="theChildType">
                                        <xsl:choose>
                                            <xsl:when test="@level = 'item'">record</xsl:when>
                                            <xsl:otherwise>record-set</xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:variable>
                                    <!--<xsl:variable name="theChildClass">
                                        <xsl:choose>
                                            <xsl:when test="$theChildType='record'">Record</xsl:when>
                                            <xsl:otherwise>RecordSet</xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:variable>-->
                                    <RiC:member>
                                        <xsl:attribute name="rdf:resource">
                                            <xsl:value-of
                                                select="concat('http://piaaf.demo.logilab.fr/resource/', $coll, '_', $theChildType, '_', $theChildUnitId)"/>

                                        </xsl:attribute>
                                    </RiC:member>
                                </xsl:for-each>
                            </xsl:if>
                            <xsl:if test="self::c">
                                
                                <xsl:variable name="parentId">
                                    <xsl:choose>
                                        <xsl:when test="parent::c">
                                            <xsl:value-of select="concat($topRecordSetId, '-c', parent::c/@id)"/>
                                        </xsl:when>
                                        <xsl:when test="parent::dsc">
                                            <xsl:value-of select="concat($topRecordSetId, '-top')"/>
                                        </xsl:when>
                                    </xsl:choose>
                                </xsl:variable>
                                <!--<xsl:variable name="theChildUnitId"
                                        select="concat($topRecordSetId, '-', $childId)"/>-->
                                <xsl:variable name="theParentType">
                                    <xsl:choose>
                                        <xsl:when test="parent::dsc">record-set</xsl:when>
                                        <xsl:otherwise>record-set</xsl:otherwise>
                                    </xsl:choose>
                                </xsl:variable>
                                <!--<xsl:variable name="theChildClass">
                                        <xsl:choose>
                                            <xsl:when test="$theChildType='record'">Record</xsl:when>
                                            <xsl:otherwise>RecordSet</xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:variable>-->
                                <RiC:memberOf>
                                    <xsl:attribute name="rdf:resource">
                                        <xsl:value-of
                                            select="concat('http://piaaf.demo.logilab.fr/resource/', $coll, '_', $theParentType, '_', $parentId)"/>
                                        
                                    </xsl:attribute>
                                </RiC:memberOf>
                                
                                
                            </xsl:if>         
                            <!-- permalien de la page correspondante dans BAM -->
                            <rdfs:seeAlso>
                                <xsl:attribute name="rdf:resource">
                                    <!--<xsl:value-of select="concat('http://archivesetmanuscrits.bnf.fr/ark:/12148/', )"/>-->
                                    <xsl:text>http://archivesetmanuscrits.bnf.fr/ark:/12148/</xsl:text>
                                    <xsl:value-of
                                        select="
                                            if (self::archdesc)
                                            then
                                                ($topRecordSetId)
                                            else
                                                (
                                                concat($topRecordSetId, '/c', @id))
                                            "/>

                                </xsl:attribute>
                            </rdfs:seeAlso>
                            <RiC:mainSubjectOf>
                                <xsl:attribute name="rdf:resource">
                                    <!--<xsl:value-of select="concat('http://archivesetmanuscrits.bnf.fr/ark:/12148/', )"/>-->
                                    <xsl:text>http://archivesetmanuscrits.bnf.fr/ark:/12148/</xsl:text>
                                    <xsl:value-of
                                        select="
                                            if (self::archdesc)
                                            then
                                                ($topRecordSetId)
                                            else
                                                (
                                                concat($topRecordSetId, '/c', @id))
                                            "/>

                                </xsl:attribute>
                            </RiC:mainSubjectOf>
                            <xsl:if test="self::archdesc">
                                <RiC:describedBy>
                                    <xsl:attribute name="rdf:resource">
                                        <xsl:value-of
                                            select="
                                                
                                                
                                                concat('http://piaaf.demo.logilab.fr/resource/FRBNF_finding-aid_', $topRecordSetId)
                                                
                                                "
                                        />
                                    </xsl:attribute>
                                </RiC:describedBy>
                            </xsl:if>
                        </rdf:Description>

                    </rdf:RDF>
                </xsl:result-document>

            </xsl:for-each>
        </xsl:if>

        <xsl:if test="$coll = 'FRAN'">
            <!-- <eadid>FRAN_IR_protoLD_DECAS_001</eadid>-->
            <!-- <eadid>FRAN_IR_protoLD_MR_001</eadid>-->
            <!-- <otherfindaid><p>cet instrument de recherche est dérivé du <extref href="FRAN_IR_009659">Répertoire numérique de la sous-direction des monuments historiques (1876-1980).</extref></p></otherfindaid>-->
            <!-- <otherfindaid>
                <head>Instrument de recherche dont est dérivé le présent instrument de recherche numérique</head>
                <bibref><persname/><title>Répertoire numérique détaillé du versement
                    20070682</title><edition/><imprint><date>2009</date></imprint></bibref>
            </otherfindaid>-->

            <!-- permalien https://www.siv.archives-nationales.culture.gouv.fr/siv/IR/FRAN_IR_009659 -->
            <!-- https://www.siv.archives-nationales.culture.gouv.fr/siv/UD/FRAN_IR_009659/d_1-->
            <xsl:for-each
                select="$collection-IR-AN/ead/archdesc | $collection-IR-AN/ead/archdesc/dsc/descendant::c[count(ancestor::c) &lt; 3][normalize-space(did) != '']">

                <xsl:variable name="IR_id">
                    <xsl:choose>
                        <xsl:when
                            test="ancestor-or-self::archdesc/otherfindaid/p[extref]/extref/@href">
                            <xsl:value-of
                                select="substring-after(ancestor-or-self::archdesc/otherfindaid/p[extref]/extref/@href, 'FRAN_')"
                            />
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of
                                select="substring-after(ancestor::ead/eadheader/eadid, 'FRAN_')"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:variable name="topRecordSetId">
                    <xsl:choose>
                        <xsl:when
                            test="ancestor-or-self::archdesc/otherfindaid/p[extref]/extref/@href">
                            <xsl:value-of
                                select="substring-after(ancestor-or-self::archdesc/otherfindaid/p[extref]/extref/@href, 'FRAN_IR_')"
                            />
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of
                                select="replace(substring-after(ancestor::ead/eadheader/eadid, 'FRAN_IR_'), '_', '')"
                            />
                        </xsl:otherwise>
                    </xsl:choose>

                </xsl:variable>
                <xsl:variable name="myId"
                    select="
                        if (self::archdesc)
                        then
                            (concat($topRecordSetId, '-top'))
                        else
                            (
                            concat($topRecordSetId, '-', @id))
                        "/>

                <xsl:variable name="unitType">
                    <xsl:choose>
                        <xsl:when test="@level = 'item'">record</xsl:when>
                        <xsl:otherwise>record-set</xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:variable name="unitClass">
                    <xsl:choose>
                        <xsl:when test="$unitType = 'record'">Record</xsl:when>
                        <xsl:otherwise>RecordSet</xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:variable name="outputFileUrl"
                    select="concat('rdf/', $unitType, 's/', $coll, '_', $unitType, '_', $myId, '.rdf')"/>
                <xsl:result-document method="xml" encoding="utf-8" indent="yes"
                    href="{$outputFileUrl}">

                    <rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                        xmlns:owl="http://www.w3.org/2002/07/owl#"
                        xmlns:RiC="http://www.ica.org/standards/RiC/ontology#"
                        xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
                        xmlns:piaaf-onto="http://piaaf.demo.logilab.fr/ontology#"
                        xmlns:skos="http://www.w3.org/2004/02/skos/core#">
                        <rdf:Description>
                            <xsl:variable name="uri"
                                select="concat('http://piaaf.demo.logilab.fr/resource/', $coll, '_', $unitType, '_', $myId)"/>
                            <xsl:attribute name="rdf:about">
                                <xsl:value-of select="$uri"/>
                            </xsl:attribute>
                            <!-- <rdf:type
                                rdf:resource="http://www.ica.org/standards/RiC/ontology#RecordSet"/>-->
                            <rdf:type>
                                <xsl:attribute name="rdf:resource">
                                    <xsl:value-of
                                        select="concat('http://www.ica.org/standards/RiC/ontology#', $unitClass)"
                                    />
                                </xsl:attribute>
                            </rdf:type>
                            <xsl:if test="@level[normalize-space(.) != 'item']">
                                <xsl:variable name="lev"
                                    select="
                                        if (@level != 'otherlevel')
                                        then
                                            @level
                                        else
                                            (@otherlevel)"/>
                                <xsl:if
                                    test="$recordSetTypes/owl:NamedIndividual[rdf:type/@rdf:resource = 'http://www.w3.org/2004/02/skos/core#Concept' and skos:prefLabel = $lev]">
                                    <RiC:recordSetType>
                                        <xsl:attribute name="rdf:resource">
                                            <xsl:value-of
                                                select="$recordSetTypes/owl:NamedIndividual[rdf:type/@rdf:resource = 'http://www.w3.org/2004/02/skos/core#Concept' and skos:prefLabel = $lev]/@rdf:about"
                                            />
                                        </xsl:attribute>
                                    </RiC:recordSetType>
                                </xsl:if>
                                <xsl:if
                                    test="not($recordSetTypes/owl:NamedIndividual[rdf:type/@rdf:resource = 'http://www.w3.org/2004/02/skos/core#Concept' and skos:prefLabel = $lev])">
                                    <RiC:recordSetType xml:lang="fr">

                                        <xsl:value-of select="$lev"/>

                                    </RiC:recordSetType>
                                </xsl:if>
                            </xsl:if>
                            <xsl:if test="normalize-space(did/unittitle) != ''">
                                <rdfs:label xml:lang="fr">
                                    <xsl:value-of select="did/unittitle"/>
                                </rdfs:label>
                                <RiC:hasTitle xml:lang="fr">
                                    <xsl:value-of select="did/unittitle"/>
                                </RiC:hasTitle>
                            </xsl:if>
                            <xsl:if test="normalize-space(did/unittitle) = ''">
                                <xsl:choose>
                                    <xsl:when test="did/unitid[normalize-space(.) != '']">
                                        <rdfs:label xml:lang="fr">
                                            <xsl:value-of
                                                select="normalize-space(did/unitid[normalize-space(.) != ''][1])"
                                            />
                                        </rdfs:label>
                                        <RiC:hasTitle xml:lang="fr">
                                            <xsl:value-of
                                                select="normalize-space(did/unitid[normalize-space(.) != ''][1])"
                                            />
                                        </RiC:hasTitle>
                                    </xsl:when>
                                    <xsl:when test="did/unitdate[normalize-space(.) != '']">
                                        <rdfs:label xml:lang="fr">
                                            <xsl:value-of
                                                select="normalize-space(did/unitdate[normalize-space(.) != ''][1])"
                                            />
                                        </rdfs:label>
                                        <RiC:hasTitle xml:lang="fr">
                                            <xsl:value-of
                                                select="normalize-space(did/unitdate[normalize-space(.) != ''][1])"
                                            />
                                        </RiC:hasTitle>
                                    </xsl:when>
                                    <xsl:otherwise/>
                                </xsl:choose>
                            </xsl:if>
                            <xsl:for-each select="did/unitid[normalize-space(.) != '']">
                                <RiC:identifiedBy>
                                    <xsl:value-of select="normalize-space(.)"/>
                                </RiC:identifiedBy>
                            </xsl:for-each>

                            <xsl:for-each
                                select="did/unitdate[normalize-space(.) != '' or normalize-space(@normal) != '']">
                                <xsl:choose>
                                    <xsl:when test="normalize-space(@normal) != ''">
                                        <xsl:choose>
                                            <xsl:when test="not(contains(@normal, '/'))">
                                                <RiC:date>
                                                  <xsl:call-template name="outputDate">
                                                  <xsl:with-param name="date"
                                                  select="normalize-space(.)"/>
                                                  <xsl:with-param name="stdDate"
                                                  select="normalize-space(@normal)"/>
                                                  </xsl:call-template>
                                                </RiC:date>
                                            </xsl:when>

                                            <xsl:when test="contains(@normal, '/')">
                                                <xsl:analyze-string
                                                  select="normalize-space(@normal)"
                                                  regex="^([0-9]{{4}})(-[01][1-9])(-[0-3][1-9])/([0-9]{{4}})(-[01][1-9])(-[0-3][1-9])$">


                                                  <xsl:matching-substring>
                                                  <RiC:beginningDate>
                                                  <xsl:attribute name="rdf:datatype">
                                                  <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                                  </xsl:attribute>
                                                  <xsl:value-of
                                                  select="concat(regex-group(1), regex-group(2), regex-group(3))"
                                                  />
                                                  </RiC:beginningDate>
                                                  <RiC:endDate>
                                                  <xsl:attribute name="rdf:datatype">
                                                  <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                                  </xsl:attribute>
                                                  <xsl:value-of
                                                  select="concat(regex-group(4), regex-group(5), regex-group(6))"
                                                  />
                                                  </RiC:endDate>

                                                  </xsl:matching-substring>
                                                  <xsl:non-matching-substring>
                                                  <xsl:analyze-string select="."
                                                  regex="^([0-9]{{4}})(-[01][1-9])/([0-9]{{4}})(-[01][1-9])$">
                                                  <xsl:matching-substring>
                                                  <RiC:beginningDate>
                                                  <xsl:attribute name="rdf:datatype">
                                                  <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                                  </xsl:attribute>
                                                  <xsl:value-of
                                                  select="concat(regex-group(1), regex-group(2))"/>
                                                  </RiC:beginningDate>
                                                  <RiC:endDate>
                                                  <xsl:attribute name="rdf:datatype">
                                                  <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                                  </xsl:attribute>
                                                  <xsl:value-of
                                                  select="concat(regex-group(3), regex-group(4))"/>
                                                  </RiC:endDate>
                                                  </xsl:matching-substring>
                                                  <xsl:non-matching-substring>
                                                  <xsl:analyze-string select="."
                                                  regex="^([0-9]{{4}})/([0-9]{{4}})$">
                                                  <xsl:matching-substring>
                                                  <RiC:beginningDate>
                                                  <xsl:attribute name="rdf:datatype">
                                                  <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text>
                                                  </xsl:attribute>
                                                  <xsl:value-of select="regex-group(1)"/>
                                                  </RiC:beginningDate>
                                                  <RiC:endDate>
                                                  <xsl:attribute name="rdf:datatype">
                                                  <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text>
                                                  </xsl:attribute>
                                                  <xsl:value-of select="regex-group(2)"/>
                                                  </RiC:endDate>
                                                  </xsl:matching-substring>
                                                  <xsl:non-matching-substring>
                                                  <RiC:date>
                                                  <xsl:value-of select="."/>
                                                  </RiC:date>

                                                  </xsl:non-matching-substring>
                                                  </xsl:analyze-string>
                                                  </xsl:non-matching-substring>
                                                  </xsl:analyze-string>
                                                  </xsl:non-matching-substring>
                                                </xsl:analyze-string>
                                            </xsl:when>



                                        </xsl:choose>


                                    </xsl:when>
                                    <xsl:otherwise>
                                        <RiC:date>
                                            <xsl:value-of select="normalize-space(.)"/>
                                        </RiC:date>

                                    </xsl:otherwise>
                                </xsl:choose>

                            </xsl:for-each>
                            <xsl:if
                                test="did/origination[normalize-space(.) != ''] or ancestor::*[did/origination[normalize-space(.) != '']]">
                                <xsl:choose>
                                    <xsl:when test="did/origination[normalize-space(.) != '']">
                                        <xsl:for-each
                                            select="did/origination[normalize-space(.) != '']">
                                            <xsl:variable name="theOrigEl" select="."/>

                                            <xsl:choose>
                                                <xsl:when test="$theOrigEl/*[@authfilenumber]">
                                                  <xsl:for-each
                                                  select="$theOrigEl/*[@authfilenumber]">
                                                  <xsl:variable name="origId"
                                                  select="substring-after(@authfilenumber, 'FRAN_NP_')"/>

                                                  <xsl:variable name="origType">
                                                  <xsl:choose>
                                                  <xsl:when test="self::persname">person</xsl:when>
                                                  <xsl:when test="self::corpname"
                                                  >corporate-body</xsl:when>
                                                  <xsl:when test="self::famname">family</xsl:when>
                                                  </xsl:choose>
                                                  </xsl:variable>
                                                  <!-- pour les AN on suppose que le produccteur est décrit dans le corpus -->
                                                  <xsl:variable name="origUri"
                                                      select="concat('http://piaaf.demo.logilab.fr/resource/FRAN_', $origType, '_', $origId)"/>
                                                  <xsl:choose>
                                                  <xsl:when
                                                  test="
                                                                    $an-prov-rels/rdf:Description[rdf:type/@rdf:resource = 'http://www.ica.org/standards/RiC/ontology#ArchivalProvenanceRelation' and
                                                                    
                                                                    RiC:provenanceEntity/@rdf:resource = $origUri
                                                                    ]">

                                                  <xsl:for-each
                                                  select="
                                                                        $an-prov-rels/rdf:Description[rdf:type/@rdf:resource = 'http://www.ica.org/standards/RiC/ontology#ArchivalProvenanceRelation' and
                                                                        
                                                                        RiC:provenanceEntity/@rdf:resource = $origUri
                                                                        ]">

                                                  <xsl:variable name="origObjectShortUri">
                                                  <xsl:value-of
                                                  select="
                                                                                if (
                                                                                ends-with(RiC:originatedObject/@rdf:resource, '-top')
                                                                                
                                                                                )
                                                                                
                                                                                then
                                                                                    (
                                                                                    
                                                                                    substring-before(RiC:originatedObject/@rdf:resource, '-top')
                                                                                    
                                                                                    
                                                                                    
                                                                                    )
                                                                                
                                                                                else
                                                                                    (
                                                                                    RiC:originatedObject/@rdf:resource
                                                                                    
                                                                                    
                                                                                    )
                                                                                
                                                                                
                                                                                
                                                                                "/>

                                                  </xsl:variable>
                                                  <xsl:comment>§origObjectShortUri=<xsl:value-of select="$origObjectShortUri"/></xsl:comment>

                                                  <xsl:choose>
                                                  <xsl:when test="$unitType = 'record-set'">
                                                  <xsl:if test="contains($uri, $origObjectShortUri)">
                                                  <!--<xsl:if test="$origObjectShortUri=$topRecordSetId">-->
                                                  <RiC:originatedBy>
                                                  <xsl:attribute name="rdf:resource">
                                                  <xsl:value-of
                                                  select="
                                                                                                @rdf:about
                                                                                                "
                                                  />
                                                  </xsl:attribute>
                                                  </RiC:originatedBy>




                                                  </xsl:if>
                                                  </xsl:when>
                                                  <xsl:when test="$unitType = 'record'">
                                                  <xsl:variable name="theObjectUri"
                                                  select="substring-after($origObjectShortUri, 'record-set')"/>
                                                  <xsl:if test="contains($uri, $theObjectUri)">
                                                  <!--<xsl:if test="$origObjectShortUri=$topRecordSetId">-->
                                                  <RiC:originatedBy>
                                                  <xsl:attribute name="rdf:resource">
                                                  <xsl:value-of
                                                  select="
                                                                                                @rdf:about
                                                                                                "
                                                  />
                                                  </xsl:attribute>
                                                  </RiC:originatedBy>




                                                  </xsl:if>
                                                  </xsl:when>
                                                  </xsl:choose>






                                                  </xsl:for-each>


                                                  </xsl:when>
                                                  <xsl:otherwise>
                                                  <xsl:comment><xsl:text>Attention, la cible du sous-élément </xsl:text>
                                                <xsl:value-of select="name()"/>
                                                    <xsl:text> (</xsl:text>
                                                    <xsl:value-of select="@authfilenumber"/>
                                                    <xsl:text>) de origination ne correspond à aucune relation de provenance préétablie</xsl:text>
                                                </xsl:comment>
                                                  <xsl:comment>
                                                    <xsl:text>$origId=</xsl:text>
                                                    <xsl:value-of select="$origId"/>
                                                    <xsl:text> ; $origType=</xsl:text>
                                                   <xsl:value-of select="$origType"/>
                                                      <xsl:text> ; $origUri=</xsl:text>
                                                   <xsl:value-of select="$origUri"/>
                                                      <xsl:text> ; $uri=</xsl:text>
                                                   <xsl:value-of select="$uri"/>
                                                </xsl:comment>
                                                  <!-- ajout de la relation directe faute de mieux -->
                                                  <RiC:createdBy rdf:resource="{$origUri}"
                                                  > </RiC:createdBy>
                                                  </xsl:otherwise>
                                                  </xsl:choose>




                                                  <!--<xsl:when test="$bnf-prov-rels/rdf:Description[rdf:type/@rdf:resource='http://www.ica.org/standards/RiC/ontology#RecordsProvenanceRelation' and 
                                                contains($uri, RiC:originatedObject/@rdf:resource)
                                                
                                                and 
                                                RiC:provenanceEntity/@rdf:resource=$origUri
                                                ]">  <RiC:originatedBy>
                                                    <xsl:attribute name="rdf:resource">
                                                        <xsl:value-of select="$bnf-prov-rels/rdf:Description[rdf:type/@rdf:resource='http://www.ica.org/standards/RiC/ontology#RecordsProvenanceRelation' and 
                                                            
                                                            contains($uri, RiC:originatedObject/@rdf:resource)
                                                            
                                                            and 
                                                            RiC:provenanceEntity/@rdf:resource=$origUri
                                                            ]/@rdf:about"/>
                                                    </xsl:attribute>
                                                </RiC:originatedBy>
                                            </xsl:when>-->





                                                  </xsl:for-each>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                  <RiC:noteOnProvenance xml:lang="fr">
                                                  <xsl:value-of select="$theOrigEl"/>
                                                  </RiC:noteOnProvenance>
                                                </xsl:otherwise>
                                            </xsl:choose>


                                        </xsl:for-each>

                                    </xsl:when>
                                    <xsl:when
                                        test="ancestor::*[did/origination[normalize-space(.) != '']]">
                                        <xsl:for-each
                                            select="ancestor::*[did/origination[normalize-space(.) != '']][1]/did/origination[normalize-space(.) != '']">
                                            <xsl:variable name="theOrigEl" select="."/>

                                            <xsl:choose>
                                                <xsl:when test="$theOrigEl/*[@authfilenumber]">
                                                  <xsl:for-each
                                                  select="$theOrigEl/*[@authfilenumber]">
                                                  <xsl:variable name="origId"
                                                  select="substring-after(@authfilenumber, 'FRAN_NP_')"/>

                                                  <xsl:variable name="origType">
                                                  <xsl:choose>
                                                  <xsl:when test="self::persname">person</xsl:when>
                                                  <xsl:when test="self::corpname"
                                                  >corporate-body</xsl:when>
                                                  <xsl:when test="self::famname">family</xsl:when>
                                                  </xsl:choose>
                                                  </xsl:variable>
                                                  <xsl:variable name="origUri"
                                                      select="concat('http://piaaf.demo.logilab.fr/resource/FRAN_', $origType, '_', $origId)"/>
                                                  <xsl:choose>
                                                  <xsl:when
                                                  test="
                                                                    $an-prov-rels/rdf:Description[rdf:type/@rdf:resource = 'http://www.ica.org/standards/RiC/ontology#ArchivalProvenanceRelation' and
                                                                    
                                                                    RiC:provenanceEntity/@rdf:resource = $origUri
                                                                    ]">

                                                  <xsl:for-each
                                                  select="
                                                                        $an-prov-rels/rdf:Description[rdf:type/@rdf:resource = 'http://www.ica.org/standards/RiC/ontology#ArchivalProvenanceRelation' and
                                                                        
                                                                        RiC:provenanceEntity/@rdf:resource = $origUri
                                                                        ]">

                                                  <xsl:variable name="origObjectShortUri">
                                                  <xsl:value-of
                                                  select="
                                                                                if (
                                                                                ends-with(RiC:originatedObject/@rdf:resource, '-top')
                                                                                
                                                                                )
                                                                                
                                                                                then
                                                                                    (
                                                                                    
                                                                                    substring-before(RiC:originatedObject/@rdf:resource, '-top')
                                                                                    
                                                                                    
                                                                                    
                                                                                    )
                                                                                
                                                                                else
                                                                                    (
                                                                                    RiC:originatedObject/@rdf:resource
                                                                                    
                                                                                    
                                                                                    )
                                                                                
                                                                                
                                                                                
                                                                                "/>

                                                  </xsl:variable>

                                                  <xsl:choose>
                                                  <xsl:when test="$unitType = 'record-set'">
                                                  <xsl:if test="contains($uri, $origObjectShortUri)">
                                                  <!--<xsl:if test="$origObjectShortUri=$topRecordSetId">-->
                                                  <RiC:originatedBy>
                                                  <xsl:attribute name="rdf:resource">
                                                  <xsl:value-of
                                                  select="
                                                                                                @rdf:about
                                                                                                "
                                                  />
                                                  </xsl:attribute>
                                                  </RiC:originatedBy>




                                                  </xsl:if>
                                                  </xsl:when>
                                                  <xsl:when test="$unitType = 'record'">
                                                  <xsl:variable name="theObjectUri"
                                                  select="substring-after($origObjectShortUri, 'record-set')"/>
                                                  <xsl:if test="contains($uri, $theObjectUri)">
                                                  <!--<xsl:if test="$origObjectShortUri=$topRecordSetId">-->
                                                  <RiC:originatedBy>
                                                  <xsl:attribute name="rdf:resource">
                                                  <xsl:value-of
                                                  select="
                                                                                                @rdf:about
                                                                                                "
                                                  />
                                                  </xsl:attribute>
                                                  </RiC:originatedBy>




                                                  </xsl:if>
                                                  </xsl:when>
                                                  </xsl:choose>

                                                  </xsl:for-each>


                                                  </xsl:when>
                                                  <xsl:otherwise>
                                                  <xsl:comment><xsl:text>Attention, la cible du sous-élément </xsl:text>
                                                <xsl:value-of select="name()"/>
                                                    <xsl:text> (</xsl:text>
                                                    <xsl:value-of select="@authfilenumber"/>
                                                    <xsl:text>) de origination ne correspond à aucune relation de provenance préétablie</xsl:text>
                                                </xsl:comment>
                                                  <xsl:comment>
                                                    <xsl:text>$origId=</xsl:text>
                                                    <xsl:value-of select="$origId"/>
                                                    <xsl:text> ; $origType=</xsl:text>
                                                   <xsl:value-of select="$origType"/>
                                                      <xsl:text> ; $origUri=</xsl:text>
                                                   <xsl:value-of select="$origUri"/>
                                                      <xsl:text> ; $uri=</xsl:text>
                                                   <xsl:value-of select="$uri"/>
                                                </xsl:comment>
                                                      <!-- ajout de la relation directe faute de mieux -->
                                                      <RiC:createdBy rdf:resource="{$origUri}"
                                                          > </RiC:createdBy>
                                                  </xsl:otherwise>
                                                  </xsl:choose>




                                                  <!--<xsl:when test="$bnf-prov-rels/rdf:Description[rdf:type/@rdf:resource='http://www.ica.org/standards/RiC/ontology#RecordsProvenanceRelation' and 
                                                contains($uri, RiC:originatedObject/@rdf:resource)
                                                
                                                and 
                                                RiC:provenanceEntity/@rdf:resource=$origUri
                                                ]">  <RiC:originatedBy>
                                                    <xsl:attribute name="rdf:resource">
                                                        <xsl:value-of select="$bnf-prov-rels/rdf:Description[rdf:type/@rdf:resource='http://www.ica.org/standards/RiC/ontology#RecordsProvenanceRelation' and 
                                                            
                                                            contains($uri, RiC:originatedObject/@rdf:resource)
                                                            
                                                            and 
                                                            RiC:provenanceEntity/@rdf:resource=$origUri
                                                            ]/@rdf:about"/>
                                                    </xsl:attribute>
                                                </RiC:originatedBy>
                                            </xsl:when>-->





                                                  </xsl:for-each>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                  <RiC:noteOnProvenance xml:lang="fr">
                                                  <xsl:value-of select="$theOrigEl"/>
                                                  </RiC:noteOnProvenance>
                                                </xsl:otherwise>
                                            </xsl:choose>


                                        </xsl:for-each>
                                    </xsl:when>
                                </xsl:choose>
                                <!--<xsl:variable name="theOrigEl"
                                    select="
                                        if (did/origination[normalize-space(.) != ''])
                                        then
                                            (did/origination[normalize-space(.) != ''])
                                        else
                                            (
                                            
                                            if (ancestor::*[did/origination[normalize-space(.) != '']])
                                            then
                                                (ancestor::*[did/origination[normalize-space(.) != ''][1]]/did/origination)
                                            else
                                                ()
                                            
                                            )
                                        
                                        "> </xsl:variable>-->


                            </xsl:if>

                            <xsl:if
                                test="did/repository[normalize-space(.) != ''] or ancestor::*[did/repository[normalize-space(.) != '']]">
                                <xsl:variable name="theReposEl"
                                    select="
                                        if (did/repository[normalize-space(.) != ''])
                                        then
                                            (did/repository[normalize-space(.) != ''])
                                        else
                                            (
                                            
                                            if (ancestor::*[did/repository[normalize-space(.) != '']])
                                            then
                                                (ancestor::*[did/repository[normalize-space(.) != ''][1]]/did/repository)
                                            else
                                                ()
                                            
                                            )
                                        
                                        "> </xsl:variable>

                                <xsl:choose>
                                    <xsl:when test="$theReposEl/*[@authfilenumber]">
                                        <xsl:for-each select="$theReposEl/*[@authfilenumber]">
                                            <xsl:variable name="reposId"
                                                select="substring-after(@authfilenumber, 'FRAN_NP_')"/>
                                            <RiC:heldBy>
                                                <xsl:attribute name="rdf:resource">
                                                  <xsl:value-of
                                                      select="concat('http://piaaf.demo.logilab.fr/resource/FRAN_corporate-body_', $reposId)"
                                                  />
                                                </xsl:attribute>
                                            </RiC:heldBy>
                                        </xsl:for-each>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <RiC:noteOnRepository xml:lang="fr">
                                            <xsl:value-of select="$theReposEl"/>
                                        </RiC:noteOnRepository>
                                    </xsl:otherwise>
                                </xsl:choose>

                            </xsl:if>
                            <xsl:if test="scopecontent[normalize-space(.) != '']">
                                <RiC:scopeAndContent xml:lang="fr">
                                    <xsl:for-each select="scopecontent/*">
                                        <xsl:apply-templates/>
                                        <xsl:if test="position() != last()">
                                            <xsl:text> </xsl:text>
                                        </xsl:if>
                                    </xsl:for-each>
                                </RiC:scopeAndContent>
                            </xsl:if>
                            <xsl:if test="did/physdesc/extent[normalize-space(.) != '']">

                                <xsl:for-each select="did/physdesc/extent[normalize-space(.) != '']">
                                    <RiC:physicalOrLogicalExtent xml:lang="fr">
                                        <xsl:apply-templates/>
                                        <xsl:if test="position() != last()">
                                            <xsl:text> </xsl:text>
                                        </xsl:if>
                                    </RiC:physicalOrLogicalExtent>
                                </xsl:for-each>

                            </xsl:if>
                            <xsl:if test="custodhist[normalize-space(.) != '']">
                                <RiC:history xml:lang="fr">
                                    <xsl:for-each select="custodhist/*">
                                        <xsl:apply-templates/>
                                        <xsl:if test="position() != last()">
                                            <xsl:text> </xsl:text>
                                        </xsl:if>
                                    </xsl:for-each>
                                </RiC:history>
                            </xsl:if>
                            <xsl:for-each select="acqinfo[normalize-space(.) != '']">
                                <RiC:noteOnAcquisition xml:lang="fr">
                                    <xsl:for-each select="*">
                                        <xsl:apply-templates/>
                                        <xsl:if test="position() != last()">
                                            <xsl:text> </xsl:text>
                                        </xsl:if>
                                    </xsl:for-each>
                                </RiC:noteOnAcquisition>
                            </xsl:for-each>
                            <xsl:if test="arrangement[normalize-space(.) != '']">
                                <RiC:arrangement xml:lang="fr">
                                    <xsl:for-each select="arrangement/*">
                                        <xsl:apply-templates/>
                                        <xsl:if test="position() != last()">
                                            <xsl:text> </xsl:text>
                                        </xsl:if>
                                    </xsl:for-each>
                                </RiC:arrangement>
                            </xsl:if>
                            <xsl:if test="processinfo[normalize-space(.) != '']">
                                <RiC:noteOnProcessApplied xml:lang="fr">
                                    <xsl:for-each select="processinfo/*">
                                        <xsl:apply-templates/>
                                        <xsl:if test="position() != last()">
                                            <xsl:text> </xsl:text>
                                        </xsl:if>
                                    </xsl:for-each>
                                </RiC:noteOnProcessApplied>
                            </xsl:if>
                            <xsl:if test="accruals[normalize-space(.) != '']">
                                <RiC:accruals xml:lang="fr">
                                    <xsl:for-each select="accruals/*">
                                        <xsl:apply-templates/>
                                        <xsl:if test="position() != last()">
                                            <xsl:text> </xsl:text>
                                        </xsl:if>
                                    </xsl:for-each>
                                </RiC:accruals>
                            </xsl:if>
                            <xsl:if test="accessrestrict[normalize-space(.) != '']">
                                <RiC:conditionsOfAccess xml:lang="fr">
                                    <xsl:for-each select="accessrestrict/*">
                                        <xsl:apply-templates/>
                                        <xsl:if test="position() != last()">
                                            <xsl:text> </xsl:text>
                                        </xsl:if>
                                    </xsl:for-each>
                                </RiC:conditionsOfAccess>
                            </xsl:if>
                            <xsl:for-each
                                select="controlaccess/genreform[normalize-space(.) != ''] | physdesc/genreform[normalize-space(.) != '']">
                                <RiC:contentType xml:lang="fr">
                                    <xsl:value-of select="normalize-space(.)"/>
                                </RiC:contentType>

                            </xsl:for-each>
                            <xsl:for-each select="controlaccess/subject[normalize-space(.) != '']">
                                <RiC:subject xml:lang="fr">
                                    <xsl:value-of select="normalize-space(.)"/>
                                </RiC:subject>

                            </xsl:for-each>
                            <xsl:for-each select="controlaccess/persname[normalize-space(.) != '']">
                                <RiC:subject>
                                    <xsl:variable name="targ" select="@authfilenumber"/>
                                    <xsl:choose>
                                        <xsl:when test="$targ">
                                            <xsl:variable name="targId"
                                                select="substring-after($targ, 'FRAN_NP_')"/>
                                            <xsl:choose>
                                                <xsl:when
                                                  test="$collection-agents/rdf:RDF/rdf:Description[rdf:type/@rdf:resource = 'http://www.ica.org/standards/RiC/ontology#Person' and substring-after(@rdf:about, 'person_') = $targId]">
                                                  <xsl:attribute name="rdf:resource">
                                                  <xsl:value-of
                                                  select="$collection-agents/rdf:RDF/rdf:Description[rdf:type/@rdf:resource = 'http://www.ica.org/standards/RiC/ontology#Person' and substring-after(@rdf:about, 'person_') = $targId]/@rdf:about"
                                                  />
                                                  </xsl:attribute>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                  <xsl:attribute name="xml:lang">fr</xsl:attribute>
                                                  <xsl:value-of select="."/>
                                                </xsl:otherwise>
                                            </xsl:choose>

                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:attribute name="xml:lang">fr</xsl:attribute>
                                            <xsl:value-of select="."/>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </RiC:subject>

                            </xsl:for-each>
                            <xsl:for-each select="controlaccess/corpname[normalize-space(.) != '']">
                                <RiC:subject>
                                    <xsl:variable name="targ" select="@authfilenumber"/>
                                    <xsl:choose>
                                        <xsl:when test="normalize-space($targ) != ''">
                                            <xsl:variable name="targId"
                                                select="substring-after($targ, 'FRAN_NP_')"/>
                                            <xsl:choose>
                                                <xsl:when
                                                  test="$collection-agents/rdf:RDF/rdf:Description[rdf:type/@rdf:resource = 'http://www.ica.org/standards/RiC/ontology#CorporateBody' and substring-after(@rdf:about, 'corporate-body_') = $targId]">
                                                  <xsl:attribute name="rdf:resource">
                                                  <xsl:value-of
                                                  select="$collection-agents/rdf:RDF/rdf:Description[rdf:type/@rdf:resource = 'http://www.ica.org/standards/RiC/ontology#CorporateBody' and substring-after(@rdf:about, 'corporate-body_') = $targId]/@rdf:about"
                                                  />
                                                  </xsl:attribute>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                  <xsl:attribute name="xml:lang">fr</xsl:attribute>
                                                  <xsl:value-of select="."/>
                                                </xsl:otherwise>
                                            </xsl:choose>

                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:attribute name="xml:lang">fr</xsl:attribute>
                                            <xsl:value-of select="."/>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </RiC:subject>

                            </xsl:for-each>
                            <xsl:for-each select="relatedmaterial[normalize-space(.) != '']">
                                <!-- <xsl:choose>
                                    <xsl:when test="extref[starts-with(@href, 'http://gallica.bnf.fr/ark:')]">
                                        <xsl:for-each select="extref[starts-with(@href, 'http://gallica.bnf.fr/ark:')]">
                                            <RiC:isRelatedWith>
                                                <xsl:attribute name="rdf:resource">
                                                    <xsl:value-of select="@href"/>
                                                </xsl:attribute>
                                            </RiC:isRelatedWith>
                                        </xsl:for-each>
                                      
                                        
                                    </xsl:when>
                                    <xsl:when test="extref[starts-with(@href, 'http://webapp.bnf.fr/')]">
                                        <xsl:for-each select="extref[starts-with(@href, 'http://webapp.bnf.fr/')]">
                                            <RiC:isRelatedWith>
                                                <xsl:attribute name="rdf:resource">
                                                    <xsl:value-of select="@href"/>
                                                </xsl:attribute>
                                            </RiC:isRelatedWith>
                                        </xsl:for-each>
                                    </xsl:when>
                                    <xsl:otherwise>-->
                                <RiC:noteOnRelatedMaterial xml:lang="fr">
                                    <xsl:for-each select="*">
                                        <xsl:apply-templates/>
                                        <xsl:if test="position != last()">
                                            <xsl:text> </xsl:text>
                                        </xsl:if>
                                    </xsl:for-each>

                                </RiC:noteOnRelatedMaterial>
                                <!--  </xsl:otherwise>
                                </xsl:choose>-->
                            </xsl:for-each>
                            <!-- <xsl:for-each select="dao[normalize-space(@href)!='']">
                                <RiC:digitalCopy>
                                    <xsl:attribute name="rdf:resource">
                                        <xsl:value-of select="@href"/>
                                    </xsl:attribute>
                                </RiC:digitalCopy>
                                
                            </xsl:for-each>-->
                            <!-- liens reliant la ressource à celles qui en sont "membres"-->
                            <!-- pour l'instant on crée des relations directes, mais en théorie il faudrait des relations n-aires-->
                            <xsl:if
                                test="self::archdesc[dsc/c] | self::c[count(ancestor::c) &lt; 2 and c]">
                                <xsl:for-each
                                    select="child::dsc/c | self::c[count(ancestor::c) &lt; 2 and c]/c">
                                    <xsl:variable name="childId" select="@id"/>
                                    <xsl:variable name="theChildUnitId"
                                        select="concat($topRecordSetId, '-', $childId)"/>
                                    <xsl:variable name="theChildType">
                                        <xsl:choose>
                                            <xsl:when test="@level = 'item'">record</xsl:when>
                                            <xsl:otherwise>record-set</xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:variable>
                                    <!--<xsl:variable name="theChildClass">
                                        <xsl:choose>
                                            <xsl:when test="$theChildType='record'">Record</xsl:when>
                                            <xsl:otherwise>RecordSet</xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:variable>-->
                                    <RiC:member>
                                        <xsl:attribute name="rdf:resource">
                                            <xsl:value-of
                                                select="concat('http://piaaf.demo.logilab.fr/resource/', $coll, '_', $theChildType, '_', $theChildUnitId)"/>

                                        </xsl:attribute>
                                    </RiC:member>
                                </xsl:for-each>
                            </xsl:if>
                            
                            <xsl:if test="self::c">
                                
                                <xsl:variable name="parentId">
                                    <xsl:choose>
                                        <xsl:when test="parent::c">
                                            <xsl:value-of select="concat($topRecordSetId, '-', parent::c/@id)"/>
                                        </xsl:when>
                                        <xsl:when test="parent::dsc">
                                            <xsl:value-of select="concat($topRecordSetId, '-top')"/>
                                        </xsl:when>
                                    </xsl:choose>
                                </xsl:variable>
                                <!--<xsl:variable name="theChildUnitId"
                                        select="concat($topRecordSetId, '-', $childId)"/>-->
                                <xsl:variable name="theParentType">
                                    <xsl:choose>
                                        <xsl:when test="parent::dsc">record-set</xsl:when>
                                        <xsl:otherwise>record-set</xsl:otherwise>
                                    </xsl:choose>
                                </xsl:variable>
                                <!--<xsl:variable name="theChildClass">
                                        <xsl:choose>
                                            <xsl:when test="$theChildType='record'">Record</xsl:when>
                                            <xsl:otherwise>RecordSet</xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:variable>-->
                                <RiC:memberOf>
                                    <xsl:attribute name="rdf:resource">
                                        <xsl:value-of
                                            select="concat('http://piaaf.demo.logilab.fr/resource/', $coll, '_', $theParentType, '_', $parentId)"/>
                                        
                                    </xsl:attribute>
                                </RiC:memberOf>
                                
                                
                            </xsl:if>         
                            
                            <!-- permalien de la page correspondante dans la SIV -->
                            <!-- permalien https://www.siv.archives-nationales.culture.gouv.fr/siv/IR/FRAN_IR_009659 -->
                            <!-- https://www.siv.archives-nationales.culture.gouv.fr/siv/UD/FRAN_IR_009659/d_1-->
                            <xsl:if test="not(contains($IR_id, 'protoLD'))">
                                <rdfs:seeAlso>
                                    <xsl:attribute name="rdf:resource">
                                        <!--<xsl:value-of select="concat('http://archivesetmanuscrits.bnf.fr/ark:/12148/', )"/>-->
                                        <xsl:text>https://www.siv.archives-nationales.culture.gouv.fr/siv/</xsl:text>
                                        <xsl:value-of
                                            select="
                                                if (self::archdesc)
                                                then
                                                    (concat('IR/FRAN_IR_', $topRecordSetId))
                                                else
                                                    (
                                                    concat('UD/FRAN_IR_', $topRecordSetId, '/', @id))
                                                "/>

                                    </xsl:attribute>
                                </rdfs:seeAlso>
                                <RiC:mainSubjectOf>
                                    <xsl:attribute name="rdf:resource">
                                        <!--<xsl:value-of select="concat('http://archivesetmanuscrits.bnf.fr/ark:/12148/', )"/>-->
                                        <xsl:text>https://www.siv.archives-nationales.culture.gouv.fr/siv/</xsl:text>
                                        <xsl:value-of
                                            select="
                                                if (self::archdesc)
                                                then
                                                    (concat('IR/FRAN_IR_', $topRecordSetId))
                                                else
                                                    (
                                                    concat('UD/FRAN_IR_', $topRecordSetId, '/', @id))
                                                "/>

                                    </xsl:attribute>
                                </RiC:mainSubjectOf>
                            </xsl:if>
                            <xsl:if test="self::archdesc">
                                <!--  <xsl:variable name="IR_id">
                    <xsl:choose>
                        <xsl:when test="ancestor-or-self::archdesc/otherfindaid/p[extref]/extref/@href">
                            <xsl:value-of select="substring-after(ancestor-or-self::archdesc/otherfindaid/p[extref]/extref/@href, 'FRAN_')"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="substring-after(ancestor::ead/eadheader/eadid, 'FRAN_')"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>-->
                                <RiC:describedBy>
                                    <xsl:attribute name="rdf:resource">
                                        <xsl:value-of
                                            select="
                                                
                                                
                                                concat('http://piaaf.demo.logilab.fr/resource/FRAN_finding-aid_', replace($IR_id, '_', ''))
                                                
                                                "
                                        />
                                    </xsl:attribute>
                                </RiC:describedBy>
                            </xsl:if>

                        </rdf:Description>

                    </rdf:RDF>
                </xsl:result-document>

            </xsl:for-each>
        </xsl:if>
        <xsl:if test="$coll = 'FRSIAF'">

            <xsl:for-each
                select="$collection-IR-SIAF/ead/archdesc | $collection-IR-SIAF/ead/archdesc/dsc/descendant::c[normalize-space(did) != '']">
                <!-- o retient ici tout l'élément eadid sinon on ne saura pas dans l'URI à quel service d'AD on se réfère -->
                <xsl:variable name="IR_id">

                    <xsl:value-of select="normalize-space(ancestor::ead/eadheader/eadid)"/>

                </xsl:variable>
                <xsl:variable name="topRecordSetId">

                    <xsl:value-of select="replace($IR_id, '_', '')"/>


                </xsl:variable>
                <xsl:variable name="myId"
                    select="
                        if (self::archdesc)
                        then
                            (concat($topRecordSetId, '-top'))
                        else
                            (
                            concat($topRecordSetId, '-', @id))
                        "/>

                <xsl:variable name="unitType">
                    <xsl:choose>
                        <xsl:when test="@level = 'item'">record</xsl:when>
                        <xsl:otherwise>record-set</xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:variable name="unitClass">
                    <xsl:choose>
                        <xsl:when test="$unitType = 'record'">Record</xsl:when>
                        <xsl:otherwise>RecordSet</xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:variable name="outputFileUrl"
                    select="concat('rdf/', $unitType, 's/', $coll, '_', $unitType, '_', $myId, '.rdf')"/>
                <xsl:result-document method="xml" encoding="utf-8" indent="yes"
                    href="{$outputFileUrl}">

                    <rdf:RDF 
                        xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                        xmlns:owl="http://www.w3.org/2002/07/owl#"
                        xmlns:RiC="http://www.ica.org/standards/RiC/ontology#"
                        xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
                        xmlns:piaaf-onto="http://piaaf.demo.logilab.fr/ontology#"
                        xmlns:skos="http://www.w3.org/2004/02/skos/core#">
                        <rdf:Description>
                            <xsl:variable name="uri"
                                select="concat('http://piaaf.demo.logilab.fr/resource/', $coll, '_', $unitType, '_', $myId)"/>
                            <xsl:attribute name="rdf:about">
                                <xsl:value-of select="$uri"/>
                            </xsl:attribute>

                            <rdf:type>
                                <xsl:attribute name="rdf:resource">
                                    <xsl:value-of
                                        select="concat('http://www.ica.org/standards/RiC/ontology#', $unitClass)"
                                    />
                                </xsl:attribute>
                            </rdf:type>
                            <xsl:if test="@level[normalize-space(.) != 'item']">
                                <xsl:variable name="lev"
                                    select="
                                        if (@level != 'otherlevel')
                                        then
                                            @level
                                        else
                                            (@otherlevel)"/>
                                <xsl:if
                                    test="$recordSetTypes/owl:NamedIndividual[rdf:type/@rdf:resource = 'http://www.w3.org/2004/02/skos/core#Concept' and skos:prefLabel = $lev]">
                                    <RiC:recordSetType>
                                        <xsl:attribute name="rdf:resource">
                                            <xsl:value-of
                                                select="$recordSetTypes/owl:NamedIndividual[rdf:type/@rdf:resource = 'http://www.w3.org/2004/02/skos/core#Concept' and skos:prefLabel = $lev]/@rdf:about"
                                            />
                                        </xsl:attribute>
                                    </RiC:recordSetType>
                                </xsl:if>
                                <xsl:if
                                    test="not($recordSetTypes/owl:NamedIndividual[rdf:type/@rdf:resource = 'http://www.w3.org/2004/02/skos/core#Concept' and skos:prefLabel = $lev])">
                                    <RiC:recordSetType xml:lang="fr">
                                        <xsl:value-of select="$lev"/>
                                    </RiC:recordSetType>
                                </xsl:if>
                            </xsl:if>
                            <xsl:if test="normalize-space(did/unittitle) != ''">
                                <rdfs:label xml:lang="fr">
                                    <xsl:value-of select="did/unittitle"/>
                                </rdfs:label>
                                <RiC:hasTitle xml:lang="fr">
                                    <xsl:value-of select="did/unittitle"/>
                                </RiC:hasTitle>
                            </xsl:if>
                            <xsl:if test="normalize-space(did/unittitle) = ''">
                                <xsl:choose>
                                    <xsl:when test="did/unitid[normalize-space(.) != '']">
                                        <rdfs:label xml:lang="fr">
                                            <xsl:value-of
                                                select="normalize-space(did/unitid[normalize-space(.) != ''][1])"
                                            />
                                        </rdfs:label>
                                        <RiC:hasTitle xml:lang="fr">
                                            <xsl:value-of
                                                select="normalize-space(did/unitid[normalize-space(.) != ''][1])"
                                            />
                                        </RiC:hasTitle>
                                    </xsl:when>
                                    <xsl:when test="did/unitdate[normalize-space(.) != '']">
                                        <rdfs:label xml:lang="fr">
                                            <xsl:value-of
                                                select="normalize-space(did/unitdate[normalize-space(.) != ''][1])"
                                            />
                                        </rdfs:label>
                                        <RiC:hasTitle xml:lang="fr">
                                            <xsl:value-of
                                                select="normalize-space(did/unitdate[normalize-space(.) != ''][1])"
                                            />
                                        </RiC:hasTitle>
                                    </xsl:when>
                                    <xsl:otherwise/>
                                </xsl:choose>
                            </xsl:if>
                            <xsl:for-each select="did/unitid[normalize-space(.) != '']">
                                <RiC:identifiedBy>
                                    <xsl:value-of select="normalize-space(.)"/>
                                </RiC:identifiedBy>
                            </xsl:for-each>

                            <xsl:for-each
                                select="did/unitdate[normalize-space(.) != '' or normalize-space(@normal) != '']">
                                <xsl:choose>
                                    <xsl:when test="normalize-space(@normal) != ''">
                                        <xsl:choose>
                                            <xsl:when test="not(contains(@normal, '/'))">
                                                <RiC:date>
                                                  <xsl:call-template name="outputDate">
                                                  <xsl:with-param name="date"
                                                  select="normalize-space(.)"/>
                                                  <xsl:with-param name="stdDate"
                                                  select="normalize-space(@normal)"/>
                                                  </xsl:call-template>
                                                </RiC:date>
                                            </xsl:when>

                                            <xsl:when test="contains(@normal, '/')">
                                                <xsl:analyze-string
                                                  select="normalize-space(@normal)"
                                                  regex="^([0-9]{{4}})(-[01][1-9])(-[0-3][1-9])/([0-9]{{4}})(-[01][1-9])(-[0-3][1-9])$">


                                                  <xsl:matching-substring>
                                                  <RiC:beginningDate>
                                                  <xsl:attribute name="rdf:datatype">
                                                  <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                                  </xsl:attribute>
                                                  <xsl:value-of
                                                  select="concat(regex-group(1), regex-group(2), regex-group(3))"
                                                  />
                                                  </RiC:beginningDate>
                                                  <RiC:endDate>
                                                  <xsl:attribute name="rdf:datatype">
                                                  <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                                  </xsl:attribute>
                                                  <xsl:value-of
                                                  select="concat(regex-group(4), regex-group(5), regex-group(6))"
                                                  />
                                                  </RiC:endDate>

                                                  </xsl:matching-substring>
                                                  <xsl:non-matching-substring>
                                                  <xsl:analyze-string select="."
                                                  regex="^([0-9]{{4}})(-[01][1-9])/([0-9]{{4}})(-[01][1-9])$">
                                                  <xsl:matching-substring>
                                                  <RiC:beginningDate>
                                                  <xsl:attribute name="rdf:datatype">
                                                  <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                                  </xsl:attribute>
                                                  <xsl:value-of
                                                  select="concat(regex-group(1), regex-group(2))"/>
                                                  </RiC:beginningDate>
                                                  <RiC:endDate>
                                                  <xsl:attribute name="rdf:datatype">
                                                  <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                                  </xsl:attribute>
                                                  <xsl:value-of
                                                  select="concat(regex-group(3), regex-group(4))"/>
                                                  </RiC:endDate>
                                                  </xsl:matching-substring>
                                                  <xsl:non-matching-substring>
                                                  <xsl:analyze-string select="."
                                                  regex="^([0-9]{{4}})/([0-9]{{4}})$">
                                                  <xsl:matching-substring>
                                                  <RiC:beginningDate>
                                                  <xsl:attribute name="rdf:datatype">
                                                  <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text>
                                                  </xsl:attribute>
                                                  <xsl:value-of select="regex-group(1)"/>
                                                  </RiC:beginningDate>
                                                  <RiC:endDate>
                                                  <xsl:attribute name="rdf:datatype">
                                                  <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text>
                                                  </xsl:attribute>
                                                  <xsl:value-of select="regex-group(2)"/>
                                                  </RiC:endDate>
                                                  </xsl:matching-substring>
                                                  <xsl:non-matching-substring>
                                                  <RiC:date>
                                                  <xsl:value-of select="."/>
                                                  </RiC:date>

                                                  </xsl:non-matching-substring>
                                                  </xsl:analyze-string>
                                                  </xsl:non-matching-substring>
                                                  </xsl:analyze-string>
                                                  </xsl:non-matching-substring>
                                                </xsl:analyze-string>
                                            </xsl:when>



                                        </xsl:choose>


                                    </xsl:when>
                                    <xsl:otherwise>
                                        <RiC:date>
                                            <xsl:value-of select="normalize-space(.)"/>
                                        </RiC:date>

                                    </xsl:otherwise>
                                </xsl:choose>

                            </xsl:for-each>
                            <xsl:if
                                test="did/origination[normalize-space(.) != ''] or ancestor::*[did/origination[normalize-space(.) != '']]">
                                <xsl:choose>
                                    <xsl:when test="did/origination[normalize-space(.) != '']">
                                        <xsl:for-each
                                            select="did/origination[normalize-space(.) != '']">
                                            <xsl:variable name="theOrigEl" select="."/>

                                            <xsl:choose>
                                                <xsl:when test="$theOrigEl/*[@authfilenumber]">
                                                  <xsl:for-each
                                                  select="$theOrigEl/*[@authfilenumber]">
                                                  <xsl:variable name="origId"
                                                  select="normalize-space(@authfilenumber)"/>

                                                  <xsl:variable name="origType">
                                                  <xsl:choose>
                                                  <xsl:when test="self::persname">person</xsl:when>
                                                  <xsl:when test="self::corpname"
                                                  >corporate-body</xsl:when>
                                                  <xsl:when test="self::famname">family</xsl:when>
                                                  </xsl:choose>
                                                  </xsl:variable>

                                                  <xsl:variable name="origUri"
                                                      select="concat('http://piaaf.demo.logilab.fr/resource/FRSIAF_', $origType, '_', $origId)"/>
                                                  <xsl:choose>
                                                  <xsl:when
                                                  test="
                                                                    $siaf-prov-rels/rdf:Description[rdf:type/@rdf:resource = 'http://www.ica.org/standards/RiC/ontology#ArchivalProvenanceRelation' and
                                                                    
                                                                    RiC:provenanceEntity/@rdf:resource = $origUri
                                                                    ]">

                                                  <xsl:for-each
                                                  select="
                                                                        $siaf-prov-rels/rdf:Description[rdf:type/@rdf:resource = 'http://www.ica.org/standards/RiC/ontology#ArchivalProvenanceRelation' and
                                                                        
                                                                        RiC:provenanceEntity/@rdf:resource = $origUri
                                                                        ]">

                                                  <xsl:variable name="origObjectShortUri">
                                                  <xsl:value-of
                                                  select="
                                                                                if (
                                                                                ends-with(RiC:originatedObject/@rdf:resource, '-top')
                                                                                
                                                                                )
                                                                                
                                                                                then
                                                                                    (
                                                                                    
                                                                                    substring-before(RiC:originatedObject/@rdf:resource, '-top')
                                                                                    
                                                                                    
                                                                                    
                                                                                    )
                                                                                
                                                                                else
                                                                                    (
                                                                                    RiC:originatedObject/@rdf:resource
                                                                                    
                                                                                    
                                                                                    )
                                                                                
                                                                                
                                                                                
                                                                                "/>

                                                  </xsl:variable>
                                                  <xsl:comment>§origObjectShortUri=<xsl:value-of select="$origObjectShortUri"/></xsl:comment>

                                                  <xsl:choose>
                                                  <xsl:when test="$unitType = 'record-set'">
                                                  <xsl:if test="contains($uri, $origObjectShortUri)">
                                                  <!--<xsl:if test="$origObjectShortUri=$topRecordSetId">-->
                                                  <RiC:originatedBy>
                                                  <xsl:attribute name="rdf:resource">
                                                  <xsl:value-of
                                                  select="
                                                                                                @rdf:about
                                                                                                "
                                                  />
                                                  </xsl:attribute>
                                                  </RiC:originatedBy>




                                                  </xsl:if>
                                                  </xsl:when>
                                                  <xsl:when test="$unitType = 'record'">
                                                  <xsl:variable name="theObjectUri"
                                                  select="substring-after($origObjectShortUri, 'record-set')"/>
                                                  <xsl:if test="contains($uri, $theObjectUri)">
                                                  <!--<xsl:if test="$origObjectShortUri=$topRecordSetId">-->
                                                  <RiC:originatedBy>
                                                  <xsl:attribute name="rdf:resource">
                                                  <xsl:value-of
                                                  select="
                                                                                                @rdf:about
                                                                                                "
                                                  />
                                                  </xsl:attribute>
                                                  </RiC:originatedBy>




                                                  </xsl:if>
                                                  </xsl:when>
                                                  </xsl:choose>






                                                  </xsl:for-each>


                                                  </xsl:when>
                                                  <xsl:otherwise>
                                                  <xsl:comment><xsl:text>Attention, la cible du sous-élément </xsl:text>
                                                <xsl:value-of select="name()"/>
                                                    <xsl:text> (</xsl:text>
                                                    <xsl:value-of select="@authfilenumber"/>
                                                    <xsl:text>) de origination ne correspond à aucune relation de provenance préétablie</xsl:text>
                                                </xsl:comment>
                                                  <xsl:comment>
                                                    <xsl:text>$origId=</xsl:text>
                                                    <xsl:value-of select="$origId"/>
                                                    <xsl:text> ; $origType=</xsl:text>
                                                   <xsl:value-of select="$origType"/>
                                                      <xsl:text> ; $origUri=</xsl:text>
                                                   <xsl:value-of select="$origUri"/>
                                                      <xsl:text> ; $uri=</xsl:text>
                                                   <xsl:value-of select="$uri"/>
                                                </xsl:comment>
                                                  <!-- ajout de la relation directe faute de mieux -->
                                                  <RiC:createdBy rdf:resource="{$origUri}"
                                                  > </RiC:createdBy>
                                                      
                                                  <!--    <!-\- génération d'une relation de provenance dans le fichier RDF du record set -\->
                                                      
                                                      <RiC:originatedBy>
                                                          <xsl:attribute name="rdf:resource">
                                                              <xsl:value-of select="concat('http://piaaf.demo.logilab.fr/resource/FRSIAF_ArchivalProvenanceRelation_', $origId, '-rel', generate-id())"/>
                                                          </xsl:attribute>
                                                      </RiC:originatedBy>
                                                      -->
                                                      
                                                  </xsl:otherwise>
                                                  </xsl:choose>




                                                  <!--<xsl:when test="$bnf-prov-rels/rdf:Description[rdf:type/@rdf:resource='http://www.ica.org/standards/RiC/ontology#RecordsProvenanceRelation' and 
                                                contains($uri, RiC:originatedObject/@rdf:resource)
                                                
                                                and 
                                                RiC:provenanceEntity/@rdf:resource=$origUri
                                                ]">  <RiC:originatedBy>
                                                    <xsl:attribute name="rdf:resource">
                                                        <xsl:value-of select="$bnf-prov-rels/rdf:Description[rdf:type/@rdf:resource='http://www.ica.org/standards/RiC/ontology#RecordsProvenanceRelation' and 
                                                            
                                                            contains($uri, RiC:originatedObject/@rdf:resource)
                                                            
                                                            and 
                                                            RiC:provenanceEntity/@rdf:resource=$origUri
                                                            ]/@rdf:about"/>
                                                    </xsl:attribute>
                                                </RiC:originatedBy>
                                            </xsl:when>-->





                                                  </xsl:for-each>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                  <!--  <RiC:noteOnProvenance xml:lang="fr">
                                                       <xsl:value-of select="$theOrigEl"/>
                                                   </RiC:noteOnProvenance>-->
                                                  <RiC:createdBy xml:lang="fr">
                                                  <xsl:value-of select="$theOrigEl"/>
                                                  </RiC:createdBy>
                                                    <!--<RiC:originatedBy>
                                                        <xsl:attribute name="rdf:resource">
                                                            <xsl:value-of select="concat('http://piaaf.demo.logilab.fr/resource/FRSIAF_ArchivalProvenanceRelation_', $origId, '-rel', generate-id())"/>
                                                        </xsl:attribute>
                                                    </RiC:originatedBy>-->
                                                </xsl:otherwise>
                                            </xsl:choose>


                                        </xsl:for-each>

                                    </xsl:when>
                                    <xsl:when
                                        test="ancestor::*[did/origination[normalize-space(.) != '']]">
                                        <xsl:for-each
                                            select="ancestor::*[did/origination[normalize-space(.) != '']][1]/did/origination[normalize-space(.) != '']">
                                            <xsl:variable name="theOrigEl" select="."/>

                                            <xsl:choose>
                                                <xsl:when test="$theOrigEl/*[@authfilenumber]">
                                                  <xsl:for-each
                                                  select="$theOrigEl/*[@authfilenumber]">
                                                  <xsl:variable name="origId"
                                                  select="normalize-space(@authfilenumber)"/>

                                                  <xsl:variable name="origType">
                                                  <xsl:choose>
                                                  <xsl:when test="self::persname">person</xsl:when>
                                                  <xsl:when test="self::corpname"
                                                  >corporate-body</xsl:when>
                                                  <xsl:when test="self::famname">family</xsl:when>
                                                  </xsl:choose>
                                                  </xsl:variable>
                                                  <xsl:variable name="origUri"
                                                      select="concat('http://piaaf.demo.logilab.fr/resource/FRSIAF_', $origType, '_', $origId)"/>
                                                  <xsl:choose>
                                                  <xsl:when
                                                  test="
                                                                    $siaf-prov-rels/rdf:Description[rdf:type/@rdf:resource = 'http://www.ica.org/standards/RiC/ontology#ArchivalProvenanceRelation' and
                                                                    
                                                                    RiC:provenanceEntity/@rdf:resource = $origUri
                                                                    ]">

                                                  <xsl:for-each
                                                  select="
                                                                        $siaf-prov-rels/rdf:Description[rdf:type/@rdf:resource = 'http://www.ica.org/standards/RiC/ontology#ArchivalProvenanceRelation' and
                                                                        
                                                                        RiC:provenanceEntity/@rdf:resource = $origUri
                                                                        ]">

                                                  <xsl:variable name="origObjectShortUri">
                                                  <xsl:value-of
                                                  select="
                                                                                if (
                                                                                ends-with(RiC:originatedObject/@rdf:resource, '-top')
                                                                                
                                                                                )
                                                                                
                                                                                then
                                                                                    (
                                                                                    
                                                                                    substring-before(RiC:originatedObject/@rdf:resource, '-top')
                                                                                    
                                                                                    
                                                                                    
                                                                                    )
                                                                                
                                                                                else
                                                                                    (
                                                                                    RiC:originatedObject/@rdf:resource
                                                                                    
                                                                                    
                                                                                    )
                                                                                
                                                                                
                                                                                
                                                                                "/>

                                                  </xsl:variable>

                                                  <xsl:choose>
                                                  <xsl:when test="$unitType = 'record-set'">
                                                  <xsl:if test="contains($uri, $origObjectShortUri)">
                                                  <!--<xsl:if test="$origObjectShortUri=$topRecordSetId">-->
                                                  <RiC:originatedBy>
                                                  <xsl:attribute name="rdf:resource">
                                                  <xsl:value-of
                                                  select="
                                                                                                @rdf:about
                                                                                                "
                                                  />
                                                  </xsl:attribute>
                                                  </RiC:originatedBy>




                                                  </xsl:if>
                                                  </xsl:when>
                                                  <xsl:when test="$unitType = 'record'">
                                                  <xsl:variable name="theObjectUri"
                                                  select="substring-after($origObjectShortUri, 'record-set')"/>
                                                  <xsl:if test="contains($uri, $theObjectUri)">
                                                  <!--<xsl:if test="$origObjectShortUri=$topRecordSetId">-->
                                                  <RiC:originatedBy>
                                                  <xsl:attribute name="rdf:resource">
                                                  <xsl:value-of
                                                  select="
                                                                                                @rdf:about
                                                                                                "
                                                  />
                                                  </xsl:attribute>
                                                  </RiC:originatedBy>




                                                  </xsl:if>
                                                  </xsl:when>
                                                  </xsl:choose>

                                                  </xsl:for-each>


                                                  </xsl:when>
                                                  <xsl:otherwise>
                                                  <xsl:comment><xsl:text>Attention, la cible du sous-élément </xsl:text>
                                                <xsl:value-of select="name()"/>
                                                    <xsl:text> (</xsl:text>
                                                    <xsl:value-of select="@authfilenumber"/>
                                                    <xsl:text>) de origination ne correspond à aucune relation de provenance préétablie</xsl:text>
                                                </xsl:comment>
                                                  <xsl:comment>
                                                    <xsl:text>$origId=</xsl:text>
                                                    <xsl:value-of select="$origId"/>
                                                    <xsl:text> ; $origType=</xsl:text>
                                                   <xsl:value-of select="$origType"/>
                                                      <xsl:text> ; $origUri=</xsl:text>
                                                   <xsl:value-of select="$origUri"/>
                                                      <xsl:text> ; $uri=</xsl:text>
                                                   <xsl:value-of select="$uri"/>
                                                </xsl:comment>
                                                  <!-- ajout de la relation directe faute de temps pour faire mieux -->
                                                  <RiC:createdBy rdf:resource="{$origUri}"
                                                  > </RiC:createdBy>
                                                     <!-- <RiC:originatedBy>
                                                          <xsl:attribute name="rdf:resource">
                                                              <xsl:value-of select="concat('http://piaaf.demo.logilab.fr/resource/FRSIAF_ArchivalProvenanceRelation_', $origId, '-rel', generate-id())"/>
                                                          </xsl:attribute>
                                                      </RiC:originatedBy>-->
                                                  </xsl:otherwise>
                                                  </xsl:choose>







                                                  </xsl:for-each>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                  <!--  <RiC:noteOnProvenance xml:lang="fr">
                                                       <xsl:value-of select="$theOrigEl"/>
                                                   </RiC:noteOnProvenance>-->
                                                  <RiC:createdBy xml:lang="fr">
                                                  <xsl:value-of select="$theOrigEl"/>
                                                  </RiC:createdBy>
                                                </xsl:otherwise>
                                            </xsl:choose>


                                        </xsl:for-each>
                                    </xsl:when>
                                </xsl:choose>


                            </xsl:if>

                            <xsl:if
                                test="did/repository[normalize-space(.) != ''] or ancestor::*[did/repository[normalize-space(.) != '']]">
                                <xsl:variable name="theReposEl"
                                    select="
                                        if (did/repository[normalize-space(.) != ''])
                                        then
                                            (did/repository[normalize-space(.) != ''])
                                        else
                                            (
                                            
                                            if (ancestor::*[did/repository[normalize-space(.) != '']])
                                            then
                                                (ancestor::*[did/repository[normalize-space(.) != ''][1]]/did/repository)
                                            else
                                                ()
                                            
                                            )
                                        
                                        "> </xsl:variable>

                                <xsl:choose>
                                    <xsl:when test="$theReposEl/*[@authfilenumber]">
                                        <xsl:for-each select="$theReposEl/*[@authfilenumber]">
                                            <xsl:variable name="reposId"
                                                select="normalize-space(@authfilenumber)"/>
                                            <RiC:heldBy>
                                                <xsl:attribute name="rdf:resource">
                                                  <xsl:value-of
                                                      select="concat('http://piaaf.demo.logilab.fr/resource/FRSIAF_corporate-body_', $reposId)"
                                                  />
                                                </xsl:attribute>
                                            </RiC:heldBy>
                                        </xsl:for-each>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <RiC:noteOnRepository xml:lang="fr">
                                            <xsl:value-of select="$theReposEl"/>
                                        </RiC:noteOnRepository>
                                    </xsl:otherwise>
                                </xsl:choose>

                            </xsl:if>
                            <xsl:if test="scopecontent[normalize-space(.) != '']">
                                <RiC:scopeAndContent xml:lang="fr">
                                    <xsl:for-each select="scopecontent/p">
                                        <xsl:apply-templates/>
                                        <xsl:if test="position() != last()">
                                            <xsl:text> </xsl:text>
                                        </xsl:if>
                                    </xsl:for-each>
                                </RiC:scopeAndContent>
                            </xsl:if>
                            <!-- <physdesc label="Description physique">
                            <extent type="nombre elements" label="Nombre d'éléments">3
                                liasses</extent>
                            <lb/>
                            <extent label="Métrage linéaire" unit="ml">0,30</extent>
                            <lb/>
                        </physdesc>-->
                            <xsl:if test="did/physdesc/extent[normalize-space(.) != '']">

                                <xsl:for-each
                                    select="did/physdesc/extent[@unit = 'ml' and normalize-space(.) != '']">
                                    <RiC:physicalOrLogicalExtent xml:lang="fr">
                                        <xsl:apply-templates/>
                                        <!-- <xsl:if test="position()!=last()">
                                            <xsl:text> </xsl:text>
                                        </xsl:if>-->
                                        <xsl:text> ml</xsl:text>
                                    </RiC:physicalOrLogicalExtent>
                                </xsl:for-each>
                                <xsl:for-each
                                    select="did/physdesc/extent[@type = 'nombre elements' and normalize-space(.) != '']">
                                    <RiC:physicalOrLogicalExtent xml:lang="fr">
                                        <xsl:apply-templates/>
                                        <!-- <xsl:if test="position()!=last()">
                                            <xsl:text> </xsl:text>
                                        </xsl:if>-->

                                    </RiC:physicalOrLogicalExtent>
                                </xsl:for-each>
                                <!-- Nombre d'unités de niveau bas-->
                                <xsl:for-each
                                    select="did/physdesc/extent[@type = 'unités description' and normalize-space(.) != '']">
                                    <RiC:physicalOrLogicalExtent xml:lang="fr">
                                        <xsl:apply-templates/>
                                        <xsl:text> unités documentaires décrites</xsl:text>
                                        <!-- <xsl:if test="position()!=last()">
                                            <xsl:text> </xsl:text>
                                        </xsl:if>-->

                                    </RiC:physicalOrLogicalExtent>
                                </xsl:for-each>
                            </xsl:if>
                            <xsl:if test="custodhist[normalize-space(.) != '']">
                                <RiC:history xml:lang="fr">
                                    <xsl:for-each select="custodhist/*">
                                        <xsl:apply-templates/>
                                        <xsl:if test="position() != last()">
                                            <xsl:text> </xsl:text>
                                        </xsl:if>
                                    </xsl:for-each>
                                </RiC:history>
                            </xsl:if>
                            <xsl:if test="arrangement[normalize-space(.) != '']">
                                <RiC:arrangement xml:lang="fr">
                                    <xsl:for-each select="arrangement/p">
                                        <xsl:apply-templates/>
                                        <xsl:if test="position() != last()">
                                            <xsl:text> </xsl:text>
                                        </xsl:if>
                                    </xsl:for-each>
                                </RiC:arrangement>
                            </xsl:if>
                            <xsl:if test="processinfo[normalize-space(.) != '']">
                                <RiC:noteOnProcessApplied xml:lang="fr">
                                    <xsl:for-each select="processinfo/*">
                                        <xsl:apply-templates/>
                                        <xsl:if test="position() != last()">
                                            <xsl:text> </xsl:text>
                                        </xsl:if>
                                    </xsl:for-each>
                                </RiC:noteOnProcessApplied>
                            </xsl:if>
                            <xsl:if test="accruals[normalize-space(.) != '']">
                                <RiC:accruals xml:lang="fr">
                                    <xsl:for-each select="accruals/*">
                                        <xsl:apply-templates/>
                                        <xsl:if test="position() != last()">
                                            <xsl:text> </xsl:text>
                                        </xsl:if>
                                    </xsl:for-each>
                                </RiC:accruals>
                            </xsl:if>
                            <xsl:if test="accessrestrict[normalize-space(.) != '']">
                                <xsl:for-each
                                    select="accessrestrict[normalize-space(.) and not(@type)]">
                                    <RiC:conditionsOfAccess xml:lang="fr">
                                        <xsl:for-each select="*[not(self::head)]">
                                            <xsl:apply-templates/>
                                            <xsl:if test="position() != last()">
                                                <xsl:text> </xsl:text>
                                            </xsl:if>
                                        </xsl:for-each>
                                    </RiC:conditionsOfAccess>
                                </xsl:for-each>
                                <xsl:for-each select="accessrestrict[normalize-space(.) and @type]">
                                    <RiC:conditionsOfAccess xml:lang="fr">
                                        <!--  <xsl:for-each select="accessrestrict/p">
                                            <xsl:apply-templates/>
                                            <xsl:if test="position()!=last()">
                                                <xsl:text> </xsl:text>
                                            </xsl:if>
                                        </xsl:for-each>-->
                                        <xsl:choose>
                                            <xsl:when test="@type = 'delai'">
                                                <xsl:text>Délai : </xsl:text>
                                            </xsl:when>
                                            <xsl:when test="@type = 'premiere-annee-communicable'">
                                                <xsl:text>Première année communicable : </xsl:text>
                                            </xsl:when>
                                        </xsl:choose>
                                        <xsl:value-of select="p"/>
                                    </RiC:conditionsOfAccess>
                                </xsl:for-each>

                            </xsl:if>
                            <xsl:for-each select="acqinfo[normalize-space(.) != '']">
                                <RiC:noteOnAcquisition xml:lang="fr">
                                    <xsl:for-each select="p">
                                        <xsl:apply-templates/>
                                        <xsl:if test="position() != last()">
                                            <xsl:text> </xsl:text>
                                        </xsl:if>
                                    </xsl:for-each>
                                </RiC:noteOnAcquisition>
                            </xsl:for-each>
                            <!--  <xsl:for-each select="controlaccess/genreform[normalize-space(.)!=''] | physdesc/genreform[normalize-space(.)!='']">
                                <RiC:contentType xml:lang="fr">
                                    <xsl:value-of select="normalize-space(.)"/>
                                </RiC:contentType>
                                
                            </xsl:for-each>-->
                            <xsl:for-each
                                select="controlaccess/subject[normalize-space(.) != '' and @source = 'ThésaurusW']">
                                <RiC:subject xml:lang="fr">
                                    <xsl:value-of select="normalize-space(.)"/>
                                </RiC:subject>

                            </xsl:for-each>
                            <!--  <xsl:for-each select="controlaccess/persname[normalize-space(.)!='']">
                                <RiC:subject>
                                    <xsl:variable name="targ" select="@authfilenumber"/>
                                    <xsl:choose>
                                        <xsl:when test="$targ">
                                            <xsl:variable name="targId" select="substring-after($targ, 'FRAN_NP_')"/>
                                            <xsl:choose>
                                                <xsl:when test="$collection-agents/rdf:RDF/rdf:Description[rdf:type/@rdf:resource='http://www.ica.org/standards/RiC/ontology#Person' and substring-after(@rdf:about, 'person_')=$targId]">
                                                    <xsl:attribute name="rdf:resource">
                                                        <xsl:value-of select="$collection-agents/rdf:RDF/rdf:Description[rdf:type/@rdf:resource='http://www.ica.org/standards/RiC/ontology#Person' and substring-after(@rdf:about, 'person_')=$targId]/@rdf:about"/>
                                                    </xsl:attribute></xsl:when>
                                                <xsl:otherwise>
                                                    <xsl:attribute name="xml:lang">fr</xsl:attribute>
                                                    <xsl:value-of select="."/>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                           
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:attribute name="xml:lang">fr</xsl:attribute>
                                            <xsl:value-of select="."/>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </RiC:subject>
                                
                            </xsl:for-each>-->
                            <!--<xsl:for-each select="controlaccess/corpname[normalize-space(.)!='']">
                                <RiC:subject>
                                    <xsl:variable name="targ" select="@authfilenumber"/>
                                    <xsl:choose>
                                        <xsl:when test="normalize-space($targ)!=''">
                                            <xsl:variable name="targId" select="substring-after($targ, 'FRAN_NP_')"/>
                                            <xsl:choose>
                                                <xsl:when test="$collection-agents/rdf:RDF/rdf:Description[rdf:type/@rdf:resource='http://www.ica.org/standards/RiC/ontology#CorporateBody' and substring-after(@rdf:about, 'corporate-body_')=$targId]">
                                                    <xsl:attribute name="rdf:resource">
                                                        <xsl:value-of select="$collection-agents/rdf:RDF/rdf:Description[rdf:type/@rdf:resource='http://www.ica.org/standards/RiC/ontology#CorporateBody' and substring-after(@rdf:about, 'corporate-body_')=$targId]/@rdf:about"/>
                                                    </xsl:attribute></xsl:when>
                                                <xsl:otherwise>
                                                    <xsl:attribute name="xml:lang">fr</xsl:attribute>
                                                    <xsl:value-of select="."/>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                            
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:attribute name="xml:lang">fr</xsl:attribute>
                                            <xsl:value-of select="."/>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </RiC:subject>
                                
                            </xsl:for-each>-->
                            <xsl:for-each select="relatedmaterial[normalize-space(.) != '']">
                                <!-- <xsl:choose>
                                    <xsl:when test="extref[starts-with(@href, 'http://gallica.bnf.fr/ark:')]">
                                        <xsl:for-each select="extref[starts-with(@href, 'http://gallica.bnf.fr/ark:')]">
                                            <RiC:isRelatedWith>
                                                <xsl:attribute name="rdf:resource">
                                                    <xsl:value-of select="@href"/>
                                                </xsl:attribute>
                                            </RiC:isRelatedWith>
                                        </xsl:for-each>
                                      
                                        
                                    </xsl:when>
                                    <xsl:when test="extref[starts-with(@href, 'http://webapp.bnf.fr/')]">
                                        <xsl:for-each select="extref[starts-with(@href, 'http://webapp.bnf.fr/')]">
                                            <RiC:isRelatedWith>
                                                <xsl:attribute name="rdf:resource">
                                                    <xsl:value-of select="@href"/>
                                                </xsl:attribute>
                                            </RiC:isRelatedWith>
                                        </xsl:for-each>
                                    </xsl:when>
                                    <xsl:otherwise>-->
                                <RiC:noteOnRelatedMaterial xml:lang="fr">
                                    <xsl:for-each select="*">
                                        <xsl:apply-templates/>
                                        <xsl:if test="position != last()">
                                            <xsl:text> </xsl:text>
                                        </xsl:if>
                                    </xsl:for-each>

                                </RiC:noteOnRelatedMaterial>
                                <!--  </xsl:otherwise>
                                </xsl:choose>-->
                            </xsl:for-each>
                            <!-- TO DO DAOGRP -->
                            <!-- http://gael.gironde.fr/img-viewer/FRAD033/T/154T01/viewer.html?np=FRAD033_154T01_0001_P.JPG&nd=FRAD033_154T01_0382_P.JPG&ns=FRAD033_154T01_0001_P.JPG-->
                            <xsl:if test="$IR_id!='FRAD033_BV_0111'">
                            <xsl:for-each
                                select="daogrp[@role = 'series' and daoloc[@role = 'image:first'] and daoloc[@role = 'image:last']]">

                                <!--  <daogrp title="3604 W" role="image">
                        <daoloc href="FRAD033/BV/3604W/FRAD033_3604W_0004_P.jpg" role="image"/>
                    </daogrp>-->
                                <!--   <daogrp title="4803 W" role="series">
                        <daoloc href="FRAD033/BV/4803W/FRAD033_4803W_0001_P.jpg" role="image:first"/>
                        <daoloc href="FRAD033/BV/4803W/FRAD033_4803W_0036_P.jpg" role="image:last"/>
                    </daogrp>-->
                                <!--   <daogrp title="154 T 1" role="series">
                                <daoloc href="FRAD033/T/154T01/FRAD033_154T01_0001_P.JPG"
                                    role="image:first"/>
                                <daoloc href="FRAD033/T/154T01/FRAD033_154T01_0382_P.JPG"
                                    role="image:last"/>
                            </daogrp>-->
                                <RiC:digitalCopy>
                                    <xsl:attribute name="rdf:resource">

                                        <xsl:text>http://gael.gironde.fr/img-viewer/</xsl:text>
                                        <xsl:value-of
                                            select="substring-before(daoloc[1]/@href, '/FRAD033_')"/>
                                        <xsl:text>/viewer.html?np=</xsl:text>
                                        <xsl:value-of select="daoloc[@role = 'image:first']/@href"/>
                                        <xsl:text>&amp;nd=</xsl:text>
                                        <xsl:value-of select="daoloc[@role = 'image:last']/@href"/>
                                    </xsl:attribute>
                                </RiC:digitalCopy>

                            </xsl:for-each>
                            </xsl:if>
                            <!-- liens reliant la ressource à celles qui en sont "membres"-->
                            <!-- pour l'instant on crée des relations directes, mais en théorie il faudrait des relations n-aires-->
                            <xsl:if test="self::archdesc[dsc/c] | self::c[c]">
                                <xsl:for-each select="child::dsc/c | self::c[c]/c">
                                    <xsl:variable name="childId" select="@id"/>
                                    <xsl:variable name="theChildUnitId"
                                        select="concat($topRecordSetId, '-', $childId)"/>
                                    <xsl:variable name="theChildType">
                                        <xsl:choose>
                                            <xsl:when test="@level = 'item'">record</xsl:when>
                                            <xsl:otherwise>record-set</xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:variable>
                                    <!--<xsl:variable name="theChildClass">
                                        <xsl:choose>
                                            <xsl:when test="$theChildType='record'">Record</xsl:when>
                                            <xsl:otherwise>RecordSet</xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:variable>-->
                                    <RiC:member>
                                        <xsl:attribute name="rdf:resource">
                                            <xsl:value-of
                                                select="concat('http://piaaf.demo.logilab.fr/resource/', $coll, '_', $theChildType, '_', $theChildUnitId)"/>

                                        </xsl:attribute>
                                    </RiC:member>

                                </xsl:for-each>
                            </xsl:if>
                            <!-- lien de la ressource au niveau immédiatement supérieur, avec la même remarque que précédemment mais bon -->
                            <xsl:if test="self::c">
                                
                                    <xsl:variable name="parentId">
                                        <xsl:choose>
                                            <xsl:when test="parent::c">
                                                <xsl:value-of select="concat($topRecordSetId, '-', parent::c/@id)"/>
                                            </xsl:when>
                                            <xsl:when test="parent::dsc">
                                                <xsl:value-of select="concat($topRecordSetId, '-top')"/>
                                            </xsl:when>
                                        </xsl:choose>
                                    </xsl:variable>
                                    <!--<xsl:variable name="theChildUnitId"
                                        select="concat($topRecordSetId, '-', $childId)"/>-->
                                    <xsl:variable name="theParentType">
                                        <xsl:choose>
                                            <xsl:when test="parent::dsc">record-set</xsl:when>
                                            <xsl:otherwise>record-set</xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:variable>
                                    <!--<xsl:variable name="theChildClass">
                                        <xsl:choose>
                                            <xsl:when test="$theChildType='record'">Record</xsl:when>
                                            <xsl:otherwise>RecordSet</xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:variable>-->
                                    <RiC:memberOf>
                                        <xsl:attribute name="rdf:resource">
                                            <xsl:value-of
                                                select="concat('http://piaaf.demo.logilab.fr/resource/', $coll, '_', $theParentType, '_', $parentId)"/>
                                            
                                        </xsl:attribute>
                                    </RiC:memberOf>
                                    
                                
                            </xsl:if>         
                            
                            <!-- "permalien" de la page correspondante dans GAEL -->
                            <!-- http://gael.gironde.fr/ead.html?id=FRAD033_IR_4T&c=FRAD033_IR_4T_tt3-1-->
                            <xsl:if test="$IR_id!='FRAD033_BV_0111'">
                            <rdfs:seeAlso>
                                <xsl:attribute name="rdf:resource">
                                    <!--<xsl:value-of select="concat('http://archivesetmanuscrits.bnf.fr/ark:/12148/', )"/>-->
                                    <xsl:text>http://gael.gironde.fr/ead.html?id=</xsl:text>
                                    <xsl:value-of select="$IR_id"/>
                                    <xsl:if test="not(self::archdesc)">
                                        <xsl:text>&amp;c=</xsl:text>
                                        <xsl:value-of select="concat($IR_id, '_', @id)"/>

                                    </xsl:if>


                                </xsl:attribute>
                                <!-- http://gael.gironde.fr/ead.html?id=FRAD033_IR_4T&c=FRAD033_IR_4T_tt1-1-->
                            </rdfs:seeAlso>
                                <RiC:mainSubjectOf>
                                    <xsl:attribute name="rdf:resource">
                                        <!--<xsl:value-of select="concat('http://archivesetmanuscrits.bnf.fr/ark:/12148/', )"/>-->
                                        <xsl:text>http://gael.gironde.fr/ead.html?id=</xsl:text>
                                        <xsl:value-of select="$IR_id"/>
                                        <xsl:if test="not(self::archdesc)">
                                            <xsl:text>&amp;c=</xsl:text>
                                            <xsl:value-of select="concat($IR_id, '_', @id)"/>
                                            
                                        </xsl:if>
                                        
                                        
                                    </xsl:attribute>
                                </RiC:mainSubjectOf>
                            </xsl:if>
                           

                            <xsl:if test="self::archdesc">
                                <!--  <xsl:variable name="IR_id">
                    <xsl:choose>
                        <xsl:when test="ancestor-or-self::archdesc/otherfindaid/p[extref]/extref/@href">
                            <xsl:value-of select="substring-after(ancestor-or-self::archdesc/otherfindaid/p[extref]/extref/@href, 'FRAN_')"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="substring-after(ancestor::ead/eadheader/eadid, 'FRAN_')"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>-->
                                <RiC:describedBy>
                                    <xsl:attribute name="rdf:resource">
                                        <xsl:value-of
                                            select="
                                                
                                                
                                                concat('http://piaaf.demo.logilab.fr/resource/FRSIAF_finding-aid_', replace($IR_id, '_', ''))
                                                
                                                "
                                        />
                                    </xsl:attribute>
                                </RiC:describedBy>
                            </xsl:if>

                        </rdf:Description>




                    </rdf:RDF>
                </xsl:result-document>

            </xsl:for-each>
        </xsl:if>

    </xsl:template>
    <xsl:template name="outputDate">
        <xsl:param name="date"/>
        <xsl:param name="stdDate"/>

        <xsl:choose>

            <xsl:when test="string-length($stdDate) = 4">
                <xsl:attribute name="rdf:datatype">
                    <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text>
                </xsl:attribute>

            </xsl:when>
            <xsl:when test="string-length($stdDate) = 7">
                <xsl:attribute name="rdf:datatype">
                    <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                </xsl:attribute>
            </xsl:when>
            <xsl:when test="string-length($stdDate) = 10">
                <xsl:attribute name="rdf:datatype">
                    <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                </xsl:attribute>
            </xsl:when>
            <xsl:otherwise/>
        </xsl:choose>

        <xsl:value-of
            select="
                if (normalize-space($stdDate) != '')
                then
                    (normalize-space($stdDate))
                else
                    (
                    if (normalize-space($date) != '')
                    then
                        (normalize-space($date))
                    else
                        ()
                    )
                "/>




    </xsl:template>
</xsl:stylesheet>
