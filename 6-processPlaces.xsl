<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:owl="http://www.w3.org/2002/07/owl#"
    xmlns:RiC="http://www.ica.org/standards/RiC/ontology#" xmlns:isni="http://isni.org/ontology#"
    xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:eac="urn:isbn:1-931666-33-4" xmlns="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:piaaf="http://www.piaaf.net" xmlns:piaaf-onto="http://piaaf.demo.logilab.fr/ontology#"
    xmlns:iso-thes="http://purl.org/iso25964/skos-thes#"
    xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:foaf="http://xmlns.com/foaf/0.1/"
    xmlns:dct="http://purl.org/dc/terms/" xmlns:xl="http://www.w3.org/2008/05/skos-xl#"
    xmlns:ginco="http://data.culture.fr/thesaurus/ginco/ns/"
    exclude-result-prefixes="xs xd eac iso-thes rdf foaf dct xl dc ginco isni xlink piaaf skos dc" version="2.0">
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> July 17, 2017</xd:p>
            <xd:p><xd:b>Author:</xd:b> Florence</xd:p>
            <xd:p>Sémantisation : étape 6, génération d'un fichier RDF pour chaque partenaire  pour
                les lieux</xd:p>
        </xd:desc>
    </xd:doc>
    
  
    <xsl:param name="coll">FRSIAF</xsl:param>
   
    
    <xsl:variable name="chemin-EAC-BnF">
        <xsl:value-of
            select="concat('fichiers-def-2/notices-EAC/BnF/', '?select=*.xml;recurse=yes;on-error=warning')"
        />
    </xsl:variable>
    <xsl:variable name="collection-EAC-BnF" select="collection($chemin-EAC-BnF)"/>
    <xsl:variable name="chemin-EAC-SIAF">
        <xsl:value-of
            select="concat('fichiers-def-2/notices-EAC/SIAF/', '?select=*.xml;recurse=yes;on-error=warning')"
        />
    </xsl:variable>
    <xsl:variable name="collection-EAC-SIAF" select="collection($chemin-EAC-SIAF)"/>
    <xsl:variable name="chemin-EAC-AN">
        <xsl:value-of
            select="concat('fichiers-def-2/notices-EAC/AN/', '?select=*.xml;recurse=yes;on-error=warning')"
        />
    </xsl:variable>
    <xsl:variable name="collection-EAC-AN" select="collection($chemin-EAC-AN)"/>
   
    <xsl:template match="piaaf:vide">
        <xsl:if test="$coll = 'FRSIAF'">
            <xsl:result-document href="rdf/places/FRSIAF_places.rdf" method="xml" encoding="utf-8" indent="yes">
                <rdf:RDF 
                    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                    xmlns:owl="http://www.w3.org/2002/07/owl#"
                    xmlns:RiC="http://www.ica.org/standards/RiC/ontology#"
                    xmlns:piaaf-onto="http://piaaf.demo.logilab.fr/ontology#"
                    xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
                    
                    xmlns:skos="http://www.w3.org/2004/02/skos/core#">
              
                    
                    <xsl:for-each-group
                        select="$collection-EAC-SIAF/eac:eac-cpf[not(starts-with(normalize-space(eac:control/eac:recordId), 'FR784'))]/eac:cpfDescription/eac:description/eac:places/eac:place[normalize-space(eac:placeEntry)!='']"
                        group-by="
                      normalize-space(eac:placeEntry)
                       
                       
                       
                        ">
                        <xsl:sort select="current-grouping-key()"></xsl:sort>
                       <!-- <xsl:variable name="num" select="format-number(number(position()), '#000')"/>-->
                        <rdf:Description>
                            <xsl:attribute name="rdf:about">
                               
                               <!-- <xsl:value-of select="concat('http://www.piaaf.net/', 'places/FRSIAF_place_', $num)"/>
                                -->
                                <xsl:text>http://piaaf.demo.logilab.fr/resource/FRSIAF_place_</xsl:text>
                                <xsl:value-of select="substring-after(current-group()[1]/@xml:id, 'pl_')"/>
                            </xsl:attribute>
                            <rdf:type
                                rdf:resource="http://www.ica.org/standards/RiC/ontology#Place"/>
                            
                            
                            <rdfs:label xml:lang="fr">
                             
                                <xsl:choose>
                                    <xsl:when test="eac:placeRole='Siège social'">
                                        <xsl:choose>
                                            <xsl:when test="parent::eac:places/eac:place[eac:placeRole='Siège']">
                                                <xsl:value-of select="concat(normalize-space(parent::eac:places/eac:place[eac:placeRole='Siège']/eac:placeEntry), '. ', current-grouping-key())"/>
                                            </xsl:when>
                                            <xsl:otherwise><xsl:value-of select="current-grouping-key()"/></xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="current-grouping-key()"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </rdfs:label>
                      <xsl:for-each select="eac:placeRole">
                          <RiC:placeType xml:lang="fr">
                              <xsl:choose>
                                  <xsl:when test="normalize-space(.)='Siège'">
                                      <xsl:text>ville</xsl:text>
                                  </xsl:when>
                                  <xsl:when test="normalize-space(.)='Siège social'">
                                      <xsl:text>édifice</xsl:text>
                                  </xsl:when>
                                  <xsl:when test="normalize-space(.)='Circonscription'">
                                      <xsl:text>circonscription administrative</xsl:text>
                                  </xsl:when>
                              </xsl:choose>
                          </RiC:placeType>
                      </xsl:for-each>
                            
                              <xsl:if test="eac:placeEntry/@latitude">
                                  <piaaf-onto:latitude><xsl:value-of select="normalize-space(eac:placeEntry/@latitude)"/></piaaf-onto:latitude>
                              </xsl:if>
                            <xsl:if test="eac:placeEntry/@longitude">
                                  <piaaf-onto:longitude><xsl:value-of select="normalize-space(eac:placeEntry/@longitude)"/></piaaf-onto:longitude>
                              </xsl:if>
                              <xsl:for-each select="eac:citation[normalize-space(@xlink:href)!='']">
                                  <RiC:subjectOf>
                                      <xsl:attribute name="rdf:resource">
                                          <xsl:value-of select="@xlink:href"/>
                                      </xsl:attribute>
                                  </RiC:subjectOf>
                              </xsl:for-each>
                             <!-- <xsl:for-each-group select="current-group()" group-by="ancestor::eac:eac-cpf/eac:control/eac:recordId">
                                  <RiC:isPlaceOf>
                                      <xsl:attribute name="rdf:resource">
                                          <xsl:text>http://www.piaaf.net/agents/FRSIAF_</xsl:text>
                                          
                                          <xsl:choose>
                                              <xsl:when test="ancestor::eac:eac-cpf/eac:cpfDescription/eac:identity/eac:entityType='person'">
                                                  <xsl:text>person_</xsl:text>
                                              </xsl:when>
                                              <xsl:when test="ancestor::eac:eac-cpf/eac:cpfDescription/eac:identity/eac:entityType='corporateBody'">
                                                  <xsl:text>corporate-body_</xsl:text>
                                              </xsl:when>
                                          </xsl:choose>
                                          <xsl:value-of select="current-grouping-key()"/>  
                                      </xsl:attribute>
                                  </RiC:isPlaceOf>
                              </xsl:for-each-group>-->
                        
                            
                        </rdf:Description>
                    </xsl:for-each-group>
                    
                 
                    
                </rdf:RDF>
            </xsl:result-document>
        </xsl:if>
        <!--<xsl:if test="$coll = 'FRBNF'">
           Finalement on ne vas générer ces ressources RDF car les structures XML initiales sont asez souvent erronées 
            <xsl:result-document href="rdf/places/FRBNF_places.rdf" method="xml" encoding="utf-8" indent="yes">
                <rdf:RDF xmlns:dc="http://purl.org/dc/elements/1.1/"
                    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                    xmlns:owl="http://www.w3.org/2002/07/owl#"
                    xmlns:RiC="http://www.ica.org/standards/RiC/ontology#"
                    
                    xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
                    xmlns:piaaf-onto="http://www.piaaf.net/ontology#"
                    xmlns:skos="http://www.w3.org/2004/02/skos/core#">
                
              
                    <xsl:for-each-group
                        select="$collection-EAC-BnF/eac:eac-cpf/eac:cpfDescription/eac:description/eac:places/eac:place[normalize-space(eac:placeEntry)!='']"
                        group-by="
                        
                       normalize-space(eac:placeEntry)
                        
                        
                        
                        
                        ">
                        <xsl:sort select="current-grouping-key()"></xsl:sort>
                   <!-\-     <xsl:variable name="num" select="format-number(number(position()), '#000')"/>-\->
                        <rdf:Description>
                            <xsl:attribute name="rdf:about">
                             <!-\- 
                                <xsl:value-of select="concat('http://www.piaaf.net/', 'places/FRBNF_place_', $num)"/>-\->
                                <xsl:text>http://www.piaaf.net/places/FRBNF_place_</xsl:text>
                                <xsl:value-of select="substring-after(current-group()[1]/@xml:id, 'pl_')"/>
                                
                            </xsl:attribute>
                            <rdf:type
                                rdf:resource="http://www.ica.org/standards/RiC/ontology#Place"/>
                            
                            
                            <rdfs:label xml:lang="fr">
                                <xsl:value-of select="current-grouping-key()"/>
                            </rdfs:label>
                            <xsl:if test="@latitude">
                                <RiC:latitude><xsl:value-of select="normalize-space(@latitude)"/></RiC:latitude>
                            </xsl:if>
                            <xsl:if test="@longitude">
                                <RiC:longitude><xsl:value-of select="normalize-space(@longitude)"/></RiC:longitude>
                            </xsl:if>
                            <xsl:for-each select="eac:citation[normalize-space(@xlink:href)!='']">
                              
                                <RiC:subjectOf>
                                    <xsl:attribute name="rdf:resource">
                                        <xsl:value-of select="@xlink:href"/>
                                    </xsl:attribute>
                                </RiC:subjectOf>
                            </xsl:for-each>
                            <xsl:for-each-group select="current-group()" group-by="ancestor::eac:eac-cpf/eac:control/eac:recordId">
                                <RiC:isPlaceOf>
                                    <xsl:attribute name="rdf:resource">
                                        <xsl:text>http://www.piaaf.net/agents/FRBNF_</xsl:text>
                                        
                                        <xsl:choose>
                                            <xsl:when test="ancestor::eac:eac-cpf/eac:cpfDescription/eac:identity/eac:entityType='person'">
                                                <xsl:text>person_</xsl:text>
                                            </xsl:when>
                                            <xsl:when test="ancestor::eac:eac-cpf/eac:cpfDescription/eac:identity/eac:entityType='corporateBody'">
                                                <xsl:text>corporate-body_</xsl:text>
                                            </xsl:when>
                                        </xsl:choose>
                                        <xsl:value-of select="substring-after(current-grouping-key(), 'DGEARC_')"/>  
                                    </xsl:attribute>
                                </RiC:isPlaceOf>
                            </xsl:for-each-group>
                       
                            
                            
                        </rdf:Description>
                    </xsl:for-each-group>
                
                    
                </rdf:RDF>
            </xsl:result-document>
        </xsl:if>-->
        <xsl:if test="$coll = 'FRAN'">
            <xsl:result-document href="rdf/places/FRAN_places.rdf" method="xml" encoding="utf-8" indent="yes">
                <rdf:RDF 
                    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                    xmlns:owl="http://www.w3.org/2002/07/owl#"
                    xmlns:RiC="http://www.ica.org/standards/RiC/ontology#"
                    
                    xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
                    xmlns:piaaf-onto="http://piaaf.demo.logilab.fr/ontology#"
                    >
                    
                    <!-- <place>
               <placeRole>Circonscription</placeRole>
               <placeEntry latitude="N42.0.0" longitude="E9.0.0">Corse, Région (France)</placeEntry>
               <citation xlink:href="http://www.geonames.org/maps/google_42_9.html"
                         xlink:type="simple"/>
            </place>-->
                    <xsl:for-each-group
                        select="$collection-EAC-AN/eac:eac-cpf/eac:cpfDescription/eac:description/eac:places/eac:place[normalize-space(eac:placeEntry)!='']"
                        group-by="
                        
                        normalize-space(eac:placeEntry)
                        
                        
                        
                        
                        ">
                        <xsl:sort select="current-grouping-key()"></xsl:sort>
                    <!--    <xsl:variable name="num" select="format-number(number(position()), '#000')"/>-->
                        <rdf:Description>
                            <xsl:attribute name="rdf:about">
                               
                               <!-- <xsl:value-of select="concat('http://www.piaaf.net/', 'places/FRAN_place_', $num)"/>-->
                                <xsl:text>http://piaaf.demo.logilab.fr/resource/FRAN_place_</xsl:text>
                                <xsl:value-of select="substring-after(current-group()[1]/@xml:id, 'pl_')"/>
                                
                            </xsl:attribute>
                            <rdf:type
                                rdf:resource="http://www.ica.org/standards/RiC/ontology#Place"/>
                            
                            
                            <rdfs:label xml:lang="fr">
                                <xsl:value-of select="current-grouping-key()"/>
                            </rdfs:label>
                            <xsl:if test="@latitude">
                                <piaaf-onto:latitude><xsl:value-of select="normalize-space(@latitude)"/></piaaf-onto:latitude>
                            </xsl:if>
                            <xsl:if test="@longitude">
                                <piaaf-onto:longitude><xsl:value-of select="normalize-space(@longitude)"/></piaaf-onto:longitude>
                            </xsl:if>
                            <xsl:for-each select="eac:citation[normalize-space(@xlink:href)!='']">
                                
                                <RiC:subjectOf>
                                    <xsl:attribute name="rdf:resource">
                                        <xsl:value-of select="@xlink:href"/>
                                    </xsl:attribute>
                                </RiC:subjectOf>
                            </xsl:for-each>
                           <!-- <xsl:for-each-group select="current-group()" group-by="ancestor::eac:eac-cpf/eac:control/eac:recordId">
                                <RiC:isPlaceOf>
                                    <xsl:attribute name="rdf:resource">
                                        <xsl:text>http://www.piaaf.net/agents/FRAN_</xsl:text>
                                        
                                        <xsl:choose>
                                            <xsl:when test="ancestor::eac:eac-cpf/eac:cpfDescription/eac:identity/eac:entityType='person'">
                                                <xsl:text>person_</xsl:text>
                                            </xsl:when>
                                            <xsl:when test="ancestor::eac:eac-cpf/eac:cpfDescription/eac:identity/eac:entityType='corporateBody'">
                                                <xsl:text>corporate-body_</xsl:text>
                                            </xsl:when>
                                        </xsl:choose>
                                        <xsl:value-of select="substring-after(current-grouping-key(), 'FRAN_NP_')"/>  
                                    </xsl:attribute>
                                </RiC:isPlaceOf>
                            </xsl:for-each-group>
                            -->
                            
                            
                        </rdf:Description>
                    </xsl:for-each-group>
                    
                    
                </rdf:RDF>
            </xsl:result-document>
        </xsl:if>
        
    </xsl:template>
    
</xsl:stylesheet>