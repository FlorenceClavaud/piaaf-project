<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:owl="http://www.w3.org/2002/07/owl#" xmlns:RiC="http://www.ica.org/standards/RiC/ontology#"
    xmlns:isni="http://isni.org/ontology#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
    xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:eac="urn:isbn:1-931666-33-4"
    xmlns="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:piaaf="http://www.piaaf.net"
    xmlns:piaaf-onto="http://piaaf.demo.logilab.fr/ontology#"
    xmlns:iso-thes="http://purl.org/iso25964/skos-thes#"
    xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:foaf="http://xmlns.com/foaf/0.1/"
    xmlns:dct="http://purl.org/dc/terms/" xmlns:xl="http://www.w3.org/2008/05/skos-xl#"
    xmlns:ginco="http://data.culture.fr/thesaurus/ginco/ns/"
    exclude-result-prefixes="xs xd eac iso-thes rdf dct xl piaaf xlink piaaf-onto skos isni foaf ginco dc" version="2.0">
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> July 4, 2017, checked and updated Dec. 8, 2017</xd:p>
            <xd:p><xd:b>Author:</xd:b> Florence Clavaud (Archives nationales)</xd:p>
            <xd:p>Sémantisation : étape 8, d: génération d'un fichier RDF pour chaque partenaire (en fait, pour les AN uniquement)
                pour les groupes de documents cibles des relations de provenance et non décrits dans le corpus.</xd:p>
            <xd:p>Génération d'un fichier RDF pour le SIAF pour les documents cibles des relations de provenance et non décrits dans le corpus</xd:p>
        </xd:desc>
    </xd:doc>
  <xsl:param name="coll">FRSIAF</xsl:param>


    <xsl:variable name="chemin-EAC-BnF">
        <xsl:value-of
            select="concat('fichiers-def-2/notices-EAC/BnF/', '?select=*.xml;recurse=yes;on-error=warning')"
        />
    </xsl:variable>
    <xsl:variable name="collection-EAC-BnF" select="collection($chemin-EAC-BnF)"/>
    <xsl:variable name="chemin-EAC-SIAF">
        <xsl:value-of
            select="concat('fichiers-def-2/notices-EAC/SIAF/', '?select=*.xml;recurse=yes;on-error=warning')"
        />
    </xsl:variable>
    <xsl:variable name="collection-EAC-SIAF" select="collection($chemin-EAC-SIAF)"/>
    <xsl:variable name="chemin-EAC-AN">
        <xsl:value-of
            select="concat('fichiers-def-2/notices-EAC/AN/', '?select=*.xml;recurse=yes;on-error=warning')"
        />
    </xsl:variable>
    <xsl:variable name="collection-EAC-AN" select="collection($chemin-EAC-AN)"/>
    <xsl:variable name="apos" select="'&#x2bc;'"/>

    <xsl:variable name="chemin-IR-AN" select="concat('fichiers-def-2/IR-EAD/AN/', '?select=*.xml;recurse=yes;on-error=warning')"/>
    <xsl:variable name="collection-IR-AN" select="collection($chemin-IR-AN)"/>
    
    <xsl:variable name="chemin-IR-SIAF" select="concat('fichiers-def-2/IR-EAD/SIAF/', '?select=*.xml;recurse=yes;on-error=warning')"/>
    <xsl:variable name="collection-IR-SIAF" select="collection($chemin-IR-SIAF)"/>
    <xsl:variable name="chemin-IR-BNF" select="concat('fichiers-def-2/IR-EAD/BnF/', '?select=*.xml;recurse=yes;on-error=warning')"/>
    <xsl:variable name="collection-IR-BnF" select="collection($chemin-IR-BNF)"/>
    
    <xsl:variable name="siaf-prov-rels" select="document('rdf/relations/FRSIAF_provenance-relations.rdf')/rdf:RDF"/>
    <xsl:variable name="bnf-prov-rels" select="document('rdf/relations/FRBNF_provenance-relations.rdf')/rdf:RDF"/>
    <xsl:variable name="an-prov-rels" select="document('rdf/relations/FRAN_provenance-relations.rdf')/rdf:RDF"/>
    
  
    
    <xsl:template match="/piaaf:vide">
       
      <!-- 
          Le cas BnF n'existe pas -->
       
        <xsl:variable name="AN-not-described-record-sets">
            <piaaf:record-sets>
                <xsl:for-each select="$collection-EAC-AN/eac:eac-cpf/eac:cpfDescription/eac:relations/eac:resourceRelation[@resourceRelationType='creatorOf' and normalize-space(@xlink:href)!='']">
                    <xsl:variable name="link" select="normalize-space(@xlink:href)"/>
                    <xsl:variable name="recId" select="ancestor::eac:eac-cpf/eac:control/eac:recordId"/>
                    <xsl:variable name="entType">
                        <xsl:choose>
                            <xsl:when test="ancestor::eac:cpfDescription/eac:identity/eac:entityType='person'">person</xsl:when>
                            <xsl:when test="ancestor::eac:cpfDescription/eac:identity/eac:entityType='corporateBody'">corporate-body</xsl:when>
                        </xsl:choose>
                    </xsl:variable>
                    <xsl:if test="starts-with($link, 'FRAN_IR_') and not($collection-IR-AN/ead[archdesc/otherfindaid/p/extref/@href[starts-with($link, normalize-space(.))]])">
                        <piaaf:record-set>
                            <xsl:attribute name="reference">
                                <!-- https://www.siv.archives-nationales.culture.gouv.fr/siv/IR/FRAN_IR_016801-->
                                <!--  <xsl:value-of select="concat('https://www.siv.archives-nationales.culture.gouv.fr/siv/', )"/>-->
                            <!--    <xsl:choose>-->
                                   <!-- <xsl:when test="contains($link, '#')">
                                        <xsl:value-of select="concat('https://www.siv.archives-nationales.culture.gouv.fr/siv/UD/', substring-before($link, '#'), '/', substring-after($link, '#'))"/>
                                    </xsl:when>-->
                                   <!-- <xsl:otherwise>-->
                                        <xsl:value-of select="concat('https://www.siv.archives-nationales.culture.gouv.fr/siv/IR/', $link)"/>
                                    <!--</xsl:otherwise>-->
                                <!--</xsl:choose>-->
                            </xsl:attribute>
                            <xsl:variable name="URI">
                                <xsl:value-of select="concat('http://piaaf.demo.logilab.fr/resource/FRAN_record-set_', substring-after($link, 'FRAN_IR_'), '-top')"/>
                            </xsl:variable>
                            <xsl:attribute name="URI" select="$URI">
                              
                            </xsl:attribute>
                            <xsl:variable name="orig">
                                <xsl:value-of select="concat('http://piaaf.demo.logilab.fr/resource/FRAN_', $entType, '_', substring-after($recId, 'FRAN_NP_'))"/>
                            </xsl:variable>
                            <piaaf:origination>
                                <xsl:attribute name="URI" select="$orig"/>
                                 
                            </piaaf:origination>
                          
                                <xsl:if test="normalize-space(eac:relationEntry)!=''">
                                    <piaaf:title>
                                        <xsl:value-of select="normalize-space(eac:relationEntry)"/>
                                    </piaaf:title>
                                </xsl:if>
                                <xsl:if test="normalize-space(eac:descriptiveNote)!=''">
                                    <piaaf:note>
                                        <xsl:value-of select="normalize-space(eac:descriptiveNote)"/>
                                    </piaaf:note>
                                </xsl:if>
                            <piaaf:provenanceRel>
                                <xsl:if test="$an-prov-rels/rdf:Description[RiC:originatedObject/@rdf:resource=$URI and RiC:provenanceEntity/@rdf:resource=$orig]">
                                    <xsl:value-of select="$an-prov-rels/rdf:Description[RiC:originatedObject/@rdf:resource=$URI and RiC:provenanceEntity/@rdf:resource=$orig]/@rdf:about"/>
                                </xsl:if>
                            </piaaf:provenanceRel>
                               
                            
                           
                        </piaaf:record-set>
                    </xsl:if>
                </xsl:for-each>
            </piaaf:record-sets>
        </xsl:variable>
        <!--<xsl:result-document href="rdf/record-sets/FRAN_not-described-record-sets.xml"  method="xml"
            encoding="utf-8" indent="yes">
            <xsl:copy-of select="$AN-not-described-record-sets"/>
        </xsl:result-document>-->
        <xsl:result-document href="rdf/record-sets/FRAN_not-described-record-sets.rdf"  method="xml"
            encoding="utf-8" indent="yes">
            <rdf:RDF 
                xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                xmlns:owl="http://www.w3.org/2002/07/owl#"
                xmlns:RiC="http://www.ica.org/standards/RiC/ontology#"
                
                xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
                
                
                >
                <xsl:for-each-group select="$AN-not-described-record-sets/piaaf:record-sets/piaaf:record-set" group-by="@URI">
                    <xsl:sort select="current-grouping-key()"></xsl:sort>
                    <rdf:Description>
                        <xsl:attribute name="rdf:about">
                            <xsl:value-of select="current-grouping-key()"/>
                        </xsl:attribute>
                        <rdf:type rdf:resource="http://www.ica.org/standards/RiC/ontology#RecordSet"/>
                        <xsl:for-each-group select="current-group()" group-by="piaaf:title">
                            <RiC:hasTitle xml:lang="fr">
                                <xsl:value-of select="current-grouping-key()"/>
                            </RiC:hasTitle>
                            <rdfs:label xml:lang="fr">
                                <xsl:value-of select="current-grouping-key()"/>
                            </rdfs:label>
                        </xsl:for-each-group>
                        <xsl:for-each-group select="current-group()" group-by="piaaf:note">
                            <RiC:description xml:lang="fr">
                                <xsl:value-of select="current-grouping-key()"/>
                            </RiC:description>
                        </xsl:for-each-group>
                     
                       <!-- <xsl:for-each-group select="current-group()" group-by="piaaf:origination/@URI">
                            <RiC:originatedBy>
                                <xsl:attribute name="rdf:resource">
                                    <xsl:value-of select="current-grouping-key()"/>
                                </xsl:attribute>
                            </RiC:originatedBy>
                        </xsl:for-each-group>-->
                        <xsl:for-each-group select="current-group()" group-by="piaaf:provenanceRel">
                            <RiC:originatedBy rdf:resource="{current-grouping-key()}"/>
                        </xsl:for-each-group>
                        <xsl:for-each-group select="current-group()" group-by="@reference">
                            <RiC:mainSubjectOf rdf:resource="{current-grouping-key()}"/>
                            <rdfs:seeAlso rdf:resource="{current-grouping-key()}"/>
                        </xsl:for-each-group>
                    </rdf:Description>
                </xsl:for-each-group>
                
            </rdf:RDF>
        </xsl:result-document>
        <xsl:variable name="SIAF-not-described-records">
            <piaaf:records>
                <xsl:for-each select="$collection-EAC-SIAF/eac:eac-cpf/eac:cpfDescription/eac:relations/eac:resourceRelation[@resourceRelationType='creatorOf' and normalize-space(@xlink:href)!='']">
                    <xsl:variable name="link" select="normalize-space(@xlink:href)"/>
                  
                    <xsl:if test="starts-with($link, 'http://catalogue.bnf.fr/ark:/12148/')">
                        <xsl:variable name="targRecId" select="substring-after($link, 'http://catalogue.bnf.fr/ark:/12148/')"/>
                        <piaaf:record>
                            <xsl:variable name="URI">
                                
                                <xsl:value-of select="concat('http://piaaf.demo.logilab.fr/resource/FRBNF_record_', $targRecId)"/>
                            </xsl:variable>
                            <xsl:attribute name="URI" select="$URI"/>
                            <xsl:variable name="orig" select="concat('http://piaaf.demo.logilab.fr/resource/FRSIAF_person_', ancestor::eac:eac-cpf/eac:control/eac:recordId)"/>
                     
                            <xsl:attribute name="reference">
                                <xsl:value-of select="$link"/>
                            </xsl:attribute>
                            <xsl:if test="normalize-space(eac:relationEntry)!=''">
                                <piaaf:title>
                                    <xsl:value-of select="normalize-space(eac:relationEntry)"/>
                                </piaaf:title>
                            </xsl:if>
                            <xsl:if test="normalize-space(eac:descriptiveNote)!=''">
                                <piaaf:note>
                                    <xsl:value-of select="normalize-space(eac:descriptiveNote)"/>
                                </piaaf:note>
                            </xsl:if>
                            
                            <piaaf:origination>
                                <xsl:attribute name="URI" select="$orig"/>
                                   <!-- <xsl:value-of select="concat('http://www.piaaf.net/agents/FRSIAF_person_', ancestor::eac:eac-cpf/eac:control/eac:recordId)"/>-->
                               
                            </piaaf:origination>
                            <piaaf:provenanceRel>
                                <xsl:if test="$siaf-prov-rels/rdf:Description[RiC:originatedObject/@rdf:resource=$URI and RiC:provenanceEntity/@rdf:resource=$orig]">
                                    <xsl:value-of select="$siaf-prov-rels/rdf:Description[RiC:originatedObject/@rdf:resource=$URI and RiC:provenanceEntity/@rdf:resource=$orig]/@rdf:about"/>
                                </xsl:if>
                            </piaaf:provenanceRel>
                        </piaaf:record>
                    </xsl:if>
                </xsl:for-each>
            </piaaf:records>
        </xsl:variable>
       <!-- <xsl:result-document href="rdf/records/FRSIAF_not-described-records.xml"  method="xml"
            encoding="utf-8" indent="yes">
            <xsl:copy-of select="$SIAF-not-described-records"/>
        </xsl:result-document>
        -->
        <xsl:result-document href="rdf/records/FRSIAF_not-described-records.rdf"  method="xml"
            encoding="utf-8" indent="yes">
            <rdf:RDF 
                xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                xmlns:owl="http://www.w3.org/2002/07/owl#"
                xmlns:RiC="http://www.ica.org/standards/RiC/ontology#"
                
                xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
                
                
                >
                <xsl:for-each-group select="$SIAF-not-described-records/piaaf:records/piaaf:record" group-by="@URI">
                    <xsl:sort select="current-grouping-key()"></xsl:sort>
                    <rdf:Description>
                        <xsl:attribute name="rdf:about">
                            <xsl:value-of select="current-grouping-key()"/>
                        </xsl:attribute>
                        <rdf:type rdf:resource="http://www.ica.org/standards/RiC/ontology#Record"/>
                        <xsl:for-each-group select="current-group()" group-by="piaaf:title">
                            <RiC:hasTitle xml:lang="fr">
                                <xsl:value-of select="current-grouping-key()"/>
                            </RiC:hasTitle>
                            <rdfs:label xml:lang="fr">
                                <xsl:value-of select="current-grouping-key()"/>
                            </rdfs:label>
                        </xsl:for-each-group>
                        <xsl:for-each-group select="current-group()" group-by="piaaf:note">
                            <RiC:description xml:lang="fr">
                                <xsl:value-of select="current-grouping-key()"/>
                            </RiC:description>
                        </xsl:for-each-group>
                        <xsl:for-each-group select="current-group()" group-by="@reference">
                            <RiC:mainSubjectOf rdf:resource="{current-grouping-key()}"/>
                            <rdfs:seeAlso rdf:resource="{current-grouping-key()}"/>
                        </xsl:for-each-group>
                    <!--    <xsl:for-each-group select="current-group()" group-by="piaaf:provenanceRel">
                            <RiC:originatedBy rdf:resource="{current-grouping-key()}"/>
                        </xsl:for-each-group>-->
                      <!--  <xsl:for-each-group select="current-group()" group-by="piaaf:origination/@URI">
                            <RiC:originatedBy>
                                <xsl:attribute name="rdf:resource">
                                    <xsl:value-of select="current-grouping-key()"/>
                                </xsl:attribute>
                            </RiC:originatedBy>
                        </xsl:for-each-group>-->
                      
                    <xsl:for-each-group select="current-group()" group-by="piaaf:provenanceRel">
                            <RiC:originatedBy rdf:resource="{current-grouping-key()}"/>
                        </xsl:for-each-group>
                      <!--  <xsl:for-each-group select="current-group()" group-by="piaaf:origination/@URI">
                            <RiC:originatedBy>
                                <xsl:attribute name="rdf:resource">
                                    <xsl:value-of select="current-grouping-key()"/>
                                </xsl:attribute>
                            </RiC:originatedBy>
                        </xsl:for-each-group>-->
                      
                    </rdf:Description>
                </xsl:for-each-group>
                
            </rdf:RDF>
        </xsl:result-document>
        
        
        
    </xsl:template>
    
    
    
    
    
    
</xsl:stylesheet>
