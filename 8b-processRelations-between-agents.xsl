<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:owl="http://www.w3.org/2002/07/owl#" xmlns:RiC="http://www.ica.org/standards/RiC/ontology#"
    xmlns:isni="http://isni.org/ontology#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
    xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:eac="urn:isbn:1-931666-33-4"
    xmlns="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:piaaf="http://www.piaaf.net"
    xmlns:piaaf-onto="http://piaaf.demo.logilab.fr/ontology#"
    xmlns:iso-thes="http://purl.org/iso25964/skos-thes#"
    xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:foaf="http://xmlns.com/foaf/0.1/"
    xmlns:dct="http://purl.org/dc/terms/" xmlns:xl="http://www.w3.org/2008/05/skos-xl#"
    xmlns:ginco="http://data.culture.fr/thesaurus/ginco/ns/"
    exclude-result-prefixes="xs xd eac iso-thes rdf dct xl piaaf xlink piaaf-onto skos isni foaf ginco dc" version="2.0">
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> July 4, 2017, checked and updated Dec. 8, 2017</xd:p>
            <xd:p><xd:b>Author:</xd:b> Florence</xd:p>
            <xd:p>Sémantisation : étape 8, b : génération d'un fichier RDF pour chaque partenaire
                pour les relations n-aires d'agent à agent</xd:p>
        </xd:desc>
    </xd:doc>
   
    

    <xsl:variable name="chemin-EAC-BnF">
        <xsl:value-of
            select="concat('fichiers-def-2/notices-EAC/BnF/', '?select=*.xml;recurse=yes;on-error=warning')"
        />
    </xsl:variable>
    <xsl:variable name="collection-EAC-BnF" select="collection($chemin-EAC-BnF)"/>
    <xsl:variable name="chemin-EAC-SIAF">
        <xsl:value-of
            select="concat('fichiers-def-2/notices-EAC/SIAF/', '?select=*.xml;recurse=yes;on-error=warning')"
        />
    </xsl:variable>
    <xsl:variable name="collection-EAC-SIAF" select="collection($chemin-EAC-SIAF)"/>
    <xsl:variable name="chemin-EAC-AN">
        <xsl:value-of
            select="concat('fichiers-def-2/notices-EAC/AN/', '?select=*.xml;recurse=yes;on-error=warning')"
        />
    </xsl:variable>
    <xsl:variable name="collection-EAC-AN" select="collection($chemin-EAC-AN)"/>
    <xsl:variable name="apos" select="'&#x2bc;'"/>

    <xsl:variable name="FRSIAF-positions" select="document('rdf/positions/FRSIAF_positions.rdf')/rdf:RDF"/>
    <xsl:variable name="FRAN-positions" select="document('rdf/positions/FRAN_positions.rdf')/rdf:RDF"/>
    <xsl:variable name="FRBNF-positions" select="document('rdf/positions/FRBNF_positions.rdf')/rdf:RDF"/>
    <xsl:variable name="SIAFAgentsRelTable">

        <piaaf:relations>
            
            <xsl:for-each
                select="$collection-EAC-SIAF/eac:eac-cpf[not(starts-with(normalize-space(eac:control/eac:recordId), 'FR784'))]/eac:cpfDescription/eac:relations/eac:cpfRelation[@cpfRelationType != 'identity' and not(ends-with(@xlink:arcrole, 'isInstanceOfGenericType'))]">

                <piaaf:rel type="{@cpfRelationType}" xml:id="{@xml:id}">
                    <xsl:if test="@xlink:arcrole">
                        <xsl:attribute name="arcrole">
                            <xsl:value-of
                                select="substring-after(@xlink:arcrole, 'http://www.piaaf.net/ontology/piaaf#')"
                            />
                        </xsl:attribute>
                    </xsl:if>
                    <piaaf:fromAgent>
                        <xsl:value-of select="ancestor::eac:eac-cpf/eac:control/eac:recordId"/>
                    </piaaf:fromAgent>
                    <piaaf:ancAgentType>
                        <xsl:value-of
                            select="ancestor::eac:eac-cpf/eac:cpfDescription/eac:identity/eac:entityType"
                        />
                    </piaaf:ancAgentType>
                    <xsl:if test="normalize-space(@xlink:href) != ''">
                        <piaaf:targetEntity>

                            <xsl:value-of select="normalize-space(@xlink:href)"/>
                        </piaaf:targetEntity>
                    </xsl:if>


                    <piaaf:targetName>
                        <xsl:value-of select="normalize-space(eac:relationEntry)"/>
                    </piaaf:targetName>
                    <!--</xsl:otherwise>
                          
                            
                </xsl:choose>-->

                  <!--  <xsl:if
                        test="eac:date[(@standardDate and normalize-space(@standardDate) != '') or normalize-space(.) != '']">
                        <piaaf:date>
                            <xsl:value-of
                                select="
                                    if (eac:date/@standardDate[normalize-space(.) != ''])
                                    then
                                        (normalize-space(eac:date/@standardDate))
                                    else
                                        (normalize-space(eac:date))
                                    "
                            />
                        </piaaf:date>
                    </xsl:if>
                    <xsl:if test="eac:dateRange">
                        <piaaf:dateRange>
                            <xsl:if
                                test="eac:dateRange/eac:fromDate[@standardDate[normalize-space(.) != ''] or normalize-space(.) != '']">
                                <xsl:choose>
                                    <xsl:when
                                        test="eac:dateRange/eac:fromDate[normalize-space(@standardDate) != '']">
                                        <piaaf:fromDate>
                                            <xsl:value-of
                                                select="eac:dateRange/eac:fromDate/@standardDate"/>
                                        </piaaf:fromDate>
                                    </xsl:when>
                                    <xsl:when
                                        test="eac:dateRange/eac:fromDate[normalize-space(.) != '']">
                                        <piaaf:fromDate>
                                            <xsl:value-of
                                                select="normalize-space(eac:dateRange/eac:fromDate)"
                                            />
                                        </piaaf:fromDate>
                                    </xsl:when>
                                </xsl:choose>

                            </xsl:if>
                            <xsl:if
                                test="eac:dateRange/eac:toDate[@standardDate[normalize-space(.) != ''] or normalize-space(.) != '']">
                                <xsl:choose>
                                    <xsl:when
                                        test="eac:dateRange/eac:toDate[normalize-space(@standardDate) != '']">
                                        <piaaf:toDate>
                                            <xsl:value-of
                                                select="eac:dateRange/eac:toDate/@standardDate"/>
                                        </piaaf:toDate>
                                    </xsl:when>
                                    <xsl:when
                                        test="eac:dateRange/eac:toDate[normalize-space(.) != '']">
                                        <piaaf:toDate>
                                            <xsl:value-of
                                                select="normalize-space(eac:dateRange/eac:toDate)"/>
                                        </piaaf:toDate>
                                    </xsl:when>
                                </xsl:choose>
                            </xsl:if>
                        </piaaf:dateRange>
                    </xsl:if>-->
                    <xsl:if
                        test="eac:date[(@standardDate and normalize-space(@standardDate) != '') or normalize-space(.) != '']">
                        <piaaf:date>
                            <!--<xsl:value-of
                            select="
                            if (eac:date/@standardDate[normalize-space(.) != ''])
                            then
                            (normalize-space(eac:date/@standardDate))
                            else
                            (normalize-space(eac:date))
                            "
                        />-->
                            <xsl:choose>
                                <xsl:when test="eac:date[normalize-space(@standardDate) != '']">
                                    <xsl:attribute name="type">iso8601</xsl:attribute>
                                    <xsl:value-of select="normalize-space(eac:date/@standardDate)"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="normalize-space(eac:date)"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </piaaf:date>
                    </xsl:if>
                    <xsl:if test="eac:dateRange">
                        <piaaf:dateRange>
                            <xsl:if
                                test="eac:dateRange/eac:fromDate[@standardDate[normalize-space(.) != ''] or normalize-space(.) != '']">
                                <xsl:choose>
                                    <xsl:when
                                        test="eac:dateRange/eac:fromDate[normalize-space(@standardDate) != '']">
                                        <piaaf:fromDate type='iso8601'>
                                            <xsl:value-of
                                                select="eac:dateRange/eac:fromDate/@standardDate"/>
                                        </piaaf:fromDate>
                                    </xsl:when>
                                    <xsl:when
                                        test="eac:dateRange/eac:fromDate[normalize-space(.) != '']">
                                        <piaaf:fromDate>
                                            <xsl:value-of
                                                select="normalize-space(eac:dateRange/eac:fromDate)"
                                            />
                                        </piaaf:fromDate>
                                    </xsl:when>
                                </xsl:choose>
                                
                            </xsl:if>
                            <xsl:if
                                test="eac:dateRange/eac:toDate[@standardDate[normalize-space(.) != ''] or normalize-space(.) != '']">
                                <xsl:choose>
                                    <xsl:when
                                        test="eac:dateRange/eac:toDate[normalize-space(@standardDate) != '']">
                                        <piaaf:toDate type='iso8601'>
                                            <xsl:value-of
                                                select="eac:dateRange/eac:toDate/@standardDate"/>
                                        </piaaf:toDate>
                                    </xsl:when>
                                    <xsl:when
                                        test="eac:dateRange/eac:toDate[normalize-space(.) != '']">
                                        <piaaf:toDate>
                                            <xsl:value-of
                                                select="normalize-space(eac:dateRange/eac:toDate)"/>
                                        </piaaf:toDate>
                                    </xsl:when>
                                </xsl:choose>
                            </xsl:if>
                        </piaaf:dateRange>
                    </xsl:if>
                    <xsl:if test="eac:descriptiveNote[normalize-space(.) != '']">
                        <piaaf:note>
                            <xsl:for-each
                                select="eac:descriptiveNote[normalize-space(.) != '']/eac:p[normalize-space(.) != '']">
                                <piaaf:p>
                                    <xsl:copy-of select="node()"/>
                                </piaaf:p>
                            </xsl:for-each>
                        </piaaf:note>
                    </xsl:if>
                </piaaf:rel>
            </xsl:for-each>
        </piaaf:relations>

    </xsl:variable>
    <xsl:variable name="BNFAgentsRelTable">

        <piaaf:relations>
            <!--<xsl:text>blah</xsl:text>-->
            <!-- collection-EAC-SIAF/eac:eac-cpf[starts-with(normalize-space(eac:control/eac:recordId), 'FR784')]/eac:cpfDescription/descendant::eac:cpfRelation[@cpfRelationType != 'identity']-->
            <xsl:for-each
                select="$collection-EAC-BnF/eac:eac-cpf/eac:cpfDescription/eac:relations/eac:cpfRelation[@cpfRelationType != 'identity' and not(ends-with(@xlink:arcrole, 'isInstanceOfGenericType'))]">

                <piaaf:rel type="{@cpfRelationType}" xml:id="{@xml:id}">
                    <xsl:if test="@xlink:arcrole">
                        <xsl:attribute name="arcrole">
                            <xsl:value-of
                                select="substring-after(@xlink:arcrole, 'http://www.piaaf.net/ontology/piaaf#')"
                            />
                        </xsl:attribute>
                    </xsl:if>
                    <piaaf:fromAgent>
                        <xsl:value-of select="ancestor::eac:eac-cpf/eac:control/eac:recordId"/>
                    </piaaf:fromAgent>
                    <piaaf:ancAgentType>
                        <xsl:value-of
                            select="ancestor::eac:eac-cpf/eac:cpfDescription/eac:identity/eac:entityType"
                        />
                    </piaaf:ancAgentType>
                    <xsl:if test="normalize-space(@xlink:href) != ''">
                        <piaaf:targetEntity>

                            <xsl:value-of select="normalize-space(@xlink:href)"/>
                        </piaaf:targetEntity>
                    </xsl:if>


                    <piaaf:targetName>
                        <xsl:value-of select="normalize-space(eac:relationEntry)"/>
                    </piaaf:targetName>
                    <!--</xsl:otherwise>
                          
                            
                </xsl:choose>-->

                   <!-- <xsl:if
                        test="eac:date[(@standardDate and normalize-space(@standardDate) != '') or normalize-space(.) != '']">
                        <piaaf:date>
                            <xsl:value-of
                                select="
                                    if (eac:date/@standardDate[normalize-space(.) != ''])
                                    then
                                        (normalize-space(eac:date/@standardDate))
                                    else
                                        (normalize-space(eac:date))
                                    "
                            />
                        </piaaf:date>
                    </xsl:if>
                    <xsl:if test="eac:dateRange">
                        <piaaf:dateRange>
                            <xsl:if
                                test="eac:dateRange/eac:fromDate[@standardDate[normalize-space(.) != ''] or normalize-space(.) != '']">
                                <xsl:choose>
                                    <xsl:when
                                        test="eac:dateRange/eac:fromDate[normalize-space(@standardDate) != '']">
                                        <piaaf:fromDate>
                                            <xsl:value-of
                                                select="eac:dateRange/eac:fromDate/@standardDate"/>
                                        </piaaf:fromDate>
                                    </xsl:when>
                                    <xsl:when
                                        test="eac:dateRange/eac:fromDate[normalize-space(.) != '']">
                                        <piaaf:fromDate>
                                            <xsl:value-of
                                                select="normalize-space(eac:dateRange/eac:fromDate)"
                                            />
                                        </piaaf:fromDate>
                                    </xsl:when>
                                </xsl:choose>

                            </xsl:if>
                            <xsl:if
                                test="eac:dateRange/eac:toDate[@standardDate[normalize-space(.) != ''] or normalize-space(.) != '']">
                                <xsl:choose>
                                    <xsl:when
                                        test="eac:dateRange/eac:toDate[normalize-space(@standardDate) != '']">
                                        <piaaf:toDate>
                                            <xsl:value-of
                                                select="eac:dateRange/eac:toDate/@standardDate"/>
                                        </piaaf:toDate>
                                    </xsl:when>
                                    <xsl:when
                                        test="eac:dateRange/eac:toDate[normalize-space(.) != '']">
                                        <piaaf:toDate>
                                            <xsl:value-of
                                                select="normalize-space(eac:dateRange/eac:toDate)"/>
                                        </piaaf:toDate>
                                    </xsl:when>
                                </xsl:choose>
                            </xsl:if>
                        </piaaf:dateRange>
                    </xsl:if>-->
                    <xsl:if
                        test="eac:date[(@standardDate and normalize-space(@standardDate) != '') or normalize-space(.) != '']">
                        <piaaf:date>
                            <!--<xsl:value-of
                            select="
                            if (eac:date/@standardDate[normalize-space(.) != ''])
                            then
                            (normalize-space(eac:date/@standardDate))
                            else
                            (normalize-space(eac:date))
                            "
                        />-->
                            <xsl:choose>
                                <xsl:when test="eac:date[normalize-space(@standardDate) != '']">
                                    <xsl:attribute name="type">iso8601</xsl:attribute>
                                    <xsl:value-of select="normalize-space(eac:date/@standardDate)"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="normalize-space(eac:date)"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </piaaf:date>
                    </xsl:if>
                    <xsl:if test="eac:dateRange">
                        <piaaf:dateRange>
                            <xsl:if
                                test="eac:dateRange/eac:fromDate[@standardDate[normalize-space(.) != ''] or normalize-space(.) != '']">
                                <xsl:choose>
                                    <xsl:when
                                        test="eac:dateRange/eac:fromDate[normalize-space(@standardDate) != '']">
                                        <piaaf:fromDate type='iso8601'>
                                            <xsl:value-of
                                                select="eac:dateRange/eac:fromDate/@standardDate"/>
                                        </piaaf:fromDate>
                                    </xsl:when>
                                    <xsl:when
                                        test="eac:dateRange/eac:fromDate[normalize-space(.) != '']">
                                        <piaaf:fromDate>
                                            <xsl:value-of
                                                select="normalize-space(eac:dateRange/eac:fromDate)"
                                            />
                                        </piaaf:fromDate>
                                    </xsl:when>
                                </xsl:choose>
                                
                            </xsl:if>
                            <xsl:if
                                test="eac:dateRange/eac:toDate[@standardDate[normalize-space(.) != ''] or normalize-space(.) != '']">
                                <xsl:choose>
                                    <xsl:when
                                        test="eac:dateRange/eac:toDate[normalize-space(@standardDate) != '']">
                                        <piaaf:toDate type='iso8601'>
                                            <xsl:value-of
                                                select="eac:dateRange/eac:toDate/@standardDate"/>
                                        </piaaf:toDate>
                                    </xsl:when>
                                    <xsl:when
                                        test="eac:dateRange/eac:toDate[normalize-space(.) != '']">
                                        <piaaf:toDate>
                                            <xsl:value-of
                                                select="normalize-space(eac:dateRange/eac:toDate)"/>
                                        </piaaf:toDate>
                                    </xsl:when>
                                </xsl:choose>
                            </xsl:if>
                        </piaaf:dateRange>
                    </xsl:if>
                    <xsl:if test="eac:descriptiveNote[normalize-space(.) != '']">
                        <piaaf:note>
                            <xsl:for-each
                                select="eac:descriptiveNote[normalize-space(.) != '']/eac:p[normalize-space(.) != '']">
                                <piaaf:p>
                                    <xsl:copy-of select="node()"/>
                                </piaaf:p>
                            </xsl:for-each>
                        </piaaf:note>
                    </xsl:if>
                </piaaf:rel>
            </xsl:for-each>
        </piaaf:relations>

    </xsl:variable>
    <xsl:variable name="ANAgentsRelTable">

        <piaaf:relations>
           
            <xsl:for-each
                select="$collection-EAC-AN/eac:eac-cpf[not(starts-with(normalize-space(eac:control/eac:recordId), 'FR784'))]/eac:cpfDescription/eac:relations/eac:cpfRelation[@cpfRelationType != 'identity' and not(ends-with(@xlink:arcrole, 'isInstanceOfGenericType'))]">

                <piaaf:rel type="{@cpfRelationType}" xml:id="{@xml:id}">
                    <xsl:if test="@xlink:arcrole">
                        <xsl:attribute name="arcrole">
                            <xsl:value-of
                                select="substring-after(@xlink:arcrole, 'http://www.piaaf.net/ontology/piaaf#')"
                            />
                        </xsl:attribute>
                    </xsl:if>
                    <piaaf:fromAgent>
                        <xsl:value-of select="ancestor::eac:eac-cpf/eac:control/eac:recordId"/>
                    </piaaf:fromAgent>
                    <piaaf:ancAgentType>
                        <xsl:value-of
                            select="ancestor::eac:eac-cpf/eac:cpfDescription/eac:identity/eac:entityType"
                        />
                    </piaaf:ancAgentType>
                    <xsl:if test="normalize-space(@xlink:href) != ''">
                        <piaaf:targetEntity>

                            <xsl:value-of select="normalize-space(@xlink:href)"/>
                        </piaaf:targetEntity>
                    </xsl:if>


                    <piaaf:targetName>
                        <xsl:value-of select="normalize-space(eac:relationEntry)"/>
                    </piaaf:targetName>
                    <!--</xsl:otherwise>
                          
                            
                </xsl:choose>-->

                 <!--   <xsl:if
                        test="eac:date[(@standardDate and normalize-space(@standardDate) != '') or normalize-space(.) != '']">
                        <piaaf:date>
                            <xsl:value-of
                                select="
                                    if (eac:date/@standardDate[normalize-space(.) != ''])
                                    then
                                        (normalize-space(eac:date/@standardDate))
                                    else
                                        (normalize-space(eac:date))
                                    "
                            />
                        </piaaf:date>
                    </xsl:if>
                    <xsl:if test="eac:dateRange">
                        <piaaf:dateRange>
                            <xsl:if
                                test="eac:dateRange/eac:fromDate[@standardDate[normalize-space(.) != ''] or normalize-space(.) != '']">
                                <xsl:choose>
                                    <xsl:when
                                        test="eac:dateRange/eac:fromDate[normalize-space(@standardDate) != '']">
                                        <piaaf:fromDate>
                                            <xsl:value-of
                                                select="eac:dateRange/eac:fromDate/@standardDate"/>
                                        </piaaf:fromDate>
                                    </xsl:when>
                                    <xsl:when
                                        test="eac:dateRange/eac:fromDate[normalize-space(.) != '']">
                                        <piaaf:fromDate>
                                            <xsl:value-of
                                                select="normalize-space(eac:dateRange/eac:fromDate)"
                                            />
                                        </piaaf:fromDate>
                                    </xsl:when>
                                </xsl:choose>

                            </xsl:if>
                            <xsl:if
                                test="eac:dateRange/eac:toDate[@standardDate[normalize-space(.) != ''] or normalize-space(.) != '']">
                                <xsl:choose>
                                    <xsl:when
                                        test="eac:dateRange/eac:toDate[normalize-space(@standardDate) != '']">
                                        <piaaf:toDate>
                                            <xsl:value-of
                                                select="eac:dateRange/eac:toDate/@standardDate"/>
                                        </piaaf:toDate>
                                    </xsl:when>
                                    <xsl:when
                                        test="eac:dateRange/eac:toDate[normalize-space(.) != '']">
                                        <piaaf:toDate>
                                            <xsl:value-of
                                                select="normalize-space(eac:dateRange/eac:toDate)"/>
                                        </piaaf:toDate>
                                    </xsl:when>
                                </xsl:choose>
                            </xsl:if>
                        </piaaf:dateRange>
                    </xsl:if>-->
                    <xsl:if
                        test="eac:date[(@standardDate and normalize-space(@standardDate) != '') or normalize-space(.) != '']">
                        <piaaf:date>
                            <!--<xsl:value-of
                            select="
                            if (eac:date/@standardDate[normalize-space(.) != ''])
                            then
                            (normalize-space(eac:date/@standardDate))
                            else
                            (normalize-space(eac:date))
                            "
                        />-->
                            <xsl:choose>
                                <xsl:when test="eac:date[normalize-space(@standardDate) != '']">
                                    <xsl:attribute name="type">iso8601</xsl:attribute>
                                    <xsl:value-of select="normalize-space(eac:date/@standardDate)"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="normalize-space(eac:date)"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </piaaf:date>
                    </xsl:if>
                    <xsl:if test="eac:dateRange">
                        <piaaf:dateRange>
                            <xsl:if
                                test="eac:dateRange/eac:fromDate[@standardDate[normalize-space(.) != ''] or normalize-space(.) != '']">
                                <xsl:choose>
                                    <xsl:when
                                        test="eac:dateRange/eac:fromDate[normalize-space(@standardDate) != '']">
                                        <piaaf:fromDate type='iso8601'>
                                            <xsl:value-of
                                                select="eac:dateRange/eac:fromDate/@standardDate"/>
                                        </piaaf:fromDate>
                                    </xsl:when>
                                    <xsl:when
                                        test="eac:dateRange/eac:fromDate[normalize-space(.) != '']">
                                        <piaaf:fromDate>
                                            <xsl:value-of
                                                select="normalize-space(eac:dateRange/eac:fromDate)"
                                            />
                                        </piaaf:fromDate>
                                    </xsl:when>
                                </xsl:choose>
                                
                            </xsl:if>
                            <xsl:if
                                test="eac:dateRange/eac:toDate[@standardDate[normalize-space(.) != ''] or normalize-space(.) != '']">
                                <xsl:choose>
                                    <xsl:when
                                        test="eac:dateRange/eac:toDate[normalize-space(@standardDate) != '']">
                                        <piaaf:toDate type='iso8601'>
                                            <xsl:value-of
                                                select="eac:dateRange/eac:toDate/@standardDate"/>
                                        </piaaf:toDate>
                                    </xsl:when>
                                    <xsl:when
                                        test="eac:dateRange/eac:toDate[normalize-space(.) != '']">
                                        <piaaf:toDate>
                                            <xsl:value-of
                                                select="normalize-space(eac:dateRange/eac:toDate)"/>
                                        </piaaf:toDate>
                                    </xsl:when>
                                </xsl:choose>
                            </xsl:if>
                        </piaaf:dateRange>
                    </xsl:if>
                    <xsl:if test="eac:descriptiveNote[normalize-space(.) != '']">
                        <piaaf:note>
                            <xsl:for-each
                                select="eac:descriptiveNote[normalize-space(.) != '']/eac:p[normalize-space(.) != '']">
                                <piaaf:p>
                                    <xsl:copy-of select="node()"/>
                                </piaaf:p>
                            </xsl:for-each>
                        </piaaf:note>
                    </xsl:if>
                </piaaf:rel>
            </xsl:for-each>
        </piaaf:relations>

    </xsl:variable>
    <xsl:variable name="SIAFAgentsrelTable_reduced">
        <piaaf:relations>
            <xsl:for-each select="$SIAFAgentsRelTable/piaaf:relations/piaaf:rel">
                <xsl:call-template name="mergeRelationsInFirstTable">
                    <xsl:with-param name="anc" select="piaaf:fromAgent"/>
                    <xsl:with-param name="targ" select="piaaf:targetEntity"/>
                    <xsl:with-param name="tName" select="piaaf:targetName"/>
                    <xsl:with-param name="date" select="piaaf:date"/>
                    <xsl:with-param name="fdate" select="piaaf:dateRange/piaaf:fromDate"/>
                    <xsl:with-param name="tdate" select="piaaf:dateRange/piaaf:toDate"/>
                    <xsl:with-param name="note" select="piaaf:note"/>
                    <xsl:with-param name="theRelId" select="@xml:id"/>
                </xsl:call-template>



            </xsl:for-each>
        </piaaf:relations>
    </xsl:variable>
    <!--<xsl:variable name="ANAgentsRelTable_fixed" select="document('rdf/relations/ANAgentsRelTable-corrige.xml')"/>-->
    <xsl:variable name="ANAgentsrelTable_reduced">
        <piaaf:relations>
            <xsl:for-each select="$ANAgentsRelTable/piaaf:relations/piaaf:rel">
                <xsl:call-template name="mergeRelationsInFirstTable">
                    <xsl:with-param name="anc" select="piaaf:fromAgent"/>
                    <xsl:with-param name="targ" select="piaaf:targetEntity"/>
                    <xsl:with-param name="tName" select="piaaf:targetName"/>
                    <xsl:with-param name="date" select="piaaf:date"/>
                    <xsl:with-param name="fdate" select="piaaf:dateRange/piaaf:fromDate"/>
                    <xsl:with-param name="tdate" select="piaaf:dateRange/piaaf:toDate"/>
                    <xsl:with-param name="note" select="piaaf:note"/>
                    <xsl:with-param name="theRelId" select="@xml:id"/>
                </xsl:call-template>



            </xsl:for-each>
        </piaaf:relations>
    </xsl:variable>
    <xsl:variable name="BNFAgentsrelTable_reduced">
        <piaaf:relations>
            <xsl:for-each select="$BNFAgentsRelTable/piaaf:relations/piaaf:rel">
                <xsl:call-template name="mergeRelationsInFirstTable">
                    <xsl:with-param name="anc" select="piaaf:fromAgent"/>
                    <xsl:with-param name="targ" select="piaaf:targetEntity"/>
                    <xsl:with-param name="tName" select="piaaf:targetName"/>
                    <xsl:with-param name="date" select="piaaf:date"/>
                    <xsl:with-param name="fdate" select="piaaf:dateRange/piaaf:fromDate"/>
                    <xsl:with-param name="tdate" select="piaaf:dateRange/piaaf:toDate"/>
                    <xsl:with-param name="note" select="piaaf:note"/>
                    <xsl:with-param name="theRelId" select="@xml:id"/>
                </xsl:call-template>



            </xsl:for-each>
        </piaaf:relations>
    </xsl:variable>
    <xsl:variable name="SIAFAgentsRelTable_final">
        <piaaf:relations>
            <xsl:for-each select="$SIAFAgentsrelTable_reduced/piaaf:relations/piaaf:rel">
                <xsl:variable name="myId" select="@xml:id"/>
                <xsl:if test="not(preceding::piaaf:rel[@xml:id=$myId])">
                <piaaf:rel>
                    <xsl:copy-of select="attribute::*"/>
                    <xsl:copy-of select="node()[not(self::piaaf:note)]"/>
                    <xsl:choose>
                        <xsl:when test="piaaf:note">
                            <piaaf:note>
                                <xsl:copy-of select="piaaf:note/node()"/>
                                <xsl:if
                                    test="$SIAFAgentsrelTable_reduced/piaaf:relations/piaaf:specialNote[@toBeMovedToRel = $myId]">
                                    <piaaf:p
                                        fromRel="{$SIAFAgentsrelTable_reduced/piaaf:relations/piaaf:specialNote[@toBeMovedToRel=$myId]/@fromRel}">
                                        <xsl:copy-of
                                            select="$SIAFAgentsrelTable_reduced/piaaf:relations/piaaf:specialNote[@toBeMovedToRel = $myId]/piaaf:p/node()"
                                        />
                                    </piaaf:p>
                                </xsl:if>
                            </piaaf:note>
                        </xsl:when>
                        <xsl:when test="not(piaaf:note)">
                            <xsl:if
                                test="$SIAFAgentsrelTable_reduced/piaaf:relations/piaaf:specialNote[@toBeMovedToRel = $myId]">
                                <piaaf:note
                                    fromRel="{$SIAFAgentsrelTable_reduced/piaaf:relations/piaaf:specialNote[@toBeMovedToRel=$myId]/@fromRel}">
                                    <xsl:copy-of
                                        select="$SIAFAgentsrelTable_reduced/piaaf:relations/piaaf:specialNote[@toBeMovedToRel = $myId]/node()"
                                    />
                                </piaaf:note>

                            </xsl:if>
                        </xsl:when>
                    </xsl:choose>

                </piaaf:rel>
                </xsl:if>
            </xsl:for-each>

        </piaaf:relations>
    </xsl:variable>
    
    <xsl:variable name="ANAgentsRelTable_final">
        <piaaf:relations>
            <xsl:for-each select="$ANAgentsrelTable_reduced/piaaf:relations/piaaf:rel">
                <xsl:variable name="myId" select="@xml:id"/>
                <xsl:if test="not(preceding::piaaf:rel[@xml:id=$myId])">
                <piaaf:rel>
                    <xsl:copy-of select="attribute::*"/>
                    <xsl:copy-of select="node()[not(self::piaaf:note)]"/>
                    <xsl:choose>
                        <xsl:when test="piaaf:note">
                            <piaaf:note>
                                <xsl:copy-of select="piaaf:note/node()"/>
                                <xsl:if
                                    test="$ANAgentsrelTable_reduced/piaaf:relations/piaaf:specialNote[@toBeMovedToRel = $myId]">
                                    <piaaf:p
                                        fromRel="{$ANAgentsrelTable_reduced/piaaf:relations/piaaf:specialNote[@toBeMovedToRel=$myId]/@fromRel}">
                                        <xsl:copy-of
                                            select="$ANAgentsrelTable_reduced/piaaf:relations/piaaf:specialNote[@toBeMovedToRel = $myId]/piaaf:p/node()"
                                        />
                                    </piaaf:p>
                                </xsl:if>
                            </piaaf:note>
                        </xsl:when>
                        <xsl:when test="not(piaaf:note)">
                            <xsl:if
                                test="$ANAgentsrelTable_reduced/piaaf:relations/piaaf:specialNote[@toBeMovedToRel = $myId]">
                                <piaaf:note
                                    fromRel="{$ANAgentsrelTable_reduced/piaaf:relations/piaaf:specialNote[@toBeMovedToRel=$myId]/@fromRel}">
                                    <xsl:copy-of
                                        select="$ANAgentsrelTable_reduced/piaaf:relations/piaaf:specialNote[@toBeMovedToRel = $myId]/node()"
                                    />
                                </piaaf:note>

                            </xsl:if>
                        </xsl:when>
                    </xsl:choose>

                </piaaf:rel>
                </xsl:if>
            </xsl:for-each>

        </piaaf:relations>
    </xsl:variable>
    <xsl:variable name="BNAgentsRelTable_final">
        <piaaf:relations>
            <xsl:for-each select="$BNFAgentsrelTable_reduced/piaaf:relations/piaaf:rel">
                <xsl:variable name="myId" select="@xml:id"/>
                <xsl:if test="not(preceding::piaaf:rel[@xml:id=$myId])">
                <piaaf:rel>
                    <xsl:copy-of select="attribute::*"/>
                    <xsl:copy-of select="node()[not(self::piaaf:note)]"/>
                    <xsl:choose>
                        <xsl:when test="piaaf:note">
                            <piaaf:note>
                                <xsl:copy-of select="piaaf:note/node()"/>
                                <xsl:if
                                    test="$BNFAgentsrelTable_reduced/piaaf:relations/piaaf:specialNote[@toBeMovedToRel = $myId]">
                                    <piaaf:p
                                        fromRel="{$BNFAgentsrelTable_reduced/piaaf:relations/piaaf:specialNote[@toBeMovedToRel=$myId]/@fromRel}">
                                        <xsl:copy-of
                                            select="$BNFAgentsrelTable_reduced/piaaf:relations/piaaf:specialNote[@toBeMovedToRel = $myId]/piaaf:p/node()"
                                        />
                                    </piaaf:p>
                                </xsl:if>
                            </piaaf:note>
                        </xsl:when>
                        <xsl:when test="not(piaaf:note)">
                            <xsl:if
                                test="$BNFAgentsrelTable_reduced/piaaf:relations/piaaf:specialNote[@toBeMovedToRel = $myId]">
                                <piaaf:note
                                    fromRel="{$BNFAgentsrelTable_reduced/piaaf:relations/piaaf:specialNote[@toBeMovedToRel=$myId]/@fromRel}">
                                    <xsl:copy-of
                                        select="$BNFAgentsrelTable_reduced/piaaf:relations/piaaf:specialNote[@toBeMovedToRel = $myId]/node()"
                                    />
                                </piaaf:note>

                            </xsl:if>
                        </xsl:when>
                    </xsl:choose>

                </piaaf:rel>
                </xsl:if>
                
            </xsl:for-each>

        </piaaf:relations>
    </xsl:variable>
    <xsl:template match="/piaaf:vide">
        <xsl:result-document href="rdf/relations/SIAFAgentsRelTable.xml" indent="yes">

            <xsl:copy-of select="$SIAFAgentsRelTable/*"/>

        </xsl:result-document>
        <xsl:result-document href="rdf/relations/SIAFAgentsRelTable_reduced.xml" indent="yes">
            <xsl:copy-of select="$SIAFAgentsrelTable_reduced/*"/>
        </xsl:result-document>
        <xsl:result-document href="rdf/relations/SIAFAgentsRelTable_final.xml" indent="yes">
            <xsl:copy-of select="$SIAFAgentsRelTable_final/*"/>
        </xsl:result-document>
        <xsl:result-document href="rdf/relations/ANAgentsRelTable.xml" indent="yes">

            <xsl:copy-of select="$ANAgentsRelTable/*"/>

        </xsl:result-document>
        <xsl:result-document href="rdf/relations/ANAgentsRelTable_reduced.xml" indent="yes">
            <xsl:copy-of select="$ANAgentsrelTable_reduced/*"/>
        </xsl:result-document>
        <xsl:result-document href="rdf/relations/ANAgentsRelTable_final.xml" indent="yes">
            <xsl:copy-of select="$ANAgentsRelTable_final/*"/>
        </xsl:result-document>
        <xsl:result-document href="rdf/relations/BNFAgentsRelTable.xml" indent="yes">

            <xsl:copy-of select="$BNFAgentsRelTable/*"/>

        </xsl:result-document>
        <xsl:result-document href="rdf/relations/BNFAgentsRelTable_reduced.xml" indent="yes">
            <xsl:copy-of select="$BNFAgentsrelTable_reduced/*"/>
        </xsl:result-document>
        <xsl:result-document href="rdf/relations/BNFAgentsRelTable_final.xml" indent="yes">
            <xsl:copy-of select="$BNAgentsRelTable_final/*"/>
        </xsl:result-document>
        <xsl:result-document href="rdf/relations/FRSIAF_relations-between-agents.rdf" method="xml"
            encoding="utf-8" indent="yes">
          <!--  <rdf:RDF xmlns:dc="http://purl.org/dc/elements/1.1/"
                xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                xmlns:owl="http://www.w3.org/2002/07/owl#"
                xmlns:RiC="http://www.ica.org/standards/RiC/ontology#"
                
                xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
                xmlns:piaaf-onto="http://www.piaaf.net/ontology#"
                xmlns:skos="http://www.w3.org/2004/02/skos/core#"
                >-->
            <rdf:RDF 
                xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                xmlns:owl="http://www.w3.org/2002/07/owl#"
                xmlns:RiC="http://www.ica.org/standards/RiC/ontology#"
                
                xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
                
                
                >


                <xsl:for-each select="$SIAFAgentsRelTable_final/piaaf:relations/piaaf:rel">
                    <xsl:call-template name="output-relations-between-agents">
                        <xsl:with-param name="coll">FRSIAF</xsl:with-param>
                        <xsl:with-param name="fromAgent" select="piaaf:fromAgent"/>
                        <xsl:with-param name="ancAgentType" select="piaaf:ancAgentType"/>
                        <xsl:with-param name="tEntity" select="piaaf:targetEntity"/>
                        <xsl:with-param name="tName" select="piaaf:targetName"/>
                    </xsl:call-template>

                </xsl:for-each>



            </rdf:RDF>
        </xsl:result-document>

        <xsl:result-document href="rdf/relations/FRAN_relations-between-agents.rdf" method="xml" encoding="utf-8"
            indent="yes">
            <rdf:RDF 
                xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                xmlns:owl="http://www.w3.org/2002/07/owl#"
                xmlns:RiC="http://www.ica.org/standards/RiC/ontology#"
                
                xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
                
                
                >
                



                <xsl:for-each select="$ANAgentsRelTable_final/piaaf:relations/piaaf:rel">
                    <xsl:call-template name="output-relations-between-agents">
                        <xsl:with-param name="coll">FRAN</xsl:with-param>
                        <xsl:with-param name="fromAgent" select="piaaf:fromAgent"/>
                        <xsl:with-param name="ancAgentType" select="piaaf:ancAgentType"/>
                        <xsl:with-param name="tEntity" select="piaaf:targetEntity"/>
                        <xsl:with-param name="tName" select="piaaf:targetName"/>
                    </xsl:call-template>

                </xsl:for-each>



            </rdf:RDF>
        </xsl:result-document>
        <xsl:result-document href="rdf/relations/FRBNF_relations-between-agents.rdf" method="xml" encoding="utf-8"
            indent="yes">
            <rdf:RDF 
                xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                xmlns:owl="http://www.w3.org/2002/07/owl#"
                xmlns:RiC="http://www.ica.org/standards/RiC/ontology#"
                
                xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
                
                
                >
                


                <xsl:for-each select="$BNAgentsRelTable_final/piaaf:relations/piaaf:rel">
                    <xsl:call-template name="output-relations-between-agents">
                        <xsl:with-param name="coll">FRBNF</xsl:with-param>
                        <xsl:with-param name="fromAgent" select="piaaf:fromAgent"/>
                        <xsl:with-param name="ancAgentType" select="piaaf:ancAgentType"/>
                        <xsl:with-param name="tEntity" select="piaaf:targetEntity"/>
                        <xsl:with-param name="tName" select="piaaf:targetName"/>
                    </xsl:call-template>

                </xsl:for-each>



            </rdf:RDF>
        </xsl:result-document>
    </xsl:template>

    <!-- template pour fusionner les relations qui doivent l'être -->
    <xsl:template name="mergeRelationsInFirstTable">
        <xsl:param name="anc"/>
        <xsl:param name="date"/>
        <xsl:param name="fdate"/>
        <xsl:param name="tdate"/>
        <xsl:param name="note"/>
        <xsl:param name="tName"/>
        <xsl:param name="targ"/>
        <xsl:param name="theRelId"/>

        <xsl:choose>

            <xsl:when test="boolean($targ) = false()">

                <xsl:copy-of select="."/>
            </xsl:when>
            <xsl:when
                test="boolean($targ) and (not($collection-EAC-SIAF/eac:eac-cpf[eac:control/eac:recordId = $targ]) and not($collection-EAC-BnF/eac:eac-cpf[eac:control/eac:recordId = $targ]) and not($collection-EAC-AN/eac:eac-cpf[eac:control/eac:recordId = $targ]))">

                <xsl:copy-of select="."/>
            </xsl:when>
            <xsl:when
                test="$collection-EAC-SIAF/eac:eac-cpf[eac:control/eac:recordId = $targ] | $collection-EAC-BnF/eac:eac-cpf[eac:control/eac:recordId = $targ] | $collection-EAC-AN/eac:eac-cpf[eac:control/eac:recordId = $targ]">


                <!-- on ne va garder que les relations dans un sens, sinon on aura des doublons -->
                <xsl:choose>
                    <xsl:when test="@arcrole = 'knows'">
                        <xsl:copy-of select="."/>
                    </xsl:when>
                    <xsl:when test="@arcrole = 'isAssociatedWithForItsControl'">
                        <xsl:copy-of select="."/>
                    </xsl:when>
                    <xsl:when test="@arcrole = 'isControlledBy'">
                        <xsl:copy-of select="."/>
                    </xsl:when>
                    <xsl:when test="@arcrole = 'controls'">
                        <xsl:choose>
                            <xsl:when
                                test="
                                    parent::piaaf:relations/piaaf:rel[@arcrole = 'isControlledBy' and piaaf:fromAgent = $targ and piaaf:targetEntity = $anc]
                                    ">
                                <xsl:variable name="theInvRel"
                                    select="parent::piaaf:relations/piaaf:rel[@arcrole = 'isControlledBy' and piaaf:fromAgent = $targ and piaaf:targetEntity = $anc]"/>

                                <xsl:variable select="$theInvRel/@xml:id" name="theInvRelId"/>

                                <xsl:choose>
                                    <xsl:when test="boolean($date)">
                                        <xsl:if test="$theInvRel/piaaf:date = $date">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                        <xsl:comment>notes différentes</xsl:comment>
                                                        <piaaf:specialNote>
                                                            <xsl:attribute name="fromRel">
                                                                <xsl:value-of select="@xml:id"/>
                                                            </xsl:attribute>
                                                            <xsl:attribute name="toBeMovedToRel">
                                                                <xsl:value-of select="$theInvRelId"/>
                                                            </xsl:attribute>
                                                            <xsl:copy-of select="$note/node()"/>
                                                        </piaaf:specialNote>
                                                    </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                    <xsl:if test="not($theInvRel/piaaf:note)">
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                    <xsl:if test="$theInvRel/piaaf:note">
                                                        
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:if>
                                        <xsl:if test="$theInvRel/piaaf:date != $date or boolean($theInvRel/piaaf:date)=false()">
                                            <xsl:copy-of select="."/>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when test="boolean($fdate) and boolean($tdate)">
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate = $fdate and $theInvRel/piaaf:dateRange/piaaf:toDate = $tdate">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                        <xsl:comment>notes différentes</xsl:comment>
                                                        <piaaf:specialNote>
                                                            <xsl:attribute name="fromRel">
                                                                <xsl:value-of select="@xml:id"/>
                                                            </xsl:attribute>
                                                            <xsl:attribute name="toBeMovedToRel">
                                                                <xsl:value-of select="$theInvRel/@xml:id"/>
                                                            </xsl:attribute>
                                                            <xsl:copy-of select="$note/node()"/>
                                                        </piaaf:specialNote>
                                                    </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                    <xsl:if test="not($theInvRel/piaaf:note)">
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                    <xsl:if test="$theInvRel/piaaf:note">
                                                        
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:if>
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate != $fdate or $theInvRel/piaaf:dateRange/piaaf:toDate != $tdate or  boolean($theInvRel/piaaf:dateRange/piaaf:fromDate)=false() or boolean($theInvRel/piaaf:dateRange/piaaf:toDate)=false()">
                                            <xsl:copy-of select="."/>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when test="boolean($fdate) and boolean($tdate) = false()">
                                        
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate = $fdate and not($theInvRel/piaaf:dateRange/piaaf:toDate)">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                        <xsl:comment>notes différentes</xsl:comment>
                                                        <piaaf:specialNote>
                                                            <xsl:attribute name="fromRel">
                                                                <xsl:value-of select="@xml:id"/>
                                                            </xsl:attribute>
                                                            <xsl:attribute name="toBeMovedToRel">
                                                                <xsl:value-of select="$theInvRel/@xml:id"/>
                                                            </xsl:attribute>
                                                            <xsl:copy-of select="$note/node()"/>
                                                        </piaaf:specialNote>
                                                    </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                    <xsl:if test="not($theInvRel/piaaf:note)">
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                    <xsl:if test="$theInvRel/piaaf:note">
                                                        
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:if>
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate != $fdate or boolean($theInvRel/piaaf:dateRange/piaaf:fromDate)=false() or $theInvRel/piaaf:dateRange/piaaf:toDate">
                                            <xsl:copy-of select="."/>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when test="boolean($fdate) = false() and boolean($tdate)">
                                        <xsl:if
                                            test="not($theInvRel/piaaf:dateRange/piaaf:fromDate) and $theInvRel/piaaf:dateRange/piaaf:toDate = $tdate">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                        <xsl:comment>notes différentes</xsl:comment>
                                                        <piaaf:specialNote>
                                                            <xsl:attribute name="fromRel">
                                                                <xsl:value-of select="@xml:id"/>
                                                            </xsl:attribute>
                                                            <xsl:attribute name="toBeMovedToRel">
                                                                <xsl:value-of select="$theInvRel/@xml:id"/>
                                                            </xsl:attribute>
                                                            <xsl:copy-of select="$note/node()"/>
                                                        </piaaf:specialNote>
                                                    </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                    <xsl:if test="not($theInvRel/piaaf:note)">
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                    <xsl:if test="$theInvRel/piaaf:note">
                                                        
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:if>
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate or $theInvRel/piaaf:dateRange/piaaf:toDate != $tdate  or boolean($theInvRel/piaaf:dateRange/piaaf:toDate)=false()">
                                            <xsl:copy-of select="."/>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when
                                        test="(boolean($fdate) = false() and boolean($tdate) = false()) or boolean($date) = false()">
                                        <xsl:choose>
                                            <xsl:when test="boolean($note)">
                                                <xsl:if
                                                    test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                    <piaaf:removedRel relId="{$theRelId}"/>
                                                </xsl:if>
                                                <xsl:if
                                                    test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                    <xsl:comment>notes différentes</xsl:comment>
                                                    
                                                    <xsl:copy-of select="."/>
                                                </xsl:if>
                                            </xsl:when>
                                            <xsl:when test="boolean($note) = false()">
                                                <xsl:copy-of select="."/>
                                            </xsl:when>
                                        </xsl:choose>
                                    </xsl:when>
                                </xsl:choose>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:copy-of select="."/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                    <xsl:when test="@arcrole = 'isDirectorOf'">
                        <xsl:copy-of select="."/>
                    </xsl:when>
                    <xsl:when test="@arcrole = 'isDirectedBy'">
                        <xsl:choose>

                            <xsl:when
                                test="
                                    parent::piaaf:relations/piaaf:rel[@arcrole = 'isDirectorOf' and piaaf:fromAgent = $targ and piaaf:targetEntity = $anc]
                                    ">
                                <xsl:for-each
                                    select="
                                        parent::piaaf:relations/piaaf:rel[@arcrole = 'isDirectorOf' and piaaf:fromAgent = $targ and piaaf:targetEntity = $anc]
                                        ">
                                    <xsl:variable name="theInvRel" select="."/>

                                    <xsl:variable select="$theInvRel/@xml:id" name="theInvRelId"/>

                                    <xsl:choose>
                                        <xsl:when test="boolean($date)">
                                            <xsl:if test="$theInvRel/piaaf:date = $date">
                                                <xsl:choose>
                                                    <xsl:when test="boolean($note)">
                                                        <xsl:if
                                                            test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                            <piaaf:removedRel relId="{$theRelId}"/>
                                                        </xsl:if>
                                                        <xsl:if
                                                            test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                            <xsl:comment>notes différentes</xsl:comment>
                                                            <piaaf:specialNote>
                                                                <xsl:attribute name="fromRel">
                                                                    <xsl:value-of select="@xml:id"/>
                                                                </xsl:attribute>
                                                                <xsl:attribute name="toBeMovedToRel">
                                                                    <xsl:value-of select="$theInvRelId"/>
                                                                </xsl:attribute>
                                                                <xsl:copy-of select="$note/node()"/>
                                                            </piaaf:specialNote>
                                                        </xsl:if>
                                                    </xsl:when>
                                                    <xsl:when test="boolean($note) = false()">
                                                        <xsl:if test="not($theInvRel/piaaf:note)">
                                                            <piaaf:removedRel relId="{$theRelId}"/>
                                                        </xsl:if>
                                                        <xsl:if test="$theInvRel/piaaf:note">
                                                            
                                                            <piaaf:removedRel relId="{$theRelId}"/>
                                                        </xsl:if>
                                                    </xsl:when>
                                                </xsl:choose>
                                            </xsl:if>
                                            <xsl:if test="$theInvRel/piaaf:date != $date or boolean($theInvRel/piaaf:date)=false()">
                                                <xsl:copy-of select="."/>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:when test="boolean($fdate) and boolean($tdate)">
                                            <xsl:if
                                                test="$theInvRel/piaaf:dateRange/piaaf:fromDate = $fdate and $theInvRel/piaaf:dateRange/piaaf:toDate = $tdate">
                                                <xsl:choose>
                                                    <xsl:when test="boolean($note)">
                                                        <xsl:if
                                                            test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                            <piaaf:removedRel relId="{$theRelId}"/>
                                                        </xsl:if>
                                                        <xsl:if
                                                            test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                            <xsl:comment>notes différentes</xsl:comment>
                                                            <piaaf:specialNote>
                                                                <xsl:attribute name="fromRel">
                                                                    <xsl:value-of select="@xml:id"/>
                                                                </xsl:attribute>
                                                                <xsl:attribute name="toBeMovedToRel">
                                                                    <xsl:value-of select="$theInvRel/@xml:id"/>
                                                                </xsl:attribute>
                                                                <xsl:copy-of select="$note/node()"/>
                                                            </piaaf:specialNote>
                                                        </xsl:if>
                                                    </xsl:when>
                                                    <xsl:when test="boolean($note) = false()">
                                                        <xsl:if test="not($theInvRel/piaaf:note)">
                                                            <piaaf:removedRel relId="{$theRelId}"/>
                                                        </xsl:if>
                                                        <xsl:if test="$theInvRel/piaaf:note">
                                                            
                                                            <piaaf:removedRel relId="{$theRelId}"/>
                                                        </xsl:if>
                                                    </xsl:when>
                                                </xsl:choose>
                                            </xsl:if>
                                            <xsl:if
                                                test="$theInvRel/piaaf:dateRange/piaaf:fromDate != $fdate or $theInvRel/piaaf:dateRange/piaaf:toDate != $tdate or  boolean($theInvRel/piaaf:dateRange/piaaf:fromDate)=false() or boolean($theInvRel/piaaf:dateRange/piaaf:toDate)=false()">
                                                <xsl:copy-of select="."/>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:when test="boolean($fdate) and boolean($tdate) = false()">
                                            
                                            <xsl:if
                                                test="$theInvRel/piaaf:dateRange/piaaf:fromDate = $fdate and not($theInvRel/piaaf:dateRange/piaaf:toDate)">
                                                <xsl:choose>
                                                    <xsl:when test="boolean($note)">
                                                        <xsl:if
                                                            test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                            <piaaf:removedRel relId="{$theRelId}"/>
                                                        </xsl:if>
                                                        <xsl:if
                                                            test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                            <xsl:comment>notes différentes</xsl:comment>
                                                            <piaaf:specialNote>
                                                                <xsl:attribute name="fromRel">
                                                                    <xsl:value-of select="@xml:id"/>
                                                                </xsl:attribute>
                                                                <xsl:attribute name="toBeMovedToRel">
                                                                    <xsl:value-of select="$theInvRel/@xml:id"/>
                                                                </xsl:attribute>
                                                                <xsl:copy-of select="$note/node()"/>
                                                            </piaaf:specialNote>
                                                        </xsl:if>
                                                    </xsl:when>
                                                    <xsl:when test="boolean($note) = false()">
                                                        <xsl:if test="not($theInvRel/piaaf:note)">
                                                            <piaaf:removedRel relId="{$theRelId}"/>
                                                        </xsl:if>
                                                        <xsl:if test="$theInvRel/piaaf:note">
                                                            
                                                            <piaaf:removedRel relId="{$theRelId}"/>
                                                        </xsl:if>
                                                    </xsl:when>
                                                </xsl:choose>
                                            </xsl:if>
                                            <xsl:if
                                                test="$theInvRel/piaaf:dateRange/piaaf:fromDate != $fdate or boolean($theInvRel/piaaf:dateRange/piaaf:fromDate)=false() or $theInvRel/piaaf:dateRange/piaaf:toDate">
                                                <xsl:copy-of select="."/>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:when test="boolean($fdate) = false() and boolean($tdate)">
                                            <xsl:if
                                                test="not($theInvRel/piaaf:dateRange/piaaf:fromDate) and $theInvRel/piaaf:dateRange/piaaf:toDate = $tdate">
                                                <xsl:choose>
                                                    <xsl:when test="boolean($note)">
                                                        <xsl:if
                                                            test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                            <piaaf:removedRel relId="{$theRelId}"/>
                                                        </xsl:if>
                                                        <xsl:if
                                                            test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                            <xsl:comment>notes différentes</xsl:comment>
                                                            <piaaf:specialNote>
                                                                <xsl:attribute name="fromRel">
                                                                    <xsl:value-of select="@xml:id"/>
                                                                </xsl:attribute>
                                                                <xsl:attribute name="toBeMovedToRel">
                                                                    <xsl:value-of select="$theInvRel/@xml:id"/>
                                                                </xsl:attribute>
                                                                <xsl:copy-of select="$note/node()"/>
                                                            </piaaf:specialNote>
                                                        </xsl:if>
                                                    </xsl:when>
                                                    <xsl:when test="boolean($note) = false()">
                                                        <xsl:if test="not($theInvRel/piaaf:note)">
                                                            <piaaf:removedRel relId="{$theRelId}"/>
                                                        </xsl:if>
                                                        <xsl:if test="$theInvRel/piaaf:note">
                                                            
                                                            <piaaf:removedRel relId="{$theRelId}"/>
                                                        </xsl:if>
                                                    </xsl:when>
                                                </xsl:choose>
                                            </xsl:if>
                                            <xsl:if
                                                test="$theInvRel/piaaf:dateRange/piaaf:fromDate or $theInvRel/piaaf:dateRange/piaaf:toDate != $tdate  or boolean($theInvRel/piaaf:dateRange/piaaf:toDate)=false()">
                                                <xsl:copy-of select="."/>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:when
                                            test="(boolean($fdate) = false() and boolean($tdate) = false()) or boolean($date) = false()">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                        <xsl:comment>notes différentes</xsl:comment>
                                                        
                                                        <xsl:copy-of select="."/>
                                                    </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                    <xsl:copy-of select="."/>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:when>
                                    </xsl:choose>
                                </xsl:for-each>


                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:copy-of select="."/>
                            </xsl:otherwise>

                        </xsl:choose>
                    </xsl:when>
                    <xsl:when test="@arcrole = 'isPartOf'">
                        <xsl:copy-of select="."/>
                    </xsl:when>
                    <xsl:when test="@arcrole = 'hasPart'">
                        <xsl:choose>
                            <xsl:when
                                test="
                                    parent::piaaf:relations/piaaf:rel[@arcrole = 'isPartOf' and piaaf:fromAgent = $targ and piaaf:targetEntity = $anc]
                                    ">

                                <xsl:variable name="theInvRel"
                                    select="parent::piaaf:relations/piaaf:rel[@arcrole = 'isPartOf' and piaaf:fromAgent = $targ and piaaf:targetEntity = $anc]"/>

                                <xsl:variable select="$theInvRel/@xml:id" name="theInvRelId"/>

                                <xsl:choose>
                                    <xsl:when test="boolean($date)">
                                        <xsl:if test="$theInvRel/piaaf:date = $date">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                        <xsl:comment>notes différentes</xsl:comment>
                                                        <piaaf:specialNote>
                                                            <xsl:attribute name="fromRel">
                                                                <xsl:value-of select="@xml:id"/>
                                                            </xsl:attribute>
                                                            <xsl:attribute name="toBeMovedToRel">
                                                                <xsl:value-of select="$theInvRelId"/>
                                                            </xsl:attribute>
                                                            <xsl:copy-of select="$note/node()"/>
                                                        </piaaf:specialNote>
                                                    </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                    <xsl:if test="not($theInvRel/piaaf:note)">
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                    <xsl:if test="$theInvRel/piaaf:note">
                                                        
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:if>
                                        <xsl:if test="$theInvRel/piaaf:date != $date or boolean($theInvRel/piaaf:date)=false()">
                                            <xsl:copy-of select="."/>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when test="boolean($fdate) and boolean($tdate)">
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate = $fdate and $theInvRel/piaaf:dateRange/piaaf:toDate = $tdate">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                        <xsl:comment>notes différentes</xsl:comment>
                                                        <piaaf:specialNote>
                                                            <xsl:attribute name="fromRel">
                                                                <xsl:value-of select="@xml:id"/>
                                                            </xsl:attribute>
                                                            <xsl:attribute name="toBeMovedToRel">
                                                                <xsl:value-of select="$theInvRel/@xml:id"/>
                                                            </xsl:attribute>
                                                            <xsl:copy-of select="$note/node()"/>
                                                        </piaaf:specialNote>
                                                    </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                    <xsl:if test="not($theInvRel/piaaf:note)">
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                    <xsl:if test="$theInvRel/piaaf:note">
                                                        
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:if>
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate != $fdate or $theInvRel/piaaf:dateRange/piaaf:toDate != $tdate or  boolean($theInvRel/piaaf:dateRange/piaaf:fromDate)=false() or boolean($theInvRel/piaaf:dateRange/piaaf:toDate)=false()">
                                            <xsl:copy-of select="."/>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when test="boolean($fdate) and boolean($tdate) = false()">
                                        
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate = $fdate and not($theInvRel/piaaf:dateRange/piaaf:toDate)">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                        <xsl:comment>notes différentes</xsl:comment>
                                                        <piaaf:specialNote>
                                                            <xsl:attribute name="fromRel">
                                                                <xsl:value-of select="@xml:id"/>
                                                            </xsl:attribute>
                                                            <xsl:attribute name="toBeMovedToRel">
                                                                <xsl:value-of select="$theInvRel/@xml:id"/>
                                                            </xsl:attribute>
                                                            <xsl:copy-of select="$note/node()"/>
                                                        </piaaf:specialNote>
                                                    </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                    <xsl:if test="not($theInvRel/piaaf:note)">
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                    <xsl:if test="$theInvRel/piaaf:note">
                                                        
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:if>
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate != $fdate or boolean($theInvRel/piaaf:dateRange/piaaf:fromDate)=false() or $theInvRel/piaaf:dateRange/piaaf:toDate">
                                            <xsl:copy-of select="."/>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when test="boolean($fdate) = false() and boolean($tdate)">
                                        <xsl:if
                                            test="not($theInvRel/piaaf:dateRange/piaaf:fromDate) and $theInvRel/piaaf:dateRange/piaaf:toDate = $tdate">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                        <xsl:comment>notes différentes</xsl:comment>
                                                        <piaaf:specialNote>
                                                            <xsl:attribute name="fromRel">
                                                                <xsl:value-of select="@xml:id"/>
                                                            </xsl:attribute>
                                                            <xsl:attribute name="toBeMovedToRel">
                                                                <xsl:value-of select="$theInvRel/@xml:id"/>
                                                            </xsl:attribute>
                                                            <xsl:copy-of select="$note/node()"/>
                                                        </piaaf:specialNote>
                                                    </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                    <xsl:if test="not($theInvRel/piaaf:note)">
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                    <xsl:if test="$theInvRel/piaaf:note">
                                                        
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:if>
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate or $theInvRel/piaaf:dateRange/piaaf:toDate != $tdate  or boolean($theInvRel/piaaf:dateRange/piaaf:toDate)=false()">
                                            <xsl:copy-of select="."/>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when
                                        test="(boolean($fdate) = false() and boolean($tdate) = false()) or boolean($date) = false()">
                                        <xsl:choose>
                                            <xsl:when test="boolean($note)">
                                                <xsl:if
                                                    test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                    <piaaf:removedRel relId="{$theRelId}"/>
                                                </xsl:if>
                                                <xsl:if
                                                    test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                    <xsl:comment>notes différentes</xsl:comment>
                                                    
                                                    <xsl:copy-of select="."/>
                                                </xsl:if>
                                            </xsl:when>
                                            <xsl:when test="boolean($note) = false()">
                                                <xsl:copy-of select="."/>
                                            </xsl:when>
                                        </xsl:choose>
                                    </xsl:when>
                                </xsl:choose>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:copy-of select="."/>
                            </xsl:otherwise>
                        </xsl:choose>

                    </xsl:when>
                    <xsl:when test="@type = 'temporal-later'">
                        <xsl:copy-of select="."/>
                    </xsl:when>
                    <xsl:when test="@type = 'temporal-earlier'">
                        <xsl:choose>
                            <xsl:when
                                test="
                                    parent::piaaf:relations/piaaf:rel[@type = 'temporal-later' and piaaf:fromAgent = $targ and piaaf:targetEntity = $anc]
                                    ">
                                <xsl:variable name="theInvRel"
                                    select="parent::piaaf:relations/piaaf:rel[@type = 'temporal-later' and piaaf:fromAgent = $targ and piaaf:targetEntity = $anc]"/>

                                <xsl:variable select="$theInvRel/@xml:id" name="theInvRelId"/>

                                <xsl:choose>
                                    <xsl:when test="boolean($date)">
                                        <xsl:if test="$theInvRel/piaaf:date = $date">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                        <xsl:comment>notes différentes</xsl:comment>
                                                        <piaaf:specialNote>
                                                            <xsl:attribute name="fromRel">
                                                                <xsl:value-of select="@xml:id"/>
                                                            </xsl:attribute>
                                                            <xsl:attribute name="toBeMovedToRel">
                                                                <xsl:value-of select="$theInvRelId"/>
                                                            </xsl:attribute>
                                                            <xsl:copy-of select="$note/node()"/>
                                                        </piaaf:specialNote>
                                                    </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                    <xsl:if test="not($theInvRel/piaaf:note)">
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                    <xsl:if test="$theInvRel/piaaf:note">
                                                        
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:if>
                                        <xsl:if test="$theInvRel/piaaf:date != $date or boolean($theInvRel/piaaf:date)=false()">
                                            <xsl:copy-of select="."/>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when test="boolean($fdate) and boolean($tdate)">
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate = $fdate and $theInvRel/piaaf:dateRange/piaaf:toDate = $tdate">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                        <xsl:comment>notes différentes</xsl:comment>
                                                        <piaaf:specialNote>
                                                            <xsl:attribute name="fromRel">
                                                                <xsl:value-of select="@xml:id"/>
                                                            </xsl:attribute>
                                                            <xsl:attribute name="toBeMovedToRel">
                                                                <xsl:value-of select="$theInvRel/@xml:id"/>
                                                            </xsl:attribute>
                                                            <xsl:copy-of select="$note/node()"/>
                                                        </piaaf:specialNote>
                                                    </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                    <xsl:if test="not($theInvRel/piaaf:note)">
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                    <xsl:if test="$theInvRel/piaaf:note">
                                                        
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:if>
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate != $fdate or $theInvRel/piaaf:dateRange/piaaf:toDate != $tdate or  boolean($theInvRel/piaaf:dateRange/piaaf:fromDate)=false() or boolean($theInvRel/piaaf:dateRange/piaaf:toDate)=false()">
                                            <xsl:copy-of select="."/>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when test="boolean($fdate) and boolean($tdate) = false()">
                                        
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate = $fdate and not($theInvRel/piaaf:dateRange/piaaf:toDate)">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                        <xsl:comment>notes différentes</xsl:comment>
                                                        <piaaf:specialNote>
                                                            <xsl:attribute name="fromRel">
                                                                <xsl:value-of select="@xml:id"/>
                                                            </xsl:attribute>
                                                            <xsl:attribute name="toBeMovedToRel">
                                                                <xsl:value-of select="$theInvRel/@xml:id"/>
                                                            </xsl:attribute>
                                                            <xsl:copy-of select="$note/node()"/>
                                                        </piaaf:specialNote>
                                                    </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                    <xsl:if test="not($theInvRel/piaaf:note)">
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                    <xsl:if test="$theInvRel/piaaf:note">
                                                        
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:if>
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate != $fdate or boolean($theInvRel/piaaf:dateRange/piaaf:fromDate)=false() or $theInvRel/piaaf:dateRange/piaaf:toDate">
                                            <xsl:copy-of select="."/>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when test="boolean($fdate) = false() and boolean($tdate)">
                                        <xsl:if
                                            test="not($theInvRel/piaaf:dateRange/piaaf:fromDate) and $theInvRel/piaaf:dateRange/piaaf:toDate = $tdate">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                        <xsl:comment>notes différentes</xsl:comment>
                                                        <piaaf:specialNote>
                                                            <xsl:attribute name="fromRel">
                                                                <xsl:value-of select="@xml:id"/>
                                                            </xsl:attribute>
                                                            <xsl:attribute name="toBeMovedToRel">
                                                                <xsl:value-of select="$theInvRel/@xml:id"/>
                                                            </xsl:attribute>
                                                            <xsl:copy-of select="$note/node()"/>
                                                        </piaaf:specialNote>
                                                    </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                    <xsl:if test="not($theInvRel/piaaf:note)">
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                    <xsl:if test="$theInvRel/piaaf:note">
                                                        
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:if>
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate or $theInvRel/piaaf:dateRange/piaaf:toDate != $tdate  or boolean($theInvRel/piaaf:dateRange/piaaf:toDate)=false()">
                                            <xsl:copy-of select="."/>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when
                                        test="(boolean($fdate) = false() and boolean($tdate) = false()) or boolean($date) = false()">
                                        <xsl:choose>
                                            <xsl:when test="boolean($note)">
                                                <xsl:if
                                                    test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                    <piaaf:removedRel relId="{$theRelId}"/>
                                                </xsl:if>
                                                <xsl:if
                                                    test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                    <xsl:comment>notes différentes</xsl:comment>
                                                    
                                                    <xsl:copy-of select="."/>
                                                </xsl:if>
                                            </xsl:when>
                                            <xsl:when test="boolean($note) = false()">
                                                <xsl:copy-of select="."/>
                                            </xsl:when>
                                        </xsl:choose>
                                    </xsl:when>
                                </xsl:choose>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:copy-of select="."/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                    <xsl:when test="@arcrole = 'isFunctionallyLinkedTo'">

                        <xsl:choose>
                            <xsl:when
                                test="preceding-sibling::piaaf:rel[@arcrole = 'isFunctionallyLinkedTo' and piaaf:fromAgent = $targ and piaaf:targetEntity = $anc] | preceding-sibling::piaaf:rel[@arcrole = 'isFunctionallyLinkedTo' and piaaf:fromAgent = $anc and piaaf:targetEntity = $targ]">
                                
                                <xsl:for-each
                                    select="preceding-sibling::piaaf:rel[@type = 'isFunctionallyLinkedTo' and (not(@arcrole) or @arcrole = '') and piaaf:fromAgent = $targ and piaaf:targetEntity = $anc] |  preceding-sibling::piaaf:rel[@type = 'isFunctionallyLinkedTo' and (not(@arcrole) or @arcrole = '') and piaaf:fromAgent = $anc and piaaf:targetEntity = $targ]">
                                
                                
                                <xsl:variable name="theInvRel"
                                    select="."/>

                                <xsl:variable select="$theInvRel/@xml:id" name="theInvRelId"/>
                                <!--  <xsl:comment> voir relation </xsl:comment>
                                       <xsl:value-of select="$theInvRelId"/>-->
                                <xsl:choose>
                                    
                                    <xsl:when test="boolean($date)">
                                        <xsl:if test="$theInvRel/piaaf:date = $date">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                        <xsl:comment>notes différentes</xsl:comment>
                                                        <piaaf:specialNote>
                                                            <xsl:attribute name="fromRel">
                                                                <xsl:value-of select="@xml:id"/>
                                                            </xsl:attribute>
                                                            <xsl:attribute name="toBeMovedToRel">
                                                                <xsl:value-of select="$theInvRelId"/>
                                                            </xsl:attribute>
                                                            <xsl:copy-of select="$note/node()"/>
                                                        </piaaf:specialNote>
                                                    </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                    <xsl:if test="not($theInvRel/piaaf:note)">
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                    <xsl:if test="$theInvRel/piaaf:note">
                                                        
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:if>
                                        <xsl:if test="$theInvRel/piaaf:date != $date or boolean($theInvRel/piaaf:date)=false()">
                                            <xsl:copy-of select="."/>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when test="boolean($fdate) and boolean($tdate)">
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate = $fdate and $theInvRel/piaaf:dateRange/piaaf:toDate = $tdate">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                        <xsl:comment>notes différentes</xsl:comment>
                                                        <piaaf:specialNote>
                                                            <xsl:attribute name="fromRel">
                                                                <xsl:value-of select="@xml:id"/>
                                                            </xsl:attribute>
                                                            <xsl:attribute name="toBeMovedToRel">
                                                                <xsl:value-of select="$theInvRel/@xml:id"/>
                                                            </xsl:attribute>
                                                            <xsl:copy-of select="$note/node()"/>
                                                        </piaaf:specialNote>
                                                    </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                    <xsl:if test="not($theInvRel/piaaf:note)">
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                    <xsl:if test="$theInvRel/piaaf:note">
                                                        
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:if>
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate != $fdate or $theInvRel/piaaf:dateRange/piaaf:toDate != $tdate or  boolean($theInvRel/piaaf:dateRange/piaaf:fromDate)=false() or boolean($theInvRel/piaaf:dateRange/piaaf:toDate)=false()">
                                            <xsl:copy-of select="."/>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when test="boolean($fdate) and boolean($tdate) = false()">
                                        
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate = $fdate and not($theInvRel/piaaf:dateRange/piaaf:toDate)">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                        <xsl:comment>notes différentes</xsl:comment>
                                                        <piaaf:specialNote>
                                                            <xsl:attribute name="fromRel">
                                                                <xsl:value-of select="@xml:id"/>
                                                            </xsl:attribute>
                                                            <xsl:attribute name="toBeMovedToRel">
                                                                <xsl:value-of select="$theInvRel/@xml:id"/>
                                                            </xsl:attribute>
                                                            <xsl:copy-of select="$note/node()"/>
                                                        </piaaf:specialNote>
                                                    </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                    <xsl:if test="not($theInvRel/piaaf:note)">
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                    <xsl:if test="$theInvRel/piaaf:note">
                                                        
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:if>
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate != $fdate or boolean($theInvRel/piaaf:dateRange/piaaf:fromDate)=false() or $theInvRel/piaaf:dateRange/piaaf:toDate">
                                            <xsl:copy-of select="."/>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when test="boolean($fdate) = false() and boolean($tdate)">
                                        <xsl:if
                                            test="not($theInvRel/piaaf:dateRange/piaaf:fromDate) and $theInvRel/piaaf:dateRange/piaaf:toDate = $tdate">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                        <xsl:comment>notes différentes</xsl:comment>
                                                        <piaaf:specialNote>
                                                            <xsl:attribute name="fromRel">
                                                                <xsl:value-of select="@xml:id"/>
                                                            </xsl:attribute>
                                                            <xsl:attribute name="toBeMovedToRel">
                                                                <xsl:value-of select="$theInvRel/@xml:id"/>
                                                            </xsl:attribute>
                                                            <xsl:copy-of select="$note/node()"/>
                                                        </piaaf:specialNote>
                                                    </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                    <xsl:if test="not($theInvRel/piaaf:note)">
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                    <xsl:if test="$theInvRel/piaaf:note">
                                                        
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:if>
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate or $theInvRel/piaaf:dateRange/piaaf:toDate != $tdate  or boolean($theInvRel/piaaf:dateRange/piaaf:toDate)=false()">
                                            <xsl:copy-of select="."/>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when
                                        test="(boolean($fdate) = false() and boolean($tdate) = false()) or boolean($date) = false()">
                                        <xsl:choose>
                                            <xsl:when test="boolean($note)">
                                                <xsl:if
                                                    test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                    <piaaf:removedRel relId="{$theRelId}"/>
                                                </xsl:if>
                                                <xsl:if
                                                    test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                    <xsl:comment>notes différentes</xsl:comment>
                                                    
                                                    <xsl:copy-of select="."/>
                                                </xsl:if>
                                            </xsl:when>
                                            <xsl:when test="boolean($note) = false()">
                                                <xsl:copy-of select="."/>
                                            </xsl:when>
                                        </xsl:choose>
                                    </xsl:when>
                                </xsl:choose>
                                </xsl:for-each>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:copy-of select="."/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                    <xsl:when test="@type = 'family'">

                        <xsl:choose>
                            <xsl:when
                                test="preceding-sibling::piaaf:rel[@type = 'family' and piaaf:fromAgent = $targ and piaaf:targetEntity = $anc] | preceding-sibling::piaaf:rel[@type = 'family' and piaaf:fromAgent = $anc and piaaf:targetEntity = $targ]">
                                
                                <xsl:variable name="theInvRel"
                                    select="preceding-sibling::piaaf:rel[@arcrole = 'family' and ((piaaf:fromAgent = $targ and piaaf:targetEntity = $anc) or (piaaf:fromAgent = $anc and piaaf:targetEntity = $targ)) ]"/>
                                

                                <xsl:variable select="$theInvRel/@xml:id" name="theInvRelId"/>
                                <!--  <xsl:comment> voir relation </xsl:comment>
                                       <xsl:value-of select="$theInvRelId"/>-->
                                <xsl:choose>
                                    <xsl:when test="boolean($date)">
                                        <xsl:if test="$theInvRel/piaaf:date = $date">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                        <xsl:comment>notes différentes</xsl:comment>
                                                        <piaaf:specialNote>
                                                            <xsl:attribute name="fromRel">
                                                                <xsl:value-of select="@xml:id"/>
                                                            </xsl:attribute>
                                                            <xsl:attribute name="toBeMovedToRel">
                                                                <xsl:value-of select="$theInvRelId"/>
                                                            </xsl:attribute>
                                                            <xsl:copy-of select="$note/node()"/>
                                                        </piaaf:specialNote>
                                                    </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                    <xsl:if test="not($theInvRel/piaaf:note)">
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                    <xsl:if test="$theInvRel/piaaf:note">
                                                        
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:if>
                                        <xsl:if test="$theInvRel/piaaf:date != $date or boolean($theInvRel/piaaf:date)=false()">
                                            <xsl:copy-of select="."/>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when test="boolean($fdate) and boolean($tdate)">
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate = $fdate and $theInvRel/piaaf:dateRange/piaaf:toDate = $tdate">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                        <xsl:comment>notes différentes</xsl:comment>
                                                        <piaaf:specialNote>
                                                            <xsl:attribute name="fromRel">
                                                                <xsl:value-of select="@xml:id"/>
                                                            </xsl:attribute>
                                                            <xsl:attribute name="toBeMovedToRel">
                                                                <xsl:value-of select="$theInvRel/@xml:id"/>
                                                            </xsl:attribute>
                                                            <xsl:copy-of select="$note/node()"/>
                                                        </piaaf:specialNote>
                                                    </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                    <xsl:if test="not($theInvRel/piaaf:note)">
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                    <xsl:if test="$theInvRel/piaaf:note">
                                                        
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:if>
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate != $fdate or $theInvRel/piaaf:dateRange/piaaf:toDate != $tdate or  boolean($theInvRel/piaaf:dateRange/piaaf:fromDate)=false() or boolean($theInvRel/piaaf:dateRange/piaaf:toDate)=false()">
                                            <xsl:copy-of select="."/>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when test="boolean($fdate) and boolean($tdate) = false()">
                                        
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate = $fdate and not($theInvRel/piaaf:dateRange/piaaf:toDate)">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                        <xsl:comment>notes différentes</xsl:comment>
                                                        <piaaf:specialNote>
                                                            <xsl:attribute name="fromRel">
                                                                <xsl:value-of select="@xml:id"/>
                                                            </xsl:attribute>
                                                            <xsl:attribute name="toBeMovedToRel">
                                                                <xsl:value-of select="$theInvRel/@xml:id"/>
                                                            </xsl:attribute>
                                                            <xsl:copy-of select="$note/node()"/>
                                                        </piaaf:specialNote>
                                                    </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                    <xsl:if test="not($theInvRel/piaaf:note)">
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                    <xsl:if test="$theInvRel/piaaf:note">
                                                        
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:if>
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate != $fdate or boolean($theInvRel/piaaf:dateRange/piaaf:fromDate)=false() or $theInvRel/piaaf:dateRange/piaaf:toDate">
                                            <xsl:copy-of select="."/>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when test="boolean($fdate) = false() and boolean($tdate)">
                                        <xsl:if
                                            test="not($theInvRel/piaaf:dateRange/piaaf:fromDate) and $theInvRel/piaaf:dateRange/piaaf:toDate = $tdate">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                        <xsl:comment>notes différentes</xsl:comment>
                                                        <piaaf:specialNote>
                                                            <xsl:attribute name="fromRel">
                                                                <xsl:value-of select="@xml:id"/>
                                                            </xsl:attribute>
                                                            <xsl:attribute name="toBeMovedToRel">
                                                                <xsl:value-of select="$theInvRel/@xml:id"/>
                                                            </xsl:attribute>
                                                            <xsl:copy-of select="$note/node()"/>
                                                        </piaaf:specialNote>
                                                    </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                    <xsl:if test="not($theInvRel/piaaf:note)">
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                    <xsl:if test="$theInvRel/piaaf:note">
                                                        
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:if>
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate or $theInvRel/piaaf:dateRange/piaaf:toDate != $tdate  or boolean($theInvRel/piaaf:dateRange/piaaf:toDate)=false()">
                                            <xsl:copy-of select="."/>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when
                                        test="(boolean($fdate) = false() and boolean($tdate) = false()) or boolean($date) = false()">
                                        <xsl:choose>
                                            <xsl:when test="boolean($note)">
                                                <xsl:if
                                                    test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                    <piaaf:removedRel relId="{$theRelId}"/>
                                                </xsl:if>
                                                <xsl:if
                                                    test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                    <xsl:comment>notes différentes</xsl:comment>
                                                    
                                                    <xsl:copy-of select="."/>
                                                </xsl:if>
                                            </xsl:when>
                                            <xsl:when test="boolean($note) = false()">
                                                <xsl:copy-of select="."/>
                                            </xsl:when>
                                        </xsl:choose>
                                    </xsl:when>
                                </xsl:choose>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:copy-of select="."/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                    <xsl:when test="@arcrole = 'isMemberOf'">
                        <xsl:copy-of select="."/>
                    </xsl:when>
                    <xsl:when test="@arcrole = 'hasMember'">
                        <xsl:choose>

                            <xsl:when
                                test="
                                    parent::piaaf:relations/piaaf:rel[@arcrole = 'isMemberOf' and piaaf:fromAgent = $targ and piaaf:targetEntity = $anc]
                                    ">
                                <xsl:for-each
                                    select="
                                        parent::piaaf:relations/piaaf:rel[@arcrole = 'isMemberOf' and piaaf:fromAgent = $targ and piaaf:targetEntity = $anc]
                                        ">
                                    <xsl:variable name="theInvRel" select="."/>

                                    <xsl:variable select="$theInvRel/@xml:id" name="theInvRelId"/>

                                    <xsl:choose>
                                        <xsl:when test="boolean($date)">
                                            <xsl:if test="$theInvRel/piaaf:date = $date">
                                                <xsl:choose>
                                                    <xsl:when test="boolean($note)">
                                                        <xsl:if
                                                            test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                            <piaaf:removedRel relId="{$theRelId}"/>
                                                        </xsl:if>
                                                        <xsl:if
                                                            test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                            <xsl:comment>notes différentes</xsl:comment>
                                                            <piaaf:specialNote>
                                                                <xsl:attribute name="fromRel">
                                                                    <xsl:value-of select="@xml:id"/>
                                                                </xsl:attribute>
                                                                <xsl:attribute name="toBeMovedToRel">
                                                                    <xsl:value-of select="$theInvRelId"/>
                                                                </xsl:attribute>
                                                                <xsl:copy-of select="$note/node()"/>
                                                            </piaaf:specialNote>
                                                        </xsl:if>
                                                    </xsl:when>
                                                    <xsl:when test="boolean($note) = false()">
                                                        <xsl:if test="not($theInvRel/piaaf:note)">
                                                            <piaaf:removedRel relId="{$theRelId}"/>
                                                        </xsl:if>
                                                        <xsl:if test="$theInvRel/piaaf:note">
                                                            
                                                            <piaaf:removedRel relId="{$theRelId}"/>
                                                        </xsl:if>
                                                    </xsl:when>
                                                </xsl:choose>
                                            </xsl:if>
                                            <xsl:if test="$theInvRel/piaaf:date != $date or boolean($theInvRel/piaaf:date)=false()">
                                                <xsl:copy-of select="."/>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:when test="boolean($fdate) and boolean($tdate)">
                                            <xsl:if
                                                test="$theInvRel/piaaf:dateRange/piaaf:fromDate = $fdate and $theInvRel/piaaf:dateRange/piaaf:toDate = $tdate">
                                                <xsl:choose>
                                                    <xsl:when test="boolean($note)">
                                                        <xsl:if
                                                            test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                            <piaaf:removedRel relId="{$theRelId}"/>
                                                        </xsl:if>
                                                        <xsl:if
                                                            test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                            <xsl:comment>notes différentes</xsl:comment>
                                                            <piaaf:specialNote>
                                                                <xsl:attribute name="fromRel">
                                                                    <xsl:value-of select="@xml:id"/>
                                                                </xsl:attribute>
                                                                <xsl:attribute name="toBeMovedToRel">
                                                                    <xsl:value-of select="$theInvRel/@xml:id"/>
                                                                </xsl:attribute>
                                                                <xsl:copy-of select="$note/node()"/>
                                                            </piaaf:specialNote>
                                                        </xsl:if>
                                                    </xsl:when>
                                                    <xsl:when test="boolean($note) = false()">
                                                        <xsl:if test="not($theInvRel/piaaf:note)">
                                                            <piaaf:removedRel relId="{$theRelId}"/>
                                                        </xsl:if>
                                                        <xsl:if test="$theInvRel/piaaf:note">
                                                            
                                                            <piaaf:removedRel relId="{$theRelId}"/>
                                                        </xsl:if>
                                                    </xsl:when>
                                                </xsl:choose>
                                            </xsl:if>
                                            <xsl:if
                                                test="$theInvRel/piaaf:dateRange/piaaf:fromDate != $fdate or $theInvRel/piaaf:dateRange/piaaf:toDate != $tdate or  boolean($theInvRel/piaaf:dateRange/piaaf:fromDate)=false() or boolean($theInvRel/piaaf:dateRange/piaaf:toDate)=false()">
                                                <xsl:copy-of select="."/>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:when test="boolean($fdate) and boolean($tdate) = false()">
                                            
                                            <xsl:if
                                                test="$theInvRel/piaaf:dateRange/piaaf:fromDate = $fdate and not($theInvRel/piaaf:dateRange/piaaf:toDate)">
                                                <xsl:choose>
                                                    <xsl:when test="boolean($note)">
                                                        <xsl:if
                                                            test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                            <piaaf:removedRel relId="{$theRelId}"/>
                                                        </xsl:if>
                                                        <xsl:if
                                                            test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                            <xsl:comment>notes différentes</xsl:comment>
                                                            <piaaf:specialNote>
                                                                <xsl:attribute name="fromRel">
                                                                    <xsl:value-of select="@xml:id"/>
                                                                </xsl:attribute>
                                                                <xsl:attribute name="toBeMovedToRel">
                                                                    <xsl:value-of select="$theInvRel/@xml:id"/>
                                                                </xsl:attribute>
                                                                <xsl:copy-of select="$note/node()"/>
                                                            </piaaf:specialNote>
                                                        </xsl:if>
                                                    </xsl:when>
                                                    <xsl:when test="boolean($note) = false()">
                                                        <xsl:if test="not($theInvRel/piaaf:note)">
                                                            <piaaf:removedRel relId="{$theRelId}"/>
                                                        </xsl:if>
                                                        <xsl:if test="$theInvRel/piaaf:note">
                                                            
                                                            <piaaf:removedRel relId="{$theRelId}"/>
                                                        </xsl:if>
                                                    </xsl:when>
                                                </xsl:choose>
                                            </xsl:if>
                                            <xsl:if
                                                test="$theInvRel/piaaf:dateRange/piaaf:fromDate != $fdate or boolean($theInvRel/piaaf:dateRange/piaaf:fromDate)=false() or $theInvRel/piaaf:dateRange/piaaf:toDate">
                                                <xsl:copy-of select="."/>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:when test="boolean($fdate) = false() and boolean($tdate)">
                                            <xsl:if
                                                test="not($theInvRel/piaaf:dateRange/piaaf:fromDate) and $theInvRel/piaaf:dateRange/piaaf:toDate = $tdate">
                                                <xsl:choose>
                                                    <xsl:when test="boolean($note)">
                                                        <xsl:if
                                                            test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                            <piaaf:removedRel relId="{$theRelId}"/>
                                                        </xsl:if>
                                                        <xsl:if
                                                            test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                            <xsl:comment>notes différentes</xsl:comment>
                                                            <piaaf:specialNote>
                                                                <xsl:attribute name="fromRel">
                                                                    <xsl:value-of select="@xml:id"/>
                                                                </xsl:attribute>
                                                                <xsl:attribute name="toBeMovedToRel">
                                                                    <xsl:value-of select="$theInvRel/@xml:id"/>
                                                                </xsl:attribute>
                                                                <xsl:copy-of select="$note/node()"/>
                                                            </piaaf:specialNote>
                                                        </xsl:if>
                                                    </xsl:when>
                                                    <xsl:when test="boolean($note) = false()">
                                                        <xsl:if test="not($theInvRel/piaaf:note)">
                                                            <piaaf:removedRel relId="{$theRelId}"/>
                                                        </xsl:if>
                                                        <xsl:if test="$theInvRel/piaaf:note">
                                                            
                                                            <piaaf:removedRel relId="{$theRelId}"/>
                                                        </xsl:if>
                                                    </xsl:when>
                                                </xsl:choose>
                                            </xsl:if>
                                            <xsl:if
                                                test="$theInvRel/piaaf:dateRange/piaaf:fromDate or $theInvRel/piaaf:dateRange/piaaf:toDate != $tdate  or boolean($theInvRel/piaaf:dateRange/piaaf:toDate)=false()">
                                                <xsl:copy-of select="."/>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:when
                                            test="(boolean($fdate) = false() and boolean($tdate) = false()) or boolean($date) = false()">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                        <xsl:comment>notes différentes</xsl:comment>
                                                        
                                                        <xsl:copy-of select="."/>
                                                    </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                    <xsl:copy-of select="."/>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:when>
                                    </xsl:choose>
                                </xsl:for-each>


                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:copy-of select="."/>
                            </xsl:otherwise>

                        </xsl:choose>
                    </xsl:when>
                    <xsl:when test="@arcrole = 'isEmployeeOf'">
                        <xsl:copy-of select="."/>
                    </xsl:when>
                    <xsl:when test="@arcrole = 'hasEmployee'">
                        <xsl:choose>

                            <xsl:when
                                test="
                                    parent::piaaf:relations/piaaf:rel[@arcrole = 'isEmployeeOf' and piaaf:fromAgent = $targ and piaaf:targetEntity = $anc]
                                    ">
                                <xsl:for-each
                                    select="
                                        parent::piaaf:relations/piaaf:rel[@arcrole = 'isEmployeeOf' and piaaf:fromAgent = $targ and piaaf:targetEntity = $anc]
                                        ">
                                    <xsl:variable name="theInvRel" select="."/>

                                    <xsl:variable select="$theInvRel/@xml:id" name="theInvRelId"/>

                                    <xsl:choose>
                                        <xsl:when test="boolean($date)">
                                            <xsl:if test="$theInvRel/piaaf:date = $date">
                                                <xsl:choose>
                                                    <xsl:when test="boolean($note)">
                                                        <xsl:if
                                                            test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                            <piaaf:removedRel relId="{$theRelId}"/>
                                                        </xsl:if>
                                                        <xsl:if
                                                            test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                            <xsl:comment>notes différentes</xsl:comment>
                                                            <piaaf:specialNote>
                                                                <xsl:attribute name="fromRel">
                                                                    <xsl:value-of select="@xml:id"/>
                                                                </xsl:attribute>
                                                                <xsl:attribute name="toBeMovedToRel">
                                                                    <xsl:value-of select="$theInvRelId"/>
                                                                </xsl:attribute>
                                                                <xsl:copy-of select="$note/node()"/>
                                                            </piaaf:specialNote>
                                                        </xsl:if>
                                                    </xsl:when>
                                                    <xsl:when test="boolean($note) = false()">
                                                        <xsl:if test="not($theInvRel/piaaf:note)">
                                                            <piaaf:removedRel relId="{$theRelId}"/>
                                                        </xsl:if>
                                                        <xsl:if test="$theInvRel/piaaf:note">
                                                            
                                                            <piaaf:removedRel relId="{$theRelId}"/>
                                                        </xsl:if>
                                                    </xsl:when>
                                                </xsl:choose>
                                            </xsl:if>
                                            <xsl:if test="$theInvRel/piaaf:date != $date or boolean($theInvRel/piaaf:date)=false()">
                                                <xsl:copy-of select="."/>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:when test="boolean($fdate) and boolean($tdate)">
                                            <xsl:if
                                                test="$theInvRel/piaaf:dateRange/piaaf:fromDate = $fdate and $theInvRel/piaaf:dateRange/piaaf:toDate = $tdate">
                                                <xsl:choose>
                                                    <xsl:when test="boolean($note)">
                                                        <xsl:if
                                                            test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                            <piaaf:removedRel relId="{$theRelId}"/>
                                                        </xsl:if>
                                                        <xsl:if
                                                            test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                            <xsl:comment>notes différentes</xsl:comment>
                                                            <piaaf:specialNote>
                                                                <xsl:attribute name="fromRel">
                                                                    <xsl:value-of select="@xml:id"/>
                                                                </xsl:attribute>
                                                                <xsl:attribute name="toBeMovedToRel">
                                                                    <xsl:value-of select="$theInvRel/@xml:id"/>
                                                                </xsl:attribute>
                                                                <xsl:copy-of select="$note/node()"/>
                                                            </piaaf:specialNote>
                                                        </xsl:if>
                                                    </xsl:when>
                                                    <xsl:when test="boolean($note) = false()">
                                                        <xsl:if test="not($theInvRel/piaaf:note)">
                                                            <piaaf:removedRel relId="{$theRelId}"/>
                                                        </xsl:if>
                                                        <xsl:if test="$theInvRel/piaaf:note">
                                                            
                                                            <piaaf:removedRel relId="{$theRelId}"/>
                                                        </xsl:if>
                                                    </xsl:when>
                                                </xsl:choose>
                                            </xsl:if>
                                            <xsl:if
                                                test="$theInvRel/piaaf:dateRange/piaaf:fromDate != $fdate or $theInvRel/piaaf:dateRange/piaaf:toDate != $tdate or  boolean($theInvRel/piaaf:dateRange/piaaf:fromDate)=false() or boolean($theInvRel/piaaf:dateRange/piaaf:toDate)=false()">
                                                <xsl:copy-of select="."/>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:when test="boolean($fdate) and boolean($tdate) = false()">
                                            
                                            <xsl:if
                                                test="$theInvRel/piaaf:dateRange/piaaf:fromDate = $fdate and not($theInvRel/piaaf:dateRange/piaaf:toDate)">
                                                <xsl:choose>
                                                    <xsl:when test="boolean($note)">
                                                        <xsl:if
                                                            test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                            <piaaf:removedRel relId="{$theRelId}"/>
                                                        </xsl:if>
                                                        <xsl:if
                                                            test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                            <xsl:comment>notes différentes</xsl:comment>
                                                            <piaaf:specialNote>
                                                                <xsl:attribute name="fromRel">
                                                                    <xsl:value-of select="@xml:id"/>
                                                                </xsl:attribute>
                                                                <xsl:attribute name="toBeMovedToRel">
                                                                    <xsl:value-of select="$theInvRel/@xml:id"/>
                                                                </xsl:attribute>
                                                                <xsl:copy-of select="$note/node()"/>
                                                            </piaaf:specialNote>
                                                        </xsl:if>
                                                    </xsl:when>
                                                    <xsl:when test="boolean($note) = false()">
                                                        <xsl:if test="not($theInvRel/piaaf:note)">
                                                            <piaaf:removedRel relId="{$theRelId}"/>
                                                        </xsl:if>
                                                        <xsl:if test="$theInvRel/piaaf:note">
                                                            
                                                            <piaaf:removedRel relId="{$theRelId}"/>
                                                        </xsl:if>
                                                    </xsl:when>
                                                </xsl:choose>
                                            </xsl:if>
                                            <xsl:if
                                                test="$theInvRel/piaaf:dateRange/piaaf:fromDate != $fdate or boolean($theInvRel/piaaf:dateRange/piaaf:fromDate)=false() or $theInvRel/piaaf:dateRange/piaaf:toDate">
                                                <xsl:copy-of select="."/>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:when test="boolean($fdate) = false() and boolean($tdate)">
                                            <xsl:if
                                                test="not($theInvRel/piaaf:dateRange/piaaf:fromDate) and $theInvRel/piaaf:dateRange/piaaf:toDate = $tdate">
                                                <xsl:choose>
                                                    <xsl:when test="boolean($note)">
                                                        <xsl:if
                                                            test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                            <piaaf:removedRel relId="{$theRelId}"/>
                                                        </xsl:if>
                                                        <xsl:if
                                                            test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                            <xsl:comment>notes différentes</xsl:comment>
                                                            <piaaf:specialNote>
                                                                <xsl:attribute name="fromRel">
                                                                    <xsl:value-of select="@xml:id"/>
                                                                </xsl:attribute>
                                                                <xsl:attribute name="toBeMovedToRel">
                                                                    <xsl:value-of select="$theInvRel/@xml:id"/>
                                                                </xsl:attribute>
                                                                <xsl:copy-of select="$note/node()"/>
                                                            </piaaf:specialNote>
                                                        </xsl:if>
                                                    </xsl:when>
                                                    <xsl:when test="boolean($note) = false()">
                                                        <xsl:if test="not($theInvRel/piaaf:note)">
                                                            <piaaf:removedRel relId="{$theRelId}"/>
                                                        </xsl:if>
                                                        <xsl:if test="$theInvRel/piaaf:note">
                                                            
                                                            <piaaf:removedRel relId="{$theRelId}"/>
                                                        </xsl:if>
                                                    </xsl:when>
                                                </xsl:choose>
                                            </xsl:if>
                                            <xsl:if
                                                test="$theInvRel/piaaf:dateRange/piaaf:fromDate or $theInvRel/piaaf:dateRange/piaaf:toDate != $tdate  or boolean($theInvRel/piaaf:dateRange/piaaf:toDate)=false()">
                                                <xsl:copy-of select="."/>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:when
                                            test="(boolean($fdate) = false() and boolean($tdate) = false()) or boolean($date) = false()">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                        <xsl:comment>notes différentes</xsl:comment>
                                                        
                                                        <xsl:copy-of select="."/>
                                                    </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                    <xsl:copy-of select="."/>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:when>
                                    </xsl:choose>
                                </xsl:for-each>


                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:copy-of select="."/>
                            </xsl:otherwise>

                        </xsl:choose>
                    </xsl:when>
                    <xsl:when test="@type = 'associative' and (not(@arcrole) or @arcrole = '')">

                        <xsl:choose>
                            <xsl:when
                                test="preceding-sibling::piaaf:rel[@type = 'associative' and (not(@arcrole) or @arcrole = '') and piaaf:fromAgent = $targ and piaaf:targetEntity = $anc] | preceding-sibling::piaaf:rel[@type = 'associative' and (not(@arcrole) or @arcrole = '') and piaaf:fromAgent = $anc and piaaf:targetEntity = $targ]">
                                <xsl:for-each
                                    select="preceding-sibling::piaaf:rel[@type = 'associative' and (not(@arcrole) or @arcrole = '') and piaaf:fromAgent = $targ and piaaf:targetEntity = $anc] |  preceding-sibling::piaaf:rel[@type = 'associative' and (not(@arcrole) or @arcrole = '') and piaaf:fromAgent = $anc and piaaf:targetEntity = $targ]">
                                    <xsl:variable name="theInvRel" select="."/>


                                    <!--  <xsl:comment> voir relation </xsl:comment>
                                       <xsl:value-of select="$theInvRelId"/>-->
                                    <xsl:choose>
                                        <xsl:when test="boolean($date)">
                                            <xsl:if test="$theInvRel/piaaf:date = $date">
                                                <xsl:choose>
                                                    <xsl:when test="boolean($note)">
                                                        <xsl:if
                                                            test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                            <piaaf:removedRel relId="{$theRelId}"/>
                                                        </xsl:if>
                                                        <xsl:if
                                                            test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                            <xsl:comment>notes différentes</xsl:comment>
                                                            <piaaf:specialNote>
                                                                <xsl:attribute name="fromRel">
                                                                    <xsl:value-of select="@xml:id"/>
                                                                </xsl:attribute>
                                                                <xsl:attribute name="toBeMovedToRel">
                                                                    <xsl:value-of select="$theInvRel/@xml:id"/>
                                                                </xsl:attribute>
                                                                <xsl:copy-of select="$note/node()"/>
                                                            </piaaf:specialNote>
                                                        </xsl:if>
                                                    </xsl:when>
                                                    <xsl:when test="boolean($note) = false()">
                                                        <xsl:if test="not($theInvRel/piaaf:note)">
                                                            <piaaf:removedRel relId="{$theRelId}"/>
                                                        </xsl:if>
                                                        <xsl:if test="$theInvRel/piaaf:note">
                                                            
                                                            <piaaf:removedRel relId="{$theRelId}"/>
                                                        </xsl:if>
                                                    </xsl:when>
                                                </xsl:choose>
                                            </xsl:if>
                                            <xsl:if test="$theInvRel/piaaf:date != $date or boolean($theInvRel/piaaf:date)=false()">
                                                <xsl:copy-of select="."/>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:when test="boolean($fdate) and boolean($tdate)">
                                            <xsl:if
                                                test="$theInvRel/piaaf:dateRange/piaaf:fromDate = $fdate and $theInvRel/piaaf:dateRange/piaaf:toDate = $tdate">
                                                <xsl:choose>
                                                    <xsl:when test="boolean($note)">
                                                        <xsl:if
                                                            test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                            <piaaf:removedRel relId="{$theRelId}"/>
                                                        </xsl:if>
                                                        <xsl:if
                                                            test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                            <xsl:comment>notes différentes</xsl:comment>
                                                            <piaaf:specialNote>
                                                                <xsl:attribute name="fromRel">
                                                                    <xsl:value-of select="@xml:id"/>
                                                                </xsl:attribute>
                                                                <xsl:attribute name="toBeMovedToRel">
                                                                    <xsl:value-of select="$theInvRel/@xml:id"/>
                                                                </xsl:attribute>
                                                                <xsl:copy-of select="$note/node()"/>
                                                            </piaaf:specialNote>
                                                        </xsl:if>
                                                    </xsl:when>
                                                    <xsl:when test="boolean($note) = false()">
                                                        <xsl:if test="not($theInvRel/piaaf:note)">
                                                            <piaaf:removedRel relId="{$theRelId}"/>
                                                        </xsl:if>
                                                        <xsl:if test="$theInvRel/piaaf:note">
                                                            
                                                            <piaaf:removedRel relId="{$theRelId}"/>
                                                        </xsl:if>
                                                    </xsl:when>
                                                </xsl:choose>
                                            </xsl:if>
                                            <xsl:if
                                                test="$theInvRel/piaaf:dateRange/piaaf:fromDate != $fdate or $theInvRel/piaaf:dateRange/piaaf:toDate != $tdate or  boolean($theInvRel/piaaf:dateRange/piaaf:fromDate)=false() or boolean($theInvRel/piaaf:dateRange/piaaf:toDate)=false()">
                                                <xsl:copy-of select="."/>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:when test="boolean($fdate) and boolean($tdate) = false()">
                                            
                                            <xsl:if
                                                test="$theInvRel/piaaf:dateRange/piaaf:fromDate = $fdate and not($theInvRel/piaaf:dateRange/piaaf:toDate)">
                                                <xsl:choose>
                                                    <xsl:when test="boolean($note)">
                                                        <xsl:if
                                                            test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                            <piaaf:removedRel relId="{$theRelId}"/>
                                                        </xsl:if>
                                                        <xsl:if
                                                            test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                            <xsl:comment>notes différentes</xsl:comment>
                                                            <piaaf:specialNote>
                                                                <xsl:attribute name="fromRel">
                                                                    <xsl:value-of select="@xml:id"/>
                                                                </xsl:attribute>
                                                                <xsl:attribute name="toBeMovedToRel">
                                                                    <xsl:value-of select="$theInvRel/@xml:id"/>
                                                                </xsl:attribute>
                                                                <xsl:copy-of select="$note/node()"/>
                                                            </piaaf:specialNote>
                                                        </xsl:if>
                                                    </xsl:when>
                                                    <xsl:when test="boolean($note) = false()">
                                                        <xsl:if test="not($theInvRel/piaaf:note)">
                                                            <piaaf:removedRel relId="{$theRelId}"/>
                                                        </xsl:if>
                                                        <xsl:if test="$theInvRel/piaaf:note">
                                                            
                                                            <piaaf:removedRel relId="{$theRelId}"/>
                                                        </xsl:if>
                                                    </xsl:when>
                                                </xsl:choose>
                                            </xsl:if>
                                            <xsl:if
                                                test="$theInvRel/piaaf:dateRange/piaaf:fromDate != $fdate or boolean($theInvRel/piaaf:dateRange/piaaf:fromDate)=false() or $theInvRel/piaaf:dateRange/piaaf:toDate">
                                                <xsl:copy-of select="."/>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:when test="boolean($fdate) = false() and boolean($tdate)">
                                            <xsl:if
                                                test="not($theInvRel/piaaf:dateRange/piaaf:fromDate) and $theInvRel/piaaf:dateRange/piaaf:toDate = $tdate">
                                                <xsl:choose>
                                                    <xsl:when test="boolean($note)">
                                                        <xsl:if
                                                            test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                            <piaaf:removedRel relId="{$theRelId}"/>
                                                        </xsl:if>
                                                        <xsl:if
                                                            test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                            <xsl:comment>notes différentes</xsl:comment>
                                                            <piaaf:specialNote>
                                                                <xsl:attribute name="fromRel">
                                                                    <xsl:value-of select="@xml:id"/>
                                                                </xsl:attribute>
                                                                <xsl:attribute name="toBeMovedToRel">
                                                                    <xsl:value-of select="$theInvRel/@xml:id"/>
                                                                </xsl:attribute>
                                                                <xsl:copy-of select="$note/node()"/>
                                                            </piaaf:specialNote>
                                                        </xsl:if>
                                                    </xsl:when>
                                                    <xsl:when test="boolean($note) = false()">
                                                        <xsl:if test="not($theInvRel/piaaf:note)">
                                                            <piaaf:removedRel relId="{$theRelId}"/>
                                                        </xsl:if>
                                                        <xsl:if test="$theInvRel/piaaf:note">
                                                            
                                                            <piaaf:removedRel relId="{$theRelId}"/>
                                                        </xsl:if>
                                                    </xsl:when>
                                                </xsl:choose>
                                            </xsl:if>
                                            <xsl:if
                                                test="$theInvRel/piaaf:dateRange/piaaf:fromDate or $theInvRel/piaaf:dateRange/piaaf:toDate != $tdate  or boolean($theInvRel/piaaf:dateRange/piaaf:toDate)=false()">
                                                <xsl:copy-of select="."/>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:when
                                            test="(boolean($fdate) = false() and boolean($tdate) = false()) or boolean($date) = false()">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                        <xsl:comment>notes différentes</xsl:comment>
                                                        
                                                        <xsl:copy-of select="."/>
                                                    </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                    <xsl:copy-of select="."/>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:when>
                                    </xsl:choose>
                                </xsl:for-each>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:copy-of select="."/>
                            </xsl:otherwise>
                        </xsl:choose>


                    </xsl:when>
                    <xsl:when test="@type = 'hierarchical-parent' and (not(@arcrole) or @arcrole='')">
                        <xsl:copy-of select="."/>
                    </xsl:when>
                    <xsl:when test="@type = 'hierarchical-child' and (not(@arcrole) or @arcrole='')">
                        <xsl:choose>
                            <xsl:when
                                test="
                                parent::piaaf:relations/piaaf:rel[@type = 'hierarchical-parent' and (not(@arcrole) or @arcrole='') and piaaf:fromAgent = $targ and piaaf:targetEntity = $anc]
                                    ">
                                <xsl:variable name="theInvRel"
                                    select="parent::piaaf:relations/piaaf:rel[@type = 'hierarchical-parent' and (not(@arcrole) or @arcrole='') and piaaf:fromAgent = $targ and piaaf:targetEntity = $anc]"/>

                                <xsl:variable select="$theInvRel/@xml:id" name="theInvRelId"/>

                                <xsl:choose>
                                    <xsl:when test="boolean($date)">
                                        <xsl:if test="$theInvRel/piaaf:date = $date">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                        <xsl:comment>notes différentes</xsl:comment>
                                                        <piaaf:specialNote>
                                                            <xsl:attribute name="fromRel">
                                                                <xsl:value-of select="@xml:id"/>
                                                            </xsl:attribute>
                                                            <xsl:attribute name="toBeMovedToRel">
                                                                <xsl:value-of select="$theInvRel/@xml:id"/>
                                                            </xsl:attribute>
                                                            <xsl:copy-of select="$note/node()"/>
                                                        </piaaf:specialNote>
                                                    </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                    <xsl:if test="not($theInvRel/piaaf:note)">
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                    <xsl:if test="$theInvRel/piaaf:note">
                                                        
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:if>
                                        <xsl:if test="$theInvRel/piaaf:date != $date or boolean($theInvRel/piaaf:date)=false()">
                                            <xsl:copy-of select="."/>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when test="boolean($fdate) and boolean($tdate)">
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate = $fdate and $theInvRel/piaaf:dateRange/piaaf:toDate = $tdate">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                        <xsl:comment>notes différentes</xsl:comment>
                                                        <piaaf:specialNote>
                                                            <xsl:attribute name="fromRel">
                                                                <xsl:value-of select="@xml:id"/>
                                                            </xsl:attribute>
                                                            <xsl:attribute name="toBeMovedToRel">
                                                                <xsl:value-of select="$theInvRel/@xml:id"/>
                                                            </xsl:attribute>
                                                            <xsl:copy-of select="$note/node()"/>
                                                        </piaaf:specialNote>
                                                    </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                    <xsl:if test="not($theInvRel/piaaf:note)">
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                    <xsl:if test="$theInvRel/piaaf:note">
                                                        
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:if>
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate != $fdate or $theInvRel/piaaf:dateRange/piaaf:toDate != $tdate or  boolean($theInvRel/piaaf:dateRange/piaaf:fromDate)=false() or boolean($theInvRel/piaaf:dateRange/piaaf:toDate)=false()">
                                            <xsl:copy-of select="."/>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when test="boolean($fdate) and boolean($tdate) = false()">
                                        
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate = $fdate and not($theInvRel/piaaf:dateRange/piaaf:toDate)">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                        <xsl:comment>notes différentes</xsl:comment>
                                                        <piaaf:specialNote>
                                                            <xsl:attribute name="fromRel">
                                                                <xsl:value-of select="@xml:id"/>
                                                            </xsl:attribute>
                                                            <xsl:attribute name="toBeMovedToRel">
                                                                <xsl:value-of select="$theInvRel/@xml:id"/>
                                                            </xsl:attribute>
                                                            <xsl:copy-of select="$note/node()"/>
                                                        </piaaf:specialNote>
                                                    </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                    <xsl:if test="not($theInvRel/piaaf:note)">
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                    <xsl:if test="$theInvRel/piaaf:note">
                                                        
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:if>
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate != $fdate or boolean($theInvRel/piaaf:dateRange/piaaf:fromDate)=false() or $theInvRel/piaaf:dateRange/piaaf:toDate">
                                            <xsl:copy-of select="."/>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when test="boolean($fdate) = false() and boolean($tdate)">
                                        <xsl:if
                                            test="not($theInvRel/piaaf:dateRange/piaaf:fromDate) and $theInvRel/piaaf:dateRange/piaaf:toDate = $tdate">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                    <xsl:if
                                                        test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                        <xsl:comment>notes différentes</xsl:comment>
                                                        <piaaf:specialNote>
                                                            <xsl:attribute name="fromRel">
                                                                <xsl:value-of select="@xml:id"/>
                                                            </xsl:attribute>
                                                            <xsl:attribute name="toBeMovedToRel">
                                                                <xsl:value-of select="$theInvRel/@xml:id"/>
                                                            </xsl:attribute>
                                                            <xsl:copy-of select="$note/node()"/>
                                                        </piaaf:specialNote>
                                                    </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                    <xsl:if test="not($theInvRel/piaaf:note)">
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                    <xsl:if test="$theInvRel/piaaf:note">
                                                        
                                                        <piaaf:removedRel relId="{$theRelId}"/>
                                                    </xsl:if>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:if>
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate or $theInvRel/piaaf:dateRange/piaaf:toDate != $tdate  or boolean($theInvRel/piaaf:dateRange/piaaf:toDate)=false()">
                                            <xsl:copy-of select="."/>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when
                                        test="(boolean($fdate) = false() and boolean($tdate) = false()) or boolean($date) = false()">
                                        <xsl:choose>
                                            <xsl:when test="boolean($note)">
                                                <xsl:if
                                                    test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                    <piaaf:removedRel relId="{$theRelId}"/>
                                                </xsl:if>
                                                <xsl:if
                                                    test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                    <xsl:comment>notes différentes</xsl:comment>
                                                    
                                                    <xsl:copy-of select="."/>
                                                </xsl:if>
                                            </xsl:when>
                                            <xsl:when test="boolean($note) = false()">
                                                <xsl:copy-of select="."/>
                                            </xsl:when>
                                        </xsl:choose>
                                    </xsl:when>
                                </xsl:choose>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:copy-of select="."/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                </xsl:choose>
            </xsl:when>

        </xsl:choose>
    </xsl:template>
    <xsl:template name="output-relations-between-agents">
        <xsl:param name="tName"/>
        <xsl:param name="fromAgent"/>
        <xsl:param name="ancAgentType"/>
        <xsl:param name="coll"/>
        <xsl:param name="tEntity"/>
        <rdf:Description>
            <xsl:attribute name="rdf:about">
                <xsl:value-of
                    select="concat('http://piaaf.demo.logilab.fr/resource/', $coll, '_agent_relation_', substring-after(@xml:id, 're_'))"
                />
            </xsl:attribute>
            <rdf:type>
                <xsl:attribute name="rdf:resource">
                    <xsl:text>http://www.ica.org/standards/RiC/ontology#</xsl:text>
                    <xsl:choose>
                        <xsl:when test="@arcrole = 'isFunctionallyLinkedTo'"
                            >BusinessRelation</xsl:when>
                        <xsl:when test="@arcrole = 'isControlledBy' or @arcrole = 'controls'"
                            >AgentControlRelation</xsl:when>
                        <xsl:when test="@arcrole = 'hasPart'">AgentWholePartRelation</xsl:when>
                        <xsl:when test="@arcrole = 'isPartOf'">AgentWholePartRelation</xsl:when>
                        <xsl:when test="@type = 'temporal-earlier' or @type = 'temporal-later'"
                            >AgentTemporalRelation</xsl:when>
                        <xsl:when test="@arcrole = 'isDirectorOf' or @arcrole = 'isDirectedBy'"
                            >AgentLeadershipRelation</xsl:when>
                        <xsl:when test="@arcrole = 'knows'">SocialRelation</xsl:when>
                        <xsl:when test="@arcrole = 'isMemberOf'">AgentMembershipRelation</xsl:when>
                        <xsl:when test="@arcrole = 'hasMember'">AgentMembershipRelation</xsl:when>
                        <xsl:when test="@arcrole = 'hasEmployee'">AgentMembershipRelation</xsl:when>
                        <xsl:when test="@arcrole = 'isEmployeeOf'"
                            >AgentMembershipRelation</xsl:when>
                        <xsl:when test="@arcrole = 'isAssociatedWithForItsControl'"
                            >AgentControlRelation</xsl:when>
                        <xsl:when test="@type = 'family'">FamilyRelation</xsl:when>
                        <xsl:when test="@type = 'associative' and (not(@arcrole) or @arcrole = '')"
                            >SocialRelation</xsl:when>
                        <xsl:when test="@type='hierarchical-parent' and (not(@arcrole) or @arcrole='')">
                            <!-- nombreux cas a priori pour les AN et le SIAF au moins -->
                            <xsl:text>AgentHierarchicalRelation</xsl:text>
                        </xsl:when>
                        <xsl:when test="@type='hierarchical-child' and (not(@arcrole) or @arcrole='')">
                            <!-- nombreux cas a priori pour les AN et le SIAF au moins -->
                            <xsl:text>AgentHierarchicalRelation</xsl:text>
                        </xsl:when>
                    </xsl:choose>

                </xsl:attribute>
            </rdf:type>
            <rdfs:label xml:lang="fr">
                <xsl:choose>
                    <xsl:when test="@arcrole = 'isFunctionallyLinkedTo'">Relation fonctionnelle de
                        travail entre</xsl:when>
                    <xsl:when
                        test="@arcrole = 'isControlledBy' or @arcrole = 'controls' or @arcrole = 'isAssociatedWithForItsControl'"
                        >Relation de contrôle ou de tutelle entre</xsl:when>
                    <xsl:when test="@arcrole = 'hasPart'">Relation du tout à une partie
                        entre</xsl:when>
                    <xsl:when test="@arcrole = 'isPartOf'">Relation du tout à une partie
                        entre</xsl:when>
                    <xsl:when test="@type = 'temporal-earlier' or @type = 'temporal-later'">Relation
                        chronologique entre</xsl:when>
                    <xsl:when test="@arcrole = 'isDirectorOf' or @arcrole = 'isDirectedBy'">Relation
                        de "leadership" ou de direction entre</xsl:when>
                    <xsl:when
                        test="@arcrole = 'isMemberOf' or @arcrole = 'hasMember' or @arcrole = 'isEmployeeOf' or @arcrole = 'hasEmployee'"
                        >Relation d'affiliation, ou de salarié à employeur, entre</xsl:when>
                    <xsl:when test="@type = 'family'">Relation familiale entre</xsl:when>
                    <xsl:when
                        test="(@type = 'associative' and (not(@arcrole) or @arcrole = '')) or @arcrole = 'knows'"
                        >Relation sociale entre</xsl:when>
                    <xsl:when test="@type='hierarchical-parent' and (not(@arcrole) or @arcrole='')">
                        <!-- nombreux cas a priori pour les AN et le SIAF au moins -->
                        <xsl:text>relation hiérarchique entre</xsl:text>
                    </xsl:when>
                    <xsl:when test="@type='hierarchical-child' and (not(@arcrole) or @arcrole='')">
                        <!-- nombreux cas a priori pour les AN et le SIAF au moins -->
                        <xsl:text>relation hiérarchique entre</xsl:text>
                    </xsl:when>
                </xsl:choose>
                <xsl:text> les agents "</xsl:text>
                <xsl:choose>
                    <xsl:when test="$coll = 'FRSIAF'">
                        <xsl:value-of
                            select="$collection-EAC-SIAF/eac:eac-cpf[eac:control/eac:recordId = $fromAgent]/eac:cpfDescription/eac:identity/eac:nameEntry[@localType = 'preferredFormForProject']/eac:part"
                        />
                    </xsl:when>
                    <xsl:when test="$coll = 'FRAN'">
                        <xsl:value-of
                            select="$collection-EAC-AN/eac:eac-cpf[eac:control/eac:recordId = $fromAgent]/eac:cpfDescription/eac:identity/eac:nameEntry[@localType = 'preferredFormForProject']/eac:part"
                        />
                    </xsl:when>
                    <xsl:when test="$coll = 'FRBNF'">
                        <xsl:value-of
                            select="$collection-EAC-BnF/eac:eac-cpf[eac:control/eac:recordId = $fromAgent]/eac:cpfDescription/eac:identity/eac:nameEntry[@localType = 'preferredFormForProject']/eac:part"
                        />
                    </xsl:when>
                </xsl:choose>

                <xsl:text>" et "</xsl:text>
                <xsl:value-of select="piaaf:targetName"/>
                <xsl:text>"</xsl:text>
            </rdfs:label>
            <xsl:if test="piaaf:note">
                <RiC:description xml:lang="fr">
                    <xsl:for-each select="piaaf:note/piaaf:p">
                        <xsl:value-of select="."/>
                        <xsl:if test="position() != last()">
                            <xsl:text> </xsl:text>
                        </xsl:if>
                    </xsl:for-each>
                </RiC:description>
            </xsl:if>
           <!-- <xsl:if test="piaaf:dateRange/piaaf:fromDate">
                <RiC:beginningDate rdf:datatype="http://www.w3.org/2001/XMLSchema#dateTime">
                    <xsl:value-of select="piaaf:dateRange/piaaf:fromDate"/>
                </RiC:beginningDate>
            </xsl:if>
            <xsl:if test="piaaf:dateRange/piaaf:toDate">
                <RiC:endDate rdf:datatype="http://www.w3.org/2001/XMLSchema#dateTime">
                    <xsl:value-of select="piaaf:dateRange/piaaf:toDate"/>
                </RiC:endDate>
            </xsl:if>
            <xsl:if test="piaaf:date">
                <RiC:date rdf:datatype="http://www.w3.org/2001/XMLSchema#dateTime">
                    <xsl:value-of select="piaaf:date"/>
                </RiC:date>
            </xsl:if>-->
            <xsl:if test="piaaf:date">
                <RiC:date>
                    <xsl:choose>
                        <xsl:when test="not(piaaf:date/@type)">
                            <xsl:value-of select="piaaf:date"/>
                        </xsl:when>
                        <xsl:when test="piaaf:date/@type='iso8601'">
                            <xsl:attribute name="rdf:datatype">
                                <xsl:choose>
                                    <xsl:when test="string-length(piaaf:date)=4">
                                        <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text>
                                    </xsl:when>
                                    <xsl:when test="string-length(piaaf:date)=7">
                                        <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                    </xsl:when>
                                    <xsl:when test="string-length(piaaf:date)=10">
                                        <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                    </xsl:when>
                                </xsl:choose>
                            </xsl:attribute>
                            <xsl:value-of select="piaaf:date"/>
                        </xsl:when>
                    </xsl:choose>
                </RiC:date>
            </xsl:if>
            <xsl:if test="piaaf:dateRange/piaaf:fromDate">
                <RiC:beginningDate>
                    <xsl:choose>
                        <xsl:when test="not(piaaf:dateRange/piaaf:fromDate/@type)">
                            <xsl:value-of select="piaaf:dateRange/piaaf:fromDate"/>
                        </xsl:when>
                        <xsl:when test="piaaf:dateRange/piaaf:fromDate/@type='iso8601'">
                            <xsl:attribute name="rdf:datatype">
                                <xsl:choose>
                                    <xsl:when test="string-length(piaaf:dateRange/piaaf:fromDate)=4">
                                        <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text>
                                    </xsl:when>
                                    <xsl:when test="string-length(piaaf:dateRange/piaaf:fromDate)=7">
                                        <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                    </xsl:when>
                                    <xsl:when test="string-length(piaaf:dateRange/piaaf:fromDate)=10">
                                        <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                    </xsl:when>
                                </xsl:choose>
                            </xsl:attribute>
                            <xsl:value-of select="piaaf:dateRange/piaaf:fromDate"/>
                        </xsl:when>
                    </xsl:choose>
                    
                </RiC:beginningDate>
            </xsl:if>
            <xsl:if test="piaaf:dateRange/piaaf:toDate">
                <!-- <RiC:endDate rdf:datatype="http://www.w3.org/2001/XMLSchema#dateTime">
                    <xsl:value-of select="piaaf:dateRange/piaaf:toDate"/>
                </RiC:endDate>-->
                <RiC:endDate>
                    <xsl:choose>
                        <xsl:when test="not(piaaf:dateRange/piaaf:toDate/@type)">
                            <xsl:value-of select="piaaf:dateRange/piaaf:toDate"/>
                        </xsl:when>
                        <xsl:when test="piaaf:dateRange/piaaf:toDate/@type='iso8601'">
                            <xsl:attribute name="rdf:datatype">
                                <xsl:choose>
                                    <xsl:when test="string-length(piaaf:dateRange/piaaf:toDate)=4">
                                        <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text>
                                    </xsl:when>
                                    <xsl:when test="string-length(piaaf:dateRange/piaaf:toDate)=7">
                                        <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                    </xsl:when>
                                    <xsl:when test="string-length(piaaf:dateRange/piaaf:toDate)=10">
                                        <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                    </xsl:when>
                                </xsl:choose>
                            </xsl:attribute>
                            <xsl:value-of select="piaaf:dateRange/piaaf:toDate"/>
                        </xsl:when>
                    </xsl:choose>
                </RiC:endDate>
            </xsl:if>
            
            <xsl:choose>
                <xsl:when test="@arcrole = 'isFunctionallyLinkedTo'">
                    <RiC:businessRelationWith>
                        <xsl:call-template name="outputObjectPropertyToAnc">

                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                        </xsl:call-template>

                    </RiC:businessRelationWith>


                    <RiC:businessRelationWith>
                        <xsl:call-template name="outputObjectPropertyToTarg">
                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                            <xsl:with-param name="arcrole" select="@arcrole"></xsl:with-param>
                        </xsl:call-template>

                    </RiC:businessRelationWith>

                </xsl:when>
                <xsl:when test="@type = 'family'">
                    <RiC:familyRelationWith>
                        <xsl:call-template name="outputObjectPropertyToAnc">

                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                        </xsl:call-template>

                    </RiC:familyRelationWith>


                    <RiC:familyRelationWith>
                        <xsl:call-template name="outputObjectPropertyToTarg">
                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                            <xsl:with-param name="arcrole" select="@arcrole"></xsl:with-param>
                        </xsl:call-template>

                    </RiC:familyRelationWith>

                </xsl:when>
                <xsl:when
                    test="@arcrole = 'knows' or (@type = 'associative' and (not(@arcrole) or @arcrole = ''))">
                    <RiC:relationAssociates>
                        <xsl:call-template name="outputObjectPropertyToAnc">

                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                        </xsl:call-template>

                    </RiC:relationAssociates>


                    <RiC:relationAssociates>
                        <xsl:call-template name="outputObjectPropertyToTarg">
                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                            <xsl:with-param name="arcrole" select="@arcrole"></xsl:with-param>
                        </xsl:call-template>

                    </RiC:relationAssociates>

                </xsl:when>
                <xsl:when test="@arcrole = 'isDirectorOf'">
                    <RiC:leadershipBy>
                        <xsl:call-template name="outputObjectPropertyToAnc">

                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                        </xsl:call-template>

                    </RiC:leadershipBy>



                    <RiC:leadershipOn>
                        <xsl:call-template name="outputObjectPropertyToTarg">
                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                            <xsl:with-param name="arcrole" select="@arcrole"></xsl:with-param>
                        </xsl:call-template>
                    </RiC:leadershipOn>
                    <xsl:call-template name="outputPositionInRel">
                        <xsl:with-param name="coll" select="$coll"/>
                        <xsl:with-param name="fromAgent" select="$fromAgent"/>
                        <xsl:with-param name="tEntity" select="$tEntity"/>
                        <xsl:with-param name="tName" select="$tName"/>
                    </xsl:call-template>


                </xsl:when>
                <xsl:when test="@arcrole = 'isDirectedBy'">
                    <RiC:leadershipOn>
                        <xsl:call-template name="outputObjectPropertyToAnc">

                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                        </xsl:call-template>

                    </RiC:leadershipOn>



                    <RiC:leadershipBy>
                        <xsl:call-template name="outputObjectPropertyToTarg">
                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                            <xsl:with-param name="arcrole" select="@arcrole"></xsl:with-param>
                        </xsl:call-template>
                    </RiC:leadershipBy>
                    <xsl:call-template name="outputPositionInRel">
                        <xsl:with-param name="coll" select="$coll"/>
                        <xsl:with-param name="fromAgent" select="$tEntity"/>
                        <xsl:with-param name="tEntity" select="$fromAgent"/>
                        <xsl:with-param name="tName"/>
                    </xsl:call-template>


                </xsl:when>
                <xsl:when test="@arcrole = 'isEmployeeOf' or @arcrole = 'isMemberOf'">
                    <RiC:hasAgentMember>
                        <xsl:call-template name="outputObjectPropertyToAnc">

                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                        </xsl:call-template>

                    </RiC:hasAgentMember>



                    <RiC:agentMembershipIn>
                        <xsl:call-template name="outputObjectPropertyToTarg">
                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                            <xsl:with-param name="arcrole" select="@arcrole"></xsl:with-param>
                        </xsl:call-template>
                    </RiC:agentMembershipIn>



                </xsl:when>
                <xsl:when test="@arcrole = 'hasEmployee' or @arcrole = 'hasMember'">
                    <RiC:agentMembershipIn>
                        <xsl:call-template name="outputObjectPropertyToAnc">

                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                        </xsl:call-template>

                    </RiC:agentMembershipIn>



                    <RiC:hasAgentMember>
                        <xsl:call-template name="outputObjectPropertyToTarg">
                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                            <xsl:with-param name="arcrole" select="@arcrole"></xsl:with-param>
                        </xsl:call-template>
                    </RiC:hasAgentMember>



                </xsl:when>
                <xsl:when
                    test="@arcrole = 'isControlledBy' or @arcrole = 'isAssociatedWithForItsControl'">
                    <RiC:controlOnAgent>
                        <xsl:call-template name="outputObjectPropertyToAnc">

                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                        </xsl:call-template>

                    </RiC:controlOnAgent>
                    <RiC:agentControlHeldBy>
                        <xsl:call-template name="outputObjectPropertyToTarg">
                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                            <xsl:with-param name="arcrole" select="@arcrole"></xsl:with-param>
                        </xsl:call-template>
                    </RiC:agentControlHeldBy>



                </xsl:when>
                <xsl:when test="@arcrole = 'controls'">
                    <RiC:controlOnAgent>
                        <xsl:call-template name="outputObjectPropertyToTarg">
                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                            <xsl:with-param name="arcrole" select="@arcrole"></xsl:with-param>
                        </xsl:call-template>

                    </RiC:controlOnAgent>
                    <RiC:agentControlHeldBy>
                        <xsl:call-template name="outputObjectPropertyToAnc">

                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                        </xsl:call-template>
                    </RiC:agentControlHeldBy>



                </xsl:when>
                <xsl:when test="@arcrole = 'hasPart'">
                    <RiC:agentPartOf>


                        <xsl:call-template name="outputObjectPropertyToAnc">

                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                        </xsl:call-template>
                    </RiC:agentPartOf>
                    <RiC:wholeHasAgentPart>
                        <xsl:call-template name="outputObjectPropertyToTarg">
                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                            <xsl:with-param name="arcrole" select="@arcrole"></xsl:with-param>
                        </xsl:call-template>
                    </RiC:wholeHasAgentPart>



                </xsl:when>
                <xsl:when test="@arcrole = 'isPartOf'">
                    <RiC:wholeHasAgentPart>
                        <xsl:call-template name="outputObjectPropertyToAnc">

                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                        </xsl:call-template>
                    </RiC:wholeHasAgentPart>
                    <RiC:agentPartOf>
                        <xsl:call-template name="outputObjectPropertyToTarg">
                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                            <xsl:with-param name="arcrole" select="@arcrole"></xsl:with-param>
                        </xsl:call-template>
                    </RiC:agentPartOf>
                </xsl:when>
                <xsl:when test="@type = 'temporal-earlier'">
                    <RiC:followingAgent>
                        <xsl:call-template name="outputObjectPropertyToAnc">

                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                        </xsl:call-template>
                    </RiC:followingAgent>
                    <RiC:precedingAgent>
                        <xsl:call-template name="outputObjectPropertyToTarg">
                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                            <xsl:with-param name="arcrole" select="@arcrole"></xsl:with-param>
                        </xsl:call-template>
                    </RiC:precedingAgent>


                </xsl:when>
                <xsl:when test="@type = 'temporal-later'">
                    <RiC:precedingAgent>
                        <xsl:call-template name="outputObjectPropertyToAnc">

                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                        </xsl:call-template>
                    </RiC:precedingAgent>
                    <RiC:followingAgent>
                        <xsl:call-template name="outputObjectPropertyToTarg">
                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                            <xsl:with-param name="arcrole" select="@arcrole"></xsl:with-param>
                        </xsl:call-template>
                    </RiC:followingAgent>


                </xsl:when>
                <xsl:when test="@type = 'hierarchical-child' and (not(@arcrole) or @arcrole='')">
                    <RiC:hasHierarchicalParent>
                        <xsl:call-template name="outputObjectPropertyToAnc">
                            
                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                        </xsl:call-template>
                    </RiC:hasHierarchicalParent>
                    <RiC:hasHierarchicalChild>
                        <xsl:call-template name="outputObjectPropertyToTarg">
                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                            <xsl:with-param name="arcrole" select="@arcrole"></xsl:with-param>
                        </xsl:call-template>
                    </RiC:hasHierarchicalChild>
                    
                    
                </xsl:when>
                <xsl:when test="@type = 'hierarchical-parent' and (not(@arcrole) or @arcrole='')">
                    <RiC:hasHierarchicalChild>
                        <xsl:call-template name="outputObjectPropertyToAnc">
                            
                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                        </xsl:call-template>
                    </RiC:hasHierarchicalChild>
                    <RiC:hasHierarchicalParent>
                        <xsl:call-template name="outputObjectPropertyToTarg">
                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                            <xsl:with-param name="arcrole" select="@arcrole"></xsl:with-param>
                        </xsl:call-template>
                    </RiC:hasHierarchicalParent>
                    
                    
                </xsl:when>
            </xsl:choose>


        </rdf:Description>
      
    </xsl:template>
    <xsl:template name="outputPositionInRel">
        <xsl:param name="tName"/>
        <xsl:param name="tEntity"/>
        <xsl:param name="fromAgent"/>
        <xsl:param name="coll"/>

        <xsl:choose>
            <xsl:when test="$coll = 'FRSIAF'">
                <xsl:comment>coll='FRSIAF'</xsl:comment>
                <xsl:if
                    test="
                        $FRSIAF-positions/rdf:Description[rdf:type/@rdf:resource = 'http://www.ica.org/standards/RiC/ontology#Position'
                        and
                        RiC:occupiedBy/@rdf:resource = concat('http://piaaf.demo.logilab.fr/resource/FRSIAF_person_', $fromAgent)
                        and
                        (RiC:positionIn/@rdf:resource = concat('http://piaaf.demo.logilab.fr/resource/FRSIAF_corporate-body_', $tEntity)
                        or
                        RiC:positionIn = $tName
                        
                        )]">
                    <xsl:comment>condition réalisée</xsl:comment>
                    <RiC:leadershipWithPosition>
                        <xsl:attribute name="rdf:resource">
                            <xsl:value-of
                                select="
                                    $FRSIAF-positions/rdf:Description[rdf:type/@rdf:resource = 'http://www.ica.org/standards/RiC/ontology#Position'
                                    and
                                    RiC:occupiedBy/@rdf:resource = concat('http://piaaf.demo.logilab.fr/resource/FRSIAF_person_', $fromAgent)
                                    and
                                    (RiC:positionIn/@rdf:resource = concat('http://piaaf.demo.logilab.fr/resource/FRSIAF_corporate-body_', $tEntity)
                                    or
                                    RiC:positionIn = $tName
                                    
                                    )]/@rdf:about"
                            />
                        </xsl:attribute>
                    </RiC:leadershipWithPosition>
                </xsl:if>

            </xsl:when>
            <xsl:when test="$coll = 'FRAN'">
                <xsl:comment>coll='FRAN'</xsl:comment>
                <xsl:if
                    test="
                        $FRAN-positions/rdf:Description[rdf:type/@rdf:resource = 'http://www.ica.org/standards/RiC/ontology#Position'
                        and
                        RiC:occupiedBy/@rdf:resource = concat('http://piaaf.demo.logilab.fr/resource/FRAN_person_', substring-after($fromAgent, 'FRAN_NP_'))
                        and
                        (RiC:positionIn/@rdf:resource = concat('http://piaaf.demo.logilab.fr/resource/FRAN_corporate-body_', substring-after($tEntity, 'FRAN_NP_'))
                        or
                        RiC:positionIn = $tName
                        or
                        ends-with(RiC:positionIn/@rdf:nodeID, substring-after($tEntity, 'FRAN_NP_'))
                        )]">
                    <RiC:leadershipWithPosition>
                        <xsl:attribute name="rdf:resource">
                            <xsl:value-of
                                select="
                                    $FRAN-positions/rdf:Description[rdf:type/@rdf:resource = 'http://www.ica.org/standards/RiC/ontology#Position'
                                    and
                                    RiC:occupiedBy/@rdf:resource = concat('http://piaaf.demo.logilab.fr/resource/FRAN_person_', substring-after($fromAgent, 'FRAN_NP_'))
                                    and
                                    (RiC:positionIn/@rdf:resource = concat('http://piaaf.demo.logilab.fr/resource/FRAN_corporate-body_', substring-after($tEntity, 'FRAN_NP_'))
                                    or
                                    RiC:positionIn = $tName
                                    or
                                    ends-with(RiC:positionIn/@rdf:nodeID, substring-after($tEntity, 'FRAN_NP_'))
                                    )]/@rdf:about"
                            />
                        </xsl:attribute>
                    </RiC:leadershipWithPosition>
                </xsl:if>
            </xsl:when>
            <xsl:when test="$coll = 'FRBNF'">
                <xsl:if
                    test="
                        $FRBNF-positions/rdf:Description[rdf:type/@rdf:resource = 'http://www.ica.org/standards/RiC/ontology#Position'
                        and
                        RiC:occupiedBy/@rdf:resource = concat('http://piaaf.demo.logilab.fr/resource/FRBNF_person_', substring-after($fromAgent, 'DGEARC_'))
                        and
                        (RiC:positionIn/@rdf:resource = concat('http://piaaf.demo.logilab.fr/resource/FRBNF_corporate-body_', substring-after($tEntity, 'DGEARC'))
                        or
                        RiC:positionIn = $tName
                        
                        )]">
                    <RiC:leadershipWithPosition>
                        <xsl:attribute name="rdf:resource">
                            <xsl:value-of
                                select="
                                    $FRBNF-positions/rdf:Description[rdf:type/@rdf:resource = 'http://www.ica.org/standards/RiC/ontology#Position'
                                    and
                                    RiC:occupiedBy/@rdf:resource = concat('http://piaaf.demo.logilab.fr/resource/FRBNF_person_', substring-after($fromAgent, 'DGEARC_'))
                                    and
                                    (RiC:positionIn/@rdf:resource = concat('http://piaaf.demo.logilab.fr/resource/FRBNF_corporate-body_', substring-after($tEntity, 'DGEARC'))
                                    or
                                    RiC:positionIn = $tName
                                    
                                    )]/@rdf:about"
                            />
                        </xsl:attribute>
                    </RiC:leadershipWithPosition>
                </xsl:if>
            </xsl:when>
        </xsl:choose>

    </xsl:template>
    <xsl:template name="outputObjectPropertyToAnc">
        <xsl:param name="tEntity"/>
        <xsl:param name="fromAgent"/>
        <xsl:param name="coll"/>
        <xsl:param name="ancAgentType"/>
        <xsl:attribute name="rdf:resource">
            <xsl:text>http://piaaf.demo.logilab.fr/resource/</xsl:text>
            <xsl:value-of select="$coll"/>
            <xsl:text>_</xsl:text>
            <xsl:choose>
                <xsl:when test="$ancAgentType = 'person'">person_</xsl:when>
                <xsl:when test="$ancAgentType = 'corporateBody'">corporate-body_</xsl:when>
            </xsl:choose>
            <xsl:value-of
                select="
                    if ($coll = 'FRSIAF')
                    then
                        ($fromAgent)
                    else
                        (
                        if ($coll = 'FRAN')
                        then
                            (substring-after($fromAgent, 'FRAN_NP_'))
                        else
                            (
                            if ($coll = 'FRBNF')
                            then
                                (substring-after($fromAgent, 'DGEARC_'))
                            else
                                ()
                            )
                        )
                    
                    "
            />
        </xsl:attribute>
    </xsl:template>
    <xsl:template name="outputObjectPropertyToTarg">

        <xsl:param name="ancAgentType"/>
        <xsl:param name="coll"/>
        <xsl:param name="fromAgent"/>
        <xsl:param name="tEntity"/>
        <xsl:param name="arcrole"/>
        <xsl:if test="normalize-space($tEntity) != ''">

            <xsl:choose>

                <xsl:when
                    test="$collection-EAC-AN/eac:eac-cpf[eac:control/eac:recordId = $tEntity] | $collection-EAC-SIAF/eac:eac-cpf[eac:control/eac:recordId = $tEntity] | $collection-EAC-BnF/eac:eac-cpf[eac:control/eac:recordId = $tEntity]">
                    <xsl:variable name="targetEntityType"
                        select="
                            if ($collection-EAC-BnF/eac:eac-cpf[eac:control/eac:recordId = $tEntity])
                            then
                                ($collection-EAC-BnF/eac:eac-cpf[eac:control/eac:recordId = $tEntity]/eac:cpfDescription/eac:identity/eac:entityType)
                            else
                                (
                                if ($collection-EAC-AN/eac:eac-cpf[eac:control/eac:recordId = $tEntity])
                                then
                                    ($collection-EAC-AN/eac:eac-cpf[eac:control/eac:recordId = $tEntity]/eac:cpfDescription/eac:identity/eac:entityType)
                                else
                                    (
                                    if ($collection-EAC-SIAF/eac:eac-cpf[eac:control/eac:recordId = $tEntity])
                                    then
                                        ($collection-EAC-SIAF/eac:eac-cpf[eac:control/eac:recordId = $tEntity]/eac:cpfDescription/eac:identity/eac:entityType)
                                    else
                                        ())
                                )
                            
                            
                            
                            "/>
                    <xsl:variable name="tEntityType">
                        <xsl:choose>
                            <xsl:when test="$targetEntityType = 'person'">person</xsl:when>
                            <xsl:when test="$targetEntityType = 'corporateBody'"
                                >corporate-body</xsl:when>

                        </xsl:choose>
                    </xsl:variable>
                    <xsl:attribute name="rdf:resource">
                        <xsl:text>http://piaaf.demo.logilab.fr/resource/</xsl:text>
                        
                        <xsl:choose>
                            <xsl:when test="starts-with($tEntity, 'FRAN_NP')">
                                <!-- <xsl:text>agents/FRAN_corporate-body_</xsl:text>-->
                                <xsl:value-of
                                    select="concat('FRAN_', $tEntityType, '_', substring-after($tEntity, 'FRAN_NP_'))"/>

                            </xsl:when>
                            <xsl:when test="starts-with($tEntity, 'FRBNF')">
                                <!--  <xsl:text>agents/BnF_corporate-body_</xsl:text>-->
                                <xsl:value-of
                                    select="concat('FRBNF_', $tEntityType, '_', substring-after($tEntity, 'DGEARC_'))"/>

                            </xsl:when>

                            <xsl:otherwise>
                                <xsl:value-of
                                    select="concat('FRSIAF_', $tEntityType, '_', $tEntity)"/>

                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:attribute>



                </xsl:when>

                <xsl:when test="starts-with($tEntity, 'FRAN_NP')">

                <!--    <xsl:attribute name="rdf:nodeID">
                        <xsl:value-of
                            select="concat('FRAN_bnode_', substring-after($tEntity, 'FRAN_NP_'), '_', generate-id())"
                        />
                    </xsl:attribute>-->
                    
                    <xsl:variable name="targEntityType">
                        <xsl:choose>
                            <!-- relation hiérarchique : entre 2 cbodies -->
                          <!--  <xsl:when test="starts-with($cpfRel, 'hierarchical')">
                                <xsl:text>corporate-body</xsl:text>
                            </xsl:when>-->
                            <xsl:when test="contains($arcrole, 'isDirectedBy')">
                                <xsl:text>person</xsl:text>
                            </xsl:when>
                            <xsl:when test="contains($arcrole, 'isDirectorOf')">
                                <xsl:text>corporate-body</xsl:text>
                            </xsl:when>
                            <xsl:when test="contains($arcrole, 'isAssociatedWithForItsControl')">
                                <xsl:text>corporate-body</xsl:text>
                            </xsl:when>
                            <xsl:when test="contains($arcrole, 'controls') or contains($arcrole, 'isControlledBy') or contains($arcrole, 'isPartOf') or contains($arcrole, 'hasPart')">
                                <xsl:text>corporate-body</xsl:text>
                            </xsl:when>
                            <xsl:when
                                test="contains($arcrole, 'hasMember') or contains($arcrole, 'hasEmployee')">
                                <xsl:text>person</xsl:text>
                            </xsl:when>
                            <xsl:when
                                test="contains($arcrole, 'isMemberOf') or contains($arcrole, 'isEmployeeOf')">
                                <xsl:text>corporate-body</xsl:text>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:text>agent</xsl:text>
                            </xsl:otherwise>
                            
                        </xsl:choose>
                    </xsl:variable>
                    
                    
                    
                    
                    
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="concat('http://piaaf.demo.logilab.fr/resource/FRAN_', $targEntityType, '_', substring-after($tEntity, 'FRAN_NP_'))"/>
                    </xsl:attribute>

                </xsl:when>
                <xsl:when test="starts-with($tEntity, 'FR784')">
                   <!-- <xsl:attribute name="rdf:nodeID">
                        <xsl:value-of select="concat('FRSIAF_bnode_', $tEntity, '_', generate-id())"
                        />
                    </xsl:attribute>-->
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="concat('http://piaaf.demo.logilab.fr/resource/FRSIAF_group-type_', $tEntity)"/>
                    </xsl:attribute>
                </xsl:when>
                <xsl:when test="starts-with($tEntity, 'FRAD')">

                 <!--   <xsl:attribute name="rdf:nodeID">
                        <xsl:value-of select="concat('FRSIAF_bnode_', $tEntity, '_', generate-id())"
                        />
                    </xsl:attribute>-->
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="concat('http://piaaf.demo.logilab.fr/resource/FRSIAF_corporate-body_', $tEntity)"/>
                    </xsl:attribute>
                </xsl:when>
                <xsl:when test="starts-with($tEntity, 'FRBNF')">
                    <!--<xsl:attribute name="rdf:nodeID">
                        <xsl:value-of
                            select="concat('FRBNF_bnode_', substring-after($tEntity, 'DGEARC_'), '_', generate-id())"
                        />
                    </xsl:attribute>-->
                    
                    <xsl:variable name="targEntityType">
                        <xsl:choose>
                            <!-- relation hiérarchique : entre 2 cbodies -->
                            <!--  <xsl:when test="starts-with($cpfRel, 'hierarchical')">
                                <xsl:text>corporate-body</xsl:text>
                            </xsl:when>-->
                            <xsl:when test="contains($arcrole, 'isDirectedBy')">
                                <xsl:text>person</xsl:text>
                            </xsl:when>
                            <xsl:when test="contains($arcrole, 'isDirectorOf')">
                                <xsl:text>corporate-body</xsl:text>
                            </xsl:when>
                            <xsl:when test="contains($arcrole, 'isAssociatedWithForItsControl')">
                                <xsl:text>corporate-body</xsl:text>
                            </xsl:when>
                            <xsl:when test="contains($arcrole, 'controls') or contains($arcrole, 'isControlledBy') or contains($arcrole, 'isPartOf') or contains($arcrole, 'hasPart')">
                                <xsl:text>corporate-body</xsl:text>
                            </xsl:when>
                            <xsl:when
                                test="contains($arcrole, 'hasMember') or contains($arcrole, 'hasEmployee')">
                                <xsl:text>person</xsl:text>
                            </xsl:when>
                            <xsl:when
                                test="contains($arcrole, 'isMemberOf') or contains($arcrole, 'isEmployeeOf')">
                                <xsl:text>corporate-body</xsl:text>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:text>agent</xsl:text>
                            </xsl:otherwise>
                            
                        </xsl:choose>
                    </xsl:variable>
                    
                    
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="concat('http://piaaf.demo.logilab.fr/resource/FRBNF_', $targEntityType, '_', substring-after($tEntity, 'DGEARC_'))"/>
                    </xsl:attribute>
                   <!-- <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="concat('http://www.piaaf.net/agents/FRBNF_agent_', substring-after($tEntity, 'DGEARC_'))"/>
                    </xsl:attribute>-->
                </xsl:when>
                <xsl:when test="starts-with($tEntity, 'http://data.bnf.fr/ark:/12148/')">
                    <!-- cas de la relation NNF d138e498-->
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="concat($tEntity, '#Organization')"/>
                    </xsl:attribute>
                </xsl:when>

            </xsl:choose>

        </xsl:if>
        <xsl:if test="normalize-space($tEntity) = ''">
            <xsl:attribute name="xml:lang">fr</xsl:attribute>
            <xsl:value-of select="piaaf:targetName"/>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>
