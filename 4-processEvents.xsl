<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:owl="http://www.w3.org/2002/07/owl#"
    xmlns:RiC="http://www.ica.org/standards/RiC/ontology#" 
    xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" 
    xmlns:eac="urn:isbn:1-931666-33-4" xmlns="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:piaaf="http://www.piaaf.net" 
   
    xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:foaf="http://xmlns.com/foaf/0.1/"
    xmlns:dct="http://purl.org/dc/terms/" xmlns:xl="http://www.w3.org/2008/05/skos-xl#"
   
    exclude-result-prefixes="xs xd eac rdf foaf dct xl dc piaaf skos" version="2.0">
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> July 3, 2017</xd:p>
            <xd:p><xd:b>Author:</xd:b> Florence</xd:p>
            <xd:p>Sémantisation : étape 4, génération d'un fichier RDF pour chaque partenaire (en fait, pour le seul SIAF) pour
                les événements affectant les agents</xd:p>
        </xd:desc>
    </xd:doc>
   
    <xsl:param name="coll">SIAF</xsl:param>
   
    <xsl:variable name="chemin-EAC-SIAF">
        <xsl:value-of
            select="concat('fichiers-def-2/notices-EAC/SIAF/', '?select=*.xml;recurse=yes;on-error=warning')"
        />
    </xsl:variable>
    <xsl:variable name="collection-EAC-SIAF" select="collection($chemin-EAC-SIAF)"/>
  
    <xsl:template match="/piaaf:vide">
        <xsl:if test="$coll = 'SIAF'">
            <xsl:result-document href="rdf/events/FRSIAF_events.rdf" method="xml" encoding="utf-8" indent="yes">
                <rdf:RDF
                    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                    xmlns:owl="http://www.w3.org/2002/07/owl#"
                    xmlns:RiC="http://www.ica.org/standards/RiC/ontology#"
                   
                    xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
                  
                    >
                  
                    
                    <xsl:for-each-group
                        select="$collection-EAC-SIAF/eac:eac-cpf/eac:cpfDescription/eac:description/descendant::eac:chronItem[normalize-space(.)!='']"
                        group-by="
                        concat(normalize-space(eac:date), ' : ', normalize-space(eac:event))
                       
                       ">
                        <xsl:sort select="current-grouping-key()"></xsl:sort>
                      <!--  <xsl:variable name="num" select="format-number(number(position()), '#000')"/>-->
                        <rdf:Description>
                            <xsl:attribute name="rdf:about">
                             <xsl:text>http://piaaf.demo.logilab.fr/resource/FRSIAF_event_</xsl:text>
                                <!--<xsl:value-of select="concat('http://www.piaaf.net/', 'events/FRSIAF_event_', $num)"/>-->
                                <xsl:value-of select="substring-after(current-group()[1]/@xml:id, 'ev_')"/>
                            </xsl:attribute>
                            <rdf:type
                                rdf:resource="http://www.ica.org/standards/RiC/ontology#Event"/>
                           
                           
                                <rdfs:label xml:lang="fr">
                                    <xsl:value-of select="current-grouping-key()"/>
                                </rdfs:label>
                            
                            <RiC:date><xsl:value-of select="normalize-space(eac:date)"/></RiC:date>
                           <RiC:certainty xml:lang="fr">certain</RiC:certainty>
                          <!--  <xsl:for-each select="current-group()">
                                
                                <RiC:affected>
                                    <xsl:attribute name="rdf:resource">
                                        <xsl:text>http://www.piaaf.net/</xsl:text>
                                        <xsl:choose>
                                            <xsl:when test="starts-with(ancestor::eac:eac-cpf/eac:control/eac:recordId, 'FR784')">
                                                <xsl:text>group-types/FRSIAF_group-type_</xsl:text>
                                                <xsl:value-of select="ancestor::eac:eac-cpf/eac:control/eac:recordId"/>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:text>agents/FRSIAF_</xsl:text>
                                                <xsl:choose>
                                                    <xsl:when test="ancestor::eac:eac-cpf/eac:cpfDescription/eac:identity/eac:entityType='person'">
                                                        <xsl:text>person_</xsl:text>
                                                    </xsl:when>
                                                    <xsl:when test="ancestor::eac:eac-cpf/eac:cpfDescription/eac:identity/eac:entityType='corporateBody'">
                                                        <xsl:text>corporate-body_</xsl:text>
                                                    </xsl:when>
                                                </xsl:choose>
                                                <xsl:value-of select="ancestor::eac:eac-cpf/eac:control/eac:recordId"/>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:attribute>
                                </RiC:affected>
                            </xsl:for-each>-->
                          
                 
                        </rdf:Description>
                    </xsl:for-each-group>
                        
              
                </rdf:RDF>
            </xsl:result-document>
        </xsl:if>
        
        
    </xsl:template>

</xsl:stylesheet>
