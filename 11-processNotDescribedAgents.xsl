<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:owl="http://www.w3.org/2002/07/owl#" xmlns:RiC="http://www.ica.org/standards/RiC/ontology#"
    xmlns:isni="http://isni.org/ontology#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
    xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:eac="urn:isbn:1-931666-33-4"
    xmlns="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:piaaf="http://www.piaaf.net"
    xmlns:piaaf-onto="http://piaaf.demo.logilab.fr/ontology#"
    xmlns:iso-thes="http://purl.org/iso25964/skos-thes#"
    xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:foaf="http://xmlns.com/foaf/0.1/"
    xmlns:dct="http://purl.org/dc/terms/" xmlns:xl="http://www.w3.org/2008/05/skos-xl#"
    xmlns:ginco="http://data.culture.fr/thesaurus/ginco/ns/"
    exclude-result-prefixes="xs xd eac iso-thes rdf dct xl piaaf xlink piaaf-onto skos foaf ginco dc" version="2.0">
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> July 4, 2017, checked and updated Dec. 12, 2017</xd:p>
            <xd:p><xd:b>Author:</xd:b> Florence Clavaud (Archives nationales)</xd:p>
            <xd:p>Sémantisation : étape 11: génération de fichiers RDF pour chacun des agents
                mentionnés par les fichiers EAC source et ne faisant pas l'objet d'un fichier
                EAC.</xd:p>
            <xd:p>Implique de dédoublonner ces agents</xd:p>
        </xd:desc>
    </xd:doc>
    <!--<xsl:param name="coll">FRAN</xsl:param>-->


    <xsl:variable name="chemin-EAC-BnF">
        <xsl:value-of
            select="concat('fichiers-def-2/notices-EAC/BnF/', '?select=*.xml;recurse=yes;on-error=warning')"
        />
    </xsl:variable>
    <xsl:variable name="collection-EAC-BnF" select="collection($chemin-EAC-BnF)"/>
    <xsl:variable name="chemin-EAC-SIAF">
        <xsl:value-of
            select="concat('fichiers-def-2/notices-EAC/SIAF/', '?select=*.xml;recurse=yes;on-error=warning')"
        />
    </xsl:variable>
    <xsl:variable name="collection-EAC-SIAF" select="collection($chemin-EAC-SIAF)"/>
    <xsl:variable name="chemin-EAC-AN">
        <xsl:value-of
            select="concat('fichiers-def-2/notices-EAC/AN/', '?select=*.xml;recurse=yes;on-error=warning')"
        />
    </xsl:variable>
    <xsl:variable name="collection-EAC-AN" select="collection($chemin-EAC-AN)"/>
    <xsl:variable name="apos" select="'&#x2bc;'"/>

    <xsl:template match="/piaaf:vide">
            
      <!--  <xsl:call-template name="output-not-described-agents">
            <xsl:with-param name="coll">FRAN</xsl:with-param>-->
          <!--<xsl:with-param name="not-described-agents">-->
        <xsl:variable name="not-described-agents">
              <piaaf:agents>
                  <xsl:for-each
                      select="$collection-EAC-AN/eac:eac-cpf/eac:cpfDescription/eac:relations/eac:cpfRelation[@cpfRelationType != 'identity' and normalize-space(@xlink:href) != '' and not(starts-with(@xlink:href, 'http:/'))] | $collection-EAC-BnF/eac:eac-cpf/eac:cpfDescription/eac:relations/eac:cpfRelation[@cpfRelationType != 'identity' and normalize-space(@xlink:href) != '' and not(starts-with(@xlink:href, 'http:/'))] | $collection-EAC-SIAF/eac:eac-cpf/eac:cpfDescription/eac:relations/eac:cpfRelation[@cpfRelationType != 'identity' and normalize-space(@xlink:href) != '' and not(starts-with(@xlink:href, 'http:/'))]">
                      <xsl:variable name="ancId"
                          select="ancestor::eac:eac-cpf/eac:control/eac:recordId"/>
                      <xsl:variable name="link" select="@xlink:href"/>
                      
                      <xsl:variable name="cpfRel" select="@cpfRelationType"/>
                      
                      <xsl:if
                          test="
                          not($collection-EAC-AN[eac:eac-cpf/eac:control/eac:recordId = $link]) and not($collection-EAC-SIAF[eac:eac-cpf/eac:control/eac:recordId = $link])
                          and not($collection-EAC-BnF[eac:eac-cpf/eac:control/eac:recordId = $link])">
                          <xsl:variable name="prov">
                              <xsl:choose>
                                  <xsl:when test="starts-with($link, 'FRAN')">FRAN</xsl:when>
                                  <xsl:when test="starts-with($link, 'FRBNF')">FRBNF</xsl:when>
                                  <xsl:otherwise>FRSIAF</xsl:otherwise>
                              </xsl:choose>
                          </xsl:variable>
                          <xsl:variable name="arcrole" select="@xlink:arcrole"/>
                          <xsl:variable name="targEntType">
                              <xsl:choose>
                                  <!-- relation hiérarchique : entre 2 cbodies -->
                                  <!--  <xsl:when test="starts-with($cpfRel, 'hierarchical')">
                                <xsl:text>corporate-body</xsl:text>
                            </xsl:when>-->
                                  <xsl:when test="starts-with($link, 'FR784')"><xsl:text>group-type</xsl:text></xsl:when>
                                  <xsl:when test="starts-with($link, 'FRAD')"><xsl:text>corporate-body</xsl:text></xsl:when>
                                  <xsl:when test="$prov='FRSIAF' and starts-with($link, 'FRSIAF')"><xsl:text>person</xsl:text></xsl:when>
                                  <xsl:when test="contains($arcrole, 'isDirectedBy')">
                                      <xsl:text>person</xsl:text>
                                  </xsl:when>
                                  <xsl:when test="contains($arcrole, 'isDirectorOf')">
                                      <xsl:text>corporate-body</xsl:text>
                                  </xsl:when>
                                  <xsl:when test="contains($arcrole, 'isAssociatedWithForItsControl')">
                                      <xsl:text>corporate-body</xsl:text>
                                  </xsl:when>
                                  <xsl:when test="contains($arcrole, 'controls') or contains($arcrole, 'isControlledBy') or contains($arcrole, 'isPartOf') or contains($arcrole, 'hasPart')">
                                      <xsl:text>corporate-body</xsl:text>
                                  </xsl:when>
                                  <xsl:when
                                      test="contains($arcrole, 'hasMember') or contains($arcrole, 'hasEmployee')">
                                      <xsl:text>person</xsl:text>
                                  </xsl:when>
                                  <xsl:when
                                      test="contains($arcrole, 'isMemberOf') or contains($arcrole, 'isEmployeeOf')">
                                      <xsl:text>corporate-body</xsl:text>
                                  </xsl:when>
                                  <xsl:otherwise>
                                      <xsl:text>agent</xsl:text>
                                  </xsl:otherwise>
                                  
                              </xsl:choose>
                          </xsl:variable>
                          
                          <!--<xsl:variable name="targEntType">
                              
                              <xsl:choose>
                                  <!-\- relation hiérarchique : entre 2 cbodies -\->
                                  <xsl:when test="starts-with($cpfRel, 'hierarchical')">
                                      <xsl:text>corporate-body</xsl:text>
                                  </xsl:when>
                                  <xsl:when test="contains($arcrole, 'isDirectedBy')">
                                      <xsl:text>person</xsl:text>
                                  </xsl:when>
                                  <xsl:when test="contains($arcrole, 'isDirectorOf')">
                                      <xsl:text>corporate-body</xsl:text>
                                  </xsl:when>
                                  <xsl:when test="contains($arcrole, 'isAssociatedWithForItsControl')">
                                      <xsl:text>corporate-body</xsl:text>
                                  </xsl:when>
                                  <xsl:when
                                      test="contains($arcrole, 'hasMember') or contains($arcrole, 'hasEmployee')">
                                      <xsl:text>person</xsl:text>
                                  </xsl:when>
                                  <xsl:when
                                      test="contains($arcrole, 'isMemberOf') or contains($arcrole, 'isEmployeeOf')">
                                      <xsl:text>corporate-body</xsl:text>
                                  </xsl:when>
                                  <xsl:otherwise>
                                      <xsl:text>agent</xsl:text>
                                  </xsl:otherwise>
                                  
                              </xsl:choose>
                              
                          </xsl:variable>-->
                          <piaaf:agent>
                             <!-- <piaaf:uri>
                                  <xsl:value-of
                                      select="concat('http://www.piaaf.net/agents/', $prov, '_', $targEntType, '_')"/>
                                  <xsl:choose>
                                      <xsl:when test="$prov = 'FRAN'">
                                          <xsl:value-of select="substring-after($link, 'FRAN_NP_')"/>
                                      </xsl:when>
                                      <xsl:when test="$prov = 'FRBNF'">
                                          <xsl:value-of select="substring-after($link, 'DGEARC_')"/>
                                      </xsl:when>
                                      <xsl:when test="$prov = 'FRSIAF'">
                                          <xsl:value-of select="$link"/>
                                      </xsl:when>
                                      <xsl:otherwise>??</xsl:otherwise>
                                  </xsl:choose>
                              </piaaf:uri>-->
                              <piaaf:id>
                                  <xsl:choose>
                                      <xsl:when test="$prov = 'FRAN'">
                                          <xsl:value-of select="substring-after($link, 'FRAN_NP_')"/>
                                      </xsl:when>
                                      <xsl:when test="$prov = 'FRBNF'">
                                          <xsl:value-of select="substring-after($link, 'DGEARC_')"/>
                                      </xsl:when>
                                      <xsl:when test="$prov = 'FRSIAF'">
                                          <xsl:value-of select="$link"/>
                                      </xsl:when>
                                  </xsl:choose>
                                  
                              </piaaf:id>
                              <piaaf:name>
                                  <xsl:value-of select="eac:relationEntry"/>
                              </piaaf:name>
                              <piaaf:prov>
                                  <xsl:value-of select="$prov"/>
                              </piaaf:prov>
                              <piaaf:type>
                                  <xsl:value-of select="$targEntType"/>
                              </piaaf:type>
                              
                          </piaaf:agent>
                      </xsl:if>
                      
                  </xsl:for-each>
              </piaaf:agents>
        </xsl:variable>
          <!--</xsl:with-param>-->
        <!--</xsl:call-template>-->
        <xsl:variable name="not-described-agents-reduced">
            <piaaf:agents>
               <!-- <xsl:for-each-group select="$not-described-agents/piaaf:agents/piaaf:agent"
                    group-by="concat(piaaf:uri, '|', piaaf:prov, '[', piaaf:type, ']', piaaf:id)">
                    <piaaf:agent>
                        <piaaf:uri>
                            <xsl:value-of select="substring-before(current-grouping-key(), '|')"/>
                        </piaaf:uri>
                        <piaaf:prov>
                            <xsl:value-of
                                select="substring-before(substring-after(current-grouping-key(), '|'), '[')"
                            />
                        </piaaf:prov>
                        <piaaf:type>
                            <xsl:value-of
                                select="substring-before(substring-after(current-grouping-key(), '['), ']')"
                            />
                        </piaaf:type>
                        <piaaf:id>
                            <xsl:value-of select="substring-after(current-grouping-key(), ']')"/>
                        </piaaf:id>
                        <xsl:for-each-group select="current-group()" group-by="piaaf:name">
                            <piaaf:name>
                                <xsl:value-of select="current-grouping-key()"/>
                            </piaaf:name>
                            
                        </xsl:for-each-group>
                    </piaaf:agent>
                </xsl:for-each-group>-->
                <xsl:for-each-group select="$not-described-agents/piaaf:agents/piaaf:agent"
                    group-by="concat(piaaf:prov, '|', piaaf:id)">
                    
                    <piaaf:agent>
                      <!--  <piaaf:uri>
                            <xsl:value-of select="substring-before(current-grouping-key(), '|')"/>
                        </piaaf:uri>-->
                        <piaaf:prov>
                            <xsl:value-of
                                select="substring-before(current-grouping-key(), '|')"
                            />
                        </piaaf:prov>
                       <!-- <piaaf:type>
                            <xsl:value-of
                                select="substring-before(substring-after(current-grouping-key(), '['), ']')"
                            />
                        </piaaf:type>-->
                        <piaaf:id>
                            <xsl:value-of select="substring-after(current-grouping-key(), '|')"/>
                        </piaaf:id>
                        
                        <xsl:for-each-group select="current-group()" group-by="piaaf:type">
                            <piaaf:type>
                                <xsl:value-of select="current-grouping-key()"/>
                            </piaaf:type>
                        </xsl:for-each-group>
                        
                        
                        <xsl:for-each-group select="current-group()" group-by="piaaf:name">
                            <piaaf:name>
                                <xsl:value-of select="current-grouping-key()"/>
                            </piaaf:name>
                            
                        </xsl:for-each-group>
                    </piaaf:agent>
                    
                </xsl:for-each-group>
            </piaaf:agents>
        </xsl:variable>
        <!--<xsl:result-document href="rdf/agents/not-described-agents.xml" method="xml"
            encoding="utf-8" indent="yes">
            <xsl:copy-of select="$not-described-agents"/>
        </xsl:result-document>
        <xsl:result-document href="rdf/agents/not-described-agents-reduced.xml" method="xml"
            encoding="utf-8" indent="yes">
            <xsl:copy-of select="$not-described-agents-reduced"/>
            
        </xsl:result-document>-->
        <xsl:for-each select="$not-described-agents-reduced/piaaf:agents/piaaf:agent">
            <xsl:variable name="prov" select="piaaf:prov"/>
           
            <xsl:variable name="id" select="piaaf:id"/>
          
          <xsl:variable name="theGoodType">
              <xsl:choose>
                  <xsl:when test="count(piaaf:type)=1">
                      <xsl:value-of select="piaaf:type"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:choose>
                        <xsl:when test="piaaf:type[.='group-type']">group-type</xsl:when>
                        <xsl:when test="piaaf:type[.='person']">person</xsl:when>
                        <xsl:when test="piaaf:type[.='corporate-body']">corporate-body</xsl:when>
                        <xsl:otherwise>
                            ???
                        </xsl:otherwise>
                    </xsl:choose>
                  </xsl:otherwise>
              </xsl:choose>
          </xsl:variable>
          <xsl:choose>
              <xsl:when test="$theGoodType='group-type'">
                  <xsl:result-document method="xml" encoding="utf-8" indent="yes"
                      href="rdf/group-types/{$prov}_{$theGoodType}_{$id}.rdf">
                      
                      <rdf:RDF 
                          xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                          xmlns:owl="http://www.w3.org/2002/07/owl#"
                          xmlns:RiC="http://www.ica.org/standards/RiC/ontology#"
                          xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
                          xmlns:piaaf-onto="http://piaaf.demo.logilab.fr/ontology#"
                          xmlns:skos="http://www.w3.org/2004/02/skos/core#">
                          <rdf:Description>
                              <xsl:attribute name="rdf:about">
                                
                                  <xsl:value-of select="concat('http://piaaf.demo.logilab.fr/resource/
                                      ', $prov, '_', $theGoodType, '_', $id)"/>
                                     
                                  
                                  
                                  
                              </xsl:attribute>
                              <rdf:type>
                                  <xsl:attribute name="rdf:resource">
                                      <xsl:text>http://www.ica.org/standards/RiC/ontology#GroupType</xsl:text>
                                     <!-- <xsl:choose>
                                          <xsl:when test="$theGoodType = 'agent'">Agent</xsl:when>
                                          <xsl:when test="$theGoodType = 'corporate-body'"
                                              >CorporateBody</xsl:when>
                                          <xsl:when test="$theGoodType = 'person'">Person</xsl:when>
                                          <xsl:when test="$theGoodType = 'group-type'">GroupType</xsl:when>
                                      </xsl:choose>-->
                                  </xsl:attribute>
                              </rdf:type>
                              <rdfs:label xml:lang="fr">
                                  <xsl:value-of select="piaaf:name[1]"/>
                              </rdfs:label>
                            
                                      <rdfs:seeAlso>
                                          <xsl:attribute name="rdf:resource">
                                              <!--  <!-\\-  https://aaf.ica-atom.org/actor/browse?subquery=FR78422804100033_000000122-\\->-->
                                              <xsl:value-of select="concat('https://aaf.ica-atom.org/actor/browse?subquery=', $id)"/>
                                          </xsl:attribute>
                                      </rdfs:seeAlso>
                               
                          </rdf:Description>
                          
                      </rdf:RDF>
                  </xsl:result-document>
              </xsl:when>
              <xsl:otherwise>
                  <xsl:result-document method="xml" encoding="utf-8" indent="yes"
                      href="rdf/agents/{$prov}_{$theGoodType}_{$id}.rdf">
                      
                      <rdf:RDF xmlns:dc="http://purl.org/dc/elements/1.1/"
                          xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                          xmlns:owl="http://www.w3.org/2002/07/owl#"
                          xmlns:RiC="http://www.ica.org/standards/RiC/ontology#"
                          xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
                          xmlns:piaaf-onto="http://piaaf.demo.logilab.fr/ontology#"
                          xmlns:skos="http://www.w3.org/2004/02/skos/core#">
                          <rdf:Description>
                              <xsl:attribute name="rdf:about">
                                  <xsl:value-of select="concat('http://piaaf.demo.logilab.fr/resource/', $prov, '_', $theGoodType, '_', $id)"/>
                                  
                                  
                                  
                              </xsl:attribute>
                              <rdf:type>
                                  <xsl:attribute name="rdf:resource">
                                      <xsl:text>http://www.ica.org/standards/RiC/ontology#</xsl:text>
                                      <xsl:choose>
                                          <xsl:when test="$theGoodType = 'agent'">Agent</xsl:when>
                                          <xsl:when test="$theGoodType = 'corporate-body'"
                                              >CorporateBody</xsl:when>
                                          <xsl:when test="$theGoodType = 'person'">Person</xsl:when>
                                         
                                      </xsl:choose>
                                  </xsl:attribute>
                              </rdf:type>
                              <rdfs:label xml:lang="fr">
                                  <xsl:value-of select="piaaf:name[1]"/>
                              </rdfs:label>
                             
                              <xsl:choose>
                                  <xsl:when test="$prov = 'FRAN'">
                                      <rdfs:seeAlso>
                                          <xsl:attribute name="rdf:resource">
                                              <xsl:value-of
                                                  select="concat('https://www.siv.archives-nationales.culture.gouv.fr/siv/NP/FRAN_NP_', $id)"
                                              />
                                          </xsl:attribute>
                                      </rdfs:seeAlso>
                                  </xsl:when>
                                 
                              </xsl:choose>
                          </rdf:Description>
                          
                      </rdf:RDF>
                  </xsl:result-document>
              </xsl:otherwise>
          </xsl:choose>
            
        </xsl:for-each>
        
        
        
        
        
        
    </xsl:template>
   
            
        
        
       


   






</xsl:stylesheet>
