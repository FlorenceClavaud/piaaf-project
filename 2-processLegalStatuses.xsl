<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:owl="http://www.w3.org/2002/07/owl#"
    
    xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:eac="urn:isbn:1-931666-33-4" xmlns="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:piaaf="http://www.piaaf.net" 
    xmlns:iso-thes="http://purl.org/iso25964/skos-thes#"
    xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:foaf="http://xmlns.com/foaf/0.1/"
    xmlns:dct="http://purl.org/dc/terms/" xmlns:xl="http://www.w3.org/2008/05/skos-xl#"
    xmlns:ginco="http://data.culture.fr/thesaurus/ginco/ns/"
    exclude-result-prefixes="xs xd eac iso-thes foaf dct xl dc ginco piaaf xlink" version="2.0">
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> July 3, 2017</xd:p>
            <xd:p><xd:b>Author:</xd:b> Florence</xd:p>
            <xd:p>Sémantisation : étape 2, génération du vocaulaire RDF/SKOS des statuts
                juridiques</xd:p>
        </xd:desc>
    </xd:doc>
    <!--  -->


    <xsl:variable name="chemin-vocabs">
        <xsl:value-of
            select="concat('fichiers-def-2/vocabulaires/', '?select=*.rdf;recurse=yes;on-error=warning')"
        />
    </xsl:variable>
    <xsl:variable name="vocabs" select="collection($chemin-vocabs)"/>


    <xsl:variable name="chemin-EAC-BnF">
        <xsl:value-of
            select="concat('fichiers-def-2/notices-EAC/BnF/', '?select=*.xml;recurse=yes;on-error=warning')"
        />
    </xsl:variable>
    <xsl:variable name="collection-EAC-BnF" select="collection($chemin-EAC-BnF)"/>
    <xsl:variable name="chemin-EAC-SIAF">
        <xsl:value-of
            select="concat('fichiers-def-2/notices-EAC/SIAF/', '?select=*.xml;recurse=yes;on-error=warning')"
        />
    </xsl:variable>
    <xsl:variable name="collection-EAC-SIAF" select="collection($chemin-EAC-SIAF)"/>
    <xsl:variable name="chemin-EAC-AN">
        <xsl:value-of
            select="concat('fichiers-def-2/notices-EAC/AN/', '?select=*.xml;recurse=yes;on-error=warning')"
        />
    </xsl:variable>
    <xsl:variable name="collection-EAC-AN" select="collection($chemin-EAC-AN)"/>


   <xsl:template match="/piaaf:vide">
       <xsl:result-document href="rdf/legal-statuses.rdf" method="xml" encoding="utf-8" indent="yes">
           <!--<rdf:RDF xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:owl="http://www.w3.org/2002/07/owl#" xmlns:RiC="http://wwww.ica.org/standards/RiC/ontology#" xmlns:isni="http://isni.org/ontology#"
               xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:piaaf-onto="http://www.piaaf.net/ontology#" xmlns:skos="http://www.w3.org/2004/02/skos/core#">-->
          
           <rdf:RDF xml:lang="fr"
               xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
               xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
               xmlns:skos="http://www.w3.org/2004/02/skos/core#"
               xmlns:dc="http://purl.org/dc/terms/">
            <skos:ConceptScheme rdf:about="http://piaaf.demo.logilab.fr/resource/legal-statuses/ls">
                   <dc:description xml:lang="fr">Vocabulaire des statuts juridiques des collectivités, élaboré pour les besoins du projet PIAAF de prototype logiciel pour la représentation RDF, l’interconnexion et la visualisation graphique de métadonnées archivistiques de provenances multiples.
                       Il a été généré à partir des notions indexées dans les notices d'autorité de producteurs d'archives réunies pour les besoins de ce projet.
                       La base de travail est le référentiel des statuts juridiques des Archives nationales
                   </dc:description>
                <dc:contributor xml:lang="fr">Florence Clavaud (Archives nationales)</dc:contributor>
                <!--   <!-\\- <skos:hasTopConcept rdf:resource="http://www.thesaurus.gouv.qc.ca/tag/terme.do?id=-1"/>-\\->-->
                   
               </skos:ConceptScheme>
               <xsl:for-each-group select="$collection-EAC-SIAF/eac:eac-cpf/eac:cpfDescription/eac:description/descendant::eac:legalStatus[not(contains(eac:term, 'indéterminé'))] | $collection-EAC-BnF/eac:eac-cpf/eac:cpfDescription/eac:description/descendant::eac:legalStatus[not(contains(eac:term, 'indéterminé'))] | $collection-EAC-AN/eac:eac-cpf/eac:cpfDescription/eac:description/descendant::eac:legalStatus[not(contains(eac:term, 'indéterminé'))]" group-by="normalize-space(eac:term)">
                   <xsl:sort select="current-grouping-key()"></xsl:sort>
                   <skos:Concept>
                       <xsl:attribute name="rdf:about">
                           <xsl:text>http://piaaf.demo.logilab.fr/resource/legal-statuses/</xsl:text>
                           <!-- <xsl:value-of select="concat('function_', generate-id())"/>-->
                           <!--  <xsl:value-of select="concat('function_', format-number(number(position()), '#000'))"/>-->
                           <xsl:text>legal-status_</xsl:text>
                          
                           
                           <xsl:choose>
                               <xsl:when test="current-group()[eac:term/@vocabularySource]">
                                   <xsl:value-of select="substring-after(current-group()[eac:term/@vocabularySource][1]/@xml:id, 'le_')"/>
                               </xsl:when>
                               <xsl:otherwise>
                                   <xsl:value-of select="substring-after(current-group()[1]/@xml:id, 'le_')"/>
                               </xsl:otherwise>
                           </xsl:choose>
                           
                           
                           <!--<xsl:value-of select="concat('http://www.piaaf.net/legal-statuses/legal-status_', format-number(number(position()), '#000'))"/>-->
                       </xsl:attribute>
                       <skos:prefLabel xml:lang="fr">
                           <xsl:value-of select="current-grouping-key()"/>
                       </skos:prefLabel>
                       <skos:inScheme>
                           <skos:ConceptScheme rdf:about="http://piaaf.demo.logilab.fr/resource/legal-statuses/ls"/>
                       </skos:inScheme>
                       <xsl:if test="current-group()[eac:term/@vocabularySource]">
                           <skos:editorialNote xml:lang="fr">
                               <xsl:text>Identifiant local du concept équivalent dans le référentiel des statuts juridiques des AN : </xsl:text>
                               <xsl:value-of select="current-group()[eac:term[@vocabularySource]][1]/eac:term[@vocabularySource][1]/@vocabularySource"/>
                           </skos:editorialNote>
                           
                       </xsl:if>
                       
                   </skos:Concept>
               </xsl:for-each-group>
               </rdf:RDF>
       </xsl:result-document>
    </xsl:template>
   
</xsl:stylesheet>
