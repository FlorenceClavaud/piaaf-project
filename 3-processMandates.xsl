<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:owl="http://www.w3.org/2002/07/owl#"
    xmlns:RiC="http://www.ica.org/standards/RiC/ontology#" 
    xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:eac="urn:isbn:1-931666-33-4" xmlns="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:piaaf="http://www.piaaf.net" 
    xmlns:iso-thes="http://purl.org/iso25964/skos-thes#"
    xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:foaf="http://xmlns.com/foaf/0.1/"
    xmlns:dct="http://purl.org/dc/terms/" xmlns:xl="http://www.w3.org/2008/05/skos-xl#"
    xmlns:ginco="http://data.culture.fr/thesaurus/ginco/ns/"
    exclude-result-prefixes="xs xd eac iso-thes foaf dct xl dc ginco piaaf xlink skos"
    version="2.0">
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> July, 3 2017</xd:p>
            <xd:p><xd:b>Author:</xd:b> Florence</xd:p>
            <xd:p>Sémantisation : étape 3, génération d'un fichier RDF pour chaque partenaire pour
                les "mandates"</xd:p>
        </xd:desc>
    </xd:doc>
   
    <xsl:param name="coll">AN</xsl:param>
    <xsl:variable name="chemin-vocabs">
        <xsl:value-of
            select="concat('fichiers-def-2/vocabulaires/', '?select=*.rdf;recurse=yes;on-error=warning')"
        />
    </xsl:variable>
    <xsl:variable name="vocabs" select="collection($chemin-vocabs)"/>


    <xsl:variable name="chemin-EAC-BnF">
        <xsl:value-of
            select="concat('fichiers-def-2/notices-EAC/BnF/', '?select=*.xml;recurse=yes;on-error=warning')"
        />
    </xsl:variable>
    <xsl:variable name="collection-EAC-BnF" select="collection($chemin-EAC-BnF)"/>
    <xsl:variable name="chemin-EAC-SIAF">
        <xsl:value-of
            select="concat('fichiers-def-2/notices-EAC/SIAF/', '?select=*.xml;recurse=yes;on-error=warning')"
        />
    </xsl:variable>
    <xsl:variable name="collection-EAC-SIAF" select="collection($chemin-EAC-SIAF)"/>
    <xsl:variable name="chemin-EAC-AN">
        <xsl:value-of
            select="concat('fichiers-def-2/notices-EAC/AN/', '?select=*.xml;recurse=yes;on-error=warning')"
        />
    </xsl:variable>
    <xsl:variable name="collection-EAC-AN" select="collection($chemin-EAC-AN)"/>
    <xsl:template match="/piaaf:vide">
        <xsl:if test="$coll = 'SIAF'">
            <xsl:result-document href="rdf/mandates/FRSIAF_mandates.rdf" method="xml" encoding="utf-8" indent="yes">
                <rdf:RDF xmlns:dc="http://purl.org/dc/elements/1.1/"
                    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                    xmlns:owl="http://www.w3.org/2002/07/owl#"
                    xmlns:RiC="http://www.ica.org/standards/RiC/ontology#"
                   
                    xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
                   
                    xmlns:skos="http://www.w3.org/2004/02/skos/core#">
                    <!-- on présuppose que les mandates mentionnés dans les notices génériques se retrouvent ailleurs et qu'il n'y en a pas d'autre -->
                    <!-- ou bien faut-il vérifier ?-->
                    <!--<xsl:for-each-group select="$collection-EAC-SIAF/eac:eac-cpf/eac:cpfDescription/eac:description/descendant::eac:mandate" group-by="normalize-space(eac:descriptiveNote)">-->
                    <!--<xsl:for-each-group select="$collection-EAC-SIAF[starts-with(eac:eac-cpf/eac:control/eac:recordId, 'FR784')]/eac:eac-cpf/eac:cpfDescription/eac:description/descendant::eac:mandate" group-by="normalize-space(eac:descriptiveNote)">-->
                    
                    <xsl:for-each-group
                        select="$collection-EAC-SIAF/eac:eac-cpf/eac:cpfDescription/eac:description/descendant::eac:mandate[not(eac:citation)]"
                        group-by="normalize-space(eac:descriptiveNote)">
                        <xsl:sort select="current-grouping-key()"></xsl:sort>
                    <!--    <xsl:variable name="num" select="format-number(number(position()), '#000')"/>-->
                        <rdf:Description>
                            <xsl:attribute name="rdf:about">
                              <!--  <xsl:text>http://www.piaaf.net/mandates/FRSIAF_mandate_a</xsl:text>
                                <xsl:value-of select="$num"/>-->
                                <xsl:text>http://piaaf.demo.logilab.fr/resource/FRSIAF_mandate_</xsl:text>
                                <xsl:value-of select="substring-after(current-group()[1]/@xml:id, 'ma_')"/>
                            </xsl:attribute>
                            <rdf:type
                                rdf:resource="http://www.ica.org/standards/RiC/ontology#Mandate"/>
                            <RiC:hasTitle xml:lang="fr">
                                <xsl:value-of select="current-grouping-key()"/>
                            </RiC:hasTitle>
                           
                                <rdfs:label xml:lang="fr">
                                    <xsl:value-of select="current-grouping-key()"/>
                                </rdfs:label>
                            
                           <!-- <xsl:if test="current-grouping-key() != ''">
                                <RiC:evidencedBy>
                                    <xsl:attribute name="rdf:resource">
                                        <xsl:text>http://www.piaaf.net/records/SIAF_record_of_mandate_</xsl:text>
                                        <xsl:value-of select="$num"/>
                                        
                                    </xsl:attribute>
                                </RiC:evidencedBy>
                            </xsl:if>-->
                        </rdf:Description>
                    </xsl:for-each-group>
                    
                    <!--<xsl:for-each-group
                        select="$collection-EAC-SIAF[starts-with(eac:eac-cpf/eac:control/eac:recordId, 'FR784')]/eac:eac-cpf/eac:cpfDescription/eac:description/descendant::eac:mandate[eac:citation]"
                        group-by="normalize-space(eac:citation/@xlink:href)">-->
                    <xsl:for-each-group
                        select="$collection-EAC-SIAF/eac:eac-cpf/eac:cpfDescription/eac:description/descendant::eac:mandate[eac:citation]"
                        group-by="normalize-space(eac:citation/@xlink:href)">
                        <xsl:sort select="current-grouping-key()"/>
                        
                   <!--     <xsl:variable name="num" select="format-number(number(position()), '#000')"/>-->
                     
                        <rdf:Description>
                            <xsl:attribute name="rdf:about">
                                <!--<xsl:text>http://www.piaaf.net/mandates/FRSIAF_mandate_b</xsl:text>
                                <xsl:value-of select="$num"/>
                            </xsl:attribute>-->
                                <xsl:text>http://piaaf.demo.logilab.fr/resource/FRSIAF_mandate_</xsl:text>
                                <xsl:value-of select="substring-after(current-group()[1]/@xml:id, 'ma_')"/>
                            </xsl:attribute>
                            <rdf:type
                                rdf:resource="http://www.ica.org/standards/RiC/ontology#Mandate"/>
                            <xsl:if test="current-grouping-key() != ''">
                                <RiC:evidencedBy>
                                    <xsl:attribute name="rdf:resource">
                                        <xsl:text>http://piaaf.demo.logilab.fr/resource/FRSIAF_record_</xsl:text>
                                       <!-- <xsl:value-of select="$num"/>-->
                                        <xsl:value-of select="substring-after(current-group()[1]/eac:citation/@xml:id, 're_')"/>
                                    </xsl:attribute>
                                </RiC:evidencedBy>
                            </xsl:if>
                        <xsl:for-each-group select="current-group()" group-by="normalize-space(eac:descriptiveNote)">
                            <xsl:sort select="current-grouping-key()"/>
                          <!--  <rdf:Description>
                                <xsl:attribute name="rdf:about">
                                    <xsl:text>http://www.piaaf.net/mandates/SIAF_mandate_</xsl:text>
                                    <xsl:value-of select="$num"/>
                                </xsl:attribute>
                            </rdf:Description>-->
                            <RiC:hasTitle xml:lang="fr">
                                <xsl:value-of select="current-grouping-key()"/>
                            </RiC:hasTitle>
                            <xsl:if test="position()=1">
                                <rdfs:label xml:lang="fr">
                                    <xsl:value-of select="current-grouping-key()"/>
                                </rdfs:label>
                            </xsl:if>
                        </xsl:for-each-group>
                        </rdf:Description>
                        
                 
                        <xsl:if test="current-grouping-key() != ''">
                            <rdf:Description>
                                <xsl:attribute name="rdf:about">
                                    <xsl:text>http://piaaf.demo.logilab.fr/resource/FRSIAF_record_</xsl:text>
                                   <!-- <xsl:value-of select="$num"/>-->
                                    <xsl:value-of select="substring-after(current-group()[1]/eac:citation/@xml:id, 're_')"/>
                                </xsl:attribute>
                                <rdf:type
                                    rdf:resource="http://www.ica.org/standards/RiC/ontology#Record"/>
                                <xsl:for-each-group select="current-group()" group-by="normalize-space(eac:descriptiveNote)">
                                    <xsl:sort select="current-grouping-key()"/>
                                    <!--  <rdf:Description>
                                <xsl:attribute name="rdf:about">
                                    <xsl:text>http://www.piaaf.net/mandates/SIAF_mandate_</xsl:text>
                                    <xsl:value-of select="$num"/>
                                </xsl:attribute>
                            </rdf:Description>-->
                                    <RiC:hasTitle xml:lang="fr">
                                        <xsl:value-of select="current-grouping-key()"/>
                                    </RiC:hasTitle>
                                    <xsl:if test="position()=1">
                                        <rdfs:label xml:lang="fr">
                                            <xsl:value-of select="current-grouping-key()"/>
                                        </rdfs:label>
                                    </xsl:if>
                                </xsl:for-each-group>
                               
                                <RiC:identifier>
                                    <xsl:value-of select="current-grouping-key()"/>
                                </RiC:identifier>

                              <!--  <RiC:isEvidenceOf>
                                    <xsl:attribute name="rdf:resource">
                                        <xsl:text>http://piaaf.demo.logilab.fr/resource/FRSIAF_mandate_</xsl:text>
                                       <!-\- <xsl:value-of select="$num"/>-\->
                                        <xsl:value-of select="substring-after(current-group()[1]/@xml:id, 'ma_')"/>
                                    </xsl:attribute>
                                </RiC:isEvidenceOf>
-->
                            </rdf:Description>
                        </xsl:if>
                    <!--</xsl:for-each-group>-->
                    </xsl:for-each-group>
                 
                    <!-- les conventions de description maintenant -->
                    <xsl:for-each-group
                        select="$collection-EAC-SIAF/eac:eac-cpf/eac:control/eac:conventionDeclaration"
                        group-by="normalize-space(eac:abbreviation)">
                        <xsl:sort select="current-grouping-key()"/>
                     <!--   <xsl:variable name="num" select="format-number(number(position()), '#000')"/>
                        <rdf:Description>-->
                        <rdf:Description>
                            <xsl:attribute name="rdf:about">
                                <xsl:text>http://piaaf.demo.logilab.fr/resource/FRSIAF_mandate_</xsl:text>
                             <!--   <xsl:value-of select="$num"/>-->
                                <xsl:value-of select="substring-after(current-group()[1]/@xml:id, 'ma_')"/>
                            </xsl:attribute>
                            <rdf:type
                                rdf:resource="http://www.ica.org/standards/RiC/ontology#Mandate"/>
                            <rdfs:label xml:lang="fr">
                                <xsl:value-of select="current-grouping-key()"/>
                            </rdfs:label>
                            <!--<xsl:choose>
                            <xsl:when test="eac:abbreviation">-->
                            <RiC:hasTitle xml:lang="fr">
                                <xsl:value-of select="current-grouping-key()"/>
                            </RiC:hasTitle>
                            <RiC:hasTitle xml:lang="fr">
                                <xsl:value-of select="normalize-space(eac:citation)"/>
                            </RiC:hasTitle>
                       

                        <!--</rdf:Description>-->
                        </rdf:Description>
                    </xsl:for-each-group>
                </rdf:RDF>
            </xsl:result-document>
        </xsl:if>
        <xsl:if test="$coll = 'BnF'">
            <xsl:result-document href="rdf/mandates/FRBNF_mandates.rdf" method="xml" encoding="utf-8" indent="yes">
                <rdf:RDF xmlns:dc="http://purl.org/dc/elements/1.1/"
                    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                    xmlns:owl="http://www.w3.org/2002/07/owl#"
                    xmlns:RiC="http://www.ica.org/standards/RiC/ontology#"
                  
                    xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
                    
                    xmlns:skos="http://www.w3.org/2004/02/skos/core#">
                  
                    <xsl:for-each-group
                        select="$collection-EAC-BnF/eac:eac-cpf/eac:cpfDescription/eac:description/descendant::eac:mandate"
                        group-by="normalize-space(eac:citation)">
                        <xsl:sort select="current-grouping-key()"/>
                       <!-- <xsl:variable name="num" select="format-number(number(position()), '#000')"/>-->
                        <rdf:Description>
                            <xsl:attribute name="rdf:about">
                                <xsl:text>http://piaaf.demo.logilab.fr/resource/FRBNF_mandate_</xsl:text>
                              <!--  <xsl:value-of select="$num"/>-->
                                <xsl:value-of select="substring-after(current-group()[1]/@xml:id, 'ma_')"/>
                            </xsl:attribute>
                            <rdf:type
                                rdf:resource="http://www.ica.org/standards/RiC/ontology#Mandate"/>

                            <rdfs:label xml:lang="fr">
                                <xsl:value-of select="normalize-space(eac:citation)"/>
                            </rdfs:label>
                            <RiC:hasTitle xml:lang="fr">
                                <xsl:value-of select="normalize-space(eac:citation)"/>
                            </RiC:hasTitle>
                            <xsl:for-each-group select="current-group()" group-by="normalize-space(eac:descriptiveNote)">
                                <xsl:sort select="current-grouping-key()"></xsl:sort>
                                <xsl:if test="current-grouping-key()!=''">
                                <RiC:description xml:lang="fr">
                                    <xsl:value-of select="current-grouping-key()"/>
                                </RiC:description>
                                </xsl:if>
                            </xsl:for-each-group>
                          <!--  <xsl:if test="eac:descriptiveNote">
                                <RiC:description xml:lang="fr">
                                    <xsl:value-of select="normalize-space(eac:descriptiveNote)"/>
                                </RiC:description>
                            </xsl:if>-->
<xsl:if test="eac:citation/@xlink:href[normalize-space(.)!='']">
                            <RiC:evidencedBy>
                                <xsl:attribute name="rdf:resource">
                                    <xsl:text>http://piaaf.demo.logilab.fr/resource/FRBNF_record_</xsl:text>
                                   <!-- <xsl:value-of select="$num"/>
-->
                                    <xsl:value-of select="substring-after(current-group()[1]/eac:citation/@xml:id, 're_')"/>
                                </xsl:attribute>
                            </RiC:evidencedBy>
</xsl:if>
                        </rdf:Description>
        
        <xsl:if test="eac:citation/@xlink:href[normalize-space(.)!='']">
                        <rdf:Description>
                            <xsl:attribute name="rdf:about">
                                <xsl:text>http://piaaf.demo.logilab.fr/resource/FRBNF_record_</xsl:text>
                             <!--   <xsl:value-of select="$num"/>
-->
                                <xsl:value-of select="substring-after(current-group()[1]/eac:citation/@xml:id, 're_')"/>
                            </xsl:attribute>
                            <rdf:type
                                rdf:resource="http://www.ica.org/standards/RiC/ontology#Record"/>
                            <rdfs:label xml:lang="fr">
                                <xsl:value-of select="normalize-space(eac:citation)"/>
                            </rdfs:label>
                            <RiC:hasTitle xml:lang="fr">
                                <xsl:value-of select="normalize-space(eac:citation)"/>
                            </RiC:hasTitle>
                            <RiC:identifier>
                               <!-- <xsl:value-of select="current-grouping-key()"/>-->
                                <xsl:value-of select="normalize-space(eac:citation/@xlink:href)"/>
                            </RiC:identifier>

                          <!--  <RiC:isEvidenceOf>
                                <xsl:attribute name="rdf:resource">
                                    <xsl:text>http://piaaf.demo.logilab.fr/resource/FRBNF_mandate_</xsl:text>
                                   <!-\- <xsl:value-of select="$num"/>-\->
                                    <xsl:value-of select="substring-after(current-group()[1]/@xml:id, 'ma_')"/>
                                </xsl:attribute>
                            </RiC:isEvidenceOf>-->

                        </rdf:Description>
        </xsl:if>
                    </xsl:for-each-group>
                 

                    <!-- les conventions de description maintenant -->
                    <xsl:for-each-group
                        select="$collection-EAC-BnF/eac:eac-cpf/eac:control/eac:conventionDeclaration"
                        group-by="normalize-space(eac:abbreviation)">
                        <xsl:sort select="current-grouping-key()"/>
                      <!--  <xsl:variable name="num" select="format-number(number(position()), '#000')"/>-->
                        <rdf:Description>
                            <xsl:attribute name="rdf:about">
                                <xsl:text>http://piaaf.demo.logilab.fr/resource/FRBNF_mandate_</xsl:text>
                               <!-- <xsl:value-of select="$num"/>-->
                                <xsl:value-of select="substring-after(current-group()[1]/@xml:id, 'ma_')"/>
                            </xsl:attribute>
                            <rdf:type
                                rdf:resource="http://www.ica.org/standards/RiC/ontology#Mandate"/>
                            <rdfs:label xml:lang="fr">
                                <xsl:value-of select="current-grouping-key()"/>
                            </rdfs:label>
                            <!--<xsl:choose>
                            <xsl:when test="eac:abbreviation">-->
                            <RiC:hasTitle xml:lang="fr">
                                <xsl:value-of select="current-grouping-key()"/>
                            </RiC:hasTitle>
                            <RiC:hasTitle xml:lang="fr">
                                <xsl:value-of select="normalize-space(eac:citation)"/>
                            </RiC:hasTitle>
                            <xsl:if test="eac:citation/@xlink:href">
                                <RiC:evidencedBy>
                                    <xsl:attribute name="rdf:resource">
                                        <xsl:text>http://piaaf.demo.logilab.fr/resource/FRBNF_record_</xsl:text>
                                      <!--  <xsl:value-of select="$num"/>-->
                                        <xsl:value-of select="substring-after(current-group()[1]/eac:citation/@xml:id, 're_')"/>
                                    </xsl:attribute>
                                </RiC:evidencedBy>

                            </xsl:if>
                          

                        </rdf:Description>
                        <xsl:if test="eac:citation/@xlink:href">
                            <rdf:Description>
                                <xsl:attribute name="rdf:about">
                                    <xsl:text>http://piaaf.demo.logilab.fr/resource/FRBNF_record_</xsl:text>
                                   <!-- <xsl:value-of select="$num"/>-->
                                    <xsl:value-of select="substring-after(current-group()[1]/eac:citation/@xml:id, 're_')"/>
                                </xsl:attribute>
                                <rdf:type
                                    rdf:resource="http://www.ica.org/standards/RiC/ontology#Record"/>
                                <rdfs:label xml:lang="fr">
                                    <xsl:value-of select="current-grouping-key()"/>
                                </rdfs:label>
                                <RiC:hasTitle xml:lang="fr">
                                    <xsl:value-of select="normalize-space(eac:citation)"/>
                                </RiC:hasTitle>
                       

                                <RiC:identifier>
                                    <xsl:value-of select="normalize-space(eac:citation/@xlink:href)"
                                    />
                                </RiC:identifier>

                                <!--<RiC:isEvidenceOf>
                                    <xsl:attribute name="rdf:resource">
                                        <xsl:text>http://piaaf.demo.logilab.fr/resource/FRBNF_mandate_</xsl:text>
                                        <!-\-<xsl:value-of select="$num"/>-\->
                                        <xsl:value-of select="substring-after(current-group()[1]/@xml:id, 'ma_')"/>
                                    </xsl:attribute>
                                </RiC:isEvidenceOf>-->
                          

                            </rdf:Description>
                        </xsl:if>

                    </xsl:for-each-group>

                </rdf:RDF>

            </xsl:result-document>
        </xsl:if>
        <xsl:if test="$coll = 'AN'">
            <xsl:result-document href="rdf/mandates/FRAN_mandates.rdf" method="xml" encoding="utf-8" indent="yes">
                <rdf:RDF xmlns:dc="http://purl.org/dc/elements/1.1/"
                    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                    xmlns:owl="http://www.w3.org/2002/07/owl#"
                    xmlns:RiC="http://www.ica.org/standards/RiC/ontology#"
                   
                    xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
                   
                    xmlns:skos="http://www.w3.org/2004/02/skos/core#">
                   
                    <xsl:for-each-group
                        select="$collection-EAC-AN/eac:eac-cpf/eac:cpfDescription/eac:description/descendant::eac:mandate"
                        group-by="
                        
                         if (contains(eac:citation, '('))
                         then (if (starts-with(normalize-space(eac:citation), '-'))
                         then (normalize-space(substring-before(substring-after(eac:citation, '-'), '(')))
                         else(
                         
                         normalize-space(substring-before(eac:citation, '('))
                         )
                         
                         )
                         else(
                          if (contains(eac:citation, ':'))
                          
                          then (if (starts-with(normalize-space(eac:citation), '-'))
                          then (normalize-space(substring-before(substring-after(eac:citation, '-'), ':')))
                          else(
                          
                          normalize-space(substring-before(eac:citation, ':'))
                          ))
                          else(
                          if (starts-with(normalize-space(eac:citation), '-'))
                          then (normalize-space(substring-after(eac:citation, '-')))
                          else(
                          
                          if (ends-with(normalize-space(eac:citation), ';'))
                          then (normalize-space(substring(normalize-space(eac:citation), 1, string-length(normalize-space(eac:citation)) -1)))
                          else(
                          
                          if (ends-with(normalize-space(eac:citation), '.'))
                          then (normalize-space(substring(normalize-space(eac:citation), 1, string-length(normalize-space(eac:citation)) -1)))
                          else(
                          
                          normalize-space(eac:citation)
                          )
                          
                          )
                          
                          )
                          )
                         )
                        ">
                        <xsl:sort select="current-grouping-key()"/>
                       <!-- <xsl:variable name="num" select="format-number(number(position()), '#000')"/>-->
                        <rdf:Description>
                            <xsl:attribute name="rdf:about">
                                <xsl:text>http://piaaf.demo.logilab.fr/resource/FRAN_mandate_</xsl:text>
                                <!--<xsl:value-of select="$num"/>-->
                                <xsl:value-of select="substring-after(current-group()[1]/@xml:id, 'ma_')"/>
                            </xsl:attribute>
                            <rdf:type
                                rdf:resource="http://www.ica.org/standards/RiC/ontology#Mandate"/>
                            
                            <rdfs:label xml:lang="fr">
                                <xsl:value-of select="current-grouping-key()"/>
                            </rdfs:label>
                            
                            <xsl:for-each-group select="current-group()" group-by=" if (starts-with(normalize-space(eac:citation), '-'))
                                then (normalize-space(substring-after(eac:citation, '-')))
                                else(
                                
                                if (ends-with(normalize-space(eac:citation), ';'))
                                then (normalize-space(substring(normalize-space(eac:citation), 1, string-length(normalize-space(eac:citation)) -1)))
                                else(
                                
                                if (ends-with(normalize-space(eac:citation), '.'))
                                then (normalize-space(substring(normalize-space(eac:citation), 1, string-length(normalize-space(eac:citation)) -1)))
                                else(
                                
                                normalize-space(eac:citation)
                                )
                                
                                )
                                
                                )">
                                <xsl:sort select="current-grouping-key()"></xsl:sort>
                                <RiC:hasTitle xml:lang="fr">
                                    <!--<xsl:value-of select="normalize-space(eac:citation)"/>-->
                                    <!--    <xsl:value-of select="current-grouping-key()"/>-->
                                    <xsl:value-of select="current-grouping-key()"/>
                                </RiC:hasTitle>
                                <xsl:if test="eac:descriptiveNote">
                                    <RiC:description xml:lang="fr">
                                        <xsl:value-of select="normalize-space(eac:descriptiveNote)"/>
                                    </RiC:description>
                                </xsl:if>
                            </xsl:for-each-group>
                       
                        </rdf:Description>
                    
                    </xsl:for-each-group>
                   
                
                  
                    <rdf:Description rdf:about="http://piaaf.demo.logilab.fr/resource/FRAN_mandate_rl001">
                        <rdf:type rdf:resource="http://www.ica.org/standards/RiC/ontology#Mandate"/>
                        <rdfs:label xml:lang="fr">AFNOR_Z44-060</rdfs:label>
                        <RiC:hasTitle xml:lang="fr">AFNOR_Z44-060</RiC:hasTitle>
                        <RiC:hasTitle xml:lang="fr">AFNOR NF Z 44-060, décembre 1996. Documentation - Catalogue d'auteurs et d'anonymes - Forme et structure des vedettes de collectivités-auteurs</RiC:hasTitle>
                    </rdf:Description>
                    <rdf:Description rdf:about="http://piaaf.demo.logilab.fr/resource/FRAN_mandate_rl002">
                        <rdf:type rdf:resource="http://www.ica.org/standards/RiC/ontology#Mandate"/>
                        <rdfs:label xml:lang="fr">AFNOR_Z44-061</rdfs:label>
                        <RiC:hasTitle xml:lang="fr">AFNOR_Z44-061</RiC:hasTitle>
                        <RiC:hasTitle xml:lang="fr">AFNOR NF Z44-061 Juin 1986 Documentation - Catalogage - Forme et structure des vedettes noms de personnes, des vedettes titres, des rubriques de classement et des titres forgés</RiC:hasTitle>
                    </rdf:Description>
                   
                  
                    <rdf:Description rdf:about="http://piaaf.demo.logilab.fr/resource/FRAN_mandate_rl003">
                        <rdf:type rdf:resource="http://www.ica.org/standards/RiC/ontology#Mandate"/>
                        <rdfs:label xml:lang="fr">ISAAR(CPF)</rdfs:label>
                        <RiC:hasTitle xml:lang="fr">ISAAR(CPF)</RiC:hasTitle>
                        <RiC:hasTitle xml:lang="fr">Norme internationale pour les notices d'autorité archivistiques (Collectivités, personnes, familles) du Conseil international des archives, 2ème édition, 1996</RiC:hasTitle>
                    </rdf:Description>
                    <rdf:Description rdf:about="http://piaaf.demo.logilab.fr/resource/FRAN_mandate_rl004">
                        <rdf:type rdf:resource="http://www.ica.org/standards/RiC/ontology#Mandate"/>
                        <rdfs:label xml:lang="fr">ISO8601</rdfs:label>
                        <RiC:hasTitle xml:lang="fr">ISO8601</RiC:hasTitle>
                        <RiC:hasTitle xml:lang="fr">Norme ISO 8601:2004 Éléments de données et formats d’échange -- Échange d’information -- Représentation de la date et de l’heure</RiC:hasTitle>
                    </rdf:Description>
                </rdf:RDF>
                
            </xsl:result-document>
        </xsl:if>
    </xsl:template>

</xsl:stylesheet>
