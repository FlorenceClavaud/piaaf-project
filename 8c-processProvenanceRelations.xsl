<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:owl="http://www.w3.org/2002/07/owl#" xmlns:RiC="http://www.ica.org/standards/RiC/ontology#"
    xmlns:isni="http://isni.org/ontology#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
    xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:eac="urn:isbn:1-931666-33-4"
    xmlns="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:piaaf="http://www.piaaf.net"
    xmlns:piaaf-onto="http://piaaf.demo.logilab.fr/ontology#"
    xmlns:iso-thes="http://purl.org/iso25964/skos-thes#"
    xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:foaf="http://xmlns.com/foaf/0.1/"
    xmlns:dct="http://purl.org/dc/terms/" xmlns:xl="http://www.w3.org/2008/05/skos-xl#"
    xmlns:ginco="http://data.culture.fr/thesaurus/ginco/ns/"
    exclude-result-prefixes="xs xd eac iso-thes rdf dct xl piaaf xlink piaaf-onto skos isni foaf ginco dc" version="2.0">
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> July 4, 2017, checked and updated Dec. 8, 2017</xd:p>
            <xd:p><xd:b>Author:</xd:b> Florence Clavaud (Archives nationales)</xd:p>
            <xd:p>Sémantisation : étape 8, c : génération d'un fichier RDF pour chaque partenaire
                pour les relations n-aires de provenance (agent à objet culturel)</xd:p>
        </xd:desc>
    </xd:doc>
  <xsl:param name="coll">FRSIAF</xsl:param>


    <xsl:variable name="chemin-EAC-BnF">
        <xsl:value-of
            select="concat('fichiers-def-2/notices-EAC/BnF/', '?select=*.xml;recurse=yes;on-error=warning')"
        />
    </xsl:variable>
    <xsl:variable name="collection-EAC-BnF" select="collection($chemin-EAC-BnF)"/>
    <xsl:variable name="chemin-EAC-SIAF">
        <xsl:value-of
            select="concat('fichiers-def-2/notices-EAC/SIAF/', '?select=*.xml;recurse=yes;on-error=warning')"
        />
    </xsl:variable>
    <xsl:variable name="collection-EAC-SIAF" select="collection($chemin-EAC-SIAF)"/>
    <xsl:variable name="chemin-EAC-AN">
        <xsl:value-of
            select="concat('fichiers-def-2/notices-EAC/AN/', '?select=*.xml;recurse=yes;on-error=warning')"
        />
    </xsl:variable>
    <xsl:variable name="collection-EAC-AN" select="collection($chemin-EAC-AN)"/>
    <xsl:variable name="apos" select="'&#x2bc;'"/>

    <xsl:variable name="chemin-IR-AN" select="concat('fichiers-def-2/IR-EAD/AN/', '?select=*.xml;recurse=yes;on-error=warning')"/>
    <xsl:variable name="collection-IR-AN" select="collection($chemin-IR-AN)"/>
    
    <xsl:variable name="chemin-IR-SIAF" select="concat('fichiers-def-2/IR-EAD/SIAF/', '?select=*.xml;recurse=yes;on-error=warning')"/>
    <xsl:variable name="collection-IR-SIAF" select="collection($chemin-IR-SIAF)"/>
    <xsl:variable name="chemin-IR-BNF" select="concat('fichiers-def-2/IR-EAD/BnF/', '?select=*.xml;recurse=yes;on-error=warning')"/>
    <xsl:variable name="collection-IR-BnF" select="collection($chemin-IR-BNF)"/>
    
    
    
    
    
    <!--<xsl:variable name="ANAgentsRelTableReduced_2" select="document('ANAgentsRelTable-corrige.xml')"/>-->
    
    
    <xsl:template match="/piaaf:vide">
       <xsl:if test="$coll='FRSIAF'">
        <xsl:result-document href="rdf/relations/FRSIAF_provenance-relations.rdf" method="xml"
            encoding="utf-8" indent="yes">
            <rdf:RDF 
                xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                xmlns:owl="http://www.w3.org/2002/07/owl#"
                xmlns:RiC="http://www.ica.org/standards/RiC/ontology#"
                
                xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
                
                
                >



                <xsl:for-each select="$collection-EAC-SIAF/eac:eac-cpf/eac:cpfDescription/eac:relations/eac:resourceRelation[@resourceRelationType='creatorOf']">
                    
                    <xsl:call-template name="outputProvenanceRelationsFromEACFiles">
                        <xsl:with-param name="recId" select="ancestor::eac:eac-cpf/eac:control/eac:recordId"></xsl:with-param>
                        <xsl:with-param name="creatorType" select="
                            if (ancestor::eac:cpfDescription/eac:identity/eac:entityType='corporateBody')
                            then ('corporate-body')
                            else (
                            if (ancestor::eac:cpfDescription/eac:identity/eac:entityType='person')
                            then ('person')
                            else (
                            
                            if (ancestor::eac:cpfDescription/eac:identity/eac:entityType='family')
                            then ('family')
                            else ()
                            )
                            
                            
                            )
                            
                            
                            
                            "/>
                        <xsl:with-param name="creatorName" select="ancestor::eac:cpfDescription/eac:identity/eac:nameEntry[@localType='preferredFormForProject']/eac:part"/>
                        <xsl:with-param name="link" select="
                            
                            if (normalize-space(@xlink:href)!='')
                            then (normalize-space(@xlink:href))
                            else (
                            'non')
                            
                            
                            "/>
                       <!-- <xsl:with-param name="num" select="format-number(number(position()), '#000')"/>-->
                        <xsl:with-param name="num" select="substring-after(@xml:id, 're_')"></xsl:with-param>
                        <xsl:with-param name="coll" select="$coll"/>
                    </xsl:call-template>
                </xsl:for-each>
            </rdf:RDF>
        </xsl:result-document>

       </xsl:if>
        <xsl:if test="$coll='FRBNF'">
            <xsl:result-document href="rdf/relations/FRBNF_provenance-relations.rdf" method="xml"
                encoding="utf-8" indent="yes">
                <rdf:RDF 
                    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                    xmlns:owl="http://www.w3.org/2002/07/owl#"
                    xmlns:RiC="http://www.ica.org/standards/RiC/ontology#"
                    
                    xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
                    
                    
                    >
                    
                    
                    
                    <xsl:for-each select="$collection-EAC-BnF/eac:eac-cpf/eac:cpfDescription/eac:relations/eac:resourceRelation[@resourceRelationType='creatorOf']">
                        
                        <xsl:call-template name="outputProvenanceRelationsFromEACFiles">
                            <xsl:with-param name="recId" select="ancestor::eac:eac-cpf/eac:control/eac:recordId"></xsl:with-param>
                            <xsl:with-param name="creatorType" select="
                                if (ancestor::eac:cpfDescription/eac:identity/eac:entityType='corporateBody')
                                then ('corporate-body')
                                else (
                                if (ancestor::eac:cpfDescription/eac:identity/eac:entityType='person')
                                then ('person')
                                else (
                                
                                if (ancestor::eac:cpfDescription/eac:identity/eac:entityType='family')
                                then ('family')
                                else ()
                                )
                                
                                
                                )
                                
                                
                                
                                "/>
                            <xsl:with-param name="creatorName" select="ancestor::eac:cpfDescription/eac:identity/eac:nameEntry[@localType='preferredFormForProject']/eac:part"/>
                            <xsl:with-param name="link" select="
                                
                                if (normalize-space(@xlink:href)!='')
                                then (normalize-space(@xlink:href))
                                else (
                                'non')
                                
                                
                                "/>
                          <!--  <xsl:with-param name="num" select="format-number(number(position()), '#000')"/>-->
                            <xsl:with-param name="num" select="substring-after(@xml:id, 're_')"></xsl:with-param>
                            <xsl:with-param name="coll" select="$coll"/>
                        </xsl:call-template>
                    </xsl:for-each>
                </rdf:RDF>
            </xsl:result-document>
            
        </xsl:if>
        
        <xsl:if test="$coll='FRAN'">
            <xsl:result-document href="rdf/relations/FRAN_provenance-relations.rdf" method="xml"
                encoding="utf-8" indent="yes">
                <rdf:RDF 
                    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                    xmlns:owl="http://www.w3.org/2002/07/owl#"
                    xmlns:RiC="http://www.ica.org/standards/RiC/ontology#"
                    
                    xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
                    
                    
                    >
                    
                    
                    
                    <xsl:for-each select="$collection-EAC-AN/eac:eac-cpf/eac:cpfDescription/eac:relations/eac:resourceRelation[@resourceRelationType='creatorOf']">
                        
                        <xsl:call-template name="outputProvenanceRelationsFromEACFiles">
                            <xsl:with-param name="recId" select="ancestor::eac:eac-cpf/eac:control/eac:recordId"></xsl:with-param>
                            <xsl:with-param name="creatorType" select="
                                if (ancestor::eac:cpfDescription/eac:identity/eac:entityType='corporateBody')
                                then ('corporate-body')
                                else (
                                if (ancestor::eac:cpfDescription/eac:identity/eac:entityType='person')
                                then ('person')
                                else (
                                
                                if (ancestor::eac:cpfDescription/eac:identity/eac:entityType='family')
                                then ('family')
                                else ()
                                )
                                
                                
                                )
                                
                                
                                
                                "/>
                            <xsl:with-param name="creatorName" select="ancestor::eac:cpfDescription/eac:identity/eac:nameEntry[@localType='preferredFormForProject']/eac:part"/>
                            <xsl:with-param name="link" select="
                                
                                if (normalize-space(@xlink:href)!='')
                                then (normalize-space(@xlink:href))
                                else (
                                'non')
                                
                                
                                "/>
                     <!--       <xsl:with-param name="num" select="format-number(number(position()), '#000')"/>-->
                            <xsl:with-param name="num" select="substring-after(@xml:id, 're_')"></xsl:with-param>
                            <xsl:with-param name="coll" select="$coll"/>
                        </xsl:call-template>
                    </xsl:for-each>
                </rdf:RDF>
            </xsl:result-document>
            
        </xsl:if>
    </xsl:template>
    <xsl:template name="outputProvenanceRelationsFromEACFiles">
        <xsl:param name="creatorName"/>
        <xsl:param name="creatorType"/>
        <xsl:param name="link"/>
        <xsl:param name="num"/>
        <xsl:param name="recId"/>
        <xsl:param name="coll"/>
   
    <!-- template pour fusionner les relations qui doivent l'être -->
    <xsl:variable name="theTrueRecId">
        <xsl:choose>
            <xsl:when test="$coll='FRSIAF'">
                <xsl:value-of select="$recId"/>
            </xsl:when>
            <xsl:when test="$coll='FRBNF'">
                <xsl:value-of select="substring-after($recId, 'DGEARC_')"/>
            </xsl:when>
            <xsl:when test="$coll='FRAN'">
                <xsl:value-of select="substring-after($recId, 'FRAN_NP_')"/>
            </xsl:when>
        </xsl:choose>
    </xsl:variable>
    <rdf:Description>
        <xsl:attribute name="rdf:about">
            <xsl:value-of select="concat('http://piaaf.demo.logilab.fr/resource/', $coll, '_ArchivalProvenanceRelation_', $num)"/>
        </xsl:attribute>
        <rdf:type rdf:resource="http://www.ica.org/standards/RiC/ontology#ArchivalProvenanceRelation"/>
        <rdfs:label xml:lang="fr">
            <xsl:text>Relation de provenance entre l'agent "</xsl:text>
            <xsl:value-of select="$creatorName"/>
            <xsl:text>" et l'objet culturel "</xsl:text>
            <xsl:value-of select="normalize-space(eac:relationEntry)"/>
            <xsl:text>"</xsl:text>
            
        </rdfs:label>
        <RiC:provenanceEntity>
            
            
            <xsl:attribute name="rdf:resource">
                <xsl:value-of select="concat('http://piaaf.demo.logilab.fr/resource/', $coll,'_', $creatorType, '_', $theTrueRecId)"/>
            </xsl:attribute>
        </RiC:provenanceEntity>
        <RiC:originatedObject>
            <!-- http://data.bnf.fr/ark:/12148/cb11863993z
                            http://catalogue.bnf.fr/ark:/12148/cb11863993z/PUBLIC
                            http://catalogue.bnf.fr/ark:/12148/cb11863993z/PUBLIC-->
            <!-- http://catalogue.bnf.fr/ark:/12148/cb437901278  http://catalogue.bnf.fr/ark:/12148/cb437901278-->
            <!-- http:///data.bnf.fr/ark:/12148/cb437901278-->
            <xsl:choose>
                <xsl:when test="$link='non'">
                    <xsl:attribute name="xml:lang">fr</xsl:attribute>
                    <xsl:value-of select="normalize-space(eac:relationEntry)"/>
                </xsl:when>
                <xsl:when test="$link!='non'">
                    <!-- dans tous les cas sauf SIAF, le producteur est spécifié au niveau de description haut, qui ne possède pas d'identifiant propre -->
                    <xsl:choose>
                        <xsl:when test="$collection-IR-SIAF/ead[$link=normalize-space(eadheader/eadid)]">
                            <!-- ce cas n'existe pas en fait -->
                            <xsl:attribute name="rdf:resource">
                                <xsl:value-of select="concat('http://piaaf.demo.logilab.fr/resource/FRSIAF_record-set_', $link, '-top')"/>
                            </xsl:attribute>
                        </xsl:when>
                      
                        
                        <xsl:when test="$collection-IR-AN/ead[archdesc/otherfindaid/p/extref/@href[starts-with($link, normalize-space(.))]]">
                        
                          
                         <!--   <xsl:attribute name="rdf:resource">
                                <xsl:value-of select="concat('http://www.piaaf.net/record-sets/FRAN_record-set_', substring-after($link, 'FRAN_'), '-top')"/>
                            </xsl:attribute>-->
                            <xsl:attribute name="rdf:resource">
                                <xsl:choose>
                                    <xsl:when test="contains($link, '#')">
                                        <xsl:value-of select="concat('http://piaaf.demo.logilab.fr/resource/FRAN_record-set_', substring-before(substring-after($link, 'FRAN_IR_'), '#'), '-top')"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="concat('http://piaaf.demo.logilab.fr/resource/FRAN_record-set_', substring-after($link, 'FRAN_IR_'), '-top')"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:attribute>
                         
                        </xsl:when>
                        <xsl:when test="$collection-IR-BnF/ead[$link=normalize-space(eadheader/eadid)]">
                            <xsl:variable name="theFindingAid" select="$collection-IR-BnF/ead[$link=normalize-space(eadheader/eadid)]"/>
                            <!-- on utilise l'identifiant ARK de l'unité de description 'IR complet' suivie du suffixe 'top'-->
                            <xsl:variable name="arkURIForBnFFindingAid">
                                <xsl:if test="$theFindingAid/archdesc/otherfindaid/p/extref/@href[starts-with(., 'http://archivesetmanuscrits.bnf.fr/ark:/12148/')]">
                                    <xsl:value-of select="substring-after($theFindingAid/archdesc/otherfindaid/p/extref/@href[starts-with(., 'http://archivesetmanuscrits.bnf.fr/ark:/12148/')], 'http://archivesetmanuscrits.bnf.fr/ark:/12148/')"/>
                                </xsl:if>
                            </xsl:variable>
                            <xsl:attribute name="rdf:resource">
                                <xsl:value-of select="concat('http://piaaf.demo.logilab.fr/resource/FRBNF_record-set_', $arkURIForBnFFindingAid, '-top')"/>
                            </xsl:attribute>
                           <!-- <xsl:text>l'IR cible est en fait l'IR</xsl:text>
                            <xsl:value-of select="$collection-IR-BnF/ead[$link=normalize-space(eadheader/eadid)]/eadheader/eadid"/>-->
                        </xsl:when>
                        <xsl:when test="starts-with($link, 'http://catalogue.bnf.fr/ark:/12148/')">
                            <xsl:variable name="targRecId" select="substring-after($link, 'http://catalogue.bnf.fr/ark:/12148/')"/>
                            <!--<xsl:attribute name="rdf:nodeID">
                                <xsl:value-of select="concat($coll, '_bnode_record_', $targRecId)"/>
                            </xsl:attribute>-->
                            <xsl:attribute name="rdf:resource">
                                <xsl:value-of select="concat('http://piaaf.demo.logilab.fr/resource/FRBNF_record_', $targRecId)"/>
                            </xsl:attribute>
                        </xsl:when>
                        <xsl:when test="starts-with($link, 'FRAN_IR_') and not($collection-IR-AN/ead[archdesc/otherfindaid/p/extref/@href[starts-with($link, normalize-space(.))]])">
                            <!--<xsl:attribute name="rdf:nodeID">
                                <xsl:value-of select="concat($coll, '_bnode_record-set_', replace(substring-after($link, 'FRAN_'), '#', '_'), '_top')"/>
                            </xsl:attribute>-->
                            <xsl:attribute name="rdf:resource">
                                <xsl:value-of select="concat('http://piaaf.demo.logilab.fr/resource/FRAN_record-set_', substring-after($link, 'FRAN_IR_'), '-top')"/>
                            </xsl:attribute>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text>RESTEAFAIRE</xsl:text>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
            </xsl:choose>
        </RiC:originatedObject>
        <xsl:if test="eac:descriptiveNote[normalize-space(.)!='']">
            <RiC:description xml:lang="fr">
                <xsl:for-each select="eac:descriptiveNote/*[normalize-space(.)!='']">
                    <xsl:apply-templates/>
                    <xsl:if test="position() != last()">
                        <xsl:text> </xsl:text>
                    </xsl:if>
                </xsl:for-each>
            </RiC:description>
        </xsl:if>
        <xsl:if test="eac:dateRange/eac:fromDate">
            <RiC:beginningDate>
                
                <xsl:choose>
                    
                                <xsl:when test="string-length(eac:dateRange/eac:fromDate/@standardDate)=4">
                                    <xsl:attribute name="rdf:datatype"> <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text></xsl:attribute>
                                   
                                </xsl:when>
                    <xsl:when test="string-length(eac:dateRange/eac:fromDate/@standardDate)=7">
                        <xsl:attribute name="rdf:datatype">
                                    <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                        </xsl:attribute>
                                </xsl:when>
                    <xsl:when test="string-length(eac:dateRange/eac:fromDate/@standardDate)=10">
                        <xsl:attribute name="rdf:datatype">
                                    <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                        </xsl:attribute>
                                </xsl:when>
                    <xsl:otherwise/>
                            </xsl:choose>
                       
                        <xsl:value-of select="
                            if (normalize-space(eac:dateRange/eac:fromDate/@standardDate)!='')
                            then (normalize-space(eac:dateRange/eac:fromDate/@standardDate))
                            else(
                            if (normalize-space(eac:dateRange/eac:fromDate)!='')
                            then (normalize-space(eac:dateRange/eac:fromDate))
                            else()
                            )
                            "/>
                  
            </RiC:beginningDate>
        </xsl:if>
        <xsl:if test="eac:dateRange/eac:toDate">
            <RiC:endDate>
            <xsl:choose>
                
                <xsl:when test="string-length(eac:dateRange/eac:toDate/@standardDate)=4">
                    <xsl:attribute name="rdf:datatype"> <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text></xsl:attribute>
                    
                </xsl:when>
                <xsl:when test="string-length(eac:dateRange/eac:toDate/@standardDate)=7">
                    <xsl:attribute name="rdf:datatype">
                        <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                    </xsl:attribute>
                </xsl:when>
                <xsl:when test="string-length(eac:dateRange/eac:toDate/@standardDate)=10">
                    <xsl:attribute name="rdf:datatype">
                        <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                    </xsl:attribute>
                </xsl:when>
                <xsl:otherwise/>
            </xsl:choose>
            
          <!--  <RiC:endDate rdf:datatype="http://www.w3.org/2001/XMLSchema#dateTime">-->
                <xsl:value-of select="
                    if (normalize-space(eac:dateRange/eac:toDate/@standardDate)!='')
                    then (normalize-space(eac:dateRange/eac:toDate/@standardDate))
                    else(
                    if (normalize-space(eac:dateRange/eac:toDate)!='')
                    then (normalize-space(eac:dateRange/eac:toDate))
                    else()
                    )
                    "/>
            </RiC:endDate>
        </xsl:if>
        <xsl:if test="eac:date">
            <RiC:date>
                <xsl:choose>
                    
                    <xsl:when test="string-length(eac:date/@standardDate)=4">
                        <xsl:attribute name="rdf:datatype"> <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text></xsl:attribute>
                        
                    </xsl:when>
                    <xsl:when test="string-length(eac:date/@standardDate)=7">
                        <xsl:attribute name="rdf:datatype">
                            <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                        </xsl:attribute>
                    </xsl:when>
                    <xsl:when test="string-length(eac:date/@standardDate)=10">
                        <xsl:attribute name="rdf:datatype">
                            <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                        </xsl:attribute>
                    </xsl:when>
                    <xsl:otherwise/>
                </xsl:choose>
                <xsl:value-of select="if (normalize-space(eac:date/@standardDate)!='')
                    then (normalize-space(eac:date/@standardDate))
                    else()"/>
            </RiC:date>
          <!--  <RiC:date rdf:datatype="http://www.w3.org/2001/XMLSchema#dateTime">
                <xsl:value-of select="if (normalize-space(eac:date/@standardDate)!='')
                    then (normalize-space(eac:date/@standardDate))
                    else()"/>
            </RiC:date>-->
        </xsl:if>
    </rdf:Description>
   
       
     
    </xsl:template>
    
    
    
    
    
</xsl:stylesheet>
