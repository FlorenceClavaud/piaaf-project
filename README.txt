-- PIAAF project --
-- Génération des fichiers RDF à partir des jeux de métadonnées source --

- Contenu du dossier -

Dossier "fichiers-def-src" : contient les fichiers source, tels que produits par les différents partenaires, après divers contrôles manuels et exécution d'un script de pré-traitement sur les fichiers EAC-CPF.
On y trouve donc : des fichiers XML/EAC-CPF - accompagnés de leur schéma W3C légèrement modifié-, XML/EAD 2002 et SKOS (= les vocabulaires construits en commun)

Dossier "fichiers-def-2" : les mêmes fichiers, la seule modification étant l'ajout d'identifiants xml:id sur les éléments XML qui serviront de base pour produire certaines ressources RDF (noms, événements, textes de référence, relations)

Dossier "rdf" : contient l'ensemble des fichiers XML/RDF produits à partir des fichiers contenus dans le dossier "fichiers-def-2"
Et l'ontologie de référence (sous-dossier ontology)

A la racine du dossier, on trouve la suite de scripts XSLT 2.0 utilisés pour opérer la conversion
Leur nom commence par un nombre qui indique dans quel ordre les exécuter ; l'ordre n'est pas toujours impératif mais parfois le script N utilise les résultats du script N-1 ou antérieur
Le premier, "0-generate-identifiers.xsl", génère les identifiants dans les fichiers XML source.
Noter qu'on a choisi de ne générer de ressources RDF pour les records et record sets que pour le niveau archdesc des fichiers EAD, + les 3 premiers niveaux hiérarchiques dans dsc

- Licences -

Les fichiers source ne sont pas placés sous licence ouverte.

L'ontologie de référence, RiC-O, n'est pas stabilisée et ne peut en l'état en aucun cas être rediffusée sans l'accord préalable écrit de Florence Clavaud, qui ne pourra le donner sans l'accord du groupe d'experts EGAD du CIA.

Les scripts XSL fonctionnent mais ne sont pas encore dans un état permettant de les diffuser. Il faut encore les nettoyer, y regrouper un bon nombre d'instructions et y harmoniser l'utilisation des paramètres initiaux, les documenter au moins sommairement, leur ajouter une licence libre.
Il faut aussi au moins ajouter un fichier batch pour Windows, permettant d'en lancer l'exécution.
Ultérieurement ils seront placés sous licence GPL.

- Informations -

Pour tout renseignement : florence.clavaud@free.fr ou florence.clavaud@culture.gouv.fr