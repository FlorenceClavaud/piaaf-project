<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:owl="http://www.w3.org/2002/07/owl#"
    xmlns:RiC="http://www.ica.org/standards/RiC/ontology#" xmlns:isni="http://isni.org/ontology#"
    xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:eac="urn:isbn:1-931666-33-4" xmlns="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:piaaf="http://www.piaaf.net" xmlns:piaaf-onto="http://piaaf.demo.logilab.fr/ontology#"
    xmlns:iso-thes="http://purl.org/iso25964/skos-thes#"
    xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:foaf="http://xmlns.com/foaf/0.1/"
    xmlns:dct="http://purl.org/dc/terms/" xmlns:xl="http://www.w3.org/2008/05/skos-xl#"
    xmlns:ginco="http://data.culture.fr/thesaurus/ginco/ns/"
    exclude-result-prefixes="xs xd eac iso-thes rdf dct xl piaaf xlink skos foaf ginco dc isni"
    version="2.0">
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> July 4, 2017, checked and updated Dec. 12, 2017</xd:p>
            <xd:p><xd:b>Author:</xd:b> Florence Clavaud (Archives nationales)</xd:p>
            <xd:p>étape 13 : génération d'un fichier RDF pour les Proxies de chacun des corpus des
                partenaires</xd:p>
            <xd:p>Les instances de la classe Proxy sont utilisées en lieu et place des instances de
                RecordSet et Record pour représenter les séquences de RSets et Records dans le
                contexte d'un RSet précis</xd:p>
            <xd:p/>
        </xd:desc>
    </xd:doc>
    <xsl:param name="coll">FRBNF</xsl:param>


    <xsl:variable name="chemin-IR-AN"
        select="concat('fichiers-def-2/IR-EAD/AN/', '?select=*.xml;recurse=yes;on-error=warning')"/>
    <xsl:variable name="collection-IR-AN" select="collection($chemin-IR-AN)"/>

    <xsl:variable name="chemin-IR-SIAF"
        select="concat('fichiers-def-2/IR-EAD/SIAF/', '?select=*.xml;recurse=yes;on-error=warning')"/>
    <xsl:variable name="collection-IR-SIAF" select="collection($chemin-IR-SIAF)"/>
    <xsl:variable name="chemin-IR-BNF"
        select="concat('fichiers-def-2/IR-EAD/BnF/', '?select=*.xml;recurse=yes;on-error=warning')"/>
    <xsl:variable name="collection-IR-BnF" select="collection($chemin-IR-BNF)"/>



    <xsl:template match="/piaaf:vide">
        <!-- les RecordSets et Records de la BNF-->

        <xsl:if test="$coll = 'FRBNF'">


            <xsl:variable name="proxies-bnf">

                <rdf:RDF 
                    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                    xmlns:owl="http://www.w3.org/2002/07/owl#"
                    xmlns:RiC="http://www.ica.org/standards/RiC/ontology#"
                    xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
                    xmlns:piaaf-onto="http://piaaf.demo.logilab.fr/ontology#"
                    xmlns:skos="http://www.w3.org/2004/02/skos/core#">
                    <xsl:for-each
                        select="$collection-IR-BnF/ead/archdesc/dsc/descendant::c[count(ancestor::c) &lt; 3][normalize-space(did) != '']">
                        <xsl:variable name="topRecordSetId">
                            <xsl:value-of
                                select="substring-after(ancestor-or-self::archdesc/otherfindaid/p[extref]/extref/@href, 'ark:/12148/')"
                            />
                        </xsl:variable>
                        <xsl:variable name="myId"
                            select="
                                
                                concat($topRecordSetId, '-c', @id)"/>

                        <xsl:variable name="unitType">
                            <xsl:choose>
                                <xsl:when test="@level = 'item'">record</xsl:when>
                                <xsl:otherwise>record-set</xsl:otherwise>
                            </xsl:choose>
                        </xsl:variable>
                        <xsl:variable name="unitClass">
                            <xsl:choose>
                                <xsl:when test="$unitType = 'record'">Record</xsl:when>
                                <xsl:otherwise>RecordSet</xsl:otherwise>
                            </xsl:choose>
                        </xsl:variable>
                        <rdf:Description>

                            <xsl:attribute name="rdf:about">
                                <xsl:value-of
                                    select="concat('http://piaaf.demo.logilab.fr/resource/', $coll, '_proxy_', $myId, '-', generate-id())"
                                />
                            </xsl:attribute>

                            <rdf:type rdf:resource="http://www.ica.org/standards/RiC/ontology#Proxy"> </rdf:type>

                            <RiC:proxyFor>
                                <xsl:attribute name="rdf:resource">
                                    <xsl:value-of
                                        select="concat('http://piaaf.demo.logilab.fr/resource/', $coll, '_', $unitType, '_', $myId)"
                                    />
                                </xsl:attribute>
                            </RiC:proxyFor>
                            <RiC:proxyIn>
                                <xsl:variable name="parentId"
                                    select="
                                        if (parent::dsc)
                                        then
                                            (concat($topRecordSetId, '-top'))
                                        else
                                            (
                                            concat($topRecordSetId, '-c', parent::c/@id)
                                            )
                                        
                                        "/>
                                <xsl:variable name="parentType">
                                    <xsl:choose>
                                        <xsl:when test="parent::dsc">
                                            <xsl:text>record-set</xsl:text>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:choose>
                                                <xsl:when
                                                  test="parent::c/@level != 'item' or not(parent::c/@level)">
                                                  <xsl:text>record-set</xsl:text>
                                                </xsl:when>
                                                <xsl:otherwise/>
                                            </xsl:choose>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </xsl:variable>
                                <xsl:attribute name="rdf:resource">
                                    <xsl:value-of
                                        select="concat('http://piaaf.demo.logilab.fr/resource/', $coll, '_', $parentType, '_', $parentId)"
                                    />
                                </xsl:attribute>
                            </RiC:proxyIn>





                        </rdf:Description>
                    </xsl:for-each>
                </rdf:RDF>
            </xsl:variable>
            <xsl:result-document method="xml" encoding="utf-8" indent="yes"
                href="rdf/proxies/FRBNF_proxies.rdf">

                <rdf:RDF 
                    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                    xmlns:owl="http://www.w3.org/2002/07/owl#"
                    xmlns:RiC="http://www.ica.org/standards/RiC/ontology#"
                    xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
                    xmlns:piaaf-onto="http://piaaf.demo.logilab.fr/ontology#"
                    xmlns:skos="http://www.w3.org/2004/02/skos/core#">

                    <xsl:for-each select="$proxies-bnf/rdf:RDF/rdf:Description">
                        <rdf:Description>
                            <xsl:attribute name="rdf:about" select="@rdf:about"/>
                            <xsl:copy-of select="*"/>
                            <xsl:variable name="proxyForId">
                                <xsl:choose>
                                    <xsl:when
                                        test="contains(RiC:proxyFor/@rdf:resource, 'record-set_')">
                                        <xsl:value-of
                                            select="substring-after(substring-after(RiC:proxyFor/@rdf:resource, 'record-set_'), '-')"
                                        />
                                    </xsl:when>
                                    <xsl:when test="contains(RiC:proxyFor/@rdf:resource, 'record_')">
                                        <xsl:value-of
                                            select="substring-after(substring-after(RiC:proxyFor/@rdf:resource, 'record_'), '-')"
                                        />
                                    </xsl:when>
                                </xsl:choose>
                            </xsl:variable>
                            <!--     <xsl:comment>$proxyForId=<xsl:value-of select="$proxyForId"/></xsl:comment>-->
                            <xsl:variable name="EADunit"
                                select="$collection-IR-BnF/ead/descendant::c[@id = substring($proxyForId, 2)]"/>

                            <xsl:variable name="EADTopUnitId"
                                select="substring-after($EADunit/ancestor::archdesc/otherfindaid/p[extref]/extref/@href, 'ark:/12148/')"/>
                            <!--  <xsl:comment>$EADTopUnitId=<xsl:value-of select="$EADTopUnitId"/></xsl:comment>-->
                            <xsl:if test="$EADunit/following-sibling::c">
                                <xsl:variable name="siblingId">
                                    <xsl:value-of select="$EADunit/following-sibling::c[1]/@id"/>
                                </xsl:variable>

                                <!-- <xsl:comment>$siblingId=<xsl:value-of select="$siblingId"/></xsl:comment>-->
                                <xsl:variable name="fullSiblingId"
                                    select="concat($EADTopUnitId, '-c', $siblingId)"/>
                                <!--  <xsl:comment>$fullSiblingId=<xsl:value-of select="$fullSiblingId"/></xsl:comment>-->
                                <xsl:variable name="proxyForSiblingId"
                                    select="$proxies-bnf/rdf:RDF/rdf:Description[ends-with(RiC:proxyFor/@rdf:resource, $fullSiblingId)]"/>
                                <RiC:hasNext>
                                    <xsl:attribute name="rdf:resource">
                                        <xsl:value-of select="$proxyForSiblingId/@rdf:about"/>
                                    </xsl:attribute>
                                </RiC:hasNext>
                            </xsl:if>
                        </rdf:Description>
                    </xsl:for-each>






                </rdf:RDF>
            </xsl:result-document>
        </xsl:if>


        <xsl:if test="$coll = 'FRAN'">


            <xsl:variable name="proxies-an">

                <rdf:RDF 
                    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                    xmlns:owl="http://www.w3.org/2002/07/owl#"
                    xmlns:RiC="http://www.ica.org/standards/RiC/ontology#"
                    xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
                    xmlns:piaaf-onto="http://piaaf.demo.logilab.fr/ontology#"
                    xmlns:skos="http://www.w3.org/2004/02/skos/core#">
                    <xsl:for-each
                        select="$collection-IR-AN/ead/archdesc/dsc/descendant::c[count(ancestor::c) &lt; 3][normalize-space(did) != '']">
                        <xsl:variable name="topRecordSetId">

                            <xsl:choose>
                                <xsl:when
                                    test="ancestor-or-self::archdesc/otherfindaid/p[extref]/extref/@href">
                                    <xsl:value-of
                                        select="substring-after(ancestor-or-self::archdesc/otherfindaid/p[extref]/extref/@href, 'FRAN_IR_')"
                                    />
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of
                                        select="replace(substring-after(ancestor::ead/eadheader/eadid, 'FRAN_IR_'), '_', '')"
                                    />
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:variable>
                        <xsl:variable name="myId"
                            select="
                                
                                concat($topRecordSetId, '-', @id)"/>

                        <xsl:variable name="unitType">
                            <xsl:choose>
                                <xsl:when test="@level = 'item'">record</xsl:when>
                                <xsl:otherwise>record-set</xsl:otherwise>
                            </xsl:choose>
                        </xsl:variable>
                        <xsl:variable name="unitClass">
                            <xsl:choose>
                                <xsl:when test="$unitType = 'record'">Record</xsl:when>
                                <xsl:otherwise>RecordSet</xsl:otherwise>
                            </xsl:choose>
                        </xsl:variable>
                        <rdf:Description>

                            <xsl:attribute name="rdf:about">
                                <xsl:value-of
                                    select="concat('http://piaaf.demo.logilab.fr/resource/', $coll, '_proxy_', $myId, '-', generate-id())"
                                />
                            </xsl:attribute>

                            <rdf:type rdf:resource="http://www.ica.org/standards/RiC/ontology#Proxy"> </rdf:type>

                            <RiC:proxyFor>
                                <xsl:attribute name="rdf:resource">
                                    <xsl:value-of
                                        select="concat('http://piaaf.demo.logilab.fr/resource/', $coll, '_', $unitType, '_', $myId)"
                                    />
                                </xsl:attribute>
                            </RiC:proxyFor>
                            <RiC:proxyIn>
                                <xsl:variable name="parentId"
                                    select="
                                        if (parent::dsc)
                                        then
                                            (concat($topRecordSetId, '-top'))
                                        else
                                            (
                                            concat($topRecordSetId, '-', parent::c/@id)
                                            )
                                        
                                        "/>
                                <xsl:variable name="parentType">
                                    <xsl:choose>
                                        <xsl:when test="parent::dsc">
                                            <xsl:text>record-set</xsl:text>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:choose>
                                                <xsl:when
                                                  test="parent::c/@level != 'item' or not(parent::c/@level)">
                                                  <xsl:text>record-set</xsl:text>
                                                </xsl:when>
                                                <xsl:otherwise/>
                                            </xsl:choose>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </xsl:variable>
                                <xsl:attribute name="rdf:resource">
                                    <xsl:value-of
                                        select="concat('http://piaaf.demo.logilab.fr/resource/', $coll, '_', $parentType, '_', $parentId)"
                                    />
                                </xsl:attribute>
                            </RiC:proxyIn>

















                        </rdf:Description>
                    </xsl:for-each>
                </rdf:RDF>
            </xsl:variable>
            <xsl:result-document method="xml" encoding="utf-8" indent="yes"
                href="rdf/proxies/FRAN_proxies.rdf">

                <rdf:RDF 
                    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                    xmlns:owl="http://www.w3.org/2002/07/owl#"
                    xmlns:RiC="http://www.ica.org/standards/RiC/ontology#"
                    xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
                    xmlns:piaaf-onto="http://piaaf.demo.logilab.fr/ontology#"
                    xmlns:skos="http://www.w3.org/2004/02/skos/core#">

                    <xsl:for-each select="$proxies-an/rdf:RDF/rdf:Description">
                        <xsl:variable name="rdfab" select="@rdf:about"/>
                        <rdf:Description>
                            <xsl:attribute name="rdf:about" select="$rdfab"/>
                            <xsl:copy-of select="*"/>
                            <!-- on a besoin de l'id de l'IR et de l'id de l'UD -->
                            <xsl:variable name="theEADid">
                                <xsl:choose>
                                    <xsl:when
                                        test="matches(substring-after($rdfab, 'http://piaaf.demo.logilab.fr/resource/FRAN_proxy_'), '^([0-9]{6})-([\w_-]*)$')">
                                        <xsl:value-of
                                            select="substring(substring-after($rdfab, 'http://piaaf.demo.logilab.fr/resource/FRAN_proxy_'), 1, 6)"
                                        />
                                    </xsl:when>
                                    <xsl:when
                                        test="matches(substring-after($rdfab, 'http://piaaf.demo.logilab.fr/resource/FRAN_proxy_'), '^protoLDDECAS([0-9]{3})-([\w_-]*)$')">
                                        <xsl:value-of
                                            select="substring(substring-after($rdfab, 'http://piaaf.demo.logilab.fr/resource/FRAN_proxy_protoLDDECAS'), 1, 3)"
                                        />
                                    </xsl:when>
                                    <xsl:when
                                        test="matches(substring-after($rdfab, 'http://piaaf.demo.logilab.fr/resource/FRAN_proxy_'), '^protoLDMR([0-9]{3})-([\w_-]*)$')">
                                        <xsl:value-of
                                            select="substring(substring-after($rdfab, 'http://piaaf.demo.logilab.fr/resource/FRAN_proxy_protoLDMR'), 1, 3)"
                                        />
                                    </xsl:when>
                                </xsl:choose>
                            </xsl:variable>
                            <!--  <xsl:comment>  <xsl:text>$theEADid=</xsl:text>
                            <xsl:value-of select="$theEADid"/></xsl:comment>
-->
                            <xsl:variable name="theFullEADid">
                                <xsl:choose>
                                    <xsl:when test="matches($theEADid, '^[0-9]{6}$')">
                                        <xsl:value-of select="$theEADid"/>
                                    </xsl:when>
                                    <xsl:when test="matches($theEADid, '^[0-9]{3}$')">
                                        <xsl:text>protoLD</xsl:text>
                                        <xsl:choose>
                                            <xsl:when
                                                test="matches(substring-after(@rdf:about, 'http://piaaf.demo.logilab.fr/resource/FRAN_proxy_'), '^protoLDDECAS([0-9]{3})-([\w_-]*)$')"
                                                >DECAS</xsl:when>
                                            <xsl:when
                                                test="matches(substring-after(@rdf:about, 'http://piaaf.demo.logilab.fr/resource/FRAN_proxy_'), '^protoLDMR([0-9]{3})-([\w_-]*)$')"
                                                >MR</xsl:when>
                                        </xsl:choose>
                                        <xsl:value-of select="$theEADid"/>
                                    </xsl:when>
                                </xsl:choose>
                            </xsl:variable>
                            <!--      <xsl:comment>  <xsl:text>$theFullEADid=</xsl:text>
                            <xsl:value-of select="$theFullEADid"/></xsl:comment>-->

                            <xsl:variable name="EADunit"
                                select="
                                    
                                    if ($collection-IR-AN/ead[archdesc/otherfindaid/p[extref/@href = concat('FRAN_IR_', $theEADid)]])
                                    
                                    then
                                        ($collection-IR-AN/ead[archdesc/otherfindaid/p[extref/@href = concat('FRAN_IR_', $theEADid)]]/descendant::c[contains($rdfab, concat('FRAN_proxy_', $theFullEADid, '-', @id, '-'))])
                                    else
                                        (
                                        if ($collection-IR-AN/ead[eadheader/eadid = concat('FRAN_IR_protoLD_MR_', $theEADid)])
                                        then
                                            ($collection-IR-AN/ead[eadheader/eadid = concat('FRAN_IR_protoLD_MR_', $theEADid)]/descendant::c[contains($rdfab, concat('FRAN_proxy_', $theFullEADid, '-', @id, '-'))])
                                        
                                        else
                                            (
                                            
                                            if ($collection-IR-AN/ead[eadheader/eadid = concat('FRAN_IR_protoLD_DECAS_', $theEADid)])
                                            then
                                                ($collection-IR-AN/ead[eadheader/eadid = concat('FRAN_IR_protoLD_DECAS_', $theEADid)]/descendant::c[contains($rdfab, concat('FRAN_proxy_', $theFullEADid, '-', @id, '-'))])
                                            else
                                                ()
                                            )
                                        
                                        
                                        )
                                    
                                    
                                    
                                    
                                    "> </xsl:variable>




                            <xsl:if test="$EADunit/following-sibling::c">
                                <xsl:variable name="siblingId">
                                    <xsl:value-of select="$EADunit/following-sibling::c[1]/@id"/>
                                </xsl:variable>
                                <!-- <xsl:comment>$siblingId=<xsl:value-of select="$siblingId"/></xsl:comment>-->
                                <xsl:variable name="fullSiblingId"
                                    select="concat($theFullEADid, '-', $siblingId)"/>
                                <!-- <xsl:comment>$fullSiblingId=<xsl:value-of select="$fullSiblingId"/></xsl:comment>-->
                                <xsl:variable name="proxyForSiblingId"
                                    select="$proxies-an/rdf:RDF/rdf:Description[ends-with(RiC:proxyFor/@rdf:resource, $fullSiblingId)]"/>
                                <RiC:hasNext>
                                    <xsl:attribute name="rdf:resource">
                                        <xsl:value-of select="$proxyForSiblingId/@rdf:about"/>
                                    </xsl:attribute>
                                </RiC:hasNext>
                            </xsl:if>
                        </rdf:Description>
                    </xsl:for-each>






                </rdf:RDF>
            </xsl:result-document>
        </xsl:if>

        <xsl:if test="$coll = 'FRSIAF'">

            <!-- <xsl:variable name="outputFileUrl" select="concat('rdf/proxies/',  $coll,'_proxy_', $myId, generate-id(), '.rdf')"/>-->
            <xsl:variable name="proxies-siaf">

                <rdf:RDF 
                    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                    xmlns:owl="http://www.w3.org/2002/07/owl#"
                    xmlns:RiC="http://www.ica.org/standards/RiC/ontology#"
                    xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
                    xmlns:piaaf-onto="http://piaaf.demo.logilab.fr/ontology#"
                    xmlns:skos="http://www.w3.org/2004/02/skos/core#">
                    <xsl:for-each
                        select="$collection-IR-SIAF/ead/archdesc/dsc/descendant::c[normalize-space(did) != '']">
                        <xsl:variable name="IR_id">

                            <xsl:value-of select="normalize-space(ancestor::ead/eadheader/eadid)"/>

                        </xsl:variable>
                        <xsl:variable name="topRecordSetId">

                            <xsl:value-of select="replace($IR_id, '_', '')"/>


                        </xsl:variable>
                        <xsl:variable name="myId"
                            select="
                                
                                concat($topRecordSetId, '-', @id)"/>

                        <xsl:variable name="unitType">
                            <xsl:choose>
                                <xsl:when test="@level = 'item'">record</xsl:when>
                                <xsl:otherwise>record-set</xsl:otherwise>
                            </xsl:choose>
                        </xsl:variable>
                        <xsl:variable name="unitClass">
                            <xsl:choose>
                                <xsl:when test="$unitType = 'record'">Record</xsl:when>
                                <xsl:otherwise>RecordSet</xsl:otherwise>
                            </xsl:choose>
                        </xsl:variable>
                        <rdf:Description>
                            <!-- <xsl:variable name="uri"
                                select="concat('http://www.piaaf.net/', $unitType, 's/', $coll, '_', $unitType, '_', $myId)"/>-->
                            <xsl:attribute name="rdf:about">
                                <xsl:value-of
                                    select="concat('http://piaaf.demo.logilab.fr/resource/', $coll, '_proxy_', $myId, '-', generate-id())"
                                />
                            </xsl:attribute>
                            <!-- <rdf:type
                                rdf:resource="http://www.ica.org/standards/RiC/ontology#RecordSet"/>-->
                            <rdf:type rdf:resource="http://www.ica.org/standards/RiC/ontology#Proxy"> </rdf:type>

                            <RiC:proxyFor>
                                <xsl:attribute name="rdf:resource">
                                    <xsl:value-of
                                        select="concat('http://piaaf.demo.logilab.fr/resource/', $coll, '_', $unitType, '_', $myId)"
                                    />
                                </xsl:attribute>
                            </RiC:proxyFor>
                            <RiC:proxyIn>
                                <xsl:variable name="parentId"
                                    select="
                                        if (parent::dsc)
                                        then
                                            (concat($topRecordSetId, '-top'))
                                        else
                                            (
                                            concat($topRecordSetId, '-', parent::c/@id)
                                            )
                                        
                                        "/>
                                <xsl:variable name="parentType">
                                    <xsl:choose>
                                        <xsl:when test="parent::dsc">
                                            <xsl:text>record-set</xsl:text>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:choose>
                                                <xsl:when
                                                  test="parent::c/@level != 'item' or not(parent::c/@level)">
                                                  <xsl:text>record-set</xsl:text>
                                                </xsl:when>
                                                <xsl:otherwise/>
                                            </xsl:choose>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </xsl:variable>
                                <xsl:attribute name="rdf:resource">
                                    <xsl:value-of
                                        select="concat('http://piaaf.demo.logilab.fr/resource/', $coll, '_', $parentType, '_', $parentId)"
                                    />
                                </xsl:attribute>
                            </RiC:proxyIn>
                            <!-- génération de RiC:hasNext-->
                            <!--  <xsl:if test="following-sibling::c">
                               <xsl:variable name="siblingId">
                                    <xsl:value-of select="concat($topRecordSetId, '-', following-sibling::c[1]/@id, '-', generate-id())"/>
                                </xsl:variable>
                               <!-\- <xsl:variable name="siblingType">
                                   
                                            <xsl:choose>
                                                <xsl:when test="following-sibling::c[1]/@level!='item' or not(following-sibling::c[1]/@level)">
                                                    <xsl:text>record-set</xsl:text>
                                                </xsl:when>
                                                <xsl:when test="following-sibling::c[1]/@level='item'">record</xsl:when>
                                             
                                            </xsl:choose>
                                </xsl:variable>-\->
                                <RiC:hasNext>
                                    <xsl:attribute name="rdf:resource">
                                        <xsl:value-of select="concat('http://www.piaaf.net/proxies/', $coll, '_proxy_', $siblingId)"/>
                                    </xsl:attribute>
                                </RiC:hasNext>
                            </xsl:if>-->
















                        </rdf:Description>
                    </xsl:for-each>
                </rdf:RDF>
            </xsl:variable>
            <xsl:result-document method="xml" encoding="utf-8" indent="yes"
                href="rdf/proxies/FRSIAF_proxies.rdf">

                <rdf:RDF 
                    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                    xmlns:owl="http://www.w3.org/2002/07/owl#"
                    xmlns:RiC="http://www.ica.org/standards/RiC/ontology#"
                    xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
                    xmlns:piaaf-onto="http://piaaf.demo.logilab.fr/ontology#"
                    xmlns:skos="http://www.w3.org/2004/02/skos/core#">

                    <xsl:for-each select="$proxies-siaf/rdf:RDF/rdf:Description">
                        <xsl:variable name="rdfab" select="@rdf:about"/>
                        <rdf:Description>
                            <xsl:attribute name="rdf:about" select="$rdfab"/>
                            <xsl:copy-of select="*"/>
                            <!-- on a besoin de l'id de l'IR et de l'id de l'UD -->
                            <xsl:variable name="theEADid">
                                <xsl:choose>
                                    <xsl:when
                                        test="matches(substring-after($rdfab, 'http://piaaf.demo.logilab.fr/resource/FRSIAF_proxy_'), 'FRAD033BV0111')">
                                        <xsl:value-of select="'FRAD033BV0111'"/>
                                    </xsl:when>
                                    <xsl:when
                                        test="matches(substring-after($rdfab, 'http://piaaf.demo.logilab.fr/resource/FRSIAF_proxy_'), 'FRAD033IR4T')">
                                        <xsl:value-of select="'FRAD033IR4T'"/>
                                    </xsl:when>

                                </xsl:choose>
                            </xsl:variable>
                            <!--   <xsl:comment>  <xsl:text>$theEADid=</xsl:text>
                            <xsl:value-of select="$theEADid"/></xsl:comment>
-->
                            <xsl:variable name="theFullEADid">
                                <xsl:choose>
                                    <xsl:when test="$theEADid = 'FRAD033IR4T'">
                                        <xsl:value-of select="'FRAD033_IR_4T'"/>
                                    </xsl:when>
                                    <xsl:when test="$theEADid = 'FRAD033BV0111'">
                                        <xsl:value-of select="'FRAD033_BV_0111'"/>
                                    </xsl:when>

                                </xsl:choose>
                            </xsl:variable>
                            <!-- <xsl:comment>  <xsl:text>$theFullEADid=</xsl:text>
                            <xsl:value-of select="$theFullEADid"/></xsl:comment>-->

                            <xsl:variable name="EADunit"
                                select="
                                    
                                    if ($collection-IR-SIAF/ead[eadheader/eadid = $theFullEADid])
                                    
                                    then
                                        ($collection-IR-SIAF/ead[eadheader/eadid = $theFullEADid]/descendant::c[contains($rdfab, concat('FRSIAF_proxy_', $theEADid, '-', @id, '-'))])
                                    else
                                        (
                                        )
                                    
                                    
                                    
                                    
                                    "> </xsl:variable>
                            <!-- <xsl:comment>
                                <xsl:if test="$collection-IR-SIAF/ead[eadheader/eadid = $theFullEADid]"><xsl:text>inventaire trouvé</xsl:text></xsl:if>
                            </xsl:comment>-->



                            <xsl:if test="$EADunit/following-sibling::c">
                                <xsl:variable name="siblingId">
                                    <xsl:value-of select="$EADunit/following-sibling::c[1]/@id"/>
                                </xsl:variable>
                                <!--    <xsl:comment>$siblingId=<xsl:value-of select="$siblingId"/></xsl:comment>-->
                                <xsl:variable name="fullSiblingId"
                                    select="concat($theEADid, '-', $siblingId)"/>
                                <!--<xsl:comment>$fullSiblingId=<xsl:value-of select="$fullSiblingId"/></xsl:comment>-->
                                <xsl:variable name="proxyForSiblingId"
                                    select="$proxies-siaf/rdf:RDF/rdf:Description[ends-with(RiC:proxyFor/@rdf:resource, $fullSiblingId)]"/>
                                <RiC:hasNext>
                                    <xsl:attribute name="rdf:resource">
                                        <xsl:value-of select="$proxyForSiblingId/@rdf:about"/>
                                    </xsl:attribute>
                                </RiC:hasNext>
                            </xsl:if>
                        </rdf:Description>
                    </xsl:for-each>






                </rdf:RDF>
            </xsl:result-document>
        </xsl:if>



    </xsl:template>

</xsl:stylesheet>
