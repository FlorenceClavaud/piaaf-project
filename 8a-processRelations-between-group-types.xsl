<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:owl="http://www.w3.org/2002/07/owl#" xmlns:RiC="http://www.ica.org/standards/RiC/ontology#"
    xmlns:isni="http://isni.org/ontology#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
    xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:eac="urn:isbn:1-931666-33-4"
    xmlns="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:piaaf="http://www.piaaf.net"
    xmlns:piaaf-onto="http://piaaf.demo.logilab.fr/ontology#"
    xmlns:iso-thes="http://purl.org/iso25964/skos-thes#"
    xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:foaf="http://xmlns.com/foaf/0.1/"
    xmlns:dct="http://purl.org/dc/terms/" xmlns:xl="http://www.w3.org/2008/05/skos-xl#"
    xmlns:ginco="http://data.culture.fr/thesaurus/ginco/ns/"
    exclude-result-prefixes="xs xd eac iso-thes rdf dct xl piaaf xlink piaaf-onto skos isni foaf ginco dc" version="2.0">
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> July 3, 2017, checked and updated Dec. 8, 2017</xd:p>
            <xd:p><xd:b>Author:</xd:b> Florence</xd:p>
            <xd:p>Sémantisation : étape 7, génération d'un fichier RDF pour chaque partenaire pour
                les relations n-aires de GType à GType</xd:p>
        </xd:desc>
    </xd:doc>
    
  
     
   

    


    <xsl:variable name="chemin-EAC-BnF">
        <xsl:value-of
            select="concat('fichiers-def-2/notices-EAC/BnF/', '?select=*.xml;recurse=yes;on-error=warning')"
        />
    </xsl:variable>
    <xsl:variable name="collection-EAC-BnF" select="collection($chemin-EAC-BnF)"/>
    <xsl:variable name="chemin-EAC-SIAF">
        <xsl:value-of
            select="concat('fichiers-def-2/notices-EAC/SIAF/', '?select=*.xml;recurse=yes;on-error=warning')"
        />
    </xsl:variable>
    <xsl:variable name="collection-EAC-SIAF" select="collection($chemin-EAC-SIAF)"/>
    <xsl:variable name="chemin-EAC-AN">
        <xsl:value-of
            select="concat('fichiers-def-2/notices-EAC/AN/', '?select=*.xml;recurse=yes;on-error=warning')"
        />
    </xsl:variable>
    <xsl:variable name="collection-EAC-AN" select="collection($chemin-EAC-AN)"/>
    <xsl:variable name="apos" select="'&#x2bc;'"/>
    
    <xsl:variable name="RelTable">
       
        <piaaf:relations>

        <xsl:for-each
            select="$collection-EAC-SIAF/eac:eac-cpf[starts-with(normalize-space(eac:control/eac:recordId), 'FR784')]/eac:cpfDescription/eac:relations/eac:cpfRelation[@cpfRelationType!='identity']">

         
            <piaaf:rel type="{@cpfRelationType}" xml:id="{@xml:id}">
                <xsl:if test="@xlink:arcrole">
                    <xsl:attribute name="arcrole">
                        <xsl:value-of
                            select="substring-after(@xlink:arcrole, 'http://www.piaaf.net/ontology/piaaf#')"
                        />
                    </xsl:attribute>
                </xsl:if>
                <piaaf:fromAgent>
                    <xsl:value-of select="ancestor::eac:eac-cpf/eac:control/eac:recordId"/>
                </piaaf:fromAgent>
                <piaaf:ancAgentType>
                    <xsl:value-of
                        select="ancestor::eac:eac-cpf/eac:cpfDescription/eac:identity/eac:entityType"
                    />
                </piaaf:ancAgentType>
                <xsl:if test="normalize-space(@xlink:href) != ''">
                    <piaaf:targetEntity>
                        
                        <xsl:value-of select="normalize-space(@xlink:href)"/>
                    </piaaf:targetEntity>
                </xsl:if>
                
                
                <piaaf:targetName>
                    <xsl:value-of select="normalize-space(eac:relationEntry)"/>
                </piaaf:targetName>
               
                <xsl:if
                    test="eac:date[(@standardDate and normalize-space(@standardDate) != '') or normalize-space(.) != '']">
                    <piaaf:date>
                       
                        <xsl:choose>
                            <xsl:when test="eac:date[normalize-space(@standardDate) != '']">
                                <xsl:attribute name="type">iso8601</xsl:attribute>
                                <xsl:value-of select="normalize-space(eac:date/@standardDate)"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="normalize-space(eac:date)"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </piaaf:date>
                </xsl:if>
                <xsl:if test="eac:dateRange">
                    <piaaf:dateRange>
                        <xsl:if
                            test="eac:dateRange/eac:fromDate[@standardDate[normalize-space(.) != ''] or normalize-space(.) != '']">
                            <xsl:choose>
                                <xsl:when
                                    test="eac:dateRange/eac:fromDate[normalize-space(@standardDate) != '']">
                                    <piaaf:fromDate type='iso8601'>
                                        <xsl:value-of
                                            select="eac:dateRange/eac:fromDate/@standardDate"/>
                                    </piaaf:fromDate>
                                </xsl:when>
                                <xsl:when
                                    test="eac:dateRange/eac:fromDate[normalize-space(.) != '']">
                                    <piaaf:fromDate>
                                        <xsl:value-of
                                            select="normalize-space(eac:dateRange/eac:fromDate)"
                                        />
                                    </piaaf:fromDate>
                                </xsl:when>
                            </xsl:choose>
                            
                        </xsl:if>
                        <xsl:if
                            test="eac:dateRange/eac:toDate[@standardDate[normalize-space(.) != ''] or normalize-space(.) != '']">
                            <xsl:choose>
                                <xsl:when
                                    test="eac:dateRange/eac:toDate[normalize-space(@standardDate) != '']">
                                    <piaaf:toDate type='iso8601'>
                                        <xsl:value-of
                                            select="eac:dateRange/eac:toDate/@standardDate"/>
                                    </piaaf:toDate>
                                </xsl:when>
                                <xsl:when
                                    test="eac:dateRange/eac:toDate[normalize-space(.) != '']">
                                    <piaaf:toDate>
                                        <xsl:value-of
                                            select="normalize-space(eac:dateRange/eac:toDate)"/>
                                    </piaaf:toDate>
                                </xsl:when>
                            </xsl:choose>
                        </xsl:if>
                    </piaaf:dateRange>
                </xsl:if>
                <xsl:if test="eac:descriptiveNote[normalize-space(.) != '']">
                    <piaaf:note>
                        <xsl:for-each
                            select="eac:descriptiveNote[normalize-space(.) != '']/eac:p[normalize-space(.) != '']">
                            <piaaf:p>
                                <xsl:copy-of select="node()"/>
                            </piaaf:p>
                        </xsl:for-each>
                    </piaaf:note>
                </xsl:if>
            <!--</piaaf:rel>-->
            </piaaf:rel>
            
        </xsl:for-each>
        </piaaf:relations>

    </xsl:variable>
    <xsl:variable name="relTable_reduced">
        <piaaf:relations>
            <xsl:for-each select="$RelTable/piaaf:relations/piaaf:rel">
                <xsl:variable name="anc" select="piaaf:fromAgent"/>
                <xsl:variable name="targ" select="piaaf:targetEntity"/>
                <xsl:variable name="tName" select="piaaf:targetName"/>
                <xsl:variable name="date" select="piaaf:date"/>
                <xsl:variable name="fdate" select="piaaf:dateRange/piaaf:fromDate"/>
                <xsl:variable name="tdate" select="piaaf:dateRange/piaaf:toDate"/>
                <xsl:variable name="note" select="piaaf:note"/>
           
               
                 
                     
                        <xsl:call-template name="mergeRelationsInFirstTable">
                            <xsl:with-param name="anc" select="$anc"/>
                            <xsl:with-param name="date" select="$date"/>
                            <xsl:with-param name="fdate" select="$fdate"/>
                            <xsl:with-param name="tdate" select="$tdate"/> 
                            <xsl:with-param name="note" select="$note"/>
                            <xsl:with-param name="targ" select="$targ"/>
                            <xsl:with-param name="tName" select="$tName"/>
                            <xsl:with-param name="theRelId" select="@xml:id"/>
                        </xsl:call-template>
                        
                        
                  
 
            </xsl:for-each>
        </piaaf:relations>
    </xsl:variable>
    <xsl:variable name="relTable_final">
        <piaaf:relations>
            <xsl:for-each select="$relTable_reduced/piaaf:relations/piaaf:rel">
                <xsl:variable name="myId" select="@xml:id"/>
                <piaaf:rel>
                    <xsl:copy-of select="attribute::*"/>
                    <xsl:copy-of select="node()[not(self::piaaf:note)]"/>
                    <xsl:choose>
                        <xsl:when test="piaaf:note">
                            <piaaf:note>
                                <xsl:copy-of select="piaaf:note/node()"/>
                                <xsl:if
                                    test="$relTable_reduced/piaaf:relations/piaaf:specialNote[@toBeMovedToRel = $myId]">
                                    <piaaf:p
                                        fromRel="{$relTable_reduced/piaaf:relations/piaaf:specialNote[@toBeMovedToRel=$myId]/@fromRel}">
                                        <xsl:copy-of
                                            select="$relTable_reduced/piaaf:relations/piaaf:specialNote[@toBeMovedToRel = $myId]/piaaf:p/node()"
                                        />
                                    </piaaf:p>
                                </xsl:if>
                            </piaaf:note>
                        </xsl:when>
                        <xsl:when test="not(piaaf:note)">
                            <xsl:if
                                test="$relTable_reduced/piaaf:relations/piaaf:specialNote[@toBeMovedToRel = $myId]">
                                <piaaf:note
                                    fromRel="{$relTable_reduced/piaaf:relations/piaaf:specialNote[@toBeMovedToRel=$myId]/@fromRel}">
                                    <xsl:copy-of
                                        select="$relTable_reduced/piaaf:relations/piaaf:specialNote[@toBeMovedToRel = $myId]/node()"
                                    />
                                </piaaf:note>
                                
                            </xsl:if>
                        </xsl:when>
                    </xsl:choose>
                    
                </piaaf:rel>
                
            </xsl:for-each>
         
        </piaaf:relations>
    </xsl:variable>
    <xsl:template match="/piaaf:vide">
        <xsl:result-document href="rdf/relations/FRSIAF_gtypes-relTable.xml" indent="yes">
            
                <xsl:copy-of select="$RelTable/*"/>
            
        </xsl:result-document>
        <xsl:result-document href="rdf/relations/FRSIAF_gtypes-relTable_reduced.xml" indent="yes">
            <xsl:copy-of select="$relTable_reduced/*"/>
        </xsl:result-document>
        <xsl:result-document href="rdf/relations/FRSIAF_gtypes-relTable_final.xml" indent="yes">
            <xsl:copy-of select="$relTable_final/*"/>
        </xsl:result-document>
        <xsl:result-document href="rdf/relations/FRSIAF_relations-between-group-types.rdf" method="xml"
            encoding="utf-8" indent="yes">
            <rdf:RDF 
                xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                xmlns:owl="http://www.w3.org/2002/07/owl#"
                xmlns:RiC="http://www.ica.org/standards/RiC/ontology#"
                
                xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
               
                
                >
                <!-- <part localType="nom">La Conté</part>
            <part localType="prenom">Marie-Christiane de</part>
            <part localType="dates_biographiques">1950- </part>-->
                <!-- on présuppose que les mandates mentionnés dans les notices génériques se retrouvent ailleurs et qu'il n'y en a pas d'autre -->
                <!-- ou bien faut-il vérifier ?-->
                <!--<xsl:for-each-group select="$collection-EAC-SIAF/eac:eac-cpf/eac:cpfDescription/eac:description/descendant::eac:mandate" group-by="normalize-space(eac:descriptiveNote)">-->
                <!--<xsl:for-each-group select="$collection-EAC-SIAF[starts-with(eac:eac-cpf/eac:control/eac:recordId, 'FR784')]/eac:eac-cpf/eac:cpfDescription/eac:description/descendant::eac:mandate" group-by="normalize-space(eac:descriptiveNote)">-->

               <xsl:for-each select="$relTable_final/piaaf:relations/piaaf:rel">
                   
                
                       <xsl:call-template name="output-relations-between-gtypes">
                           <xsl:with-param name="coll">FRSIAF</xsl:with-param>
                           <xsl:with-param name="fromAgent" select="piaaf:fromAgent"/>
                           <xsl:with-param name="ancAgentType" select="piaaf:ancAgentType"/>
                           <xsl:with-param name="tEntity" select="piaaf:targetEntity"/>
                           <xsl:with-param name="tName" select="piaaf:targetName"/>
                       </xsl:call-template>
                       
                   
                 
               </xsl:for-each>
            </rdf:RDF>
        </xsl:result-document>


    </xsl:template>
<xsl:template name="mergeRelationsInFirstTable">
        <xsl:param name="anc"/>
        <xsl:param name="date"/>
        <xsl:param name="fdate"/>
        <xsl:param name="tdate"/>
        <xsl:param name="note"/>
        <xsl:param name="tName"/>
        <xsl:param name="targ"/>
        <xsl:param name="theRelId"/>

        <xsl:choose>

            <xsl:when test="boolean($targ) = false()">

                <xsl:copy-of select="."/>
            </xsl:when>
            <xsl:when
                test="boolean($targ) and (not($collection-EAC-SIAF/eac:eac-cpf[eac:control/eac:recordId = $targ]) and not($collection-EAC-BnF/eac:eac-cpf[eac:control/eac:recordId = $targ]) and not($collection-EAC-AN/eac:eac-cpf[eac:control/eac:recordId = $targ]))">

                <xsl:copy-of select="."/>
            </xsl:when>
            <xsl:when
                test="$collection-EAC-SIAF/eac:eac-cpf[eac:control/eac:recordId = $targ] | $collection-EAC-BnF/eac:eac-cpf[eac:control/eac:recordId = $targ] | $collection-EAC-AN/eac:eac-cpf[eac:control/eac:recordId = $targ]">


                <!-- on ne va garder que les relations dans un sens, sinon on aura des doublons -->
                <xsl:choose>
                    <xsl:when test="@arcrole = 'knows'">
                        <xsl:copy-of select="."/>
                    </xsl:when>
                    <xsl:when test="@arcrole = 'isAssociatedWithForItsControl'">
                        <xsl:copy-of select="."/>
                    </xsl:when>
                    <xsl:when test="@arcrole = 'isControlledBy'">
                        <xsl:copy-of select="."/>
                    </xsl:when>
                    <xsl:when test="@arcrole = 'controls'">
                        <xsl:choose>
                            <xsl:when
                                test="
                                    parent::piaaf:relations/piaaf:rel[@arcrole = 'isControlledBy' and piaaf:fromAgent = $targ and piaaf:targetEntity = $anc]
                                    ">
                                <xsl:variable name="theInvRel"
                                    select="parent::piaaf:relations/piaaf:rel[@arcrole = 'isControlledBy' and piaaf:fromAgent = $targ and piaaf:targetEntity = $anc]"/>

                                <xsl:variable select="$theInvRel/@xml:id" name="theInvRelId"/>

                                <xsl:choose>
                                    <xsl:when test="boolean($date)">
                                        <xsl:if test="$theInvRel/piaaf:date = $date">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                  <xsl:comment>notes différentes</xsl:comment>
                                                  <piaaf:specialNote>
                                                  <xsl:attribute name="fromRel">
                                                  <xsl:value-of select="@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:attribute name="toBeMovedToRel">
                                                  <xsl:value-of select="$theInvRelId"/>
                                                  </xsl:attribute>
                                                  <xsl:copy-of select="$note/node()"/>
                                                  </piaaf:specialNote>
                                                  </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                  <xsl:if test="not($theInvRel/piaaf:note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if test="$theInvRel/piaaf:note">

                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:if>
                                        <xsl:if test="$theInvRel/piaaf:date != $date">
                                            <xsl:copy-of select="."/>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when test="boolean($fdate) and boolean($tdate)">
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate = $fdate and $theInvRel/piaaf:dateRange/piaaf:toDate = $tdate">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                  <xsl:comment>notes différentes</xsl:comment>
                                                  <piaaf:specialNote>
                                                  <xsl:attribute name="fromRel">
                                                  <xsl:value-of select="@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:attribute name="toBeMovedToRel">
                                                  <xsl:value-of select="$theInvRel/@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:copy-of select="$note/node()"/>
                                                  </piaaf:specialNote>
                                                  </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                  <xsl:if test="not($theInvRel/piaaf:note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if test="$theInvRel/piaaf:note">

                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:if>
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate != $fdate or $theInvRel/piaaf:dateRange/piaaf:toDate != $tdate">
                                            <xsl:copy-of select="."/>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when test="boolean($fdate) and boolean($tdate) = false()">

                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate = $fdate and not($theInvRel/piaaf:dateRange/piaaf:toDate)">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                  <xsl:comment>notes différentes</xsl:comment>
                                                  <piaaf:specialNote>
                                                  <xsl:attribute name="fromRel">
                                                  <xsl:value-of select="@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:attribute name="toBeMovedToRel">
                                                  <xsl:value-of select="$theInvRel/@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:copy-of select="$note/node()"/>
                                                  </piaaf:specialNote>
                                                  </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                  <xsl:if test="not($theInvRel/piaaf:note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if test="$theInvRel/piaaf:note">

                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:if>
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate != $fdate or $theInvRel/piaaf:dateRange/piaaf:toDate">
                                            <xsl:copy-of select="."/>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when test="boolean($fdate) = false() and boolean($tdate)">
                                        <xsl:if
                                            test="not($theInvRel/piaaf:dateRange/piaaf:fromDate) and $theInvRel/piaaf:dateRange/piaaf:toDate = $tdate">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                  <xsl:comment>notes différentes</xsl:comment>
                                                  <piaaf:specialNote>
                                                  <xsl:attribute name="fromRel">
                                                  <xsl:value-of select="@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:attribute name="toBeMovedToRel">
                                                  <xsl:value-of select="$theInvRel/@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:copy-of select="$note/node()"/>
                                                  </piaaf:specialNote>
                                                  </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                  <xsl:if test="not($theInvRel/piaaf:note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if test="$theInvRel/piaaf:note">

                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:if>
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate or $theInvRel/piaaf:dateRange/piaaf:toDate != $tdate">
                                            <xsl:copy-of select="."/>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when
                                        test="(boolean($fdate) = false() and boolean($tdate) = false()) or boolean($date) = false()">
                                        <xsl:choose>
                                            <xsl:when test="boolean($note)">
                                                <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                </xsl:if>
                                                <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                  <xsl:comment>notes différentes</xsl:comment>

                                                  <xsl:copy-of select="."/>
                                                </xsl:if>
                                            </xsl:when>
                                            <xsl:when test="boolean($note) = false()">
                                                <xsl:copy-of select="."/>
                                            </xsl:when>
                                        </xsl:choose>
                                    </xsl:when>
                                </xsl:choose>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:copy-of select="."/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                    <xsl:when test="@arcrole = 'isDirectorOf'">
                        <xsl:copy-of select="."/>
                    </xsl:when>
                    <xsl:when test="@arcrole = 'isDirectedBy'">
                        <xsl:choose>

                            <xsl:when
                                test="
                                    parent::piaaf:relations/piaaf:rel[@arcrole = 'isDirectorOf' and piaaf:fromAgent = $targ and piaaf:targetEntity = $anc]
                                    ">
                                <xsl:for-each
                                    select="
                                        parent::piaaf:relations/piaaf:rel[@arcrole = 'isDirectorOf' and piaaf:fromAgent = $targ and piaaf:targetEntity = $anc]
                                        ">
                                    <xsl:variable name="theInvRel" select="."/>

                                    <xsl:variable select="$theInvRel/@xml:id" name="theInvRelId"/>

                                    <xsl:choose>
                                        <xsl:when test="boolean($date)">
                                            <xsl:if test="$theInvRel/piaaf:date = $date">
                                                <xsl:choose>
                                                  <xsl:when test="boolean($note)">
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                  <xsl:comment>notes différentes</xsl:comment>
                                                  <piaaf:specialNote>
                                                  <xsl:attribute name="fromRel">
                                                  <xsl:value-of select="@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:attribute name="toBeMovedToRel">
                                                  <xsl:value-of select="$theInvRelId"/>
                                                  </xsl:attribute>
                                                  <xsl:copy-of select="$note/node()"/>
                                                  </piaaf:specialNote>
                                                  </xsl:if>
                                                  </xsl:when>
                                                  <xsl:when test="boolean($note) = false()">
                                                  <xsl:if test="not($theInvRel/piaaf:note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if test="$theInvRel/piaaf:note">

                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  </xsl:when>
                                                </xsl:choose>
                                            </xsl:if>
                                            <xsl:if test="$theInvRel/piaaf:date != $date">
                                                <xsl:copy-of select="."/>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:when test="boolean($fdate) and boolean($tdate)">
                                            <xsl:if
                                                test="$theInvRel/piaaf:dateRange/piaaf:fromDate = $fdate and $theInvRel/piaaf:dateRange/piaaf:toDate = $tdate">
                                                <xsl:choose>
                                                  <xsl:when test="boolean($note)">
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                  <xsl:comment>notes différentes</xsl:comment>
                                                  <piaaf:specialNote>
                                                  <xsl:attribute name="fromRel">
                                                  <xsl:value-of select="@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:attribute name="toBeMovedToRel">
                                                  <xsl:value-of select="$theInvRel/@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:copy-of select="$note/node()"/>
                                                  </piaaf:specialNote>
                                                  </xsl:if>
                                                  </xsl:when>
                                                  <xsl:when test="boolean($note) = false()">
                                                  <xsl:if test="not($theInvRel/piaaf:note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if test="$theInvRel/piaaf:note">

                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  </xsl:when>
                                                </xsl:choose>
                                            </xsl:if>
                                            <xsl:if
                                                test="$theInvRel/piaaf:dateRange/piaaf:fromDate != $fdate or $theInvRel/piaaf:dateRange/piaaf:toDate != $tdate">
                                                <xsl:copy-of select="."/>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:when
                                            test="boolean($fdate) and boolean($tdate) = false()">

                                            <xsl:if
                                                test="$theInvRel/piaaf:dateRange/piaaf:fromDate = $fdate and not($theInvRel/piaaf:dateRange/piaaf:toDate)">
                                                <xsl:choose>
                                                  <xsl:when test="boolean($note)">
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                  <xsl:comment>notes différentes</xsl:comment>
                                                  <piaaf:specialNote>
                                                  <xsl:attribute name="fromRel">
                                                  <xsl:value-of select="@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:attribute name="toBeMovedToRel">
                                                  <xsl:value-of select="$theInvRel/@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:copy-of select="$note/node()"/>
                                                  </piaaf:specialNote>
                                                  </xsl:if>
                                                  </xsl:when>
                                                  <xsl:when test="boolean($note) = false()">
                                                  <xsl:if test="not($theInvRel/piaaf:note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if test="$theInvRel/piaaf:note">

                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  </xsl:when>
                                                </xsl:choose>
                                            </xsl:if>
                                            <xsl:if
                                                test="$theInvRel/piaaf:dateRange/piaaf:fromDate != $fdate or $theInvRel/piaaf:dateRange/piaaf:toDate">
                                                <xsl:copy-of select="."/>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:when
                                            test="boolean($fdate) = false() and boolean($tdate)">
                                            <xsl:if
                                                test="not($theInvRel/piaaf:dateRange/piaaf:fromDate) and $theInvRel/piaaf:dateRange/piaaf:toDate = $tdate">
                                                <xsl:choose>
                                                  <xsl:when test="boolean($note)">
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                  <xsl:comment>notes différentes</xsl:comment>
                                                  <piaaf:specialNote>
                                                  <xsl:attribute name="fromRel">
                                                  <xsl:value-of select="@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:attribute name="toBeMovedToRel">
                                                  <xsl:value-of select="$theInvRel/@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:copy-of select="$note/node()"/>
                                                  </piaaf:specialNote>
                                                  </xsl:if>
                                                  </xsl:when>
                                                  <xsl:when test="boolean($note) = false()">
                                                  <xsl:if test="not($theInvRel/piaaf:note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if test="$theInvRel/piaaf:note">

                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  </xsl:when>
                                                </xsl:choose>
                                            </xsl:if>
                                            <xsl:if
                                                test="$theInvRel/piaaf:dateRange/piaaf:fromDate or $theInvRel/piaaf:dateRange/piaaf:toDate != $tdate">
                                                <xsl:copy-of select="."/>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:when
                                            test="(boolean($fdate) = false() and boolean($tdate) = false()) or boolean($date) = false()">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                  <xsl:comment>notes différentes</xsl:comment>

                                                  <xsl:copy-of select="."/>
                                                  </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                  <xsl:copy-of select="."/>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:when>
                                    </xsl:choose>
                                </xsl:for-each>


                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:copy-of select="."/>
                            </xsl:otherwise>

                        </xsl:choose>
                    </xsl:when>
                    <xsl:when test="@arcrole = 'isPartOf'">
                        <xsl:copy-of select="."/>
                    </xsl:when>
                    <xsl:when test="@arcrole = 'hasPart'">
                        <xsl:choose>
                            <xsl:when
                                test="
                                    parent::piaaf:relations/piaaf:rel[@arcrole = 'isPartOf' and piaaf:fromAgent = $targ and piaaf:targetEntity = $anc]
                                    ">

                                <xsl:variable name="theInvRel"
                                    select="parent::piaaf:relations/piaaf:rel[@arcrole = 'isPartOf' and piaaf:fromAgent = $targ and piaaf:targetEntity = $anc]"/>

                                <xsl:variable select="$theInvRel/@xml:id" name="theInvRelId"/>

                                <xsl:choose>
                                    <xsl:when test="boolean($date)">
                                        <xsl:if test="$theInvRel/piaaf:date = $date">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                  <xsl:comment>notes différentes</xsl:comment>
                                                  <piaaf:specialNote>
                                                  <xsl:attribute name="fromRel">
                                                  <xsl:value-of select="@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:attribute name="toBeMovedToRel">
                                                  <xsl:value-of select="$theInvRelId"/>
                                                  </xsl:attribute>
                                                  <xsl:copy-of select="$note/node()"/>
                                                  </piaaf:specialNote>
                                                  </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                  <xsl:if test="not($theInvRel/piaaf:note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if test="$theInvRel/piaaf:note">

                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:if>
                                        <xsl:if test="$theInvRel/piaaf:date != $date">
                                            <xsl:copy-of select="."/>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when test="boolean($fdate) and boolean($tdate)">
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate = $fdate and $theInvRel/piaaf:dateRange/piaaf:toDate = $tdate">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                  <xsl:comment>notes différentes</xsl:comment>
                                                  <piaaf:specialNote>
                                                  <xsl:attribute name="fromRel">
                                                  <xsl:value-of select="@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:attribute name="toBeMovedToRel">
                                                  <xsl:value-of select="$theInvRel/@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:copy-of select="$note/node()"/>
                                                  </piaaf:specialNote>
                                                  </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                  <xsl:if test="not($theInvRel/piaaf:note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if test="$theInvRel/piaaf:note">

                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:if>
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate != $fdate or $theInvRel/piaaf:dateRange/piaaf:toDate != $tdate">
                                            <xsl:copy-of select="."/>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when test="boolean($fdate) and boolean($tdate) = false()">

                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate = $fdate and not($theInvRel/piaaf:dateRange/piaaf:toDate)">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                  <xsl:comment>notes différentes</xsl:comment>
                                                  <piaaf:specialNote>
                                                  <xsl:attribute name="fromRel">
                                                  <xsl:value-of select="@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:attribute name="toBeMovedToRel">
                                                  <xsl:value-of select="$theInvRel/@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:copy-of select="$note/node()"/>
                                                  </piaaf:specialNote>
                                                  </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                  <xsl:if test="not($theInvRel/piaaf:note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if test="$theInvRel/piaaf:note">

                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:if>
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate != $fdate or $theInvRel/piaaf:dateRange/piaaf:toDate">
                                            <xsl:copy-of select="."/>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when test="boolean($fdate) = false() and boolean($tdate)">
                                        <xsl:if
                                            test="not($theInvRel/piaaf:dateRange/piaaf:fromDate) and $theInvRel/piaaf:dateRange/piaaf:toDate = $tdate">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                  <xsl:comment>notes différentes</xsl:comment>
                                                  <piaaf:specialNote>
                                                  <xsl:attribute name="fromRel">
                                                  <xsl:value-of select="@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:attribute name="toBeMovedToRel">
                                                  <xsl:value-of select="$theInvRel/@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:copy-of select="$note/node()"/>
                                                  </piaaf:specialNote>
                                                  </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                  <xsl:if test="not($theInvRel/piaaf:note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if test="$theInvRel/piaaf:note">

                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:if>
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate or $theInvRel/piaaf:dateRange/piaaf:toDate != $tdate">
                                            <xsl:copy-of select="."/>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when
                                        test="(boolean($fdate) = false() and boolean($tdate) = false()) or boolean($date) = false()">
                                        <xsl:choose>
                                            <xsl:when test="boolean($note)">
                                                <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                </xsl:if>
                                                <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                  <xsl:comment>notes différentes</xsl:comment>

                                                  <xsl:copy-of select="."/>
                                                </xsl:if>
                                            </xsl:when>
                                            <xsl:when test="boolean($note) = false()">
                                                <xsl:copy-of select="."/>
                                            </xsl:when>
                                        </xsl:choose>
                                    </xsl:when>
                                </xsl:choose>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:copy-of select="."/>
                            </xsl:otherwise>
                        </xsl:choose>

                    </xsl:when>
                    <xsl:when test="@type = 'temporal-later'">
                        <xsl:copy-of select="."/>
                    </xsl:when>
                    <xsl:when test="@type = 'temporal-earlier'">
                        <xsl:choose>
                            <xsl:when
                                test="
                                    parent::piaaf:relations/piaaf:rel[@type = 'temporal-later' and piaaf:fromAgent = $targ and piaaf:targetEntity = $anc]
                                    ">
                                <xsl:variable name="theInvRel"
                                    select="parent::piaaf:relations/piaaf:rel[@type = 'temporal-later' and piaaf:fromAgent = $targ and piaaf:targetEntity = $anc]"/>

                                <xsl:variable select="$theInvRel/@xml:id" name="theInvRelId"/>

                                <xsl:choose>
                                    <xsl:when test="boolean($date)">
                                        <xsl:if test="$theInvRel/piaaf:date = $date">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                  <xsl:comment>notes différentes</xsl:comment>
                                                  <piaaf:specialNote>
                                                  <xsl:attribute name="fromRel">
                                                  <xsl:value-of select="@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:attribute name="toBeMovedToRel">
                                                  <xsl:value-of select="$theInvRelId"/>
                                                  </xsl:attribute>
                                                  <xsl:copy-of select="$note/node()"/>
                                                  </piaaf:specialNote>
                                                  </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                  <xsl:if test="not($theInvRel/piaaf:note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if test="$theInvRel/piaaf:note">

                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:if>
                                        <xsl:if test="$theInvRel/piaaf:date != $date">
                                            <xsl:copy-of select="."/>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when test="boolean($fdate) and boolean($tdate)">
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate = $fdate and $theInvRel/piaaf:dateRange/piaaf:toDate = $tdate">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                  <xsl:comment>notes différentes</xsl:comment>
                                                  <piaaf:specialNote>
                                                  <xsl:attribute name="fromRel">
                                                  <xsl:value-of select="@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:attribute name="toBeMovedToRel">
                                                  <xsl:value-of select="$theInvRel/@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:copy-of select="$note/node()"/>
                                                  </piaaf:specialNote>
                                                  </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                  <xsl:if test="not($theInvRel/piaaf:note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if test="$theInvRel/piaaf:note">

                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:if>
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate != $fdate or $theInvRel/piaaf:dateRange/piaaf:toDate != $tdate">
                                            <xsl:copy-of select="."/>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when test="boolean($fdate) and boolean($tdate) = false()">

                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate = $fdate and not($theInvRel/piaaf:dateRange/piaaf:toDate)">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                  <xsl:comment>notes différentes</xsl:comment>
                                                  <piaaf:specialNote>
                                                  <xsl:attribute name="fromRel">
                                                  <xsl:value-of select="@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:attribute name="toBeMovedToRel">
                                                  <xsl:value-of select="$theInvRel/@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:copy-of select="$note/node()"/>
                                                  </piaaf:specialNote>
                                                  </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                  <xsl:if test="not($theInvRel/piaaf:note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if test="$theInvRel/piaaf:note">

                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:if>
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate != $fdate or $theInvRel/piaaf:dateRange/piaaf:toDate">
                                            <xsl:copy-of select="."/>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when test="boolean($fdate) = false() and boolean($tdate)">
                                        <xsl:if
                                            test="not($theInvRel/piaaf:dateRange/piaaf:fromDate) and $theInvRel/piaaf:dateRange/piaaf:toDate = $tdate">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                  <xsl:comment>notes différentes</xsl:comment>
                                                  <piaaf:specialNote>
                                                  <xsl:attribute name="fromRel">
                                                  <xsl:value-of select="@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:attribute name="toBeMovedToRel">
                                                  <xsl:value-of select="$theInvRel/@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:copy-of select="$note/node()"/>
                                                  </piaaf:specialNote>
                                                  </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                  <xsl:if test="not($theInvRel/piaaf:note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if test="$theInvRel/piaaf:note">

                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:if>
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate or $theInvRel/piaaf:dateRange/piaaf:toDate != $tdate">
                                            <xsl:copy-of select="."/>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when
                                        test="(boolean($fdate) = false() and boolean($tdate) = false()) or boolean($date) = false()">
                                        <xsl:choose>
                                            <xsl:when test="boolean($note)">
                                                <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                </xsl:if>
                                                <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                  <xsl:comment>notes différentes</xsl:comment>

                                                  <xsl:copy-of select="."/>
                                                </xsl:if>
                                            </xsl:when>
                                            <xsl:when test="boolean($note) = false()">
                                                <xsl:copy-of select="."/>
                                            </xsl:when>
                                        </xsl:choose>
                                    </xsl:when>
                                </xsl:choose>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:copy-of select="."/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                    <xsl:when test="@arcrole = 'isFunctionallyLinkedTo'">

                        <xsl:choose>
                            <xsl:when
                                test="preceding-sibling::piaaf:rel[@arcrole = 'isFunctionallyLinkedTo' and piaaf:fromAgent = $targ and piaaf:targetEntity = $anc] | preceding-sibling::piaaf:rel[@arcrole = 'isFunctionallyLinkedTo' and piaaf:fromAgent = $anc and piaaf:targetEntity = $targ] ">
                                <xsl:variable name="theInvRel"
                                    select="preceding-sibling::piaaf:rel[@arcrole = 'isFunctionallyLinkedTo' and ((piaaf:fromAgent = $targ and piaaf:targetEntity = $anc) or (piaaf:fromAgent = $anc and piaaf:targetEntity = $targ)) ]"/>

                                <xsl:variable select="$theInvRel/@xml:id" name="theInvRelId"/>
                                <!--  <xsl:comment> voir relation </xsl:comment>
                                       <xsl:value-of select="$theInvRelId"/>-->
                                <xsl:choose>
                                    <xsl:when test="boolean($date)">
                                        <xsl:if test="$theInvRel/piaaf:date = $date">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                  <xsl:comment>notes différentes</xsl:comment>
                                                  <piaaf:specialNote>
                                                  <xsl:attribute name="fromRel">
                                                  <xsl:value-of select="@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:attribute name="toBeMovedToRel">
                                                  <xsl:value-of select="$theInvRelId"/>
                                                  </xsl:attribute>
                                                  <xsl:copy-of select="$note/node()"/>
                                                  </piaaf:specialNote>
                                                  </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                  <xsl:if test="not($theInvRel/piaaf:note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if test="$theInvRel/piaaf:note">

                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:if>
                                        <xsl:if test="$theInvRel/piaaf:date != $date">
                                            <xsl:copy-of select="."/>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when test="boolean($fdate) and boolean($tdate)">
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate = $fdate and $theInvRel/piaaf:dateRange/piaaf:toDate = $tdate">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                  <xsl:comment>notes différentes</xsl:comment>
                                                  <piaaf:specialNote>
                                                  <xsl:attribute name="fromRel">
                                                  <xsl:value-of select="@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:attribute name="toBeMovedToRel">
                                                  <xsl:value-of select="$theInvRel/@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:copy-of select="$note/node()"/>
                                                  </piaaf:specialNote>
                                                  </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                  <xsl:if test="not($theInvRel/piaaf:note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if test="$theInvRel/piaaf:note">

                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:if>
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate != $fdate or $theInvRel/piaaf:dateRange/piaaf:toDate != $tdate">
                                            <xsl:copy-of select="."/>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when test="boolean($fdate) and boolean($tdate) = false()">

                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate = $fdate and not($theInvRel/piaaf:dateRange/piaaf:toDate)">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                  <xsl:comment>notes différentes</xsl:comment>
                                                  <piaaf:specialNote>
                                                  <xsl:attribute name="fromRel">
                                                  <xsl:value-of select="@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:attribute name="toBeMovedToRel">
                                                  <xsl:value-of select="$theInvRel/@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:copy-of select="$note/node()"/>
                                                  </piaaf:specialNote>
                                                  </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                  <xsl:if test="not($theInvRel/piaaf:note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if test="$theInvRel/piaaf:note">

                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:if>
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate != $fdate or $theInvRel/piaaf:dateRange/piaaf:toDate">
                                            <xsl:copy-of select="."/>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when test="boolean($fdate) = false() and boolean($tdate)">
                                        <xsl:if
                                            test="not($theInvRel/piaaf:dateRange/piaaf:fromDate) and $theInvRel/piaaf:dateRange/piaaf:toDate = $tdate">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                  <xsl:comment>notes différentes</xsl:comment>
                                                  <piaaf:specialNote>
                                                  <xsl:attribute name="fromRel">
                                                  <xsl:value-of select="@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:attribute name="toBeMovedToRel">
                                                  <xsl:value-of select="$theInvRel/@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:copy-of select="$note/node()"/>
                                                  </piaaf:specialNote>
                                                  </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                  <xsl:if test="not($theInvRel/piaaf:note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if test="$theInvRel/piaaf:note">

                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:if>
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate or $theInvRel/piaaf:dateRange/piaaf:toDate != $tdate">
                                            <xsl:copy-of select="."/>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when
                                        test="(boolean($fdate) = false() and boolean($tdate) = false()) or boolean($date) = false()">
                                        <xsl:choose>
                                            <xsl:when test="boolean($note)">
                                                <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                </xsl:if>
                                                <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                  <xsl:comment>notes différentes</xsl:comment>

                                                  <xsl:copy-of select="."/>
                                                </xsl:if>
                                            </xsl:when>
                                            <xsl:when test="boolean($note) = false()">
                                                <xsl:copy-of select="."/>
                                            </xsl:when>
                                        </xsl:choose>
                                    </xsl:when>
                                </xsl:choose>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:copy-of select="."/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                    <xsl:when test="@type = 'family'">

                        <xsl:choose>
                            <xsl:when
                                test="preceding-sibling::piaaf:rel[@type = 'family' and piaaf:fromAgent = $targ and piaaf:targetEntity = $anc] | preceding-sibling::piaaf:rel[@type = 'family' and piaaf:fromAgent = $anc and piaaf:targetEntity = $targ]">
                               
                                <xsl:variable name="theInvRel"
                                    select="preceding-sibling::piaaf:rel[@arcrole = 'family' and ((piaaf:fromAgent = $targ and piaaf:targetEntity = $anc) or (piaaf:fromAgent = $anc and piaaf:targetEntity = $targ)) ]"/>

                                <xsl:variable select="$theInvRel/@xml:id" name="theInvRelId"/>
                                <!--  <xsl:comment> voir relation </xsl:comment>
                                       <xsl:value-of select="$theInvRelId"/>-->
                                <xsl:choose>
                                    <xsl:when test="boolean($date)">
                                        <xsl:if test="$theInvRel/piaaf:date = $date">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                  <xsl:comment>notes différentes</xsl:comment>
                                                  <piaaf:specialNote>
                                                  <xsl:attribute name="fromRel">
                                                  <xsl:value-of select="@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:attribute name="toBeMovedToRel">
                                                  <xsl:value-of select="$theInvRelId"/>
                                                  </xsl:attribute>
                                                  <xsl:copy-of select="$note/node()"/>
                                                  </piaaf:specialNote>
                                                  </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                  <xsl:if test="not($theInvRel/piaaf:note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if test="$theInvRel/piaaf:note">

                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:if>
                                        <xsl:if test="$theInvRel/piaaf:date != $date">
                                            <xsl:copy-of select="."/>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when test="boolean($fdate) and boolean($tdate)">
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate = $fdate and $theInvRel/piaaf:dateRange/piaaf:toDate = $tdate">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                  <xsl:comment>notes différentes</xsl:comment>
                                                  <piaaf:specialNote>
                                                  <xsl:attribute name="fromRel">
                                                  <xsl:value-of select="@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:attribute name="toBeMovedToRel">
                                                  <xsl:value-of select="$theInvRel/@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:copy-of select="$note/node()"/>
                                                  </piaaf:specialNote>
                                                  </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                  <xsl:if test="not($theInvRel/piaaf:note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if test="$theInvRel/piaaf:note">

                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:if>
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate != $fdate or $theInvRel/piaaf:dateRange/piaaf:toDate != $tdate">
                                            <xsl:copy-of select="."/>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when test="boolean($fdate) and boolean($tdate) = false()">

                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate = $fdate and not($theInvRel/piaaf:dateRange/piaaf:toDate)">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                  <xsl:comment>notes différentes</xsl:comment>
                                                  <piaaf:specialNote>
                                                  <xsl:attribute name="fromRel">
                                                  <xsl:value-of select="@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:attribute name="toBeMovedToRel">
                                                  <xsl:value-of select="$theInvRel/@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:copy-of select="$note/node()"/>
                                                  </piaaf:specialNote>
                                                  </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                  <xsl:if test="not($theInvRel/piaaf:note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if test="$theInvRel/piaaf:note">

                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:if>
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate != $fdate or $theInvRel/piaaf:dateRange/piaaf:toDate">
                                            <xsl:copy-of select="."/>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when test="boolean($fdate) = false() and boolean($tdate)">
                                        <xsl:if
                                            test="not($theInvRel/piaaf:dateRange/piaaf:fromDate) and $theInvRel/piaaf:dateRange/piaaf:toDate = $tdate">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                  <xsl:comment>notes différentes</xsl:comment>
                                                  <piaaf:specialNote>
                                                  <xsl:attribute name="fromRel">
                                                  <xsl:value-of select="@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:attribute name="toBeMovedToRel">
                                                  <xsl:value-of select="$theInvRel/@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:copy-of select="$note/node()"/>
                                                  </piaaf:specialNote>
                                                  </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                  <xsl:if test="not($theInvRel/piaaf:note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if test="$theInvRel/piaaf:note">

                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:if>
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate or $theInvRel/piaaf:dateRange/piaaf:toDate != $tdate">
                                            <xsl:copy-of select="."/>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when
                                        test="(boolean($fdate) = false() and boolean($tdate) = false()) or boolean($date) = false()">
                                        <xsl:choose>
                                            <xsl:when test="boolean($note)">
                                                <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                </xsl:if>
                                                <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                  <xsl:comment>notes différentes</xsl:comment>

                                                  <xsl:copy-of select="."/>
                                                </xsl:if>
                                            </xsl:when>
                                            <xsl:when test="boolean($note) = false()">
                                                <xsl:copy-of select="."/>
                                            </xsl:when>
                                        </xsl:choose>
                                    </xsl:when>
                                </xsl:choose>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:copy-of select="."/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                    <xsl:when test="@arcrole = 'isMemberOf'">
                        <xsl:copy-of select="."/>
                    </xsl:when>
                    <xsl:when test="@arcrole = 'hasMember'">
                        <xsl:choose>

                            <xsl:when
                                test="
                                    parent::piaaf:relations/piaaf:rel[@arcrole = 'isMemberOf' and piaaf:fromAgent = $targ and piaaf:targetEntity = $anc]
                                    ">
                                <xsl:for-each
                                    select="
                                        parent::piaaf:relations/piaaf:rel[@arcrole = 'isMemberOf' and piaaf:fromAgent = $targ and piaaf:targetEntity = $anc]
                                        ">
                                    <xsl:variable name="theInvRel" select="."/>

                                    <xsl:variable select="$theInvRel/@xml:id" name="theInvRelId"/>

                                    <xsl:choose>
                                        <xsl:when test="boolean($date)">
                                            <xsl:if test="$theInvRel/piaaf:date = $date">
                                                <xsl:choose>
                                                  <xsl:when test="boolean($note)">
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                  <xsl:comment>notes différentes</xsl:comment>
                                                  <piaaf:specialNote>
                                                  <xsl:attribute name="fromRel">
                                                  <xsl:value-of select="@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:attribute name="toBeMovedToRel">
                                                  <xsl:value-of select="$theInvRelId"/>
                                                  </xsl:attribute>
                                                  <xsl:copy-of select="$note/node()"/>
                                                  </piaaf:specialNote>
                                                  </xsl:if>
                                                  </xsl:when>
                                                  <xsl:when test="boolean($note) = false()">
                                                  <xsl:if test="not($theInvRel/piaaf:note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if test="$theInvRel/piaaf:note">

                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  </xsl:when>
                                                </xsl:choose>
                                            </xsl:if>
                                            <xsl:if test="$theInvRel/piaaf:date != $date">
                                                <xsl:copy-of select="."/>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:when test="boolean($fdate) and boolean($tdate)">
                                            <xsl:if
                                                test="$theInvRel/piaaf:dateRange/piaaf:fromDate = $fdate and $theInvRel/piaaf:dateRange/piaaf:toDate = $tdate">
                                                <xsl:choose>
                                                  <xsl:when test="boolean($note)">
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                  <xsl:comment>notes différentes</xsl:comment>
                                                  <piaaf:specialNote>
                                                  <xsl:attribute name="fromRel">
                                                  <xsl:value-of select="@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:attribute name="toBeMovedToRel">
                                                  <xsl:value-of select="$theInvRel/@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:copy-of select="$note/node()"/>
                                                  </piaaf:specialNote>
                                                  </xsl:if>
                                                  </xsl:when>
                                                  <xsl:when test="boolean($note) = false()">
                                                  <xsl:if test="not($theInvRel/piaaf:note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if test="$theInvRel/piaaf:note">

                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  </xsl:when>
                                                </xsl:choose>
                                            </xsl:if>
                                            <xsl:if
                                                test="$theInvRel/piaaf:dateRange/piaaf:fromDate != $fdate or $theInvRel/piaaf:dateRange/piaaf:toDate != $tdate">
                                                <xsl:copy-of select="."/>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:when
                                            test="boolean($fdate) and boolean($tdate) = false()">

                                            <xsl:if
                                                test="$theInvRel/piaaf:dateRange/piaaf:fromDate = $fdate and not($theInvRel/piaaf:dateRange/piaaf:toDate)">
                                                <xsl:choose>
                                                  <xsl:when test="boolean($note)">
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                  <xsl:comment>notes différentes</xsl:comment>
                                                  <piaaf:specialNote>
                                                  <xsl:attribute name="fromRel">
                                                  <xsl:value-of select="@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:attribute name="toBeMovedToRel">
                                                  <xsl:value-of select="$theInvRel/@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:copy-of select="$note/node()"/>
                                                  </piaaf:specialNote>
                                                  </xsl:if>
                                                  </xsl:when>
                                                  <xsl:when test="boolean($note) = false()">
                                                  <xsl:if test="not($theInvRel/piaaf:note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if test="$theInvRel/piaaf:note">

                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  </xsl:when>
                                                </xsl:choose>
                                            </xsl:if>
                                            <xsl:if
                                                test="$theInvRel/piaaf:dateRange/piaaf:fromDate != $fdate or $theInvRel/piaaf:dateRange/piaaf:toDate">
                                                <xsl:copy-of select="."/>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:when
                                            test="boolean($fdate) = false() and boolean($tdate)">
                                            <xsl:if
                                                test="not($theInvRel/piaaf:dateRange/piaaf:fromDate) and $theInvRel/piaaf:dateRange/piaaf:toDate = $tdate">
                                                <xsl:choose>
                                                  <xsl:when test="boolean($note)">
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                  <xsl:comment>notes différentes</xsl:comment>
                                                  <piaaf:specialNote>
                                                  <xsl:attribute name="fromRel">
                                                  <xsl:value-of select="@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:attribute name="toBeMovedToRel">
                                                  <xsl:value-of select="$theInvRel/@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:copy-of select="$note/node()"/>
                                                  </piaaf:specialNote>
                                                  </xsl:if>
                                                  </xsl:when>
                                                  <xsl:when test="boolean($note) = false()">
                                                  <xsl:if test="not($theInvRel/piaaf:note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if test="$theInvRel/piaaf:note">

                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  </xsl:when>
                                                </xsl:choose>
                                            </xsl:if>
                                            <xsl:if
                                                test="$theInvRel/piaaf:dateRange/piaaf:fromDate or $theInvRel/piaaf:dateRange/piaaf:toDate != $tdate">
                                                <xsl:copy-of select="."/>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:when
                                            test="(boolean($fdate) = false() and boolean($tdate) = false()) or boolean($date) = false()">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                  <xsl:comment>notes différentes</xsl:comment>

                                                  <xsl:copy-of select="."/>
                                                  </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                  <xsl:copy-of select="."/>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:when>
                                    </xsl:choose>
                                </xsl:for-each>


                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:copy-of select="."/>
                            </xsl:otherwise>

                        </xsl:choose>
                    </xsl:when>
                    <xsl:when test="@arcrole = 'isEmployeeOf'">
                        <xsl:copy-of select="."/>
                    </xsl:when>
                    <xsl:when test="@arcrole = 'hasEmployee'">
                        <xsl:choose>

                            <xsl:when
                                test="
                                    parent::piaaf:relations/piaaf:rel[@arcrole = 'isEmployeeOf' and piaaf:fromAgent = $targ and piaaf:targetEntity = $anc]
                                    ">
                                <xsl:for-each
                                    select="
                                        parent::piaaf:relations/piaaf:rel[@arcrole = 'isEmployeeOf' and piaaf:fromAgent = $targ and piaaf:targetEntity = $anc]
                                        ">
                                    <xsl:variable name="theInvRel" select="."/>

                                    <xsl:variable select="$theInvRel/@xml:id" name="theInvRelId"/>

                                    <xsl:choose>
                                        <xsl:when test="boolean($date)">
                                            <xsl:if test="$theInvRel/piaaf:date = $date">
                                                <xsl:choose>
                                                  <xsl:when test="boolean($note)">
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                  <xsl:comment>notes différentes</xsl:comment>
                                                  <piaaf:specialNote>
                                                  <xsl:attribute name="fromRel">
                                                  <xsl:value-of select="@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:attribute name="toBeMovedToRel">
                                                  <xsl:value-of select="$theInvRelId"/>
                                                  </xsl:attribute>
                                                  <xsl:copy-of select="$note/node()"/>
                                                  </piaaf:specialNote>
                                                  </xsl:if>
                                                  </xsl:when>
                                                  <xsl:when test="boolean($note) = false()">
                                                  <xsl:if test="not($theInvRel/piaaf:note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if test="$theInvRel/piaaf:note">

                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  </xsl:when>
                                                </xsl:choose>
                                            </xsl:if>
                                            <xsl:if test="$theInvRel/piaaf:date != $date">
                                                <xsl:copy-of select="."/>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:when test="boolean($fdate) and boolean($tdate)">
                                            <xsl:if
                                                test="$theInvRel/piaaf:dateRange/piaaf:fromDate = $fdate and $theInvRel/piaaf:dateRange/piaaf:toDate = $tdate">
                                                <xsl:choose>
                                                  <xsl:when test="boolean($note)">
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                  <xsl:comment>notes différentes</xsl:comment>
                                                  <piaaf:specialNote>
                                                  <xsl:attribute name="fromRel">
                                                  <xsl:value-of select="@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:attribute name="toBeMovedToRel">
                                                  <xsl:value-of select="$theInvRel/@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:copy-of select="$note/node()"/>
                                                  </piaaf:specialNote>
                                                  </xsl:if>
                                                  </xsl:when>
                                                  <xsl:when test="boolean($note) = false()">
                                                  <xsl:if test="not($theInvRel/piaaf:note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if test="$theInvRel/piaaf:note">

                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  </xsl:when>
                                                </xsl:choose>
                                            </xsl:if>
                                            <xsl:if
                                                test="$theInvRel/piaaf:dateRange/piaaf:fromDate != $fdate or $theInvRel/piaaf:dateRange/piaaf:toDate != $tdate">
                                                <xsl:copy-of select="."/>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:when
                                            test="boolean($fdate) and boolean($tdate) = false()">

                                            <xsl:if
                                                test="$theInvRel/piaaf:dateRange/piaaf:fromDate = $fdate and not($theInvRel/piaaf:dateRange/piaaf:toDate)">
                                                <xsl:choose>
                                                  <xsl:when test="boolean($note)">
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                  <xsl:comment>notes différentes</xsl:comment>
                                                  <piaaf:specialNote>
                                                  <xsl:attribute name="fromRel">
                                                  <xsl:value-of select="@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:attribute name="toBeMovedToRel">
                                                  <xsl:value-of select="$theInvRel/@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:copy-of select="$note/node()"/>
                                                  </piaaf:specialNote>
                                                  </xsl:if>
                                                  </xsl:when>
                                                  <xsl:when test="boolean($note) = false()">
                                                  <xsl:if test="not($theInvRel/piaaf:note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if test="$theInvRel/piaaf:note">

                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  </xsl:when>
                                                </xsl:choose>
                                            </xsl:if>
                                            <xsl:if
                                                test="$theInvRel/piaaf:dateRange/piaaf:fromDate != $fdate or $theInvRel/piaaf:dateRange/piaaf:toDate">
                                                <xsl:copy-of select="."/>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:when
                                            test="boolean($fdate) = false() and boolean($tdate)">
                                            <xsl:if
                                                test="not($theInvRel/piaaf:dateRange/piaaf:fromDate) and $theInvRel/piaaf:dateRange/piaaf:toDate = $tdate">
                                                <xsl:choose>
                                                  <xsl:when test="boolean($note)">
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                  <xsl:comment>notes différentes</xsl:comment>
                                                  <piaaf:specialNote>
                                                  <xsl:attribute name="fromRel">
                                                  <xsl:value-of select="@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:attribute name="toBeMovedToRel">
                                                  <xsl:value-of select="$theInvRel/@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:copy-of select="$note/node()"/>
                                                  </piaaf:specialNote>
                                                  </xsl:if>
                                                  </xsl:when>
                                                  <xsl:when test="boolean($note) = false()">
                                                  <xsl:if test="not($theInvRel/piaaf:note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if test="$theInvRel/piaaf:note">

                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  </xsl:when>
                                                </xsl:choose>
                                            </xsl:if>
                                            <xsl:if
                                                test="$theInvRel/piaaf:dateRange/piaaf:fromDate or $theInvRel/piaaf:dateRange/piaaf:toDate != $tdate">
                                                <xsl:copy-of select="."/>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:when
                                            test="(boolean($fdate) = false() and boolean($tdate) = false()) or boolean($date) = false()">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                  <xsl:comment>notes différentes</xsl:comment>

                                                  <xsl:copy-of select="."/>
                                                  </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                  <xsl:copy-of select="."/>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:when>
                                    </xsl:choose>
                                </xsl:for-each>


                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:copy-of select="."/>
                            </xsl:otherwise>

                        </xsl:choose>
                    </xsl:when>
                    <xsl:when test="@type = 'associative' and (not(@arcrole) or @arcrole = '')">

                        <xsl:choose>
                            <xsl:when
                                test="preceding-sibling::piaaf:rel[@type = 'associative' and (not(@arcrole) or @arcrole = '') and piaaf:fromAgent = $targ and piaaf:targetEntity = $anc] | preceding-sibling::piaaf:rel[@type = 'associative' and (not(@arcrole) or @arcrole = '') and piaaf:fromAgent = $anc and piaaf:targetEntity = $targ]">
                                <xsl:for-each
                                    select="preceding-sibling::piaaf:rel[@type = 'associative' and (not(@arcrole) or @arcrole = '') and piaaf:fromAgent = $targ and piaaf:targetEntity = $anc] |  preceding-sibling::piaaf:rel[@type = 'associative' and (not(@arcrole) or @arcrole = '') and piaaf:fromAgent = $anc and piaaf:targetEntity = $targ]">
                                    <xsl:variable name="theInvRel" select="."/>


                                    <!--  <xsl:comment> voir relation </xsl:comment>
                                       <xsl:value-of select="$theInvRelId"/>-->
                                    <xsl:choose>
                                        <xsl:when test="boolean($date)">
                                            <xsl:if test="$theInvRel/piaaf:date = $date">
                                                <xsl:choose>
                                                  <xsl:when test="boolean($note)">
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                  <xsl:comment>notes différentes</xsl:comment>
                                                  <piaaf:specialNote>
                                                  <xsl:attribute name="fromRel">
                                                  <xsl:value-of select="@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:attribute name="toBeMovedToRel">
                                                  <xsl:value-of select="$theInvRel/@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:copy-of select="$note/node()"/>
                                                  </piaaf:specialNote>
                                                  </xsl:if>
                                                  </xsl:when>
                                                  <xsl:when test="boolean($note) = false()">
                                                  <xsl:if test="not($theInvRel/piaaf:note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if test="$theInvRel/piaaf:note">

                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  </xsl:when>
                                                </xsl:choose>
                                            </xsl:if>
                                            <xsl:if test="$theInvRel/piaaf:date != $date">
                                                <xsl:copy-of select="."/>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:when test="boolean($fdate) and boolean($tdate)">
                                            <xsl:if
                                                test="$theInvRel/piaaf:dateRange/piaaf:fromDate = $fdate and $theInvRel/piaaf:dateRange/piaaf:toDate = $tdate">
                                                <xsl:choose>
                                                  <xsl:when test="boolean($note)">
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                  <xsl:comment>notes différentes</xsl:comment>
                                                  <piaaf:specialNote>
                                                  <xsl:attribute name="fromRel">
                                                  <xsl:value-of select="$theRelId"/>
                                                  </xsl:attribute>
                                                  <xsl:attribute name="toBeMovedToRel">
                                                  <xsl:value-of select="$theInvRel/@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:copy-of select="$note/node()"/>
                                                  </piaaf:specialNote>
                                                  </xsl:if>
                                                  </xsl:when>
                                                  <xsl:when test="boolean($note) = false()">
                                                  <xsl:if test="not($theInvRel/piaaf:note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if test="$theInvRel/piaaf:note">

                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  </xsl:when>
                                                </xsl:choose>
                                            </xsl:if>
                                            <xsl:if
                                                test="$theInvRel/piaaf:dateRange/piaaf:fromDate != $fdate or $theInvRel/piaaf:dateRange/piaaf:toDate != $tdate">
                                                <xsl:copy-of select="."/>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:when
                                            test="boolean($fdate) and boolean($tdate) = false()">

                                            <xsl:if
                                                test="$theInvRel/piaaf:dateRange/piaaf:fromDate = $fdate and not($theInvRel/piaaf:dateRange/piaaf:toDate)">
                                                <xsl:choose>
                                                  <xsl:when test="boolean($note)">
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                  <xsl:comment>notes différentes</xsl:comment>
                                                  <piaaf:specialNote>
                                                  <xsl:attribute name="fromRel">
                                                  <xsl:value-of select="@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:attribute name="toBeMovedToRel">
                                                  <xsl:value-of select="$theInvRel/@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:copy-of select="$note/node()"/>
                                                  </piaaf:specialNote>
                                                  </xsl:if>
                                                  </xsl:when>
                                                  <xsl:when test="boolean($note) = false()">
                                                  <xsl:if test="not($theInvRel/piaaf:note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if test="$theInvRel/piaaf:note">

                                                  <piaaf:removedRel relId="{$theInvRel}/@xml:id"/>
                                                  </xsl:if>
                                                  </xsl:when>
                                                </xsl:choose>
                                            </xsl:if>
                                            <xsl:if
                                                test="$theInvRel/piaaf:dateRange/piaaf:fromDate != $fdate or $theInvRel/piaaf:dateRange/piaaf:toDate">
                                                <xsl:copy-of select="."/>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:when
                                            test="boolean($fdate) = false() and boolean($tdate)">
                                            <xsl:if
                                                test="not($theInvRel/piaaf:dateRange/piaaf:fromDate) and $theInvRel/piaaf:dateRange/piaaf:toDate = $tdate">
                                                <xsl:choose>
                                                  <xsl:when test="boolean($note)">
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                  <xsl:comment>notes différentes</xsl:comment>
                                                  <piaaf:specialNote>
                                                  <xsl:attribute name="fromRel">
                                                  <xsl:value-of select="@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:attribute name="toBeMovedToRel">
                                                  <xsl:value-of select="$theInvRel/@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:copy-of select="$note/node()"/>
                                                  </piaaf:specialNote>
                                                  </xsl:if>
                                                  </xsl:when>
                                                  <xsl:when test="boolean($note) = false()">
                                                  <xsl:if test="not($theInvRel/piaaf:note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if test="$theInvRel/piaaf:note">

                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  </xsl:when>
                                                </xsl:choose>
                                            </xsl:if>
                                            <xsl:if
                                                test="$theInvRel/piaaf:dateRange/piaaf:fromDate or $theInvRel/piaaf:dateRange/piaaf:toDate != $tdate">
                                                <xsl:copy-of select="."/>
                                            </xsl:if>
                                        </xsl:when>
                                        <xsl:when
                                            test="(boolean($fdate) = false() and boolean($tdate) = false()) or boolean($date) = false()">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                  <xsl:comment>notes différentes</xsl:comment>

                                                  <xsl:copy-of select="."/>
                                                  </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                  <xsl:copy-of select="."/>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:when>
                                    </xsl:choose>
                                </xsl:for-each>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:copy-of select="."/>
                            </xsl:otherwise>
                        </xsl:choose>


                    </xsl:when>
                    <xsl:when test="@type = 'hierarchical-parent' and (not(@arcrole) or @arcrole='')">
                        <xsl:copy-of select="."/>
                    </xsl:when>
                    <xsl:when test="@type = 'hierarchical-child' and (not(@arcrole) or @arcrole='')">
                        <xsl:choose>
                            <xsl:when
                                test="
                                parent::piaaf:relations/piaaf:rel[@type = 'hierarchical-parent' and (not(@arcrole) or @arcrole='') and piaaf:fromAgent = $targ and piaaf:targetEntity = $anc]
                                    ">
                                <xsl:variable name="theInvRel"
                                    select="parent::piaaf:relations/piaaf:rel[@type = 'hierarchical-parent' and (not(@arcrole) or @arcrole='') and piaaf:fromAgent = $targ and piaaf:targetEntity = $anc]"/>

                                <xsl:variable select="$theInvRel/@xml:id" name="theInvRelId"/>

                                <xsl:choose>
                                    <xsl:when test="boolean($date)">
                                        <xsl:if test="$theInvRel/piaaf:date = $date">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                  <xsl:comment>notes différentes</xsl:comment>
                                                  <piaaf:specialNote>
                                                  <xsl:attribute name="fromRel">
                                                  <xsl:value-of select="@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:attribute name="toBeMovedToRel">
                                                  <xsl:value-of select="$theInvRelId"/>
                                                  </xsl:attribute>
                                                  <xsl:copy-of select="$note/node()"/>
                                                  </piaaf:specialNote>
                                                  </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                  <xsl:if test="not($theInvRel/piaaf:note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if test="$theInvRel/piaaf:note">

                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:if>
                                        <xsl:if test="$theInvRel/piaaf:date != $date">
                                            <xsl:copy-of select="."/>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when test="boolean($fdate) and boolean($tdate)">
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate = $fdate and $theInvRel/piaaf:dateRange/piaaf:toDate = $tdate">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                  <xsl:comment>notes différentes</xsl:comment>
                                                  <piaaf:specialNote>
                                                  <xsl:attribute name="fromRel">
                                                  <xsl:value-of select="@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:attribute name="toBeMovedToRel">
                                                  <xsl:value-of select="$theInvRel/@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:copy-of select="$note/node()"/>
                                                  </piaaf:specialNote>
                                                  </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                  <xsl:if test="not($theInvRel/piaaf:note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if test="$theInvRel/piaaf:note">

                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:if>
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate != $fdate or $theInvRel/piaaf:dateRange/piaaf:toDate != $tdate">
                                            <xsl:copy-of select="."/>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when test="boolean($fdate) and boolean($tdate) = false()">

                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate = $fdate and not($theInvRel/piaaf:dateRange/piaaf:toDate)">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                  <xsl:comment>notes différentes</xsl:comment>
                                                  <piaaf:specialNote>
                                                  <xsl:attribute name="fromRel">
                                                  <xsl:value-of select="@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:attribute name="toBeMovedToRel">
                                                  <xsl:value-of select="$theInvRel/@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:copy-of select="$note/node()"/>
                                                  </piaaf:specialNote>
                                                  </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                  <xsl:if test="not($theInvRel/piaaf:note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if test="$theInvRel/piaaf:note">

                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:if>
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate != $fdate or $theInvRel/piaaf:dateRange/piaaf:toDate">
                                            <xsl:copy-of select="."/>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when test="boolean($fdate) = false() and boolean($tdate)">
                                        <xsl:if
                                            test="not($theInvRel/piaaf:dateRange/piaaf:fromDate) and $theInvRel/piaaf:dateRange/piaaf:toDate = $tdate">
                                            <xsl:choose>
                                                <xsl:when test="boolean($note)">
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                  <xsl:comment>notes différentes</xsl:comment>
                                                  <piaaf:specialNote>
                                                  <xsl:attribute name="fromRel">
                                                  <xsl:value-of select="@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:attribute name="toBeMovedToRel">
                                                  <xsl:value-of select="$theInvRel/@xml:id"/>
                                                  </xsl:attribute>
                                                  <xsl:copy-of select="$note/node()"/>
                                                  </piaaf:specialNote>
                                                  </xsl:if>
                                                </xsl:when>
                                                <xsl:when test="boolean($note) = false()">
                                                  <xsl:if test="not($theInvRel/piaaf:note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                  <xsl:if test="$theInvRel/piaaf:note">

                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                  </xsl:if>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:if>
                                        <xsl:if
                                            test="$theInvRel/piaaf:dateRange/piaaf:fromDate or $theInvRel/piaaf:dateRange/piaaf:toDate != $tdate">
                                            <xsl:copy-of select="."/>
                                        </xsl:if>
                                    </xsl:when>
                                    <xsl:when
                                        test="(boolean($fdate) = false() and boolean($tdate) = false()) or boolean($date) = false()">
                                        <xsl:choose>
                                            <xsl:when test="boolean($note)">
                                                <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) = normalize-space($note)">
                                                  <piaaf:removedRel relId="{$theRelId}"/>
                                                </xsl:if>
                                                <xsl:if
                                                  test="normalize-space($theInvRel/piaaf:note) != normalize-space($note)">
                                                  <xsl:comment>notes différentes</xsl:comment>

                                                  <xsl:copy-of select="."/>
                                                </xsl:if>
                                            </xsl:when>
                                            <xsl:when test="boolean($note) = false()">
                                                <xsl:copy-of select="."/>
                                            </xsl:when>
                                        </xsl:choose>
                                    </xsl:when>
                                </xsl:choose>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:copy-of select="."/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                </xsl:choose>
            </xsl:when>

        </xsl:choose>
    </xsl:template>
    <xsl:template name="output-relations-between-gtypes">
        <xsl:param name="tName"/>
        <xsl:param name="fromAgent"/>
        <xsl:param name="ancAgentType"/>
        <xsl:param name="coll"/>
        <xsl:param name="tEntity"/>
        <rdf:Description>
            <xsl:attribute name="rdf:about">
                <xsl:value-of
                    select="concat('http://piaaf.demo.logilab.fr/resource/', $coll, '_group-type_relation_', substring-after(@xml:id, 're_'))"
                />
            </xsl:attribute>
            <rdf:type>
                <xsl:attribute name="rdf:resource">
                    <xsl:text>http://www.ica.org/standards/RiC/ontology#</xsl:text>
                    <xsl:choose>
                        <xsl:when test="@arcrole = 'isFunctionallyLinkedTo'"
                            >BusinessRelation</xsl:when>
                        <xsl:when test="@arcrole = 'isControlledBy' or @arcrole = 'controls'"
                            >AuthorityRelation</xsl:when>
                        <xsl:when test="@arcrole = 'hasPart'">WholePartRelation</xsl:when>
                        <xsl:when test="@arcrole = 'isPartOf'">WholePartRelation</xsl:when>
                        <xsl:when test="@type = 'temporal-earlier' or @type = 'temporal-later'"
                            >TemporalRelation</xsl:when>
                        <xsl:when test="@arcrole = 'isDirectorOf' or @arcrole = 'isDirectedBy'"
                            >AgentLeadershipRelation</xsl:when>
                        <xsl:when test="@arcrole = 'knows'">SocialRelation</xsl:when>
                        <xsl:when test="@arcrole = 'isMemberOf'">MembershipRelation</xsl:when>
                        <xsl:when test="@arcrole = 'hasMember'">MembershipRelation</xsl:when>
                        <xsl:when test="@arcrole = 'hasEmployee'">MembershipRelation</xsl:when>
                        <xsl:when test="@arcrole = 'isEmployeeOf'"
                            >MembershipRelation</xsl:when>
                        <xsl:when test="@arcrole = 'isAssociatedWithForItsControl'"
                            >AuthorityRelation</xsl:when>
                        <xsl:when test="@type = 'family'">FamilyRelation</xsl:when>
                        <xsl:when test="@type = 'associative' and (not(@arcrole) or @arcrole = '')"
                            >SocialRelation</xsl:when>
                        <xsl:when test="@type='hierarchical-parent' and (not(@arcrole) or @arcrole='')">
                            <!-- nombreux cas a priori pour les AN et le SIAF au moins -->
                            <xsl:text>AgentHierarchicalRelation</xsl:text>
                        </xsl:when>
                        <xsl:when test="@type='hierarchical-child' and (not(@arcrole) or @arcrole='')">
                            <!-- nombreux cas a priori pour les AN et le SIAF au moins -->
                            <xsl:text>AgentHierarchicalRelation</xsl:text>
                        </xsl:when>
                    </xsl:choose>

                </xsl:attribute>
            </rdf:type>
            <rdfs:label xml:lang="fr">
                <xsl:choose>
                    <xsl:when test="@arcrole = 'isFunctionallyLinkedTo'">Relation fonctionnelle de
                        travail entre</xsl:when>
                    <xsl:when
                        test="@arcrole = 'isControlledBy' or @arcrole = 'controls' or @arcrole = 'isAssociatedWithForItsControl'"
                        >Relation de contrôle ou de tutelle entre</xsl:when>
                    <xsl:when test="@arcrole = 'hasPart'">Relation du tout à une partie
                        entre</xsl:when>
                    <xsl:when test="@arcrole = 'isPartOf'">Relation du tout à une partie
                        entre</xsl:when>
                    <xsl:when test="@type = 'temporal-earlier' or @type = 'temporal-later'">Relation
                        chronologique entre</xsl:when>
                    <xsl:when test="@arcrole = 'isDirectorOf' or @arcrole = 'isDirectedBy'">Relation
                        de "leadership" ou de direction entre</xsl:when>
                    <xsl:when
                        test="@arcrole = 'isMemberOf' or @arcrole = 'hasMember' or @arcrole = 'isEmployeeOf' or @arcrole = 'hasEmployee'"
                        >Relation d'affiliation, ou de salarié à employeur, entre</xsl:when>
                    <xsl:when test="@type = 'family'">Relation familiale entre</xsl:when>
                    <xsl:when
                        test="(@type = 'associative' and (not(@arcrole) or @arcrole = '')) or @arcrole = 'knows'"
                        >Relation sociale entre</xsl:when>
                    <xsl:when test="@type='hierarchical-parent' and (not(@arcrole) or @arcrole='')">
                        <!-- nombreux cas a priori pour les AN et le SIAF au moins -->
                        <xsl:text>relation hiérarchique entre</xsl:text>
                    </xsl:when>
                    <xsl:when test="@type='hierarchical-child' and (not(@arcrole) or @arcrole='')">
                        <!-- nombreux cas a priori pour les AN et le SIAF au moins -->
                        <xsl:text>relation hiérarchique entre</xsl:text>
                    </xsl:when>
                </xsl:choose>
                <xsl:text> les catégories de collectivités et agents "</xsl:text>
                
                        <xsl:value-of
                            select="$collection-EAC-SIAF/eac:eac-cpf[eac:control/eac:recordId = $fromAgent]/eac:cpfDescription/eac:identity/eac:nameEntry[@localType = 'preferredFormForProject']/eac:part"
                        />
                   

                <xsl:text>" et "</xsl:text>
                <xsl:value-of select="piaaf:targetName"/>
                <xsl:text>"</xsl:text>
            </rdfs:label>
            <xsl:if test="piaaf:note">
                <RiC:description xml:lang="fr">
                    <xsl:for-each select="piaaf:note/piaaf:p">
                        <xsl:value-of select="."/>
                        <xsl:if test="position() != last()">
                            <xsl:text> </xsl:text>
                        </xsl:if>
                    </xsl:for-each>
                </RiC:description>
            </xsl:if>
            <xsl:if test="piaaf:date">
                <RiC:date>
                    <xsl:choose>
                        <xsl:when test="not(piaaf:date/@type)">
                            <xsl:value-of select="piaaf:date"/>
                        </xsl:when>
                        <xsl:when test="piaaf:date/@type='iso8601'">
                            <xsl:attribute name="rdf:datatype">
                                <xsl:choose>
                                    <xsl:when test="string-length(piaaf:date)=4">
                                        <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text>
                                    </xsl:when>
                                    <xsl:when test="string-length(piaaf:date)=7">
                                        <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                    </xsl:when>
                                    <xsl:when test="string-length(piaaf:date)=10">
                                        <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                    </xsl:when>
                                </xsl:choose>
                            </xsl:attribute>
                            <xsl:value-of select="piaaf:date"/>
                        </xsl:when>
                    </xsl:choose>
                </RiC:date>
            </xsl:if>
            <xsl:if test="piaaf:dateRange/piaaf:fromDate">
                <RiC:beginningDate>
                    <xsl:choose>
                        <xsl:when test="not(piaaf:dateRange/piaaf:fromDate/@type)">
                            <xsl:value-of select="piaaf:dateRange/piaaf:fromDate"/>
                        </xsl:when>
                        <xsl:when test="piaaf:dateRange/piaaf:fromDate/@type='iso8601'">
                            <xsl:attribute name="rdf:datatype">
                                <xsl:choose>
                                    <xsl:when test="string-length(piaaf:dateRange/piaaf:fromDate)=4">
                                        <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text>
                                    </xsl:when>
                                    <xsl:when test="string-length(piaaf:dateRange/piaaf:fromDate)=7">
                                        <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                    </xsl:when>
                                    <xsl:when test="string-length(piaaf:dateRange/piaaf:fromDate)=10">
                                        <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                    </xsl:when>
                                </xsl:choose>
                            </xsl:attribute>
                            <xsl:value-of select="piaaf:dateRange/piaaf:fromDate"/>
                        </xsl:when>
                    </xsl:choose>
                  
                </RiC:beginningDate>
            </xsl:if>
            <xsl:if test="piaaf:dateRange/piaaf:toDate">
               <!-- <RiC:endDate rdf:datatype="http://www.w3.org/2001/XMLSchema#dateTime">
                    <xsl:value-of select="piaaf:dateRange/piaaf:toDate"/>
                </RiC:endDate>-->
                <RiC:endDate>
                <xsl:choose>
                    <xsl:when test="not(piaaf:dateRange/piaaf:toDate/@type)">
                        <xsl:value-of select="piaaf:dateRange/piaaf:toDate"/>
                    </xsl:when>
                    <xsl:when test="piaaf:dateRange/piaaf:toDate/@type='iso8601'">
                        <xsl:attribute name="rdf:datatype">
                            <xsl:choose>
                                <xsl:when test="string-length(piaaf:dateRange/piaaf:toDate)=4">
                                    <xsl:text>http://www.w3.org/2001/XMLSchema#gYear</xsl:text>
                                </xsl:when>
                                <xsl:when test="string-length(piaaf:dateRange/piaaf:toDate)=7">
                                    <xsl:text>http://www.w3.org/2001/XMLSchema#gYearMonth</xsl:text>
                                </xsl:when>
                                <xsl:when test="string-length(piaaf:dateRange/piaaf:toDate)=10">
                                    <xsl:text>http://www.w3.org/2001/XMLSchema#date</xsl:text>
                                </xsl:when>
                            </xsl:choose>
                        </xsl:attribute>
                        <xsl:value-of select="piaaf:dateRange/piaaf:toDate"/>
                    </xsl:when>
                </xsl:choose>
                </RiC:endDate>
            </xsl:if>

            <xsl:choose>
                <xsl:when test="@arcrole = 'isFunctionallyLinkedTo'">
                    <RiC:businessRelationWith>
                        <xsl:call-template name="outputObjectPropertyToAnc">

                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                        </xsl:call-template>

                    </RiC:businessRelationWith>


                    <RiC:businessRelationWith>
                        <xsl:call-template name="outputObjectPropertyToTarg">
                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                            <xsl:with-param name="arcrole" select="@arcrole"></xsl:with-param>
                        </xsl:call-template>

                    </RiC:businessRelationWith>

                </xsl:when>
                <xsl:when test="@type = 'family'">
                    <RiC:familyRelationWith>
                        <xsl:call-template name="outputObjectPropertyToAnc">

                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                        </xsl:call-template>

                    </RiC:familyRelationWith>


                    <RiC:familyRelationWith>
                        <xsl:call-template name="outputObjectPropertyToTarg">
                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                            <xsl:with-param name="arcrole" select="@arcrole"></xsl:with-param>
                        </xsl:call-template>

                    </RiC:familyRelationWith>

                </xsl:when>
                <xsl:when
                    test="@arcrole = 'knows' or (@type = 'associative' and (not(@arcrole) or @arcrole = ''))">
                    <RiC:relationAssociates>
                        <xsl:call-template name="outputObjectPropertyToAnc">

                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                        </xsl:call-template>

                    </RiC:relationAssociates>


                    <RiC:relationAssociates>
                        <xsl:call-template name="outputObjectPropertyToTarg">
                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                            <xsl:with-param name="arcrole" select="@arcrole"></xsl:with-param>
                        </xsl:call-template>

                    </RiC:relationAssociates>

                </xsl:when>
                <xsl:when test="@arcrole = 'isDirectorOf'">
                    <RiC:leadershipBy>
                        <xsl:call-template name="outputObjectPropertyToAnc">

                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                        </xsl:call-template>

                    </RiC:leadershipBy>



                    <RiC:leadershipOn>
                        <xsl:call-template name="outputObjectPropertyToTarg">
                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                            <xsl:with-param name="arcrole" select="@arcrole"></xsl:with-param>
                        </xsl:call-template>
                    </RiC:leadershipOn>
                <!--    <xsl:call-template name="outputPositionInRel">
                        <xsl:with-param name="coll" select="$coll"/>
                        <xsl:with-param name="fromAgent" select="$fromAgent"/>
                        <xsl:with-param name="tEntity" select="$tEntity"/>
                        <xsl:with-param name="tName" select="$tName"/>
                    </xsl:call-template>
-->

                </xsl:when>
                <xsl:when test="@arcrole = 'isDirectedBy'">
                    <RiC:leadershipOn>
                        <xsl:call-template name="outputObjectPropertyToAnc">

                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                        </xsl:call-template>

                    </RiC:leadershipOn>



                    <RiC:leadershipBy>
                        <xsl:call-template name="outputObjectPropertyToTarg">
                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                            <xsl:with-param name="arcrole" select="@arcrole"></xsl:with-param>
                        </xsl:call-template>
                    </RiC:leadershipBy>
                   <!-- <xsl:call-template name="outputPositionInRel">
                        <xsl:with-param name="coll" select="$coll"/>
                        <xsl:with-param name="fromAgent" select="$tEntity"/>
                        <xsl:with-param name="tEntity" select="$fromAgent"/>
                        <xsl:with-param name="tName"/>
                    </xsl:call-template>-->


                </xsl:when>
                <xsl:when test="@arcrole = 'isEmployeeOf' or @arcrole = 'isMemberOf'">
                    <RiC:hasAgentMember>
                        <xsl:call-template name="outputObjectPropertyToAnc">

                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                        </xsl:call-template>

                    </RiC:hasAgentMember>



                    <RiC:agentMembershipIn>
                        <xsl:call-template name="outputObjectPropertyToTarg">
                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                            <xsl:with-param name="arcrole" select="@arcrole"></xsl:with-param>
                        </xsl:call-template>
                    </RiC:agentMembershipIn>



                </xsl:when>
                <xsl:when test="@arcrole = 'hasEmployee' or @arcrole = 'hasMember'">
                    <RiC:agentMembershipIn>
                        <xsl:call-template name="outputObjectPropertyToAnc">

                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                        </xsl:call-template>

                    </RiC:agentMembershipIn>



                    <RiC:hasAgentMember>
                        <xsl:call-template name="outputObjectPropertyToTarg">
                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                            <xsl:with-param name="arcrole" select="@arcrole"></xsl:with-param>
                        </xsl:call-template>
                    </RiC:hasAgentMember>



                </xsl:when>
                <xsl:when
                    test="@arcrole = 'isControlledBy' or @arcrole = 'isAssociatedWithForItsControl'">
                    <RiC:authorityOn>
                        <xsl:call-template name="outputObjectPropertyToAnc">

                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                        </xsl:call-template>

                    </RiC:authorityOn>
                    <RiC:authorityHeldBy>
                        <xsl:call-template name="outputObjectPropertyToTarg">
                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                            <xsl:with-param name="arcrole" select="@arcrole"></xsl:with-param>
                        </xsl:call-template>
                    </RiC:authorityHeldBy>



                </xsl:when>
                <xsl:when test="@arcrole = 'controls'">
                    <RiC:authorityHeldBy>
                        <xsl:call-template name="outputObjectPropertyToAnc">
                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                        </xsl:call-template>

                    </RiC:authorityHeldBy>
                    <RiC:authorityOn>
                        <xsl:call-template name="outputObjectPropertyToTarg">
                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                            <xsl:with-param name="arcrole" select="@arcrole"></xsl:with-param>
                        </xsl:call-template>
                    </RiC:authorityOn>



                </xsl:when>
                <xsl:when test="@arcrole = 'hasPart'">
                    <RiC:partOf>


                        <xsl:call-template name="outputObjectPropertyToAnc">

                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                        </xsl:call-template>
                    </RiC:partOf>
                    <RiC:wholeHasPart>
                        <xsl:call-template name="outputObjectPropertyToTarg">
                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                            <xsl:with-param name="arcrole" select="@arcrole"></xsl:with-param>
                        </xsl:call-template>
                    </RiC:wholeHasPart>



                </xsl:when>
                <xsl:when test="@arcrole = 'isPartOf'">
                    <RiC:wholeHasPart>
                        <xsl:call-template name="outputObjectPropertyToAnc">

                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                        </xsl:call-template>
                    </RiC:wholeHasPart>
                    <RiC:partOf>
                        <xsl:call-template name="outputObjectPropertyToTarg">
                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                            <xsl:with-param name="arcrole" select="@arcrole"></xsl:with-param>
                        </xsl:call-template>
                    </RiC:partOf>
                </xsl:when>
                <xsl:when test="@type = 'temporal-earlier'">
                    <RiC:followingThingInTime>
                        <xsl:call-template name="outputObjectPropertyToAnc">

                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                        </xsl:call-template>
                    </RiC:followingThingInTime>
                    <RiC:precedingThingInTime>
                        <xsl:call-template name="outputObjectPropertyToTarg">
                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                            <xsl:with-param name="arcrole" select="@arcrole"></xsl:with-param>
                        </xsl:call-template>
                    </RiC:precedingThingInTime>


                </xsl:when>
                <xsl:when test="@type = 'temporal-later'">
                    <RiC:precedingThingInTime>
                        <xsl:call-template name="outputObjectPropertyToAnc">

                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                        </xsl:call-template>
                    </RiC:precedingThingInTime>
                    <RiC:followingThingInTime>
                        <xsl:call-template name="outputObjectPropertyToTarg">
                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                            <xsl:with-param name="arcrole" select="@arcrole"></xsl:with-param>
                        </xsl:call-template>
                    </RiC:followingThingInTime>


                </xsl:when>
                <xsl:when test="@type = 'hierarchical-child' and (not(@arcrole) or @arcrole='')">
                    <RiC:hasHierarchicalParent>
                        <xsl:call-template name="outputObjectPropertyToAnc">
                            
                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                        </xsl:call-template>
                    </RiC:hasHierarchicalParent>
                    <RiC:hasHierarchicalChild>
                        <xsl:call-template name="outputObjectPropertyToTarg">
                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                            <xsl:with-param name="arcrole" select="@arcrole"></xsl:with-param>
                        </xsl:call-template>
                    </RiC:hasHierarchicalChild>
                    
                    
                </xsl:when>
                <xsl:when test="@type = 'hierarchical-parent' and (not(@arcrole) or @arcrole='')">
                    <RiC:hasHierarchicalChild>
                        <xsl:call-template name="outputObjectPropertyToAnc">
                            
                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                        </xsl:call-template>
                    </RiC:hasHierarchicalChild>
                    <RiC:hasHierarchicalParent>
                        <xsl:call-template name="outputObjectPropertyToTarg">
                            <xsl:with-param name="coll" select="$coll"/>
                            <xsl:with-param name="fromAgent" select="$fromAgent"/>
                            <xsl:with-param name="ancAgentType" select="$ancAgentType"/>
                            <xsl:with-param name="tEntity" select="$tEntity"/>
                            <xsl:with-param name="arcrole" select="@arcrole"></xsl:with-param>
                        </xsl:call-template>
                    </RiC:hasHierarchicalParent>
                    
                    
                </xsl:when>
            </xsl:choose>


        </rdf:Description>
      <!--  <xsl:if
            test="not($collection-EAC-AN/eac:eac-cpf[eac:control/eac:recordId = $tEntity]) and starts-with($tEntity, 'FRAN_NP')">


            <rdf:Description>
                <xsl:attribute name="rdf:nodeID">

                    <xsl:value-of
                        select="concat('FRAN_bnode_gtypeRel_', substring-after($tEntity, 'FRAN_NP_'), '_', generate-id())"
                    />
                </xsl:attribute>
                <rdf:type rdf:resource="http://www.ica.org/standards/RiC/ontology#Agent"/>
                <rdfs:label xml:lang="fr">
                    <xsl:value-of select="piaaf:targetName"/>
                </rdfs:label>
                <RiC:mainSubjectOf>

                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of
                            select="concat('https://www.siv.archives-nationales.culture.gouv.fr/siv/NP/', $tEntity)"
                        />
                    </xsl:attribute>
                </RiC:mainSubjectOf>
            </rdf:Description>
        </xsl:if>-->
       <!-- <xsl:if
            test="not($collection-EAC-SIAF/eac:eac-cpf[eac:control/eac:recordId = $tEntity]) and starts-with($tEntity, 'FR784')">
            <rdf:Description>
                <xsl:attribute name="rdf:nodeID">

                    <xsl:value-of select="concat('FRSIAF_bnode_gtypeRel_', $tEntity, '_', generate-id())"/>
                </xsl:attribute>
                <rdf:type rdf:resource="http://www.ica.org/standards/RiC/ontology#GroupType"/>
                <rdfs:label xml:lang="fr">
                    <xsl:value-of select="piaaf:targetName"/>
                </rdfs:label>
                <RiC:mainSubjectOf>

                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of
                            select="concat('https://aaf.ica-atom.org/actor/browse?subquery=', $tEntity)"
                        />
                    </xsl:attribute>
                </RiC:mainSubjectOf>
            </rdf:Description>
        </xsl:if>-->
        <!--<xsl:if
            test="not($collection-EAC-SIAF/eac:eac-cpf[eac:control/eac:recordId = $tEntity]) and starts-with($tEntity, 'FRAD')">
            <rdf:Description>
                <xsl:attribute name="rdf:nodeID">
                    
                    <xsl:value-of select="concat('FRSIAF_bnode_gtypeRel_', $tEntity, '_', generate-id())"/>
                </xsl:attribute>
                <rdf:type rdf:resource="http://www.ica.org/standards/RiC/ontology#Agent"/>
                <rdfs:label xml:lang="fr">
                    <xsl:value-of select="piaaf:targetName"/>
                </rdfs:label>
               
            </rdf:Description>
        </xsl:if>-->
        
      <!--  <!-\-<xsl:if
            test="not($collection-EAC-BnF/eac:eac-cpf[eac:control/eac:recordId = $tEntity]) and starts-with($tEntity, 'FRBNF')">
            <rdf:Description>
                <xsl:attribute name="rdf:nodeID">

                    <xsl:value-of
                        select="concat('FRBNF_bnode_gtypeRel_', substring-after($tEntity, 'DGEARC_'), '_', generate-id())"
                    />
                </xsl:attribute>
                <rdf:type rdf:resource="http://www.ica.org/standards/RiC/ontology#Agent"/>
                <rdfs:label xml:lang="fr">
                    <xsl:value-of select="piaaf:targetName"/>
                </rdfs:label>
                <!-\\- <RiC:mainSubjectOf>
                
                <xsl:attribute name="rdf:resource">
                    <xsl:value-of select="concat('https://aaf.ica-atom.org/actor/browse?subquery=', $tEntity)"/>
                </xsl:attribute>
            </RiC:mainSubjectOf>-\\->
            </rdf:Description>-\->
        </xsl:if>-->

    </xsl:template>
    <xsl:template name="outputObjectPropertyToAnc">
        <xsl:param name="tEntity"/>
        <xsl:param name="fromAgent"/>
        <xsl:param name="coll"/>
        <xsl:param name="ancAgentType"/>
        <xsl:attribute name="rdf:resource">
            <xsl:text>http://piaaf.demo.logilab.fr/resource/FRSIAF_group-type_</xsl:text>
           <!-- <xsl:value-of select="$coll"/>
            <xsl:text>_</xsl:text>-->
           
            <xsl:value-of
                select="
                if ($coll = 'FRSIAF')
                then
                ($fromAgent)
                else
                (
                if ($coll = 'FRAN')
                then
                (substring-after($fromAgent, 'FRAN_NP_'))
                else
                (
                if ($coll = 'FRBNF')
                then
                (substring-after($fromAgent, 'DGEARC_'))
                else
                ()
                )
                )
                
                "
            />
        </xsl:attribute>
    </xsl:template>
    <xsl:template name="outputObjectPropertyToTarg">
        
        <xsl:param name="ancAgentType"/>
        <xsl:param name="coll"/>
        <xsl:param name="fromAgent"/>
        <xsl:param name="tEntity"/>
        <xsl:param name="arcrole"/>
        <xsl:if test="normalize-space($tEntity) != ''">
            
            <xsl:choose>
                
                <xsl:when
                    test="$collection-EAC-AN/eac:eac-cpf[eac:control/eac:recordId = $tEntity] | $collection-EAC-SIAF/eac:eac-cpf[eac:control/eac:recordId = $tEntity] | $collection-EAC-BnF/eac:eac-cpf[eac:control/eac:recordId = $tEntity]">
                    <xsl:variable name="targetEntityType"
                        select="
                        if ($collection-EAC-BnF/eac:eac-cpf[eac:control/eac:recordId = $tEntity])
                        then
                        ($collection-EAC-BnF/eac:eac-cpf[eac:control/eac:recordId = $tEntity]/eac:cpfDescription/eac:identity/eac:entityType)
                        else
                        (
                        if ($collection-EAC-AN/eac:eac-cpf[eac:control/eac:recordId = $tEntity])
                        then
                        ($collection-EAC-AN/eac:eac-cpf[eac:control/eac:recordId = $tEntity]/eac:cpfDescription/eac:identity/eac:entityType)
                        else
                        (
                        if ($collection-EAC-SIAF/eac:eac-cpf[eac:control/eac:recordId = $tEntity and starts-with($tEntity, 'FRAD')])
                        then
                        ($collection-EAC-SIAF/eac:eac-cpf[eac:control/eac:recordId = $tEntity]/eac:cpfDescription/eac:identity/eac:entityType)
                        else
                        (
                        if ($collection-EAC-SIAF/eac:eac-cpf[eac:control/eac:recordId = $tEntity and starts-with($tEntity, 'FR784')])
                        then (
                        'group-type'
                        )
                        else()
                        ))
                        )
                        
                        
                        
                        "/>
                    <xsl:variable name="tEntityType">
                        <xsl:choose>
                            <xsl:when test="$targetEntityType = 'person'">person</xsl:when>
                            <xsl:when test="$targetEntityType = 'corporateBody'"
                                >corporate-body</xsl:when>
                            <xsl:when test="$targetEntityType= 'group-type'">group-type</xsl:when>
                            
                        </xsl:choose>
                    </xsl:variable>
                    <xsl:attribute name="rdf:resource">
                        <xsl:text>http://piaaf.demo.logilab.fr/resource/</xsl:text>
                        <!--<xsl:choose>
                            <xsl:when test="$tEntityType='group-type'">group-types/</xsl:when>
                            <xsl:otherwise>agents/</xsl:otherwise>
                        </xsl:choose>-->
                        <xsl:choose>
                            <xsl:when test="starts-with($tEntity, 'FRAN_NP')">
                                <!-- <xsl:text>agents/FRAN_corporate-body_</xsl:text>-->
                                <xsl:value-of
                                    select="concat('FRAN_', $tEntityType, '_', substring-after($tEntity, 'FRAN_NP_'))"/>
                                
                            </xsl:when>
                            <xsl:when test="starts-with($tEntity, 'FRBNF')">
                                <!--  <xsl:text>agents/BnF_corporate-body_</xsl:text>-->
                                <xsl:value-of
                                    select="concat('FRBNF_', $tEntityType, '_', substring-after($tEntity, 'DGEARC_'))"/>
                                
                            </xsl:when>
                            
                            <xsl:otherwise>
                                <xsl:value-of
                                    select="concat('FRSIAF_', $tEntityType, '_', $tEntity)"/>
                                
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:attribute>
                    
                    
                    
                </xsl:when>
                
                <xsl:when test="starts-with($tEntity, 'FRAN_NP')">
                    
                    
                    <xsl:variable name="targEntityType">
                        <xsl:choose>
                            <!-- relation hiérarchique : entre 2 cbodies -->
                            <!--  <xsl:when test="starts-with($cpfRel, 'hierarchical')">
                                <xsl:text>corporate-body</xsl:text>
                            </xsl:when>-->
                            <xsl:when test="contains($arcrole, 'isDirectedBy')">
                                <xsl:text>person</xsl:text>
                            </xsl:when>
                            <xsl:when test="contains($arcrole, 'isDirectorOf')">
                                <xsl:text>corporate-body</xsl:text>
                            </xsl:when>
                            <xsl:when test="contains($arcrole, 'isAssociatedWithForItsControl')">
                                <xsl:text>corporate-body</xsl:text>
                            </xsl:when>
                            <xsl:when test="contains($arcrole, 'controls') or contains($arcrole, 'isControlledBy') or contains($arcrole, 'isPartOf') or contains($arcrole, 'hasPart')">
                                <xsl:text>corporate-body</xsl:text>
                            </xsl:when>
                            <xsl:when
                                test="contains($arcrole, 'hasMember') or contains($arcrole, 'hasEmployee')">
                                <xsl:text>person</xsl:text>
                            </xsl:when>
                            <xsl:when
                                test="contains($arcrole, 'isMemberOf') or contains($arcrole, 'isEmployeeOf')">
                                <xsl:text>corporate-body</xsl:text>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:text>agent</xsl:text>
                            </xsl:otherwise>
                            
                        </xsl:choose>
                    </xsl:variable>
                    
                    
                    
                    
                    
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="concat('http://piaaf.demo.logilab.fr/resource/FRAN_', $targEntityType, '_', substring-after($tEntity, 'FRAN_NP_'))"/>
                    </xsl:attribute>
                    
                    
                    
                    
                    
                    
                  <!--  <xsl:attribute name="rdf:nodeID">
                        <xsl:value-of
                            select="concat('FRAN_bnode_gtypeRel_', substring-after($tEntity, 'FRAN_NP_'), '_', generate-id())"
                        />
                    </xsl:attribute>-->
                   <!-- <xsl:attribute name="rdf:resource">
                    <xsl:value-of select="concat('http://www.piaaf.net/agents/FRAN_agent_', substring-after($tEntity, 'FRAN_NP_'))"/>
                    </xsl:attribute>-->
                    
                </xsl:when>
                <xsl:when test="starts-with($tEntity, 'FR784')">
                   <!-- <xsl:attribute name="rdf:nodeID">
                        <xsl:value-of select="concat('FRSIAF_bnode_gtypeRel_', $tEntity, '_', generate-id())"
                        />
                    </xsl:attribute>-->
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="concat('http://piaaf.demo.logilab.fr/resource/FRSIAF_group-type_', $tEntity)"/>
                    </xsl:attribute>
                </xsl:when>
                <xsl:when test="starts-with($tEntity, 'FRAD')">
                    
                 <!--   <xsl:attribute name="rdf:nodeID">
                        <xsl:value-of select="concat('FRSIAF_bnode_gtypeRel_', $tEntity, '_', generate-id())"
                        />
                    </xsl:attribute>-->
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="concat('http://piaaf.demo.logilab.fr/resource/FRSIAF_corporate-body_', $tEntity)"/>
                    </xsl:attribute>
                </xsl:when>
                <xsl:when test="starts-with($tEntity, 'FRBNF')">
                   <!-- <xsl:attribute name="rdf:nodeID">
                        <xsl:value-of
                            select="concat('FRBNF_bnode_gtypeRel_', substring-after($tEntity, 'DGEARC_'), '_', generate-id())"
                        />
                    </xsl:attribute>-->
                    <xsl:variable name="targEntityType">
                        <xsl:choose>
                            <!-- relation hiérarchique : entre 2 cbodies -->
                            <!--  <xsl:when test="starts-with($cpfRel, 'hierarchical')">
                                <xsl:text>corporate-body</xsl:text>
                            </xsl:when>-->
                            <xsl:when test="contains($arcrole, 'isDirectedBy')">
                                <xsl:text>person</xsl:text>
                            </xsl:when>
                            <xsl:when test="contains($arcrole, 'isDirectorOf')">
                                <xsl:text>corporate-body</xsl:text>
                            </xsl:when>
                            <xsl:when test="contains($arcrole, 'isAssociatedWithForItsControl')">
                                <xsl:text>corporate-body</xsl:text>
                            </xsl:when>
                            <xsl:when test="contains($arcrole, 'controls') or contains($arcrole, 'isControlledBy') or contains($arcrole, 'isPartOf') or contains($arcrole, 'hasPart')">
                                <xsl:text>corporate-body</xsl:text>
                            </xsl:when>
                            <xsl:when
                                test="contains($arcrole, 'hasMember') or contains($arcrole, 'hasEmployee')">
                                <xsl:text>person</xsl:text>
                            </xsl:when>
                            <xsl:when
                                test="contains($arcrole, 'isMemberOf') or contains($arcrole, 'isEmployeeOf')">
                                <xsl:text>corporate-body</xsl:text>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:text>agent</xsl:text>
                            </xsl:otherwise>
                            
                        </xsl:choose>
                    </xsl:variable>
                    
                    
                    
                    
                    
                    <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="concat('http://piaaf.demo.logilab.fr/resource/', $targEntityType, '_', substring-after($tEntity, 'DGEARC_'))"/>
                    </xsl:attribute>
                    
                 <!--   <xsl:attribute name="rdf:resource">
                        <xsl:value-of select="concat('http://www.piaaf.net/agents/FRBNF_agent_', substring-after($tEntity, 'DGEARC_'))"/>
                    </xsl:attribute>-->
                    
                </xsl:when>
                
            </xsl:choose>
            
        </xsl:if>
        <xsl:if test="normalize-space($tEntity) = ''">
            <xsl:attribute name="xml:lang">fr</xsl:attribute>
            <xsl:value-of select="piaaf:targetName"/>
        </xsl:if>
    </xsl:template>

</xsl:stylesheet>
